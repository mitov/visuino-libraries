////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>
#include <SPI.h>

#include "Mitov_BuildChecks_Begin.h"
#include "Mitov_LoRa_RFM9X.h"

//#define ___MITOV_LORA_SX126X_DEBUG__

namespace Mitov
{
//---------------------------------------------------------------------------
namespace SX126XConst
{
// SX126X physical layer properties
	static constexpr double XTAL_FREQ                       = 32000000;
	static constexpr double FREQ_DIV                        = pow( 2.0, 25.0 );
	static constexpr double FREQ_STEP                       = ( XTAL_FREQ / FREQ_DIV );

// SX126X physical layer properties
//	static constexpr double SX126X_FREQUENCY_STEP_SIZE                     0.9536743164
//	static const uint8_t SX126X_MAX_PACKET_LENGTH                       255
	static constexpr double SX126X_CRYSTAL_FREQ                            = 32.0;
	static constexpr uint8_t SX126X_DIV_EXPONENT                           = 25;

// SX126X SPI commands
// operational modes commands
	static const uint8_t SX126X_CMD_NOP                                = 0x00;
	static const uint8_t SX126X_CMD_SET_SLEEP                          = 0x84;
	static const uint8_t SX126X_CMD_SET_STANDBY                        = 0x80;
	static const uint8_t SX126X_CMD_SET_FS                             = 0xC1;
	static const uint8_t SX126X_CMD_SET_TX                             = 0x83;
	static const uint8_t SX126X_CMD_SET_RX                             = 0x82;
	static const uint8_t SX126X_CMD_STOP_TIMER_ON_PREAMBLE             = 0x9F;
	static const uint8_t SX126X_CMD_SET_RX_DUTY_CYCLE                  = 0x94;
	static const uint8_t SX126X_CMD_SET_CAD                            = 0xC5;
	static const uint8_t SX126X_CMD_SET_TX_CONTINUOUS_WAVE             = 0xD1;
	static const uint8_t SX126X_CMD_SET_TX_INFINITE_PREAMBLE           = 0xD2;
	static const uint8_t SX126X_CMD_SET_REGULATOR_MODE                 = 0x96;
	static const uint8_t SX126X_CMD_CALIBRATE                          = 0x89;
	static const uint8_t SX126X_CMD_CALIBRATE_IMAGE                    = 0x98;
	static const uint8_t SX126X_CMD_SET_PA_CONFIG                      = 0x95;
	static const uint8_t SX126X_CMD_SET_RX_TX_FALLBACK_MODE            = 0x93;

// register and buffer access commands
	static const uint8_t SX126X_CMD_WRITE_REGISTER                     = 0x0D;
	static const uint8_t SX126X_CMD_READ_REGISTER                      = 0x1D;
	static const uint8_t SX126X_CMD_WRITE_BUFFER                       = 0x0E;
	static const uint8_t SX126X_CMD_READ_BUFFER                        = 0x1E;

// DIO and IRQ control
	static const uint8_t SX126X_CMD_SET_DIO_IRQ_PARAMS                 = 0x08;
	static const uint8_t SX126X_CMD_GET_IRQ_STATUS                     = 0x12;
	static const uint8_t SX126X_CMD_CLEAR_IRQ_STATUS                   = 0x02;
	static const uint8_t SX126X_CMD_SET_DIO2_AS_RF_SWITCH_CTRL         = 0x9D;
	static const uint8_t SX126X_CMD_SET_DIO3_AS_TCXO_CTRL              = 0x97;

// RF, modulation and packet commands
	static const uint8_t SX126X_CMD_SET_RF_FREQUENCY                   = 0x86;
	static const uint8_t SX126X_CMD_SET_PACKET_TYPE                    = 0x8A;
	static const uint8_t SX126X_CMD_GET_PACKET_TYPE                    = 0x11;
	static const uint8_t SX126X_CMD_SET_TX_PARAMS                      = 0x8E;
	static const uint8_t SX126X_CMD_SET_MODULATION_PARAMS              = 0x8B;
	static const uint8_t SX126X_CMD_SET_PACKET_PARAMS                  = 0x8C;
	static const uint8_t SX126X_CMD_SET_CAD_PARAMS                     = 0x88;
	static const uint8_t SX126X_CMD_SET_BUFFER_BASE_ADDRESS            = 0x8F;
	static const uint8_t SX126X_CMD_SET_LORA_SYMB_NUM_TIMEOUT          = 0xA0;

	static const uint8_t SX126X_PA_CONFIG_SX1261                       = 0x01;
	static const uint8_t SX126X_PA_CONFIG_SX1262                       = 0x00;

// status commands
	static const uint8_t SX126X_CMD_GET_STATUS                         = 0xC0;
	static const uint8_t SX126X_CMD_GET_RSSI_INST                      = 0x15;
	static const uint8_t SX126X_CMD_GET_RX_BUFFER_STATUS               = 0x13;
	static const uint8_t SX126X_CMD_GET_PACKET_STATUS                  = 0x14;
	static const uint8_t SX126X_CMD_GET_DEVICE_ERRORS                  = 0x17;
	static const uint8_t SX126X_CMD_CLEAR_DEVICE_ERRORS                = 0x07;
	static const uint8_t SX126X_CMD_GET_STATS                          = 0x10;
	static const uint8_t SX126X_CMD_RESET_STATS                        = 0x00;

//SX126X_CMD_SET_STANDBY
	static const uint8_t SX126X_STANDBY_RC                             = 0x00;        //  7     0     standby mode: 13 MHz RC oscillator
	static const uint8_t SX126X_STANDBY_XOSC                           = 0x01;        //  7     0                   32 MHz crystal oscillator

//SX126X_CMD_SET_RX_TX_FALLBACK_MODE
	static const uint8_t SX126X_RX_TX_FALLBACK_MODE_FS                 = 0x40;        //  7     0     after Rx/Tx go to: FS mode
	static const uint8_t SX126X_RX_TX_FALLBACK_MODE_STDBY_XOSC         = 0x30;        //  7     0                        standby with crystal oscillator
	static const uint8_t SX126X_RX_TX_FALLBACK_MODE_STDBY_RC           = 0x20;        //  7     0                        standby with RC oscillator (default)

//SX126X_CMD_SET_CAD_PARAMS
	static const uint8_t SX126X_CAD_ON_1_SYMB                          = 0x00;        //  7     0     number of symbols used for CAD: 1
	static const uint8_t SX126X_CAD_ON_2_SYMB                          = 0x01;        //  7     0                                     2
	static const uint8_t SX126X_CAD_ON_4_SYMB                          = 0x02;        //  7     0                                     4
	static const uint8_t SX126X_CAD_ON_8_SYMB                          = 0x03;        //  7     0                                     8
	static const uint8_t SX126X_CAD_ON_16_SYMB                         = 0x04;        //  7     0                                     16
	static const uint8_t SX126X_CAD_GOTO_STDBY                         = 0x00;        //  7     0     after CAD is done, always go to STDBY_RC mode
	static const uint8_t SX126X_CAD_GOTO_RX                            = 0x01;        //  7     0     after CAD is done, go to Rx mode if activity is detected

	static const uint8_t SX126X_CAD_PARAM_DEFAULT                      = 0xFF;        //  7     0   used by the CAD methods to specify default parameter value
	static const uint8_t SX126X_CAD_PARAM_DET_MIN                      = 10;          //  7     0   default detMin CAD parameter

//SX126X_CMD_SET_DIO_IRQ_PARAMS
	static const uint16_t SX126X_IRQ_TIMEOUT                          = 0b1000000000;  //  9     9     Rx or Tx timeout
	static const uint16_t SX126X_IRQ_CAD_DETECTED                     = 0b0100000000;  //  8     8     channel activity detected
	static const uint16_t SX126X_IRQ_CAD_DONE                         = 0b0010000000;  //  7     7     channel activity detection finished
	static const uint16_t SX126X_IRQ_CRC_ERR                          = 0b0001000000;  //  6     6     wrong CRC received
	static const uint16_t SX126X_IRQ_HEADER_ERR                       = 0b0000100000;  //  5     5     LoRa header CRC error
	static const uint16_t SX126X_IRQ_HEADER_VALID                     = 0b0000010000;  //  4     4     valid LoRa header received
	static const uint16_t SX126X_IRQ_SYNC_WORD_VALID                  = 0b0000001000;  //  3     3     valid sync word detected
	static const uint16_t SX126X_IRQ_PREAMBLE_DETECTED                = 0b0000000100;  //  2     2     preamble detected
	static const uint16_t SX126X_IRQ_RX_DONE                          = 0b0000000010;  //  1     1     packet received
	static const uint16_t SX126X_IRQ_TX_DONE                          = 0b0000000001;  //  0     0     packet transmission completed
	static const uint16_t SX126X_IRQ_ALL                              = 0b1111111111;  //  9     0     all interrupts
	static const uint16_t SX126X_IRQ_NONE                             = 0b0000000000;  //  9     0     no interrupts

//SX126X_CMD_CALIBRATE
	static const uint8_t SX126X_CALIBRATE_IMAGE_OFF                    = 0b00000000;  //  6     6     image calibration: disabled
	static const uint8_t SX126X_CALIBRATE_IMAGE_ON                     = 0b01000000;  //  6     6                        enabled
	static const uint8_t SX126X_CALIBRATE_ADC_BULK_P_OFF               = 0b00000000;  //  5     5     ADC bulk P calibration: disabled
	static const uint8_t SX126X_CALIBRATE_ADC_BULK_P_ON                = 0b00100000;  //  5     5                             enabled
	static const uint8_t SX126X_CALIBRATE_ADC_BULK_N_OFF               = 0b00000000;  //  4     4     ADC bulk N calibration: disabled
	static const uint8_t SX126X_CALIBRATE_ADC_BULK_N_ON                = 0b00010000;  //  4     4                             enabled
	static const uint8_t SX126X_CALIBRATE_ADC_PULSE_OFF                = 0b00000000;  //  3     3     ADC pulse calibration: disabled
	static const uint8_t SX126X_CALIBRATE_ADC_PULSE_ON                 = 0b00001000;  //  3     3                            enabled
	static const uint8_t SX126X_CALIBRATE_PLL_OFF                      = 0b00000000;  //  2     2     PLL calibration: disabled
	static const uint8_t SX126X_CALIBRATE_PLL_ON                       = 0b00000100;  //  2     2                      enabled
	static const uint8_t SX126X_CALIBRATE_RC13M_OFF                    = 0b00000000;  //  1     1     13 MHz RC osc. calibration: disabled
	static const uint8_t SX126X_CALIBRATE_RC13M_ON                     = 0b00000010;  //  1     1                                 enabled
	static const uint8_t SX126X_CALIBRATE_RC64K_OFF                    = 0b00000000;  //  0     0     64 kHz RC osc. calibration: disabled
	static const uint8_t SX126X_CALIBRATE_RC64K_ON                     = 0b00000001;  //  0     0                                 enabled

//SX126X_CMD_GET_STATUS
	static const uint8_t SX126X_STATUS_MODE_STDBY_RC                   = 0b00000010;  //  6     4   current chip mode: STDBY_RC
	static const uint8_t SX126X_STATUS_MODE_STDBY_XOSC                 = 0b00000011;  //  6     4                      STDBY_XOSC
	static const uint8_t SX126X_STATUS_MODE_FS                         = 0b00000100;  //  6     4                      FS
	static const uint8_t SX126X_STATUS_MODE_RX                         = 0b00000101;  //  6     4                      RX
	static const uint8_t SX126X_STATUS_MODE_TX                         = 0b00000110;  //  6     4                      TX
	static const uint8_t SX126X_STATUS_DATA_AVAILABLE                  = 0b01000000;  //  3     1   command status: packet received and data can be retrieved
	static const uint8_t SX126X_STATUS_CMD_TIMEOUT                     = 0b01100000;  //  3     1                   SPI command timed out
	static const uint8_t SX126X_STATUS_CMD_INVALID                     = 0b10000000;  //  3     1                   invalid SPI command
	static const uint8_t SX126X_STATUS_CMD_FAILED                      = 0b10100000;  //  3     1                   SPI command failed to execute
	static const uint8_t SX126X_STATUS_TX_DONE                         = 0b11000000;  //  3     1                   packet transmission done
	static const uint8_t SX126X_STATUS_SPI_FAILED                      = 0b11111111;  //  7     0   SPI transaction failed

//SX126X_CMD_SET_TX_PARAMS
	static const uint8_t SX126X_PA_RAMP_10U                            = 0x00;        //  7     0     ramp time: 10 us
	static const uint8_t SX126X_PA_RAMP_20U                            = 0x01;        //  7     0                20 us
	static const uint8_t SX126X_PA_RAMP_40U                            = 0x02;        //  7     0                40 us
	static const uint8_t SX126X_PA_RAMP_80U                            = 0x03;        //  7     0                80 us
	static const uint8_t SX126X_PA_RAMP_200U                           = 0x04;        //  7     0                200 us
	static const uint8_t SX126X_PA_RAMP_800U                           = 0x05;        //  7     0                800 us
	static const uint8_t SX126X_PA_RAMP_1700U                          = 0x06;        //  7     0                1700 us
	static const uint8_t SX126X_PA_RAMP_3400U                          = 0x07;        //  7     0                3400 us

// SX126X register map
	static const uint16_t SX126X_REG_WHITENING_INITIAL_MSB              = 0x06B8;
	static const uint16_t SX126X_REG_WHITENING_INITIAL_LSB              = 0x06B9;
	static const uint16_t SX126X_REG_CRC_INITIAL_MSB                    = 0x06BC;
	static const uint16_t SX126X_REG_CRC_INITIAL_LSB                    = 0x06BD;
	static const uint16_t SX126X_REG_CRC_POLYNOMIAL_MSB                 = 0x06BE;
	static const uint16_t SX126X_REG_CRC_POLYNOMIAL_LSB                 = 0x06BF;
	static const uint16_t SX126X_REG_SYNC_WORD_0                        = 0x06C0;
	static const uint16_t SX126X_REG_SYNC_WORD_1                        = 0x06C1;
	static const uint16_t SX126X_REG_SYNC_WORD_2                        = 0x06C2;
	static const uint16_t SX126X_REG_SYNC_WORD_3                        = 0x06C3;
	static const uint16_t SX126X_REG_SYNC_WORD_4                        = 0x06C4;
	static const uint16_t SX126X_REG_SYNC_WORD_5                        = 0x06C5;
	static const uint16_t SX126X_REG_SYNC_WORD_6                        = 0x06C6;
	static const uint16_t SX126X_REG_SYNC_WORD_7                        = 0x06C7;
	static const uint16_t SX126X_REG_NODE_ADDRESS                       = 0x06CD;
	static const uint16_t SX126X_REG_BROADCAST_ADDRESS                  = 0x06CE;
	static const uint16_t SX126X_REG_LORA_SYNC_WORD_MSB                 = 0x0740;
	static const uint16_t SX126X_REG_LORA_SYNC_WORD_LSB                 = 0x0741;
	static const uint16_t SX126X_REG_RANDOM_NUMBER_0                    = 0x0819;
	static const uint16_t SX126X_REG_RANDOM_NUMBER_1                    = 0x081A;
	static const uint16_t SX126X_REG_RANDOM_NUMBER_2                    = 0x081B;
	static const uint16_t SX126X_REG_RANDOM_NUMBER_3                    = 0x081C;
	static const uint16_t SX126X_REG_RX_GAIN                            = 0x08AC;
	static const uint16_t SX126X_REG_OCP_CONFIGURATION                  = 0x08E7;
	static const uint16_t SX126X_REG_XTA_TRIM                           = 0x0911;
	static const uint16_t SX126X_REG_XTB_TRIM                           = 0x0912;

	//SX126X_CMD_SET_REGULATOR_MODE
	static const uint8_t SX126X_REGULATOR_LDO                           = 0x00;        //  7     0     set regulator mode: LDO (default)
	static const uint8_t SX126X_REGULATOR_DC_DC                         = 0x01;        //  7     0                         DC-DC

	//RADIOLIB_SX126X_CMD_SET_PACKET_PARAMS
	static const uint8_t SX126X_GFSK_PREAMBLE_DETECT_OFF                = 0x00;        //  7     0   GFSK minimum preamble length before reception starts: detector disabled
	static const uint8_t SX126X_GFSK_PREAMBLE_DETECT_8                  = 0x04;        //  7     0                                                         8 bits
	static const uint8_t SX126X_GFSK_PREAMBLE_DETECT_16                 = 0x05;        //  7     0                                                         16 bits
	static const uint8_t SX126X_GFSK_PREAMBLE_DETECT_24                 = 0x06;        //  7     0                                                         24 bits
	static const uint8_t SX126X_GFSK_PREAMBLE_DETECT_32                 = 0x07;        //  7     0                                                         32 bits
}
//---------------------------------------------------------------------------
	template <
		typename T_ResetOutputPin
	> class LoRaSX1262_PinReset :
		public T_ResetOutputPin
	{
	public:
		_V_PIN_( ResetOutputPin )

	protected:
		void Reset()
		{
			if( T_ResetOutputPin::GetPinIsConnected() )
			{
				delay(10);
				T_ResetOutputPin::SetPinValueLow();
				delay(20);
				T_ResetOutputPin::SetPinValueHigh();
				delay(10);
			}
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_BulkN,
		typename T_BulkP,
		typename T_Pulse
	> class TArduinoLoRaSX126X_ADCCalibrate :
		public T_BulkN,
		public T_BulkP,
		public T_Pulse
	{
	public:
		_V_PROP_( BulkN )
		_V_PROP_( BulkP )
		_V_PROP_( Pulse )

	};
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
		typename T_ADCCalibrate,
		typename T_Enabled,
		typename T_Image,
		typename T_Oscillator13MHz,
		typename T_Oscillator64kHz,
		typename T_PhaseLockedLoop
	> class TArduinoLoRaSX126X_ElementCalibrate :
		public T_ADCCalibrate,
		public T_Enabled,
		public T_Image,
		public T_Oscillator13MHz,
		public T_Oscillator64kHz,
		public T_PhaseLockedLoop
	{
	public:
		_V_PROP_( ADCCalibrate )
		_V_PROP_( Enabled )
		_V_PROP_( Image )
		_V_PROP_( Oscillator13MHz )
		_V_PROP_( Oscillator64kHz )
		_V_PROP_( PhaseLockedLoop )

	public:
		inline void ClockInputPin_o_Receive( void *_Data )
		{
			if( ! Enabled().GetValue() )
				return;

			uint8_t AValue = Image() ? SX126XConst::SX126X_CALIBRATE_IMAGE_ON : 0;

			if( Oscillator13MHz() )
				AValue |= SX126XConst::SX126X_CALIBRATE_RC13M_ON;

			if( Oscillator64kHz() )
				AValue |= SX126XConst::SX126X_CALIBRATE_RC64K_ON;

			if( PhaseLockedLoop() )
				AValue |= SX126XConst::SX126X_CALIBRATE_PLL_ON;

			if( ADCCalibrate().Pulse() )
				AValue |= SX126XConst::SX126X_CALIBRATE_ADC_PULSE_ON;

			if( ADCCalibrate().BulkP() )
				AValue |= SX126XConst::SX126X_CALIBRATE_ADC_BULK_P_ON;

			if( ADCCalibrate().BulkN() )
				AValue |= SX126XConst::SX126X_CALIBRATE_ADC_BULK_N_ON;

			C_OWNER.Calibrate( AValue );
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_Enabled,
		typename T_Value
	> class TArduinoLoRaSX126X_ElementCalibrateFrequency :
		public T_Enabled,
		public T_Value
	{
	public:
		_V_PROP_( Enabled )
		_V_PROP_( Value )

	};
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
		typename T_Enabled,
		typename T_Frequency
	> class TArduinoLoRaSX126X_ElementCalibrateImage :
		public T_Enabled,
		public T_Frequency
	{
	public:
		_V_PROP_( Enabled )
		_V_PROP_( Frequency )

	public:
		inline void ClockInputPin_o_Receive( void *_Data )
		{
			if( ! Enabled().GetValue() )
				return;

			C_OWNER.CalibrateImage( (( Frequency().Enabled() ) ? Frequency().Value().GetValue() : C_OWNER.Frequency().GetValue() ) * 1000000 + 0.5 );
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
		typename T_Enabled,
		typename T_WarmStart
	> class TArduinoLoRaSX126X_ElementSleep :
		public T_Enabled,
		public T_WarmStart
	{
	public:
		_V_PROP_( Enabled )
		_V_PROP_( WarmStart )

	public:
		inline void ClockInputPin_o_Receive( void *_Data )
		{
			if( ! Enabled().GetValue() )
				return;

			uint8_t data[] = { SX126XConst::SX126X_CMD_SET_SLEEP, ( WarmStart() ) ? 1 : 0 };
			C_OWNER.SPIWrite( data, 2 );			
		}

	};
//---------------------------------------------------------------------------
	template <
//		typename T_ChannelActivityOutputPin,
		typename T_PreambleOutputPin,
		typename T_ReceivedHeaderOutputPin
	> class TArduinoLoRaSX126X_DetectedLoRa :
//		public T_ChannelActivityOutputPin,
		public T_PreambleOutputPin,
		public T_ReceivedHeaderOutputPin
	{
	public:
//		_V_PIN_( ChannelActivityOutputPin )
		_V_PIN_( PreambleOutputPin )
		_V_PIN_( ReceivedHeaderOutputPin )

	};
//---------------------------------------------------------------------------
	template <
		typename T_PreambleOutputPin,
		typename T_SyncWordOutputPin
	> class TArduinoLoRaSX126X_DetectedFSK :
		public T_PreambleOutputPin,
		public T_SyncWordOutputPin
	{
	public:
		_V_PIN_( PreambleOutputPin )
		_V_PIN_( SyncWordOutputPin )

	};
//---------------------------------------------------------------------------
/*
template <
		typename T_DataCRCOutputPin,
		typename T_HeaderCRCOutputPin
	> class TArduinoLoRaSX126X_Errors :
		public T_DataCRCOutputPin,
		public T_HeaderCRCOutputPin
	{
	public:
		_V_PIN_( DataCRCOutputPin )
		_V_PIN_( HeaderCRCOutputPin )

	};
*/
//---------------------------------------------------------------------------
	template <
		typename T_Broadcast,
		typename T_Enabled,
		typename T_NodeAddress
	> class TArduinoLoRaSX126X_AddressFiltering :
		public T_Broadcast,
		public T_Enabled,
		public T_NodeAddress
	{
	public:
		_V_PROP_( Broadcast )
		_V_PROP_( Enabled )
		_V_PROP_( NodeAddress )

	};
//---------------------------------------------------------------------------
	template <
		typename T_Bytes,
		typename T_InitialValue,
		typename T_Inverted,
		typename T_Polynomial
	> class TArduinoLoRaSX126X_FSK_CRC :
		public T_Bytes,
		public T_InitialValue,
		public T_Inverted,
		public T_Polynomial
	{
	public:
		_V_PROP_( Bytes )
		_V_PROP_( InitialValue )
		_V_PROP_( Inverted )
		_V_PROP_( Polynomial )

	};
//---------------------------------------------------------------------------
	template <
		typename T_Value,
		typename T_Variable
	> class TArduinoLoRaSX126X_PacketSize :
		public T_Value,
		public T_Variable
	{
	public:
		_V_PROP_( Value )
		_V_PROP_( Variable )

	};
//---------------------------------------------------------------------------
	template <
		typename T_Enabled,
		typename T_InitialValue
	> class TArduinoLoRaSX126X_FSK_Whitening :
		public T_Enabled,
		public T_InitialValue
	{
	public:
		_V_PROP_( Enabled )
		_V_PROP_( InitialValue )

	};
//---------------------------------------------------------------------------
	template <
		typename T_AddressFiltering,
		typename T_Bandwidth,
		typename T_BitRate,
		typename T_CRC,
		typename T_Detected,
		typename T_FrequencyDeviation,
		typename T_GaussianPulseShape,
		typename T_PacketSize,
		typename T_PreambleLength,
		typename T_SyncWord,
		typename T_SyncWord_ApplyValues,
		typename T_SyncWord_GetValue,
		typename T_Whitening
	> class TArduinoLoRaSX126X_Packet_FrequencyShiftKeying :
		public T_AddressFiltering,
		public T_Bandwidth,
		public T_BitRate,
		public T_CRC,
		public T_Detected,
		public T_FrequencyDeviation,
		public T_GaussianPulseShape,
		public T_PacketSize,
		public T_PreambleLength,
		public T_SyncWord,
		public T_SyncWord_ApplyValues,
		public T_SyncWord_GetValue,
		public T_Whitening
	{
	public:
		_V_PROP_( AddressFiltering )
		_V_PROP_( Bandwidth )
        _V_PROP_( BitRate )
		_V_PROP_( CRC )
		_V_PROP_( Detected )
		_V_PROP_( FrequencyDeviation )
		_V_PROP_( GaussianPulseShape )
		_V_PROP_( PacketSize )
		_V_PROP_( PreambleLength )
		_V_PROP_( SyncWord )
		_V_PROP_( Whitening )

	};
//---------------------------------------------------------------------------
	template <
		typename T_CodingRate,
		typename T_Detected,
		typename T_HasCRC,
		typename T_HeaderCRCErrorOutputPin,
		typename T_InvertIQ,
		typename T_LowDataRateOptimize,
		typename T_PreambleLength,
		typename T_SignalBandwidth,
		typename T_SpreadingFactorOrder
	> class TArduinoLoRaSX126X_Packet_LoRa :
		public T_CodingRate,
		public T_Detected,
		public T_HasCRC,
		public T_HeaderCRCErrorOutputPin,
		public T_InvertIQ,
		public T_LowDataRateOptimize,
		public T_PreambleLength,
		public T_SignalBandwidth,
		public T_SpreadingFactorOrder
	{
	public:
		_V_PIN_( HeaderCRCErrorOutputPin )

	public:
		_V_PROP_( CodingRate )
		_V_PROP_( Detected )
		_V_PROP_( HasCRC )
		_V_PROP_( InvertIQ )
		_V_PROP_( LowDataRateOptimize )
		_V_PROP_( PreambleLength )
		_V_PROP_( SignalBandwidth )
		_V_PROP_( SpreadingFactorOrder )

	};
//---------------------------------------------------------------------------
	template <
		typename T_FrequencyShiftKeying,
		typename T_HasHeader,
		typename T_IsLoRa,
		typename T_LoRa,
		typename T_Size
	> class TArduinoLoRaSX126X_Packet :
		public T_FrequencyShiftKeying,
		public T_HasHeader,
		public T_IsLoRa,
		public T_LoRa,
		public T_Size
	{
	public:
		_V_PROP_( FrequencyShiftKeying )
		_V_PROP_( HasHeader )
		_V_PROP_( IsLoRa )
		_V_PROP_( LoRa )
		_V_PROP_( Size )

	};
//---------------------------------------------------------------------------
/*
	template <
		typename T_Timeout
	> class TArduinoLoRaSX126X_Transmitter :
		public T_Timeout
	{
	public:
		_V_PROP_( Timeout )

	};
*/
//---------------------------------------------------------------------------
	template <
		typename T_Enabled,
		typename T_Oscillator32MHz
	> class TArduinoLoRaSX126X_Standby :
		public T_Enabled,
		public T_Oscillator32MHz
	{
	public:
		_V_PROP_( Enabled )
		_V_PROP_( Oscillator32MHz )

	};
//---------------------------------------------------------------------------
	template <
		typename T_BootTime,
		typename T_Enabled,
		typename T_Voltage
	> class TArduinoLoRaSX126X_OscillatorControl :
		public T_BootTime,
		public T_Enabled,
		public T_Voltage
	{
	public:
		_V_PROP_( BootTime )
		_V_PROP_( Enabled )
		_V_PROP_( Voltage )

	};
//---------------------------------------------------------------------------
	template <
		typename T_Address,
		typename T_Enabled
	> class TArduinoLoRaSX126X_AddressFiltering_Address_Broadcast :
		public T_Address,
		public T_Enabled
	{
	public:
		_V_PROP_( Address )
		_V_PROP_( Enabled )

	};
//---------------------------------------------------------------------------
	template <
		typename T_0_RESET,
		typename T_SPI, T_SPI &C_SPI,
		typename T_BUSY_PIN, T_BUSY_PIN &C_BUSY_PIN,
		typename T_2_INTERRUPT,
		bool C_IsSX1262,
		typename T_ChipSelectOutputPin,
		typename T_ControlExternalRFswitch,
//		typename T_CodingRate,
        typename T_DataCRCErrorOutputPin,
//		typename T_Detected,
//		typename T_Errors,
//		typename T_FHasInterrupt,
		typename T_Frequency,
//		typename T_FrequencyShiftKeyingMode,
//		typename T_Gain,
//		typename T_Header,
		typename T_InterruptPinNumber,
//		typename T_LoRaTMMode,
//		typename T_LowNoiseAmplifierHighFrequencyBoost,
//		typename T_MaxSendQueue,
//		typename T_MobileNode,
		typename T_OscillatorControl,
		typename T_OutputPin,
		typename T_OverCurrentProtection,
		typename T_Packet,
		typename T_Power,
//		typename T_PreambleLength,
		typename T_RampTime,
		typename T_SPISpeed,
		typename T_SendingOutputPin,
//		typename T_SignalBandwidth,
//		typename T_SignalStrengthOutputPin,
//		typename T_Sleep,
//		typename T_SpreadingFactorOrder,
//		typename T_SymbolTimeout
		typename T_Standby,
//		typename T_Transmitter,
		typename T_UseLowDropoutRegulator
	> class MitovLoRaSX126X :
        public T_0_RESET,
		public T_2_INTERRUPT,
		public T_ChipSelectOutputPin,
		public T_ControlExternalRFswitch,
//		public T_CodingRate,
        public T_DataCRCErrorOutputPin,
//		public T_Detected,
//		public T_Errors,
//		public T_FHasInterrupt,
		public T_Frequency,
//		public T_FrequencyShiftKeyingMode,
//		public T_Gain,
//		public T_Header,
		public T_InterruptPinNumber,
//		public T_LoRaTMMode,
//		public T_LowNoiseAmplifierHighFrequencyBoost,
//		public T_MaxSendQueue,
//		public T_MobileNode,
		public T_OscillatorControl,
		public T_OutputPin,
		public T_OverCurrentProtection,
		public T_Packet,
		public T_Power,
//		public T_PreambleLength,
		public T_RampTime,
		public T_SPISpeed,
		public T_SendingOutputPin,
//		public T_SignalBandwidth,
//		public T_SignalStrengthOutputPin,
//		public T_Sleep,
//		public T_SpreadingFactorOrder,
//		public T_SymbolTimeout
		public T_Standby,
//		public T_Transmitter,
		public T_UseLowDropoutRegulator
	{
	public:
		_V_PIN_( ChipSelectOutputPin )
		_V_PIN_( DataCRCErrorOutputPin )
		_V_PIN_( OutputPin )
		_V_PIN_( SendingOutputPin )
//		_V_PIN_( SignalStrengthOutputPin )

	public:
		_V_PROP_( ControlExternalRFswitch )
//		_V_PROP_( Detected )
//		_V_PROP_( Errors )
		_V_PROP_( OscillatorControl )
		_V_PROP_( OverCurrentProtection )
		_V_PROP_( Packet )
		_V_PROP_( Power )
//		_V_PROP_( PreambleLength )
		_V_PROP_( RampTime )
		_V_PROP_( Frequency )
//		_V_PROP_( FrequencyShiftKeyingMode )
//		_V_PROP_( SignalBandwidth )
//		_V_PROP_( MaxSendQueue )
		_V_PROP_( Standby )
//		_V_PROP_( Transmitter )
		_V_PROP_( UseLowDropoutRegulator )

//		_V_PROP_( SymbolTimeout )
//		_V_PROP_( LoRaTMMode )
//		_V_PROP_( MobileNode )
//		_V_PROP_( LowNoiseAmplifierHighFrequencyBoost )
//		_V_PROP_( Gain )
//		_V_PROP_( Header )
		_V_PROP_( InterruptPinNumber )
//		_V_PROP_( CodingRate )
		_V_PROP_( SPISpeed )
//		_V_PROP_( SpreadingFactorOrder )
//		_V_PROP_( Sleep )

//	protected:
//		_V_PROP_( FHasInterrupt )

	protected:
		void SPIreadCommand( uint8_t cmd, uint8_t* data, uint8_t numBytes, bool waitForBusy = true )
		{
			SPItransfer( &cmd, 1, data, numBytes, waitForBusy );
		}

		void WriteRegister( uint16_t ARegister, uint8_t AValue )
		{
			uint8_t buf[] =
            {
                SX126XConst::SX126X_CMD_WRITE_REGISTER,
                ((ARegister & 0xFF00) >> 8),
                (ARegister & 0x00FF),
                AValue
            };            

			SPIWrite( buf, sizeof( buf ) );
		}

		void WriteRegister16( uint16_t ARegister, uint16_t AValue )
		{
			uint8_t buf[] =
            {
                SX126XConst::SX126X_CMD_WRITE_REGISTER,
                ((ARegister & 0xFF00) >> 8),
                (ARegister & 0x00FF),
                ((AValue & 0xFF00) >> 8),
                (AValue & 0x00FF),
            };            

			SPIWrite( buf, sizeof( buf ) );
		}

		void writeBuffer( const uint8_t* data, uint8_t numBytes, uint8_t offset ) 
		{
#ifdef ___MITOV_LORA_SX126X_DEBUG__
			Serial0.println( "writeBuffer" );
#endif
/*
			uint8_t *ABuffer = new uint8_t[ numBytes ];
			ABuffer[ 0 ] = SX126XConst::SX126X_CMD_WRITE_BUFFER;
			ABuffer[ 1 ] = offset;
			memcpy( ABuffer + 2, data, numBytes );
//			uint8_t cmd[] = { SX126XConst::SX126X_CMD_WRITE_BUFFER, offset };
			SPItransfer( ABuffer, numBytes + 2, nullptr, 0, false );
			delete [] ABuffer;
*/

			// ensure BUSY is low (state meachine ready)
			// TODO timeout
			while( C_BUSY_PIN.DigitalRead());
//			while(digitalRead(SX126x_BUSY));

			// start transfer
			T_ChipSelectOutputPin::SetPinValueLow();
//			digitalWrite(SX126x_SPI_SELECT, LOW);
			C_SPI.beginTransaction( SPISpeed(), MSBFIRST, SPI_MODE0 );

			C_SPI.transfer( SX126XConst::SX126X_CMD_WRITE_BUFFER );
			C_SPI.transfer( offset );

			// send data
//			Serial0.print("SPI write: CMD=0x");
//			Serial0.print(cmd[ 0 ], HEX);
// 
//			Serial0.print(" DataOut: ");
			for(uint8_t n = 0; n < numBytes; ++ n ) 
			{
				uint8_t in = C_SPI.transfer( data[ n ] );
//				Serial0.print(dataOut[n], HEX);
//				Serial0.print(" ");
			}
//			Serial0.println();
  
			// stop transfer
			C_SPI.endTransaction();
			T_ChipSelectOutputPin::SetPinValueHigh();
//			digitalWrite(SX126x_SPI_SELECT, HIGH);

/*
			// wait for BUSY to go high and then low
			// TODO timeout
			if( waitForBusy ) 
			{
				delayMicroseconds( 1 );
				while( C_BUSY_PIN.DigitalRead());
//				while(digitalRead(SX126x_BUSY));
			}
*/
		}

		void SPItransfer( uint8_t cmd, bool write, const uint8_t* dataOut, uint8_t* dataIn, uint8_t numBytes, bool waitForBusy ) 
		{  
			SPItransfer( &cmd, 1, write, dataOut, dataIn, numBytes, waitForBusy );
		}

		void Reset()
		{
			T_ChipSelectOutputPin::SetPinValueHigh();
	        T_0_RESET::Reset();

//			delay( 2000 );
//			Serial0.println( "TEST1" );

			if( T_0_RESET::ResetOutputPin().GetPinIsConnected() )
				while( C_BUSY_PIN.DigitalRead());


			UpdateStandby();
			UpdateControlExternalRFswitch();

			Calibrate(  SX126XConst::SX126X_CALIBRATE_IMAGE_ON
							| SX126XConst::SX126X_CALIBRATE_ADC_BULK_P_ON
							| SX126XConst::SX126X_CALIBRATE_ADC_BULK_N_ON
							| SX126XConst::SX126X_CALIBRATE_ADC_PULSE_ON
							| SX126XConst::SX126X_CALIBRATE_PLL_ON
							| SX126XConst::SX126X_CALIBRATE_RC13M_ON
							| SX126XConst::SX126X_CALIBRATE_RC64K_ON
							);

			UpdateRegulatorMode();

			UpdateStandby();
			ClearDeviceErrors();

			UpdateOscillatorControl();
//			uint8_t buf1[] = { SX126XConst::SX126X_CMD_SET_DIO3_AS_TCXO_CTRL, 0x0, 0x0, 0x1, 0x40 };
//		    SPIWrite( buf1, sizeof( buf1 ) );

			uint8_t data1[] = { SX126XConst::SX126X_CMD_SET_BUFFER_BASE_ADDRESS, 0, 0 };
			SPIWrite( data1, sizeof( data1 ) );

//			uint8_t data2[1] = { 1 }; // Modem Type
//			SPIwriteCommand( SX126XConst::SX126X_CMD_SET_PACKET_TYPE, data2, 1 );
//			UpdateModemType();

			UpdatePacketFSKNodeAddress();
			UpdatePacketFSKBroadcastAddress();
			UpdatePacketFSKWhiteningInitialValue();
			UpdatePacketFSK_CRCInitialValue();
			UpdatePacketFSK_CRCPolynomial();

			// set Rx/Tx fallback mode to STDBY_RC
//			data[0] = this->standbyXOSC ? SX126XConst::SX126X_RX_TX_FALLBACK_MODE_STDBY_XOSC : SX126XConst::SX126X_RX_TX_FALLBACK_MODE_STDBY_RC;
			uint8_t data3[] = { SX126XConst::SX126X_CMD_SET_RX_TX_FALLBACK_MODE, SX126XConst::SX126X_RX_TX_FALLBACK_MODE_STDBY_RC };
			SPIWrite( data3, sizeof( data3 ) );

  // set some CAD parameters - will be overwritten when calling CAD anyway
/*
  data4[0] = SX126XConst::SX126X_CAD_ON_8_SYMB;
  data4[1] = this->spreadingFactor + 13;
  data4[2] = SX126XConst::SX126X_CAD_PARAM_DET_MIN;
  data4[3] = SX126XConst::SX126X_CAD_GOTO_STDBY;
  data4[4] = 0x00;
  data4[5] = 0x00;
  data4[6] = 0x00;
*/
/*
			uint8_t data4[] = 
			{
                SX126XConst::SX126X_CMD_SET_CAD_PARAMS,
				SX126XConst::SX126X_CAD_ON_8_SYMB,
				0x16, //this->spreadingFactor + 13,
				SX126XConst::SX126X_CAD_PARAM_DET_MIN,
				SX126XConst::SX126X_CAD_GOTO_STDBY,
				0x00,
				0x00,
				0x00
			};

			SPIWrite( data4, 8 );
*/
//			uint8_t data[] = { (uint8_t)((clearIrqParams >> 8) & 0xFF), (uint8_t)(clearIrqParams & 0xFF) };
			ClearIrqStatus( SX126XConst::SX126X_IRQ_ALL );
//			uint8_t data4a[] = { 0x43, 0xFF };
//			SPIwriteCommand( SX126XConst::SX126X_CMD_CLEAR_IRQ_STATUS, data4a, 2 );

//			uint8_t data5[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };
//			SPIwriteCommand( SX126XConst::SX126X_CMD_SET_DIO_IRQ_PARAMS, data5, 8 );
 
			// 500/9/8  - 0x09 0x04 0x03 0x00 - SF9, BW125, 4/8
			// 500/11/8 - 0x0B 0x04 0x03 0x00 - SF11 BW125, 4/7
//			uint8_t data6[4] = {sf, bw, cr, this->ldrOptimize};
//			uint8_t data6[4] = { 0x09, 0x06, 0x03, 0x00 };
//			SPIwriteCommand( SX126XConst::SX126X_CMD_SET_MODULATION_PARAMS, data6, 4 );

//			uint8_t data7[6] = {(uint8_t)((preambleLen >> 8) & 0xFF), (uint8_t)(preambleLen & 0xFF), hdrType, payloadLen, crcType, invertIQ};
			UpdatePacketParams();
//			uint8_t data7[] = { SX126XConst::SX126X_CMD_SET_PACKET_PARAMS, 0, 8, 0, 0xFF, 1, 0 };
//			SPIWrite( data7, sizeof( data7 ) );

////			uint8_t data8 = 1;
////  			SPIwriteCommand( SX126XConst::SX126X_CMD_SET_DIO2_AS_RF_SWITCH_CTRL, &data8, 1 );

//			uint8_t data6b[] = { SX126XConst::SX126X_CMD_SET_MODULATION_PARAMS, 9, 6, 3, 0 };
//			SPIWrite( data6b, sizeof( data6b ) );

//			uint8_t data9[] = { SX126XConst::SX126X_CMD_CALIBRATE_IMAGE, 0x6B, 0x6F };
//			SPIWrite( data9, sizeof( data9 ) );


//			uint8_t data6c[4] = { 9, 4, 3, 0 };
//			SPIwriteCommand( SX126XConst::SX126X_CMD_SET_MODULATION_PARAMS, data6c, 4 );

//			uint8_t data10[] = { SX126XConst::SX126X_CMD_SET_RF_FREQUENCY, 0x38, 0x93, 0x33, 0x40 };
//			SPIWrite( data10, 5 );

			UpdateModulationParameters();

//			uint8_t data11[] = { paDutyCycle, hpMax, deviceSel, paLut };

////			uint8_t data11[] = { SX126XConst::SX126X_CMD_SET_PA_CONFIG, 4, 7, 0, 1 }; //2//
////			SPIWrite( data11, sizeof( data11 ) );

//			uint8_t data12[] = { pwr, rampTime };
////			uint8_t data12[] = { SX126XConst::SX126X_CMD_SET_TX_PARAMS, 0, 4 }; //2//
////			SPIWrite( data12, sizeof( data12 ) );


			UpdateOverCurrentProtection();
			UpdateTransmitPower();

///*
			SetDioIrqParams( SX126XConst::SX126X_IRQ_ALL,  //all interrupts enabled
							( InterruptPinNumber() == 1 ) ? ( SX126XConst::SX126X_IRQ_ALL ) : SX126XConst::SX126X_IRQ_NONE, //interrupts on DIO1
							( InterruptPinNumber() == 2 ) ? ( SX126XConst::SX126X_IRQ_ALL ) : SX126XConst::SX126X_IRQ_NONE, //interrupts on DIO2
							( InterruptPinNumber() == 3 ) ? ( SX126XConst::SX126X_IRQ_ALL ) : SX126XConst::SX126X_IRQ_NONE //interrupts on DIO3
				);
//*/

/*
			uint8_t data13[8] = {(uint8_t)((irqMask >> 8) & 0xFF), (uint8_t)(irqMask & 0xFF),
								(uint8_t)((dio1Mask >> 8) & 0xFF), (uint8_t)(dio1Mask & 0xFF),
								(uint8_t)((dio2Mask >> 8) & 0xFF), (uint8_t)(dio2Mask & 0xFF),
								(uint8_t)((dio3Mask >> 8) & 0xFF), (uint8_t)(dio3Mask & 0xFF)};
*/
//			uint8_t data13[] = { SX126XConst::SX126X_CMD_SET_DIO_IRQ_PARAMS, 2, 0x72, 0, 2, 0, 0, 0, 0 };
//			SPIWrite( data13, sizeof( data13 ) );

//			uint8_t data14[2] = {txBaseAddress, rxBaseAddress};
//			uint8_t data14[2] = { 0, 0 };
//			SPIwriteCommand( SX126XConst::SX126X_CMD_SET_BUFFER_BASE_ADDRESS, data14, 2 );

			UpdateFrequency();
			SetRx();
		}

		void SPItransfer( const uint8_t* dataOut, uint8_t numBytesOut, uint8_t* dataIn, uint8_t numBytesIn, bool waitForBusy ) 
		{
			// ensure BUSY is low (state meachine ready)
			// TODO timeout
			while( C_BUSY_PIN.DigitalRead());
//			while(digitalRead(SX126x_BUSY));

			// start transfer
			T_ChipSelectOutputPin::SetPinValueLow();
//			digitalWrite(SX126x_SPI_SELECT, LOW);
			C_SPI.beginTransaction( SPISpeed(), MSBFIRST, SPI_MODE0 );

			// send data
#ifdef ___MITOV_LORA_SX126X_DEBUG__
			Serial0.print("SPI write: CMD= ");
			for(uint8_t n = 0; n < numBytesOut; ++ n ) 
			{
				Serial0.print(dataOut[n], HEX);
				Serial0.print(" ");
			}

			Serial0.println();
#endif
// 
//			Serial0.print(" DataOut: ");
			for(uint8_t n = 0; n < numBytesOut; ++ n ) 
				C_SPI.transfer(dataOut[n]);
  
			//Serial.print("SPI read:  CMD=0x");
			//Serial.print(cmd, HEX);
			// skip the first byte for read-type commands (status-only)
			uint8_t in = C_SPI.transfer( 0 );
			////Serial.println((SX126X_CMD_NOP, HEX));
			//Serial.print(" DataIn: ");

			for(uint8_t n = 0; n < numBytesIn; ++ n ) 
			{
				dataIn[n] = C_SPI.transfer( 0 );
//				Serial0.print(dataIn[n], HEX);
//				Serial0.print(" ");
			}
//			Serial0.println();

			// stop transfer
			C_SPI.endTransaction();
			T_ChipSelectOutputPin::SetPinValueHigh();
//			digitalWrite(SX126x_SPI_SELECT, HIGH);

			// wait for BUSY to go high and then low
			// TODO timeout
			if( waitForBusy ) 
			{
				delayMicroseconds( 1 );
				while( C_BUSY_PIN.DigitalRead());
//				while(digitalRead(SX126x_BUSY));
			}
		}

		void SetDioIrqParams( uint16_t irqMask, uint16_t dio1Mask, uint16_t dio2Mask, uint16_t dio3Mask )
		{
#ifdef ___MITOV_LORA_SX126X_DEBUG__
			Serial0.println( "SetDioIrqParams" );
#endif
			uint8_t buf[] =
			{
				SX126XConst::SX126X_CMD_SET_DIO_IRQ_PARAMS,
				uint8_t((irqMask >> 8) & 0x00FF),
				uint8_t(irqMask & 0x00FF),
				uint8_t((dio1Mask >> 8) & 0x00FF),
				uint8_t(dio1Mask & 0x00FF),
				uint8_t((dio2Mask >> 8) & 0x00FF),
				uint8_t(dio2Mask & 0x00FF),
				uint8_t((dio3Mask >> 8) & 0x00FF),
				uint8_t(dio3Mask & 0x00FF)
			};

			SPIWrite( buf, 9 );
		}

		void SetRx( void )
		{
			uint8_t buf[] = { SX126XConst::SX126X_CMD_SET_RX, 0xFF, 0xFF, 0xFF };
//			uint8_t buf[] = { SX126XConst::SX126X_CMD_SET_RX, 0, 0, 0 };

//			buf[0] = (uint8_t)((timeout >> 16) & 0xFF);
//			buf[1] = (uint8_t)((timeout >> 8) & 0xFF);
//			buf[2] = (uint8_t )(timeout & 0xFF);
			SPIWrite( buf, sizeof( buf ) );
		}

/*
		void SetTx( uint64_t timeoutInMs )
		{
			uint32_t tout = uint32_t( timeoutInMs / 15.625 );
			uint8_t buf[] =
			{
				SX126XConst::SX126X_CMD_SET_TX,
				uint8_t((tout >> 16) & 0xFF),
				uint8_t((tout >> 8) & 0xFF),
				uint8_t(tout & 0xFF)
			};

			SPIWrite( buf, sizeof( buf ) );

			FSending() = true;
		}
*/
		void SetTx()
		{
			uint8_t buf[] =
			{
				SX126XConst::SX126X_CMD_SET_TX,
				0xFF,
				0xFF,
				0xFF				
			};

			SPIWrite( buf, sizeof( buf ) );

//			FSending() = true;
		}

		uint16_t GetIrqStatus( void )
		{
			uint8_t data[ 2 ];
			SPIreadCommand( SX126XConst::SX126X_CMD_GET_IRQ_STATUS, data, 2 );
			return ( data[ 0 ] << 8) | data[ 1 ];
		}

		uint8_t GetStatus()
		{
			uint8_t data;
			SPIreadCommand( SX126XConst::SX126X_CMD_GET_STATUS, &data, 1 );
			return data;
		}

		void ClearIrqStatus( uint16_t irq )
		{
			uint8_t buf[3] = 
            {
                SX126XConst::SX126X_CMD_CLEAR_IRQ_STATUS,
                uint8_t(((uint16_t)irq >> 8) & 0xFF),
                uint8_t((uint16_t)irq & 0xFF)
            };

			SPIWrite( buf, sizeof( buf ) );
		}

		void GetRxBufferStatus( uint8_t &payloadLength, uint8_t &rxStartBufferPointer )
		{
			uint8_t buf[2];

			SPIreadCommand( SX126XConst::SX126X_CMD_GET_RX_BUFFER_STATUS, buf, 2 );

			payloadLength = buf[0];
			rxStartBufferPointer = buf[1];
		}

		void readBuffer( uint8_t* data, uint8_t numBytes, uint8_t offset ) 
		{
			uint8_t cmd[] = { SX126XConst::SX126X_CMD_READ_BUFFER, offset };
			SPItransfer( cmd, sizeof( cmd ), data, numBytes, false );
//			SPIreadStream( cmd, 2, data, numBytes );
		}

		void ClearDeviceErrors()
		{
			uint8_t data[] = { SX126XConst::SX126X_CMD_CLEAR_DEVICE_ERRORS, 0, 0 };
			SPIWrite( data, 3 );
		}

		void UpdatePacketParamsSend( uint8_t ASize )
		{
			UpdateModemType();
			if( Packet().IsLoRa() )
			{
				uint8_t buf[] = 
				{ 
					SX126XConst::SX126X_CMD_SET_PACKET_PARAMS,
					Packet().LoRa().PreambleLength() >> 8,
					Packet().LoRa().PreambleLength(),
					Packet().HasHeader() ? 0 : 1,
					ASize,
					Packet().LoRa().HasCRC() ? 1 : 0,
					Packet().LoRa().InvertIQ() ? 1 : 0
				};

#ifdef ___MITOV_LORA_SX126X_DEBUG__
				for( int i = 0; i < sizeof( buf ); ++ i )
				{
					Serial0.print( " " );
					Serial0.print( buf[ i ], HEX );
				}

				Serial0.println();
#endif

				SPIWrite( buf, sizeof( buf ), true );
			}

			else
			{
				uint8_t buf[ 10 ];
				buf[ 0 ] = SX126XConst::SX126X_CMD_SET_PACKET_PARAMS;
				buf[ 1 ] = uint8_t(( Packet().FrequencyShiftKeying().PreambleLength().GetValue() >> 8) & 0xFF );
				buf[ 2 ] = uint8_t( Packet().FrequencyShiftKeying().PreambleLength().GetValue() & 0xFF );
				
				uint32_t maxDetLen = MitovMin<uint32_t>( Packet().FrequencyShiftKeying().SyncWord().GetCount() * 8, Packet().FrequencyShiftKeying().PreambleLength().GetValue() );

				uint8_t syncWordLength = Packet().FrequencyShiftKeying().SyncWord().GetCount() * 8;

#ifdef ___MITOV_LORA_SX126X_DEBUG__
				Serial0.print( "syncWordLength " ); Serial0.println( syncWordLength );
				Serial0.print( "preambleLengthFSK " ); Serial0.println( int( Packet().FrequencyShiftKeying().PreambleLength().GetValue() ));
				Serial0.print( "maxDetLen " ); Serial0.println( maxDetLen );
#endif

				buf[ 3 ] = maxDetLen >= 32 ? SX126XConst::SX126X_GFSK_PREAMBLE_DETECT_32 :
										  maxDetLen >= 24 ? SX126XConst::SX126X_GFSK_PREAMBLE_DETECT_24 :
										  maxDetLen >= 16 ? SX126XConst::SX126X_GFSK_PREAMBLE_DETECT_16 :
										  maxDetLen >   0 ? SX126XConst::SX126X_GFSK_PREAMBLE_DETECT_8 :
										  SX126XConst::SX126X_GFSK_PREAMBLE_DETECT_OFF;

				buf[ 4 ] = syncWordLength;

				if( Packet().FrequencyShiftKeying().AddressFiltering().Enabled() )
				{
					if( Packet().FrequencyShiftKeying().AddressFiltering().Broadcast().Enabled() )
					{
						WriteRegister( SX126XConst::SX126X_REG_BROADCAST_ADDRESS, Packet().FrequencyShiftKeying().AddressFiltering().Broadcast().Address() );
						buf[ 5 ] = 0x02;
					}

					else
						buf[ 5 ] = 0x01;

					WriteRegister( SX126XConst::SX126X_REG_NODE_ADDRESS, Packet().FrequencyShiftKeying().AddressFiltering().NodeAddress() );
				}

				else
					buf[ 5 ] = 0x00;


				buf[ 6 ] = ( Packet().FrequencyShiftKeying().PacketSize().Variable() ) ? 1 : 0;
				buf[ 7 ] = Packet().FrequencyShiftKeying().PacketSize().Value();


				if( Packet().FrequencyShiftKeying().CRC().Bytes().GetValue() == 0 )
					buf[ 8 ] = 0x01;

				else
				{
					if( Packet().FrequencyShiftKeying().CRC().Inverted() )
						buf[ 8 ] = ( Packet().FrequencyShiftKeying().CRC().Bytes().GetValue() == 2 ) ? 6 : 4;

					else
						buf[ 8 ] = ( Packet().FrequencyShiftKeying().CRC().Bytes().GetValue() == 2 ) ? 2 : 0;

					WriteRegister16( SX126XConst::SX126X_REG_CRC_INITIAL_MSB, Packet().FrequencyShiftKeying().CRC().InitialValue().GetValue() );
					WriteRegister16( SX126XConst::SX126X_REG_CRC_POLYNOMIAL_MSB, Packet().FrequencyShiftKeying().CRC().Polynomial().GetValue() );
				}

				if( Packet().FrequencyShiftKeying().Whitening().Enabled() ) 
				{
					WriteRegister16( SX126XConst::SX126X_REG_WHITENING_INITIAL_MSB, Packet().FrequencyShiftKeying().Whitening().InitialValue().GetValue() );
					buf[ 9 ] = 0x01;
				}
				
				else
					buf[ 9 ] = 0x00;

				buf[ 9 ] = ( Packet().FrequencyShiftKeying().Whitening().Enabled() ) ? 0x01 : 0x00;

				SPIWrite( buf, sizeof( buf ), true );

				uint8_t ASyncWordBuf[ 11 ];
				ASyncWordBuf[ 0 ] = SX126XConst::SX126X_CMD_WRITE_REGISTER;
				ASyncWordBuf[ 1 ] = uint8_t( SX126XConst::SX126X_REG_SYNC_WORD_0 >> 8 );
				ASyncWordBuf[ 2 ] = uint8_t( SX126XConst::SX126X_REG_SYNC_WORD_0 );

				Packet().FrequencyShiftKeying().SyncWord().CopyData( ASyncWordBuf + 3 );

#ifdef ___MITOV_LORA_SX126X_DEBUG__
				Serial0.println( "TEST1" );
				Serial0.println( 3 + Packet().FrequencyShiftKeying().SyncWord().GetCount() );
#endif

				SPIWrite( ASyncWordBuf, 3 + Packet().FrequencyShiftKeying().SyncWord().GetCount() );
			}
		}

	public:
		void SPIWrite( const uint8_t* data, uint8_t numBytes, bool waitForBusy = true )
		{
			SPItransfer( data, numBytes, nullptr, 0, waitForBusy );
		}

		void Calibrate( uint8_t calibParam )
		{
			uint8_t data[] = { SX126XConst::SX126X_CMD_CALIBRATE, calibParam };
			SPIWrite( data, 2 );
		}

		inline void CalibrateImage(uint32_t frequency)
		{
			uint8_t data[ 3 ];
			data[ 0 ] = SX126XConst::SX126X_CMD_CALIBRATE_IMAGE;

			if( frequency > 900000000 )
			{
				data[1] = 0xE1;
				data[2] = 0xE9;
			}

			else if( frequency > 850000000 )
			{
				data[1] = 0xD7;
				data[2] = 0xD8;
			}

			else if( frequency > 770000000 )
			{
				data[1] = 0xC1;
				data[2] = 0xC5;
			}

			else if( frequency > 460000000 )
			{
				data[1] = 0x75;
				data[2] = 0x81;
			}

			else if( frequency > 425000000 )
			{
				data[1] = 0x6B;
				data[2] = 0x6F;
			}

			SPIWrite( data, 3 );
		}

		void UpdateTransmitPower( void )
		{
/*
{
uint8_t buf[ 5 ] = { SX126XConst::SX126X_CMD_SET_PA_CONFIG, 4, 7, 0, 1 };
SPIWrite( buf, 5 );
return;
}
*/

			uint8_t buf[ 5 ];
            buf[ 0 ] = SX126XConst::SX126X_CMD_SET_PA_CONFIG;
			buf[4] = 1;
			if( C_IsSX1262 )
			{
				buf[3] = 0;
				if( Power() >= 22 )
				{
					buf[1] = 4; //paDutyCycle;
					buf[2] = 7; // hpMax;
				}

				else if( Power() >= 20 )
				{
					buf[1] = 3; //paDutyCycle;
					buf[2] = 5; // hpMax;
				}

				else if( Power() >= 17 )
				{
					buf[1] = 2; //paDutyCycle;
					buf[2] = 3; // hpMax;
				}

				else
				{
					buf[1] = 2; //paDutyCycle;
					buf[2] = 2; // hpMax;
				}
			}

			else
			{
				buf[2] = 0; // hpMax;
				buf[3] = 1;

				if( Power() >= 15 )
					buf[1] = 6;

				else if( Power() >= 14 )
					buf[1] = 4;

				else
					buf[1] = 1;

			}

			SPIWrite( buf, 5 );

			buf[ 0 ] = SX126XConst::SX126X_CMD_SET_TX_PARAMS;

			buf[ 1 ] = Power();
			if( RampTime() >= 3400 )
				buf[ 2 ] = SX126XConst::SX126X_PA_RAMP_3400U;

			else if( RampTime() >= 1700 )
				buf[ 2 ] = SX126XConst::SX126X_PA_RAMP_1700U;

			else if( RampTime() >= 800 )
				buf[ 2 ] = SX126XConst::SX126X_PA_RAMP_800U;

			else if( RampTime() >= 200 )
				buf[ 2 ] = SX126XConst::SX126X_PA_RAMP_200U;

			else if( RampTime() >= 80 )
				buf[ 2 ] = SX126XConst::SX126X_PA_RAMP_80U;

			else if( RampTime() >= 40 )
				buf[ 2 ] = SX126XConst::SX126X_PA_RAMP_40U;

			else if( RampTime() >= 20 )
				buf[ 2 ] = SX126XConst::SX126X_PA_RAMP_20U;

			else
				buf[ 2 ] = SX126XConst::SX126X_PA_RAMP_10U;

			SPIWrite( buf, 3 );
		}

		void UpdateFrequency()
		{
			uint32_t frequency = Frequency().GetValue() * 1000000 + 0.5;
			CalibrateImage( frequency );

//			uint32_t freq = uint32_t( double( frequency ) / double( SX126XConst::FREQ_STEP ));
			uint32_t freq = ( Frequency().GetValue() * (uint32_t(1) << SX126XConst::SX126X_DIV_EXPONENT)) / SX126XConst::SX126X_CRYSTAL_FREQ;

			uint8_t buf[] =
			{
				SX126XConst::SX126X_CMD_SET_RF_FREQUENCY,
				uint8_t((freq >> 24) & 0xFF),
				uint8_t((freq >> 16) & 0xFF),
				uint8_t((freq >> 8) & 0xFF),
				uint8_t(freq & 0xFF)
			};

			SPIWrite( buf, 5 );
		}

		void UpdateRegulatorMode()
		{
			uint8_t data[] =
			{
				SX126XConst::SX126X_CMD_SET_REGULATOR_MODE,
				UseLowDropoutRegulator() ? SX126XConst::SX126X_REGULATOR_LDO : SX126XConst::SX126X_REGULATOR_DC_DC
			};

			SPIWrite( data, 2 );
		}

		inline void UpdatePacketParams()
		{
			UpdatePacketParamsSend( Packet().Size() );
		}

		void UpdateOverCurrentProtection()
		{
			uint8_t value = OverCurrentProtection().GetValue() * 2.5;
			WriteRegister( SX126XConst::SX126X_REG_OCP_CONFIGURATION, value );
		}

		void UpdateControlExternalRFswitch()
		{
			uint8_t data[] =
			{
				SX126XConst::SX126X_CMD_SET_DIO2_AS_RF_SWITCH_CTRL,
				ControlExternalRFswitch() ? 1 : 0
			};

			SPIWrite( data, 2 );
		}

		void UpdateStandby()
		{
			uint8_t data[] =
			{
				SX126XConst::SX126X_CMD_SET_STANDBY,
				SX126XConst::SX126X_STANDBY_RC
			};

			SPIWrite( data, 2 );
		}

		inline void UpdateModemType()
		{
			uint8_t data[] =
			{
				SX126XConst::SX126X_CMD_SET_PACKET_TYPE, 
				( Packet().IsLoRa() ) ? 1 : 0  // Modem Type
			};

			SPIWrite( data, 2 );
		}

		void UpdateModulationParameters()
		{
			uint8_t data[ 9 ];
            
            data[ 0 ] = SX126XConst::SX126X_CMD_SET_MODULATION_PARAMS;
			if( Packet().IsLoRa() )
			{
				data[ 1 ] = Packet().LoRa().SpreadingFactorOrder();
				if( Packet().LoRa().SignalBandwidth().GetValue() >= 500.0 - 0.005 )
					data[ 2 ] = 0x06;

				else if( Packet().LoRa().SignalBandwidth().GetValue() >= 250.0 - 0.005 )
					data[ 2 ] = 0x05;

				else if( Packet().LoRa().SignalBandwidth().GetValue() >= 125.0 - 0.005 )
					data[ 2 ] = 0x04;

				else if( Packet().LoRa().SignalBandwidth().GetValue() >= 62.50 - 0.005 )
					data[ 2 ] = 0x03;

				else if( Packet().LoRa().SignalBandwidth().GetValue() >= 41.67 - 0.005 )
					data[ 2 ] = 0x0A;

				else if( Packet().LoRa().SignalBandwidth().GetValue() >= 31.25 - 0.005 )
					data[ 2 ] = 0x02;

				else if( Packet().LoRa().SignalBandwidth().GetValue() >= 20.83 - 0.005 )
					data[ 2 ] = 0x09;

				else if( Packet().LoRa().SignalBandwidth().GetValue() >= 15.63 - 0.005 )
					data[ 2 ] = 0x01;

				else if( Packet().LoRa().SignalBandwidth().GetValue() >= 10.42 - 0.005 )
					data[ 2 ] = 0x08;

				else
					data[ 2 ] = 0x00;

				data[ 3 ] = uint8_t( Packet().LoRa().CodingRate() ) + 1;
				data[ 4 ] = Packet().LoRa().LowDataRateOptimize() ? 1 : 0;

				SPIWrite( data, 5 );
			}

			else
			{
				uint32_t brRaw = uint32_t(( SX126XConst::SX126X_CRYSTAL_FREQ * 1000000.0 * 32.0 ) / ( Packet().FrequencyShiftKeying().BitRate().GetValue() * 1000.0 ));
				data[ 1 ] = uint8_t((brRaw >> 16) & 0xFF );
				data[ 2 ] = uint8_t((brRaw >> 8) & 0xFF );
				data[ 3 ] = uint8_t( brRaw & 0xFF );

				if( Packet().FrequencyShiftKeying().GaussianPulseShape().GetValue() >= 1.0 - 0.005 )
					data[ 4 ] = 0x0B;

				else if( Packet().FrequencyShiftKeying().GaussianPulseShape().GetValue() >= 0.7 - 0.005 )
					data[ 4 ] = 0x0A;

				else if( Packet().FrequencyShiftKeying().GaussianPulseShape().GetValue() >= 0.5 - 0.005 )
					data[ 4 ] = 0x09;

				else if( Packet().FrequencyShiftKeying().GaussianPulseShape().GetValue() >= 0.3 - 0.005 )
					data[ 4 ] = 0x08;

				else
					data[ 4 ] = 0x00;

				if( Packet().FrequencyShiftKeying().Bandwidth().GetValue() >= 467.0 - 0.005 )
					data[ 5 ] = 0x09;

				else if( Packet().FrequencyShiftKeying().Bandwidth().GetValue() >= 373.6 - 0.005 )
					data[ 5 ] = 0x11;

				else if( Packet().FrequencyShiftKeying().Bandwidth().GetValue() >= 312.0 - 0.005 )
					data[ 5 ] = 0x19;

				else if( Packet().FrequencyShiftKeying().Bandwidth().GetValue() >= 232.3 - 0.005 )
					data[ 5 ] = 0x0A;

				else if( Packet().FrequencyShiftKeying().Bandwidth().GetValue() >= 187.2 - 0.005 )
					data[ 5 ] = 0x12;

				else if( Packet().FrequencyShiftKeying().Bandwidth().GetValue() >= 156.2 - 0.005 )
					data[ 5 ] = 0x1A;

				else if( Packet().FrequencyShiftKeying().Bandwidth().GetValue() >= 117.3 - 0.005 )
					data[ 5 ] = 0x0B;

				else if( Packet().FrequencyShiftKeying().Bandwidth().GetValue() >= 93.8 - 0.005 )
					data[ 5 ] = 0x13;

				else if( Packet().FrequencyShiftKeying().Bandwidth().GetValue() >= 78.2 - 0.005 )
					data[ 5 ] = 0x1B;

				else if( Packet().FrequencyShiftKeying().Bandwidth().GetValue() >= 58.6 - 0.005 )
					data[ 5 ] = 0x0C;

				else if( Packet().FrequencyShiftKeying().Bandwidth().GetValue() >= 46.9 - 0.005 )
					data[ 5 ] = 0x14;

				else if( Packet().FrequencyShiftKeying().Bandwidth().GetValue() >= 39.0 - 0.005 )
					data[ 5 ] = 0x1C;

				else if( Packet().FrequencyShiftKeying().Bandwidth().GetValue() >= 29.3 - 0.005 )
					data[ 5 ] = 0x0D;

				else if( Packet().FrequencyShiftKeying().Bandwidth().GetValue() >= 23.4 - 0.005 )
					data[ 5 ] = 0x15;

				else if( Packet().FrequencyShiftKeying().Bandwidth().GetValue() >= 19.5 - 0.005 )
					data[ 5 ] = 0x1D;

				else if( Packet().FrequencyShiftKeying().Bandwidth().GetValue() >= 14.6 - 0.005 )
					data[ 5 ] = 0x0E;

				else if( Packet().FrequencyShiftKeying().Bandwidth().GetValue() >= 11.7 - 0.005 )
					data[ 5 ] = 0x16;

				else if( Packet().FrequencyShiftKeying().Bandwidth().GetValue() >= 9.7 - 0.005 )
					data[ 5 ] = 0x1E;

				else if( Packet().FrequencyShiftKeying().Bandwidth().GetValue() >= 7.3 - 0.005 )
					data[ 5 ] = 0x0F;

				else if( Packet().FrequencyShiftKeying().Bandwidth().GetValue() >= 5.8 - 0.005 )
					data[ 5 ] = 0x17;

				else
					data[ 5 ] = 0x1F;

				uint32_t freqDevRaw = uint32_t((( Packet().FrequencyShiftKeying().FrequencyDeviation().GetValue() * 1000.0) * float(uint32_t(1) << 25)) / ( SX126XConst::SX126X_CRYSTAL_FREQ * 1000000.0));

#ifdef ___MITOV_LORA_SX126X_DEBUG__
				Serial0.print( "freqDevRaw " ); Serial0.println( freqDevRaw );
#endif
				data[ 6 ] = uint8_t((freqDevRaw >> 16) & 0xFF );
				data[ 7 ] = uint8_t((freqDevRaw >> 8) & 0xFF );
				data[ 8 ] = uint8_t( freqDevRaw & 0xFF );

				SPIWrite( data, 9 );
			}

		}

		void UpdateOscillatorControl()
		{
			if( OscillatorControl().Enabled() )
			{
				uint8_t buf[ 5 ];
				buf[ 0 ] = SX126XConst::SX126X_CMD_SET_DIO3_AS_TCXO_CTRL;
				if( OscillatorControl().Voltage() >= 3.3 - 0.005 )
					buf[ 1 ] = 0x07;

				else if( OscillatorControl().Voltage() >= 3.0 - 0.005 )
					buf[ 1 ] = 0x06;

				else if( OscillatorControl().Voltage() >= 2.7 - 0.005 )
					buf[ 1 ] = 0x05;

				else if( OscillatorControl().Voltage() >= 2.4 - 0.005 )
					buf[ 1 ] = 0x04;

				else if( OscillatorControl().Voltage() >= 2.2 - 0.005 )
					buf[ 1 ] = 0x03;

				else if( OscillatorControl().Voltage() >= 1.8 - 0.005 )
					buf[ 1 ] = 0x02;

				else if( OscillatorControl().Voltage() >= 1.7 - 0.005 )
					buf[ 1 ] = 0x01;

				else
					buf[ 1 ] = 0x00;

				uint32_t tout = uint32_t( OscillatorControl().BootTime() / 15.625 );
				buf[ 2 ] = uint8_t((tout >> 16) & 0xFF);
				buf[ 3 ] = uint8_t((tout >> 8) & 0xFF);
				buf[ 4 ] = uint8_t(tout & 0xFF);

				SPIWrite( buf, sizeof( buf ) );
			}
		}

		void UpdateOscillatorControlEnabled()
		{
			if( OscillatorControl().Enabled() )
				UpdateOscillatorControl();

			else
				Reset();

		}

		inline void UpdatePacketFSKNodeAddress()
		{
			if( ! Packet().IsLoRa().GetValue() )
				if( Packet().FrequencyShiftKeying().AddressFiltering().Enabled() )
					WriteRegister( SX126XConst::SX126X_REG_NODE_ADDRESS, Packet().FrequencyShiftKeying().AddressFiltering().NodeAddress() );

		}

		inline void UpdatePacketFSKBroadcastAddress()
		{
			if( ! Packet().IsLoRa().GetValue() )
				if( Packet().FrequencyShiftKeying().AddressFiltering().Broadcast().Enabled() )
					WriteRegister( SX126XConst::SX126X_REG_BROADCAST_ADDRESS, Packet().FrequencyShiftKeying().AddressFiltering().Broadcast().Address() );

		}

		inline void UpdatePacketFSKWhiteningInitialValue()
		{
			if( Packet().FrequencyShiftKeying().Whitening().Enabled() )
				WriteRegister16( SX126XConst::SX126X_REG_WHITENING_INITIAL_MSB, Packet().FrequencyShiftKeying().Whitening().InitialValue().GetValue() );

		}

		inline void UpdatePacketFSK_CRCInitialValue()
		{
			if( Packet().FrequencyShiftKeying().CRC().Bytes().GetValue() != 0 )
				WriteRegister16( SX126XConst::SX126X_REG_CRC_INITIAL_MSB, Packet().FrequencyShiftKeying().CRC().InitialValue().GetValue() );
		}

		inline void UpdatePacketFSK_CRCPolynomial()
		{
			if( Packet().FrequencyShiftKeying().CRC().Bytes().GetValue() != 0 )
					WriteRegister16( SX126XConst::SX126X_REG_CRC_POLYNOMIAL_MSB, Packet().FrequencyShiftKeying().CRC().Polynomial().GetValue() );

		}

	public:
		void send( const uint8_t* data, uint8_t len )
		{
//return;
//			uint8_t buf0[] = { 0x80, 0 };
//			SPIWrite( buf0, sizeof( buf0 ), true );

			uint8_t buf[] = { 0x8C, 0, 8, 0, len, 1, 0 };
			SPIWrite( buf, sizeof( buf ), true );

//			UpdatePacketParamsSend( len );

//			uint8_t buf1_1[] = { 0xE, 0x54, 0x65, 0x73, 0x74, 0x31 };
//			SPIWrite( buf1_1, sizeof( buf1_1 ), true );

//			uint8_t buf1_2[] = { 0x83, 0, 0, 0 };
//			SPIWrite( buf1_2, sizeof( buf1_1 ), true );

/*
			uint8_t buf1[] = { 0x8, 2, 1, 0, 1, 0, 0, 0, 0 };
			SPIWrite( buf1, sizeof( buf1 ), true );
			

			uint8_t data7[] = { SX126XConst::SX126X_CMD_SET_PACKET_PARAMS, 0, 8, 0, len, 1, 0, };
			SPIWrite( data7, sizeof( data7 ));

*/
			writeBuffer( data, len, 0 );

			T_SendingOutputPin::SetPinValueHigh();
			SetTx();
//			SetTx( Transmitter().Timeout() );
/*		
			uint8_t buf1[] =
			{
				SX126XConst::SX126X_CMD_SET_TX,
				0xFF,
				0xFF,
				0xFF
			};

			SPIWrite( buf1, sizeof( buf1 ) );
*/
		}

	public:
		void Print( Mitov::String AValue )
		{
			AValue += "\r\n";
			send( (uint8_t *)AValue.c_str(), AValue.length() );
		}

		void Print( float AValue )
		{
			char AText[ __VISUINO_FLOAT_TO_STR_LEN__ ];
			dtostrf( AValue,  1, 2, AText );
			Print( Mitov::String( AText ));
		}

		void Print( int32_t AValue )
		{
			char AText[ 16 ];
			ltoa( AValue, AText, 10 );
			Print( Mitov::String( AText ));
		}

		void Print( uint32_t AValue )
		{
			char AText[ 16 ];
			ltoa( AValue, AText, 10 );
			Print( Mitov::String( AText ));
		}

		inline void PrintChar( char AValue )
		{
			send( (uint8_t*)&AValue, 1 );
		}

		inline void PrintChar( uint8_t AValue )
		{
			send( &AValue, 1 );
		}

		inline void Write( uint8_t *AData, uint32_t ASize )
		{
			send( AData, ASize );
		}

	public:
		void __ICACHE_RAM_ATTR__ InterruptHandler( bool AValue )
		{
			T_2_INTERRUPT::SetValue( true );
		}

	public:
		void ResetInputPin_o_Receive( void *_Data )
		{
			Reset();
		}

	public:
		inline void SystemInit()
		{
//return;
			Reset();
		}

		inline void SystemStart()
		{
//			Packet().LoRa().Detected().ChannelActivityOutputPin().SetPinValue( false );
		}

		inline void SystemLoopBegin()
		{
			if( ! T_2_INTERRUPT::GetValue() )
				return;

			T_2_INTERRUPT::SetValue( false );
//return;
			if( C_BUSY_PIN.DigitalRead())
				return;

//return;
//			uint8_t AStatus1 = GetStatus();
//Serial0.print( "Status1: " );
//Serial0.println( AStatus1, BIN );

			uint16_t AStatus = GetIrqStatus();
//Serial0.print( "Status: " );
//Serial0.println( AStatus, BIN );
//delay( 100 );
/*
			if( FSending() )
			{
				SetRx();
				FSending() = false;
			}
*/
/*
			if( AStatus & SX126XConst::SX126X_IRQ_CAD_DETECTED )
			{
#ifdef ___MITOV_LORA_SX126X_DEBUG__
				Serial0.println( "SX126X_IRQ_CAD_DETECTED" );
#endif
				ClearIrqStatus( SX126XConst::SX126X_IRQ_CAD_DETECTED );
				Packet().LoRa().Detected().ChannelActivityOutputPin().SetPinValue( true );
			}
*/
			if( AStatus & SX126XConst::SX126X_IRQ_HEADER_VALID )
			{
#ifdef ___MITOV_LORA_SX126X_DEBUG__
				Serial0.println( "SX126X_IRQ_HEADER_VALID" );
#endif
				ClearIrqStatus( SX126XConst::SX126X_IRQ_HEADER_VALID );
				Packet().LoRa().Detected().ReceivedHeaderOutputPin().ClockPin();
			}

			if( AStatus & SX126XConst::SX126X_IRQ_SYNC_WORD_VALID )
			{
#ifdef ___MITOV_LORA_SX126X_DEBUG__
				Serial0.println( "SX126X_IRQ_SYNC_WORD_VALID" );
#endif
				ClearIrqStatus( SX126XConst::SX126X_IRQ_SYNC_WORD_VALID );
				Packet().FrequencyShiftKeying().Detected().SyncWordOutputPin().ClockPin();
			}

			if( AStatus & SX126XConst::SX126X_IRQ_PREAMBLE_DETECTED )
			{
#ifdef ___MITOV_LORA_SX126X_DEBUG__
				Serial0.println( "SX126X_IRQ_PREAMBLE_DETECTED" );
#endif
				ClearIrqStatus( SX126XConst::SX126X_IRQ_PREAMBLE_DETECTED );
				if( Packet().IsLoRa() )
					Packet().LoRa().Detected().PreambleOutputPin().ClockPin();

				else
					Packet().FrequencyShiftKeying().Detected().PreambleOutputPin().ClockPin();

			}

			if( AStatus & SX126XConst::SX126X_IRQ_CRC_ERR )
			{
#ifdef ___MITOV_LORA_SX126X_DEBUG__
				Serial0.println( "SX126X_IRQ_CRC_ERR" );
#endif
				ClearIrqStatus( SX126XConst::SX126X_IRQ_CRC_ERR );
				DataCRCErrorOutputPin().ClockPin();
			}

			if( AStatus & SX126XConst::SX126X_IRQ_HEADER_ERR )
			{
#ifdef ___MITOV_LORA_SX126X_DEBUG__
				Serial0.println( "SX126X_IRQ_HEADER_ERR" );
#endif
				ClearIrqStatus( SX126XConst::SX126X_IRQ_HEADER_ERR );
				Packet().LoRa().HeaderCRCErrorOutputPin().ClockPin();
			}

			if( AStatus & SX126XConst::SX126X_IRQ_TX_DONE )
			{
#ifdef ___MITOV_LORA_SX126X_DEBUG__
				Serial0.println( "SX126X_IRQ_TX_DONE" );
#endif
				ClearIrqStatus( SX126XConst::SX126X_IRQ_TX_DONE );
				T_SendingOutputPin::SetPinValueLow();
//				SetRx();
//				FSending() = false;
			}

			if( AStatus & SX126XConst::SX126X_IRQ_RX_DONE )
			{
#ifdef ___MITOV_LORA_SX126X_DEBUG__
				Serial0.println( "SX126X_IRQ_RX_DONE" );
#endif

				uint8_t payloadLength;
				uint8_t rxStartBufferPointer;
				GetRxBufferStatus( payloadLength, rxStartBufferPointer );

//				Serial0.println( rxStartBufferPointer );
//				Serial0.println( payloadLength );
				if( payloadLength > 0 )
				{
					uint8_t *ABuffer = new uint8_t[ payloadLength ];

					readBuffer( ABuffer, payloadLength, rxStartBufferPointer );
					ClearIrqStatus( SX126XConst::SX126X_IRQ_RX_DONE );

//				Serial0.println( "Result:" );
//				for( int i = 0; i < payloadLength; ++ i )
//					Serial0.println( ABuffer[ i ], HEX );

					T_OutputPin::SetPinValue( Mitov::TDataBlock( payloadLength, ABuffer ));

					delete [] ABuffer;
				}
			}
/*
			if( AStatus & SX126XConst::SX126X_IRQ_CAD_DONE )
			{
#ifdef ___MITOV_LORA_SX126X_DEBUG__
				Serial0.println( "SX126X_IRQ_CAD_DONE" );
				ClearIrqStatus( SX126XConst::SX126X_IRQ_CAD_DONE );
				Packet().LoRa().Detected().ChannelActivityOutputPin().SetPinValue( false );
			}
*/
		}

	public:
		inline MitovLoRaSX126X()
		{
			T_2_INTERRUPT::SetValue( false );
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"
