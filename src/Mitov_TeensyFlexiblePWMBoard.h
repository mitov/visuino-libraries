////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>

namespace Mitov
{
//---------------------------------------------------------------------------
	template <
		typename T_PWMResolution
	> class TeensyFlexiblePWMBoard :
		public T_PWMResolution
	{
	public:
		_V_PROP_( PWMResolution )

	public:
		inline void Update_PWMResolution()
		{
			analogWriteResolution( PWMResolution().GetValue() );
		}

	public:
		inline void SystemInit()
		{
			Update_PWMResolution();
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_AutoConfig,
		typename T_InitialValue,
		typename T_IsAnalog,
		typename T_IsCombinedInOut,
		typename T_IsOpenDrain,
		typename T_IsOutput,
		typename T_IsPullDown,
		typename T_IsPullUp,
		typename T_IsRawInput,
		typename T_OutputPin,
		uint8_t C_PIN_NUMBER,
		typename T_PWMFrequency
	> class TeensyPWMBoardChannel :
		public T_AutoConfig,
		public T_InitialValue,
		public T_IsAnalog,
		public T_IsCombinedInOut,
		public T_IsOpenDrain,
		public T_IsOutput,
		public T_IsPullDown,
		public T_IsPullUp,
		public T_IsRawInput,
		public T_OutputPin,
		public T_PWMFrequency
	{
	public:
		_V_PROP_( PWMFrequency )

	public:
		_V_PIN_( OutputPin )

	public:
		_V_PROP_( IsAnalog )
		_V_PROP_( IsCombinedInOut )
		_V_PROP_( IsOpenDrain )
		_V_PROP_( IsPullUp )
		_V_PROP_( IsPullDown )
		_V_PROP_( IsRawInput )
		_V_PROP_( IsOutput )
		_V_PROP_( InitialValue )

	public:
		inline void Update_PWMFrequency()
		{
			analogWriteFrequency( C_PIN_NUMBER, PWMFrequency().GetValue() );
		}

	public:
        void UpdatePinDirections() // Used as Live Binding updater! Do not rename!
        {
//			if( IsRawInput() )
//				return;

            if( IsOutput() )
			{
				pinMode( C_PIN_NUMBER, ( IsOpenDrain().GetValue() ) ? OUTPUT_OPEN_DRAIN : OUTPUT );
				if( ! IsAnalog().GetValue() )
					Digital.Write( C_PIN_NUMBER, InitialValue().GetValue() );
			}

            else
				pinMode( C_PIN_NUMBER, ( IsPullUp().GetValue() ) ? INPUT_PULLUP : ( IsPullDown() ? INPUT_PULLDOWN : INPUT ) );
        }

	public:
		inline bool DigitalRead()
		{
			return Digital.Read( C_PIN_NUMBER );
		}

		inline bool GetIsConnected() { return true; }

	public:
		inline void SystemInit()
		{
			Update_PWMFrequency();
            UpdatePinDirections();
		}

		inline void SystemStart()
		{
//			if( IsRawInput() )
//				return;
            if( ! IsOutput().GetValue() )
				if( T_OutputPin::GetPinIsConnected() )
	    			T_OutputPin::SetPinValue( Digital.Read( C_PIN_NUMBER ), false );

		}

		inline void SystemLoopBegin()
        {
            if( ! IsOutput().GetValue() )
	    		T_OutputPin::SetPinValue( Digital.Read( C_PIN_NUMBER ), true );

        }

	public:
		inline void UpdatePinOutputValue()
		{
			Digital.Write( C_PIN_NUMBER, InitialValue().GetValue() );
		}

	public:
		inline void AnalogInputPin_o_Receive( void *_Data )
        {
            if( IsOutput().GetValue() && IsAnalog().GetValue() )
            {
                float AValue = *(float*)_Data;
                Analog.Write( C_PIN_NUMBER, AValue );
            }
        }

		inline void DigitalInputPin_o_Receive( void *_Data )
		{
			bool AValue = *(bool *)_Data;
			InitialValue() = AValue;
/*
			if( IsRawInput() )
			{
				Digital.Write( C_PIN_NUMBER, AValue );
//				*((int*)_Data) = C_PIN_NUMBER;
				return;
			}
*/
            if( ( IsCombinedInOut().GetValue() || IsOutput().GetValue() ) && (! IsAnalog().GetValue() ))
            {
				if( AValue )
				  if( IsCombinedInOut() )
					  pinMode( C_PIN_NUMBER, OUTPUT );

                Digital.Write( C_PIN_NUMBER, AValue );

				if( ! AValue )
					if( IsCombinedInOut() )
						pinMode( C_PIN_NUMBER, IsPullUp() ? INPUT_PULLUP : ( IsPullDown() ? INPUT_PULLDOWN : INPUT ) );

            }
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_AutoConfig,
		typename T_InitialValue,
		typename T_IsAnalog,
		typename T_IsCombinedInOut,
		typename T_IsOpenDrain,
		typename T_IsOutput,
		typename T_IsPullDown,
		typename T_IsPullUp,
		typename T_IsRawInput,
		typename T_IsTouch,
		typename T_OutputPin_Analog_,
		typename T_OutputPin_Digital_,
		uint8_t C_PIN_NUMBER,
		typename T_PWMFrequency
	> class TeensyPWMBoardDigitalAnalogTouchChannel :
		public T_AutoConfig,
		public T_InitialValue,
		public T_IsAnalog,
		public T_IsCombinedInOut,
		public T_IsOpenDrain,
		public T_IsOutput,
		public T_IsPullDown,
		public T_IsPullUp,
		public T_IsRawInput,
		public T_IsTouch,
		public T_OutputPin_Analog_,
		public T_OutputPin_Digital_,
		public T_PWMFrequency
	{
	public:
		_V_PROP_( PWMFrequency )

	public:
		_V_PIN_( OutputPin_Analog_ )
		_V_PIN_( OutputPin_Digital_ )

	public:
		_V_PROP_( IsAnalog )
		_V_PROP_( IsCombinedInOut )
		_V_PROP_( IsOpenDrain )
		_V_PROP_( IsPullUp )
		_V_PROP_( IsPullDown )
		_V_PROP_( IsRawInput )
		_V_PROP_( IsTouch )
		_V_PROP_( IsOutput )
		_V_PROP_( InitialValue )

	public:
		inline void Update_PWMFrequency()
		{
			analogWriteFrequency( C_PIN_NUMBER, PWMFrequency().GetValue() );
		}

	public:
        void UpdatePinDirections() // Used as Live Binding updater! Do not rename!
        {
//			if( IsRawInput() )
//				return;

            if( IsOutput() )
			{
				pinMode( C_PIN_NUMBER, ( IsOpenDrain().GetValue() ) ? OUTPUT_OPEN_DRAIN : OUTPUT );
				if( ! IsAnalog().GetValue() )
					Digital.Write( C_PIN_NUMBER, InitialValue().GetValue() );
			}

            else
				pinMode( C_PIN_NUMBER, ( IsPullUp().GetValue() ) ? INPUT_PULLUP : ( IsPullDown() ? INPUT_PULLDOWN : INPUT ) );
        }

	public:
		inline bool DigitalRead()
		{
			if( IsTouch() )
				return touchRead( C_PIN_NUMBER ) > VISUINO_ANALOG_IN_THRESHOLD;

			return Digital.Read( C_PIN_NUMBER );
		}

		inline bool GetIsConnected() { return true; }

	public:
		inline void SystemInit()
		{
			Update_PWMFrequency();
            UpdatePinDirections();
		}

		inline void SystemStart()
		{
//			if( IsRawInput() )
//				return;
            if( ! IsOutput().GetValue() )
			{
				if( T_OutputPin_Digital_::GetPinIsConnected() )
	    			T_OutputPin_Digital_::SetPinValue( Digital.Read( C_PIN_NUMBER ), false );

				if( T_OutputPin_Analog_::GetPinIsConnected() )
	    			T_OutputPin_Analog_::SetPinValue( Analog.Read( C_PIN_NUMBER ), false );

			}
		}

		inline void SystemLoopBegin()
        {
            if( ! IsOutput().GetValue() )
			{
				if( T_OutputPin_Digital_::GetPinIsConnected() )
	    			T_OutputPin_Digital_::SetPinValue( Digital.Read( C_PIN_NUMBER ), true );

				if( T_OutputPin_Analog_::GetPinIsConnected() )
	    			T_OutputPin_Analog_::SetPinValue( Analog.Read( C_PIN_NUMBER ), true );

			}
        }

	public:
		inline void UpdatePinOutputValue()
		{
			Digital.Write( C_PIN_NUMBER, InitialValue().GetValue() );
		}

	public:
		inline void AnalogInputPin_o_Receive( void *_Data )
        {
            if( IsOutput().GetValue() && IsAnalog().GetValue() )
            {
                float AValue = *(float*)_Data;
                Analog.Write( C_PIN_NUMBER, AValue );
            }
        }

		inline void DigitalInputPin_o_Receive( void *_Data )
		{
			bool AValue = *(bool *)_Data;
			InitialValue() = AValue;
/*
			if( IsRawInput() )
			{
				Digital.Write( C_PIN_NUMBER, AValue );
//				*((int*)_Data) = C_PIN_NUMBER;
				return;
			}
*/
            if( ( IsCombinedInOut().GetValue() || IsOutput().GetValue() ) && (! IsAnalog().GetValue() ))
            {
				if( AValue )
				  if( IsCombinedInOut() )
					  pinMode( C_PIN_NUMBER, OUTPUT );

                Digital.Write( C_PIN_NUMBER, AValue );

				if( ! AValue )
					if( IsCombinedInOut() )
						pinMode( C_PIN_NUMBER, IsPullUp() ? INPUT_PULLUP : ( IsPullDown() ? INPUT_PULLDOWN : INPUT ) );

            }
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_AutoConfig,
		typename T_InitialValue,
		typename T_IsAnalog,
		typename T_IsCombinedInOut,
		typename T_IsOpenDrain,
		typename T_IsOutput,
		typename T_IsPullDown,
		typename T_IsPullUp,
		typename T_IsRawInput,
		typename T_OutputPin_Analog_,
		typename T_OutputPin_Digital_,
		uint8_t C_PIN_NUMBER,
		typename T_PWMFrequency
	> class TeensyPWMBoardDigitalAnalogChannel :
		public T_AutoConfig,
		public T_InitialValue,
		public T_IsAnalog,
		public T_IsCombinedInOut,
		public T_IsOpenDrain,
		public T_IsOutput,
		public T_IsPullDown,
		public T_IsPullUp,
		public T_IsRawInput,
		public T_OutputPin_Analog_,
		public T_OutputPin_Digital_,
		public T_PWMFrequency
	{
	public:
		_V_PROP_( PWMFrequency )

	public:
		_V_PIN_( OutputPin_Analog_ )
		_V_PIN_( OutputPin_Digital_ )

	public:
		_V_PROP_( IsAnalog )
		_V_PROP_( IsCombinedInOut )
		_V_PROP_( IsOpenDrain )
		_V_PROP_( IsPullUp )
		_V_PROP_( IsPullDown )
		_V_PROP_( IsRawInput )
		_V_PROP_( IsOutput )
		_V_PROP_( InitialValue )

	public:
		inline void Update_PWMFrequency()
		{
			analogWriteFrequency( C_PIN_NUMBER, PWMFrequency().GetValue() );
		}

	public:
        void UpdatePinDirections() // Used as Live Binding updater! Do not rename!
        {
//			if( IsRawInput() )
//				return;

            if( IsOutput() )
			{
				pinMode( C_PIN_NUMBER, ( IsOpenDrain().GetValue() ) ? OUTPUT_OPEN_DRAIN : OUTPUT );
				if( ! IsAnalog().GetValue() )
					Digital.Write( C_PIN_NUMBER, InitialValue().GetValue() );
			}

            else
				pinMode( C_PIN_NUMBER, ( IsPullUp().GetValue() ) ? INPUT_PULLUP : ( IsPullDown() ? INPUT_PULLDOWN : INPUT ) );
        }

	public:
		inline bool DigitalRead()
		{
			return Digital.Read( C_PIN_NUMBER );
		}

		inline bool GetIsConnected() { return true; }

	public:
		inline void SystemInit()
		{
			Update_PWMFrequency();
            UpdatePinDirections();
		}

		inline void SystemStart()
		{
//			if( IsRawInput() )
//				return;
            if( ! IsOutput().GetValue() )
			{
				if( T_OutputPin_Digital_::GetPinIsConnected() )
	    			T_OutputPin_Digital_::SetPinValue( Digital.Read( C_PIN_NUMBER ), false );

				if( T_OutputPin_Analog_::GetPinIsConnected() )
	    			T_OutputPin_Analog_::SetPinValue( Analog.Read( C_PIN_NUMBER ), false );

			}
		}

		inline void SystemLoopBegin()
        {
            if( ! IsOutput().GetValue() )
			{
				if( T_OutputPin_Digital_::GetPinIsConnected() )
	    			T_OutputPin_Digital_::SetPinValue( Digital.Read( C_PIN_NUMBER ), true );

				if( T_OutputPin_Analog_::GetPinIsConnected() )
	    			T_OutputPin_Analog_::SetPinValue( Analog.Read( C_PIN_NUMBER ), true );

			}
        }

	public:
		inline void UpdatePinOutputValue()
		{
			Digital.Write( C_PIN_NUMBER, InitialValue().GetValue() );
		}

	public:
		inline void AnalogInputPin_o_Receive( void *_Data )
        {
            if( IsOutput().GetValue() && IsAnalog().GetValue() )
            {
                float AValue = *(float*)_Data;
                Analog.Write( C_PIN_NUMBER, AValue );
            }
        }

		inline void DigitalInputPin_o_Receive( void *_Data )
		{
			bool AValue = *(bool *)_Data;
			InitialValue() = AValue;
/*
			if( IsRawInput() )
			{
				Digital.Write( C_PIN_NUMBER, AValue );
//				*((int*)_Data) = C_PIN_NUMBER;
				return;
			}
*/
            if( ( IsCombinedInOut().GetValue() || IsOutput().GetValue() ) && (! IsAnalog().GetValue() ))
            {
				if( AValue )
				  if( IsCombinedInOut() )
					  pinMode( C_PIN_NUMBER, OUTPUT );

                Digital.Write( C_PIN_NUMBER, AValue );

				if( ! AValue )
					if( IsCombinedInOut() )
						pinMode( C_PIN_NUMBER, IsPullUp() ? INPUT_PULLUP : ( IsPullDown() ? INPUT_PULLDOWN : INPUT ) );

            }
		}

	};
//---------------------------------------------------------------------------
}

