////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov_Arduino_SPI.h>

//---------------------------------------------------------------------------
namespace Mitov
{
//---------------------------------------------------------------------------
	template <
		SPIClass &C_SPI,
		typename T_MISO,
		typename T_MOSI,
		typename T_SCK
	> class Arduino_ESP32_SPI :
		public ArduinoSPI<C_SPI>,
		public T_MISO,
		public T_MOSI,
		public T_SCK
	{
	public:
		_V_PROP_( MISO )
		_V_PROP_( MOSI )
		_V_PROP_( SCK )

	public:
		inline void SystemInit()
		{
#ifndef __BORLANDC__
  #ifdef VISUINO_MILK_V
			C_SPI.begin( SCK(), MISO(), MOSI(), -1 );
  #else // VISUINO_MILK_V 
			C_SPI.begin( SCK(), MISO(), MOSI() );
  #endif // VISUINO_MILK_V 
#endif
		}

	};
//---------------------------------------------------------------------------
	template <
		SPIClass &C_SPI,
		int8_t C_AUTO_MISO,
		int8_t C_AUTO_MOSI,
		int8_t C_AUTO_SCK,
        typename T_AutoConfig,
		typename T_MISO,
		typename T_MOSI,
		typename T_SCK
	> class Arduino_ESP32_AutoConfig_SPI :
		public ArduinoSPI<C_SPI>,
        public T_AutoConfig,
		public T_MISO,
		public T_MOSI,
		public T_SCK
	{
	public:
		_V_PROP_( AutoConfig )
		_V_PROP_( MISO )
		_V_PROP_( MOSI )
		_V_PROP_( SCK )

	public:
		inline void SystemInit()
		{
#ifndef __BORLANDC__
			C_SPI.begin( 
				( AutoConfig().GetValue() && ( C_AUTO_SCK >= 0 )) ?
				C_AUTO_SCK :
				SCK(), 

				( AutoConfig().GetValue() && ( C_AUTO_MISO >= 0 )) ?
				C_AUTO_MISO :
				MISO(), 

				( AutoConfig().GetValue() && ( C_AUTO_MOSI >= 0 )) ?
				C_AUTO_MOSI :
				MOSI() 
			);
#endif
		}

	};
//---------------------------------------------------------------------------
} // Mitov
