////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
	template <
		typename T_SERIAL, T_SERIAL &C_SERIAL,
		typename T_DirectionalButtonsOutputPins_Up,
		typename T_DirectionalButtonsOutputPins_Down,
		typename T_DirectionalButtonsOutputPins_Left,
		typename T_DirectionalButtonsOutputPins_Right,
		typename T_Enabled,
		typename T_ErrorOutputPin,
		typename T_FSmallMotor,
		typename T_FLargeMotor,
		typename T_LeftFrontButtonsOutputPins_L1,
		typename T_LeftFrontButtonsOutputPins_L2,
		typename T_LeftStickOutputPins_X,
		typename T_LeftStickOutputPins_Y,
		typename T_LeftStickOutputPins_Button,
		typename T_RightFrontButtonsOutputPins_R1,
		typename T_RightFrontButtonsOutputPins_R2,
		typename T_RightStickOutputPins_X,
		typename T_RightStickOutputPins_Y,
		typename T_RightStickOutputPins_Button,
		typename T_SelectButtonOutputPin,
		typename T_ShapeColorButtonsOutputPins_Green_Triangle,
		typename T_ShapeColorButtonsOutputPins_Blue_Cross,
		typename T_ShapeColorButtonsOutputPins_Pink_Square,
		typename T_ShapeColorButtonsOutputPins_Red_Circle,
		typename T_StartButtonOutputPin
	> class PS2ControllerSerial :
		public T_DirectionalButtonsOutputPins_Up,
		public T_DirectionalButtonsOutputPins_Down,
		public T_DirectionalButtonsOutputPins_Left,
		public T_DirectionalButtonsOutputPins_Right,
		public T_Enabled,
		public T_ErrorOutputPin,
		public T_FSmallMotor,
		public T_FLargeMotor,
		public T_LeftFrontButtonsOutputPins_L1,
		public T_LeftFrontButtonsOutputPins_L2,
		public T_LeftStickOutputPins_X,
		public T_LeftStickOutputPins_Y,
		public T_LeftStickOutputPins_Button,
		public T_RightFrontButtonsOutputPins_R1,
		public T_RightFrontButtonsOutputPins_R2,
		public T_RightStickOutputPins_X,
		public T_RightStickOutputPins_Y,
		public T_RightStickOutputPins_Button,
		public T_SelectButtonOutputPin,
		public T_ShapeColorButtonsOutputPins_Green_Triangle,
		public T_ShapeColorButtonsOutputPins_Blue_Cross,
		public T_ShapeColorButtonsOutputPins_Pink_Square,
		public T_ShapeColorButtonsOutputPins_Red_Circle,
		public T_StartButtonOutputPin
	{
	public:
		_V_PIN_( DirectionalButtonsOutputPins_Up )
		_V_PIN_( DirectionalButtonsOutputPins_Down )
		_V_PIN_( DirectionalButtonsOutputPins_Left )
		_V_PIN_( DirectionalButtonsOutputPins_Right )
		_V_PIN_( ErrorOutputPin )
		_V_PIN_( LeftFrontButtonsOutputPins_L1 )
		_V_PIN_( LeftFrontButtonsOutputPins_L2 )
		_V_PIN_( LeftStickOutputPins_X )
		_V_PIN_( LeftStickOutputPins_Y )
		_V_PIN_( LeftStickOutputPins_Button )
		_V_PIN_( RightFrontButtonsOutputPins_R1 )
		_V_PIN_( RightFrontButtonsOutputPins_R2 )
		_V_PIN_( RightStickOutputPins_X )
		_V_PIN_( RightStickOutputPins_Y )
		_V_PIN_( RightStickOutputPins_Button )
		_V_PIN_( SelectButtonOutputPin )
		_V_PIN_( ShapeColorButtonsOutputPins_Green_Triangle )
		_V_PIN_( ShapeColorButtonsOutputPins_Blue_Cross )
		_V_PIN_( ShapeColorButtonsOutputPins_Pink_Square )
		_V_PIN_( ShapeColorButtonsOutputPins_Red_Circle )
		_V_PIN_( StartButtonOutputPin )

	public:
		_V_PROP_( Enabled )

	protected:
		_V_PROP_( FSmallMotor )
		_V_PROP_( FLargeMotor );

	protected:
		enum 
		{
			// Digital button
			PS2_SELECT,
			PS2_JOYSTICK_LEFT,
			PS2_JOYSTICK_RIGHT,
			PS2_START,
			PS2_UP,
			PS2_RIGHT,
			PS2_DOWN,
			PS2_LEFT,
			PS2_LEFT_2,
			PS2_RIGHT_2,
			PS2_LEFT_1,
			PS2_RIGHT_1,
			PS2_TRIANGLE,
			PS2_CIRCLE,
			PS2_CROSS,
			PS2_SQUARE,
			// Analog button
			PS2_JOYSTICK_LEFT_X_AXIS,
			PS2_JOYSTICK_LEFT_Y_AXIS,
			PS2_JOYSTICK_RIGHT_X_AXIS,
			PS2_JOYSTICK_RIGHT_Y_AXIS,
			PS2_JOYSTICK_LEFT_UP,
			PS2_JOYSTICK_LEFT_DOWN,
			PS2_JOYSTICK_LEFT_LEFT,
			PS2_JOYSTICK_LEFT_RIGHT,
			PS2_JOYSTICK_RIGHT_UP,
			PS2_JOYSTICK_RIGHT_DOWN,
			PS2_JOYSTICK_RIGHT_LEFT,
			PS2_JOYSTICK_RIGHT_RIGHT,
			// Check connection status
			PS2_CONNECTION_STATUS,
			// Control motor vibrarion
			PS2_MOTOR_1,
			PS2_MOTOR_2,
			// Read all button
			PS2_BUTTON_JOYSTICK
		};

	public:
		inline void SmallVibrateMotorInputPin_o_Receive( void *_Data )
		{
			FSmallMotor() = *(bool *)_Data;
		}

		inline void LargeVibrateMotorInputPin_o_Receive( void *_Data )
		{
			FLargeMotor() = uint8_t( MitovConstrain( *(float *)_Data, 0.0f, 1.0f ) * 255 );
		}

	protected:
		void WriteByte( uint8_t AValue )
		{
			while( C_SERIAL.GetStream().available() > 0 )
				C_SERIAL.GetStream().read();

			C_SERIAL.GetStream().write( AValue );
			C_SERIAL.GetStream().flush(); 	//wait for all data transmitted
		}

		uint8_t ReadByte()
		{
			long waitcount = 0;

			while(true)
			{
				if( C_SERIAL.GetStream().available() > 0 )
				{
					uint8_t rec_data = C_SERIAL.GetStream().read();
//					SERIAL_ERR=false;
					return( rec_data );
				}

				waitcount++;
				if( waitcount > 50000 )
				{
//					SERIAL_ERR=true;
					return (0x7F);
				}

			}
		}

	public:
		inline bool ReadDigital( unsigned int AIndex )
		{
			WriteByte( AIndex );
			return ( ReadByte() == 0 );
		}

		inline float ReadAnalog( unsigned int AIndex )
		{
			WriteByte( AIndex );
			return float(ReadByte()) / 255.0;
//			return ((float)Fps2x.Analog( AIndex )) / 255;
		}

	public:
		inline void ClockInputPin_o_Receive( void *_Data )
		{
			ReadData( false );
		}

	protected:
		void ReadData( bool AChangeOnly )
		{
			if( ! Enabled() )
				return;

			WriteByte( 29 ); // Small Motor
			WriteByte( ( FSmallMotor().GetValue() ) ? 1 : 0 );
			WriteByte( 30 ); // Large Motor
			WriteByte( FLargeMotor().GetValue() );

			if( T_ErrorOutputPin::GetPinIsConnected() )
			{
				WriteByte( PS2_CONNECTION_STATUS );
				T_ErrorOutputPin::SetPinValue( ReadByte() != 1 );
			}

			if( T_DirectionalButtonsOutputPins_Up::GetPinIsConnected() )
				T_DirectionalButtonsOutputPins_Up::SetPinValue( ReadDigital( PS2_UP ) );

			if( T_DirectionalButtonsOutputPins_Down::GetPinIsConnected() )
				T_DirectionalButtonsOutputPins_Down::SetPinValue( ReadDigital( PS2_DOWN ) );

			if( T_DirectionalButtonsOutputPins_Left::GetPinIsConnected() )
				T_DirectionalButtonsOutputPins_Left::SetPinValue( ReadDigital( PS2_LEFT ) );

			if( T_DirectionalButtonsOutputPins_Right::GetPinIsConnected() )
				T_DirectionalButtonsOutputPins_Right::SetPinValue( ReadDigital( PS2_RIGHT ) );

			if( T_LeftFrontButtonsOutputPins_L1::GetPinIsConnected() )
				T_LeftFrontButtonsOutputPins_L1::SetPinValue( ReadDigital( PS2_LEFT_1 ) );

			if( T_LeftStickOutputPins_X::GetPinIsConnected() )
				T_LeftStickOutputPins_X::SetPinValue( ReadAnalog( PS2_JOYSTICK_LEFT_X_AXIS ) );

			if( T_LeftStickOutputPins_Y::GetPinIsConnected() )
				T_LeftStickOutputPins_Y::SetPinValue( 1.0f - ReadAnalog( PS2_JOYSTICK_LEFT_Y_AXIS ) );

			if( T_LeftStickOutputPins_Button::GetPinIsConnected() )
				T_LeftStickOutputPins_Button::SetPinValue( ReadDigital( PS2_JOYSTICK_LEFT ) );

			if( T_LeftFrontButtonsOutputPins_L2::GetPinIsConnected() )
				T_LeftFrontButtonsOutputPins_L2::SetPinValue( ReadDigital( PS2_LEFT_2 ) );

			if( T_RightFrontButtonsOutputPins_R1::GetPinIsConnected() )
				T_RightFrontButtonsOutputPins_R1::SetPinValue( ReadDigital( PS2_RIGHT_1 ) );

			if( T_RightFrontButtonsOutputPins_R2::GetPinIsConnected() )
				T_RightFrontButtonsOutputPins_R2::SetPinValue( ReadDigital( PS2_RIGHT_2 ) );

			if( T_RightStickOutputPins_X::GetPinIsConnected() )
				T_RightStickOutputPins_X::SetPinValue( ReadAnalog( PS2_JOYSTICK_RIGHT_X_AXIS ) );

			if( T_RightStickOutputPins_Y::GetPinIsConnected() )
				T_RightStickOutputPins_Y::SetPinValue( 1.0f - ReadAnalog( PS2_JOYSTICK_RIGHT_Y_AXIS ) );

			if( T_RightStickOutputPins_Button::GetPinIsConnected() )
				T_RightStickOutputPins_Button::SetPinValue( ReadDigital( PS2_JOYSTICK_RIGHT ) );

			if( T_SelectButtonOutputPin::GetPinIsConnected() )
				T_SelectButtonOutputPin::SetPinValue( ReadDigital( PS2_SELECT ) );

			if( T_ShapeColorButtonsOutputPins_Green_Triangle::GetPinIsConnected() )
				T_ShapeColorButtonsOutputPins_Green_Triangle::SetPinValue( ReadDigital( PS2_TRIANGLE ) );

			if( T_ShapeColorButtonsOutputPins_Blue_Cross::GetPinIsConnected() )
				T_ShapeColorButtonsOutputPins_Blue_Cross::SetPinValue( ReadDigital( PS2_CROSS ) );

			if( T_ShapeColorButtonsOutputPins_Pink_Square::GetPinIsConnected() )
				T_ShapeColorButtonsOutputPins_Pink_Square::SetPinValue( ReadDigital( PS2_SQUARE ) );

			if( T_ShapeColorButtonsOutputPins_Red_Circle::GetPinIsConnected() )
				T_ShapeColorButtonsOutputPins_Red_Circle::SetPinValue( ReadDigital( PS2_CIRCLE ) );

			if( T_StartButtonOutputPin::GetPinIsConnected() )
				T_StartButtonOutputPin::SetPinValue( ReadDigital( PS2_START ) );

		}

	public:
		inline void SystemStart()
		{
			ReadData( false );
		}

		inline void SystemLoopBegin()
		{
			ReadData( true );
		}

	public:
		inline PS2ControllerSerial()
		{
			FSmallMotor() = false;
			FLargeMotor() = 0;
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"