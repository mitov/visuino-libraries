////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov_M5_Stack_Display_ILI9341.h>
#include <Mitov_Core2_AXP192.h>
#include <Mitov_FocalTech_FT6336U.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
	template <
	    typename T_Enabled,
	    typename T_InactivityPeriod,
	    typename T_ReportPeriod
    > class TArduinoM5StackDisplayTouchMonitorMode :
	    public T_Enabled,
	    public T_InactivityPeriod,
	    public T_ReportPeriod
    {
	public:
		_V_PROP_( Enabled )
		_V_PROP_( InactivityPeriod )
		_V_PROP_( ReportPeriod )

    };
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"

