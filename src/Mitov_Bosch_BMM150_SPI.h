////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>
#include <Mitov_Arduino_Basic_SPI.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
	template <
		typename T_SPI, T_SPI &C_SPI,
		typename T_ChipSelectOutputPin,
		typename T_SPISpeed
	> class BoschBMM150_InterfaceSPISpeed :
		public InterfaceSPISpeed <
				T_SPI, C_SPI,
				T_ChipSelectOutputPin,
				T_SPISpeed
			>
	{
	public:
		inline void UpdateSampleRate( float AValue ) {}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"
