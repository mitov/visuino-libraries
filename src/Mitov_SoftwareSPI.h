////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>
#include <SPI.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
	template <
		typename T_MISO_PIN, T_MISO_PIN &C_MISO_PIN,
		typename T_MOSIOutputPin,
		typename T_ClockOutputPin,
		typename T_FIs_Inverted,
		typename T_FIs_LSBFIRST,
		typename T_FIs_Phase
	> class VisuinoSoftwareSPI :
		public T_MOSIOutputPin,
		public T_ClockOutputPin,
		public T_FIs_Inverted,
		public T_FIs_LSBFIRST,
		public T_FIs_Phase
	{
	public:
		_V_PIN_( MOSIOutputPin )
		_V_PIN_( ClockOutputPin )

	protected:
		_V_PROP_( FIs_Inverted )
		_V_PROP_( FIs_LSBFIRST )
		_V_PROP_( FIs_Phase )

	public:
		uint16_t transfer16( uint16_t data )
		{
//			shiftOut( MOSI_PIN_NUMBER, CLOCK_PIN_NUMBER, MSBFIRST, data >> 8 );
//			shiftOut( MOSI_PIN_NUMBER, CLOCK_PIN_NUMBER, MSBFIRST, data );

			if ( FIs_LSBFIRST() )
			{
				transfer( data );
				transfer( data >> 8 );
			}

			else
			{
				transfer( data >> 8 );
				transfer( data );
			}

			return 0;
		}

		uint8_t transfer( uint8_t data )
		{
//			shiftOut( MOSI_PIN_NUMBER, CLOCK_PIN_NUMBER, MSBFIRST, data );

			if( C_MISO_PIN.GetIsConnected() )
			{
				uint8_t value = 0;

				if( T_MOSIOutputPin::GetPinIsConnected() )
				{
					for( uint8_t i = 0; i < 8; ++ i )
					{
						T_ClockOutputPin::SetPinValue( FIs_Inverted() );

						if ( FIs_LSBFIRST() )
						{
							value |= C_MISO_PIN.DigitalRead() << i;
//							value |= digitalRead(dataPin) << i;

							T_MOSIOutputPin::SetPinValue( data & 1 );
							data >>= 1;
						}

						else
						{
							value |= C_MISO_PIN.DigitalRead() << (7 - i);

							T_MOSIOutputPin::SetPinValue( ( data & 128 ) != 0 );
							data <<= 1;
						}

						T_ClockOutputPin::SetPinValue( ! FIs_Inverted().GetValue() );
					}
				}

				else
				{
					for( uint8_t i = 0; i < 8; ++ i )
					{
						T_ClockOutputPin::SetPinValue( FIs_Inverted() );
	//					digitalWrite(clockPin, HIGH);
						if ( FIs_LSBFIRST() )
							value |= C_MISO_PIN.DigitalRead() << i;
//							value |= digitalRead(dataPin) << i;

						else
							value |= C_MISO_PIN.DigitalRead() << (7 - i);
//							value |= digitalRead(dataPin) << (7 - i);

						T_ClockOutputPin::SetPinValue( ! FIs_Inverted().GetValue() );
	//					digitalWrite(clockPin, LOW);
					}

					return value;
				}
			}

			else if( T_MOSIOutputPin::GetPinIsConnected() )
			{
					for( uint8_t i = 0; i < 8; ++ i )
					{
						T_ClockOutputPin::SetPinValue( FIs_Inverted() );
						if ( FIs_LSBFIRST() )
						{
							T_MOSIOutputPin::SetPinValue( data & 1 );
//							digitalWrite(dataPin, data & 1);
							data >>= 1;
						}
						else 
						{	
							T_MOSIOutputPin::SetPinValue( ( data & 128 ) != 0 );
//							digitalWrite(dataPin, (data & 128) != 0);
							data <<= 1;
						}
			
//						digitalWrite(clockPin, HIGH);
//						digitalWrite(clockPin, LOW);		
						T_ClockOutputPin::SetPinValue( ! FIs_Inverted().GetValue() );
					}
			}

			return 0;
		}

		void transfer( const void *buf, size_t count )
		{
			for( int i = 0; i < count; ++i )
				transfer( ((uint8_t *)buf)[ i ] );
//				shiftOut( MOSI_PIN_NUMBER, CLOCK_PIN_NUMBER, MSBFIRST, ((uint8_t *)buf)[ i ] );
		}

//		void beginTransaction(SPISettings settings)
		void beginTransaction( uint32_t clock = 4000000, uint8_t bitOrder = MSBFIRST, uint8_t dataMode = SPI_MODE0 )
		{
			FIs_LSBFIRST() = ( bitOrder == LSBFIRST );

			switch( dataMode )
			{
				case SPI_MODE0:
					FIs_Inverted() = false;
					FIs_Phase() = false;
					break;

				case SPI_MODE1:
					FIs_Inverted() = false;
					FIs_Phase() = true;
					break;

				case SPI_MODE2:
					FIs_Inverted() = true;
					FIs_Phase() = false;
					break;

				case SPI_MODE3:
					FIs_Inverted() = true;
					FIs_Phase() = true;
					break;

			}

			T_ClockOutputPin::SetPinValue( FIs_Phase() );
		}

		inline void endTransaction()
		{
			T_ClockOutputPin::SetPinValue( FIs_Phase() );
		}

		inline void setDataMode( uint8_t dataMode )
		{
		}

/*
		void setBitOrder( uint8_t bitOrder )
		{
		}
*/
		inline void setClockDivider( uint8_t clockDiv )
		{
		}

		inline void usingInterrupt( uint8_t interruptNumber )
		{
		}

	public:
		inline void SystemInit()
		{
//			pinMode( MOSI_PIN_NUMBER, OUTPUT );
//			pinMode( CLOCK_PIN_NUMBER, OUTPUT );
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"