////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>
#include <IPAddress.h>
#include <Mitov_Sockets.h>
#if defined( VISUINO_ESP8266 )
	#include <WiFiClientSecure.h>
#endif
//#include <Ethernet.h>

#include "Mitov_BuildChecks_Begin.h"

// #define __TCP_SERVER__DEBUG__
// #define __TCP_CLIENT__DEBUG__

namespace Mitov
{
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
		typename T_CLIENT,
		typename T_Certificate,
		typename T_ConnectedOutputPin,
		typename T_Enabled,
//		typename T_FConnecting,
		typename T_FIsConnected,
//		typename T_FRunning,
		typename T_Host,
		typename T_IPAddress,
		typename T_OutputPin,
		typename T_Port,
		typename T_PrivateKey,
		typename T_ReconnectDelay
	> class TCPSecureClientSocket :
		public T_Certificate,
		public T_ConnectedOutputPin,
		public T_Enabled,
//		public T_FConnecting,
		public T_FIsConnected,
//		public T_FRunning,
		public T_Host,
		public T_IPAddress,
		public T_OutputPin,
		public T_Port,
		public T_PrivateKey,
		public T_ReconnectDelay
	{
	public:
		_V_PIN_( OutputPin )
		_V_PIN_( ConnectedOutputPin )

	public:
		_V_PROP_( Port )
		_V_PROP_( Enabled )
		_V_PROP_( ReconnectDelay )

	protected:
//		_V_PROP_( FRunning )
//		_V_PROP_( FConnecting )
		_V_PROP_( FIsConnected )
//		bool			FResolved : 1;

	public:
		_V_PROP_( Host )
		_V_PROP_( IPAddress )

	public:
		_V_PROP_( Certificate )
		_V_PROP_( PrivateKey )

	protected:
		T_CLIENT FClient;
		unsigned long FLastConnectTime;

	public:
		void SetEnabled( bool AValue )
		{
            if( Enabled() == AValue )
                return;

//			Serial.println( "SetEnabled" );
            Enabled() = AValue;
			UpdateEnabled();
		}

		void UpdateEnabled()
		{
			if( IsEnabled() )
                ForceStartSocket();

			else
				StopSocket();

		}

	public:
		inline void SetRemotePort( uint32_t APort )
		{
		}

		inline void BeginPacket()
		{
		}

		inline void EndPacket()
		{
		}

/*
	public:
		void TryStartSocket( uint32_t AIndex, uint32_t ACurrentIndex )
		{
//			Serial.println( Enabled );
			if( AIndex == ACurrentIndex )
				if( Enabled )
					StartSocket();

		}
*/
	public:
		inline void FlushInputPin_o_Receive( void *_Data )
		{
			FClient.flush();
		}

	public:
		constexpr inline bool GetIsSecure() { return true; }

		inline void TryStartSocket() // For direct socket access components
		{
			if( Enabled() )
				ForceStartSocket();
		}

		void ForceStart() // For direct socket access components
		{
            FLastConnectTime = millis() + ReconnectDelay().GetValue();
			Enabled() = true;
			TryStartSocket();
		}

		void TryStartSocket( uint32_t AIndex, uint32_t ACurrentIndex )
		{

//			Serial.println( Enabled );
			if( AIndex == ACurrentIndex )
				TryStartSocket();

		}

		inline bool IsEnabled()
		{
			return Enabled() && C_OWNER.Enabled();
		}

		inline bool CanSend()
		{
/*
			Serial.print( "CanSend: Enabled() = " );
			Serial.print( Enabled().GetValue() );
			Serial.print( " C_OWNER.Enabled() = " );
			Serial.print( C_OWNER.Enabled().GetValue() );
			Serial.print( " FIsConnected() = " );
			Serial.print( FIsConnected().GetValue() );
			Serial.print( " C_OWNER.GetIsStarted() = " );
			Serial.print( C_OWNER.GetIsStarted() );
			Serial.println();
*/
			return Enabled() && C_OWNER.Enabled() && FIsConnected() && C_OWNER.GetIsStarted();
		}

		inline bool GetReadyToConnect()
		{
			return C_OWNER.Enabled() && C_OWNER.GetIsStarted();
		}

		inline Print &GetPrint()
		{
			return FClient;
		}

	public:
		inline void SystemInit()
		{
			T_ConnectedOutputPin::SetPinValue( false, false );
		}

	public:
		void DisconnectInputPin_o_Receive( void *_Data )
		{
			FClient.flush();
			FClient.stop();
			T_ConnectedOutputPin::SetPinValue( false, true );
			Enabled() = false;

//			Serial.println( "DoDisconnect" );
//			Serial.println( FClient.connected() );
		}

	public:
		void StopSocket()
		{
			if( ! FIsConnected() )
				return;

			FIsConnected() = false;

			FClient.flush();
			FClient.stop();
			T_ConnectedOutputPin::SetPinValue( false, true );
//			inherited::Enabled = false;
		}

	public:
		inline void SystemLoopBegin()
		{
			StartSocket();
			for( int i = 0; i < 20; ++i )
			{
				if( ! FClient.available() )
					break;

				unsigned char AByte = FClient.read();
				T_OutputPin::SendPinValue( Mitov::TDataBlock( 1, &AByte ));
//				OutputPin.SendValue( Mitov::TDataBlock( 1, &AByte ));
			}

			bool AIsConnected = FClient.connected();

			T_ConnectedOutputPin::SetPinValue( AIsConnected, true );

			if ( ! AIsConnected )
			{
				while ( FClient.available() )
				{
					unsigned char AByte = FClient.read();
					T_OutputPin::SendPinValue( Mitov::TDataBlock( 1, &AByte ));
				}

//				FClient.stop(); // Do we need this?
				T_ConnectedOutputPin::SetPinValue( false, true );

				if( FIsConnected() )
				{
					if( C_OWNER.Enabled() )
						Enabled() = false;

					FIsConnected() = false;
				}
			}
		}

	protected:
        void ForceStartSocket()
        {
            FLastConnectTime = millis() + ReconnectDelay().GetValue();
            StartSocket();
        }

		inline void StartSocket()
		{
			if( ! Enabled().GetValue() )
				return;

			if( ! C_OWNER.GetIsStarted() )
				return;

			if( FIsConnected() )
				return;

			if( millis() - FLastConnectTime < ReconnectDelay().GetValue() )
				return;

			if( Certificate().GetValue() != "" )
			{
#if defined( VISUINO_ESP8266 )
				FIsConnected() = FClient.setCertificate( (uint8_t*)Certificate().c_str(), Certificate().length() );
#else
				FIsConnected() = FClient.setCertificate( Certificate().c_str() );
#endif
				return;
			}

			if( PrivateKey != "" )
			{
#if defined( VISUINO_ESP8266 )
				FIsConnected() = FClient.setPrivateKey( (uint8_t*)PrivateKey().c_str(), PrivateKey().length() );
#else
				FIsConnected() = FClient.setPrivateKey( PrivateKey().c_str() );
#endif
				return;
			}

//			Serial.println( "StartSocket1" );
			if( FIsConnected() )
				return;

//			Serial.println( "StartSocket" );
			if( Host().length() )
				FIsConnected() = FClient.connect( Host().c_str(), Port() );

			else
			{
//				IPAddress.printTo( Serial );
				FIsConnected() = FClient.connect( GetIPAddressValue( IPAddress() ), Port() );
			}

            FLastConnectTime = millis();
		}

	public:
		inline TCPSecureClientSocket()
		{
			FLastConnectTime = millis() + ReconnectDelay().GetValue();
//			FRunning() = false;
//			FConnecting() = false;
			FIsConnected() = false;
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
#if defined( VISUINO_ESP8266 ) || defined( VISUINO_RPI_PICO )
		typename T_AllowSelfSigned,
#endif
		typename T_CACert,
		typename T_CLIENT,
#if ! ( defined( VISUINO_ESP8266 ) || defined( VISUINO_RPI_PICO ) )
		typename T_Certificate,
#endif
		typename T_ConnectedOutputPin,
		typename T_Enabled,
//		typename T_FConnecting,
		typename T_FIsConnected,
//		typename T_FRunning,
#if defined( VISUINO_ESP8266 ) || defined( VISUINO_RPI_PICO )
		typename T_Fingerprint,
#endif
		typename T_Host,
		typename T_IPAddress,
#if ! defined( SEEEDUINO_WIO_TERMINAL )
		typename T_Insecure,
#endif
		typename T_OutputPin,
		typename T_Port,
#if ! ( defined( VISUINO_ESP8266 ) || defined( VISUINO_RPI_PICO ) )
		typename T_PrivateKey,
#endif
		typename T_ReconnectDelay
	> class TCPCACertSecureClientSocket :
#if defined( VISUINO_ESP8266 ) || defined( VISUINO_RPI_PICO )
		public T_AllowSelfSigned,
#endif
		public T_CACert,
#if ! ( defined( VISUINO_ESP8266 ) || defined( VISUINO_RPI_PICO ) )
		public T_Certificate,
#endif
		public T_ConnectedOutputPin,
		public T_Enabled,
//		public T_FConnecting,
		public T_FIsConnected,
//		public T_FRunning,
#if defined( VISUINO_ESP8266 ) || defined( VISUINO_RPI_PICO )
		public T_Fingerprint,
#endif
		public T_Host,
		public T_IPAddress,
#if ! defined( SEEEDUINO_WIO_TERMINAL )
		public T_Insecure,
#endif
		public T_OutputPin,
		public T_Port,
#if ! ( defined( VISUINO_ESP8266 ) || defined( VISUINO_RPI_PICO ) )
		public T_PrivateKey,
#endif
		public T_ReconnectDelay
	{
	public:
		_V_PIN_( OutputPin )
		_V_PIN_( ConnectedOutputPin )

	public:
		_V_PROP_( Port )
		_V_PROP_( Enabled )
#if ! defined( SEEEDUINO_WIO_TERMINAL )
		_V_PROP_( Insecure )
#endif
		_V_PROP_( ReconnectDelay )

	protected:
//		_V_PROP_( FRunning )
//		_V_PROP_( FConnecting )
		_V_PROP_( FIsConnected )
//		bool			FResolved : 1;

	public:
		_V_PROP_( Host )
		_V_PROP_( IPAddress )

	public:
#if defined( VISUINO_ESP8266 ) || defined( VISUINO_RPI_PICO )
		_V_PROP_( AllowSelfSigned )
		_V_PROP_( Fingerprint )
#else
		_V_PROP_( Certificate )
		_V_PROP_( PrivateKey )
#endif

		_V_PROP_( CACert )

	protected:
		T_CLIENT FClient;
#if defined( VISUINO_ESP8266 ) || defined( VISUINO_RPI_PICO )
		X509List *FX509List = nullptr;
#endif
		unsigned long FLastConnectTime;

	public:
		void SetEnabled( bool AValue )
		{
            if( Enabled() == AValue )
                return;

//			Serial.println( "SetEnabled" );
            Enabled() = AValue;
			UpdateEnabled();
		}

		void UpdateEnabled()
		{
			if( IsEnabled() )
				ForceStartSocket();

			else
				StopSocket();

		}

	public:
		inline void SetRemotePort( uint32_t APort )
		{
		}

		inline void BeginPacket()
		{
		}

		inline void EndPacket()
		{
		}

/*
	public:
		void TryStartSocket( uint32_t AIndex, uint32_t ACurrentIndex )
		{
//			Serial.println( Enabled );
			if( AIndex == ACurrentIndex )
				if( Enabled )
					StartSocket();

		}
*/
		constexpr inline bool GetIsSecure() { return true; }

		inline void TryStartSocket() // For direct socket access components
		{
			if( Enabled() )
				ForceStartSocket();
		}

        void ForceStartSocket()
        {
//            Serial.println( "ForceStartSocket" );
            FLastConnectTime = millis() + ReconnectDelay().GetValue();
            StartSocket();
        }

		void ForceStart() // For direct socket access components
		{
//            Serial.println( "ForceStart" );
			Enabled() = true;
            FLastConnectTime = millis() + ReconnectDelay().GetValue();
			TryStartSocket();
		}

		void TryStartSocket( uint32_t AIndex, uint32_t ACurrentIndex )
		{
//			Serial.println( Enabled );
			if( AIndex == ACurrentIndex )
				TryStartSocket();

		}

	public:
		inline void FlushInputPin_o_Receive( void *_Data )
		{
			FClient.flush();
		}

	public:
		inline bool IsEnabled()
		{
			return Enabled() && C_OWNER.Enabled();
		}

		inline bool CanSend()
		{
/*
            static bool ALastEnabled = false;
            static bool ALastOwnerEnabled = false;
            static bool AFIsConnected = false;
            static bool AGetIsStarted = false;
            if( ( ALastEnabled != Enabled().GetValue() ) || ( ALastOwnerEnabled != C_OWNER.Enabled().GetValue() ) || ( AFIsConnected != FIsConnected().GetValue() ) || ( AGetIsStarted != C_OWNER.GetIsStarted() ))
            {
                Serial.print( "CanSend: Enabled() = " );
                Serial.print( Enabled().GetValue() );
                Serial.print( " C_OWNER.Enabled() = " );
                Serial.print( C_OWNER.Enabled().GetValue() );
                Serial.print( " FIsConnected() = " );
                Serial.print( FIsConnected().GetValue() );
                Serial.print( " C_OWNER.GetIsStarted() = " );
                Serial.print( C_OWNER.GetIsStarted() );
                Serial.println();
            }

            ALastEnabled = Enabled().GetValue();
            ALastOwnerEnabled = C_OWNER.Enabled().GetValue();
            AFIsConnected = FIsConnected().GetValue();
            AGetIsStarted = C_OWNER.GetIsStarted();
*/
/*
			Serial.print( "CanSend: Enabled() = " );
			Serial.print( Enabled().GetValue() );
			Serial.print( " C_OWNER.Enabled() = " );
			Serial.print( C_OWNER.Enabled().GetValue() );
			Serial.print( " FIsConnected() = " );
			Serial.print( FIsConnected().GetValue() );
			Serial.print( " C_OWNER.GetIsStarted() = " );
			Serial.print( C_OWNER.GetIsStarted() );
			Serial.println();
*/
			return Enabled() && C_OWNER.Enabled() && FIsConnected() && C_OWNER.GetIsStarted();
		}

		inline bool GetReadyToConnect()
		{
			return C_OWNER.Enabled() && C_OWNER.GetIsStarted();
		}

		inline Print &GetPrint()
		{
			return FClient;
		}

	public:
		inline void SystemInit()
		{
			T_ConnectedOutputPin::SetPinValue( false, false );
		}

	public:
		void DisconnectInputPin_o_Receive( void *_Data )
		{
			FClient.flush();
			FClient.stop();
			T_ConnectedOutputPin::SetPinValue( false, true );
			Enabled() = false;

//			Serial.println( "DoDisconnect" );
//			Serial.println( FClient.connected() );
		}

	public:
		void StopSocket()
		{
			if( ! FIsConnected() )
				return;

			FIsConnected() = false;

			FClient.flush();
			FClient.stop();
			T_ConnectedOutputPin::SetPinValue( false, true );
#if defined( VISUINO_ESP8266 ) || defined( VISUINO_RPI_PICO )
			if( FX509List )
			{
				delete FX509List;
				FX509List = nullptr;
			}
#endif
//			inherited::Enabled = false;
		}

	public:
		inline void SystemLoopBegin()
		{
			StartSocket();
			if ( FClient.available() )
			{
				unsigned char AByte = FClient.read();
				T_OutputPin::SendPinValue( Mitov::TDataBlock( 1, &AByte ));
//				OutputPin.SendValue( Mitov::TDataBlock( 1, &AByte ));
//				OutputPin.Notify( &AByte );
			}

			bool AIsConnected = FClient.connected();

			T_ConnectedOutputPin::SetPinValue( AIsConnected, true );
			if ( ! AIsConnected )
			{
				while ( FClient.available() )
				{
					unsigned char AByte = FClient.read();
					T_OutputPin::SendPinValue( Mitov::TDataBlock( 1, &AByte ));
				}

//				FClient.stop(); // Do we need this?
//				T_ConnectedOutputPin::SetPinValue( false, true );

				if( FIsConnected() )
				{
#if defined( VISUINO_ESP8266 ) || defined( VISUINO_RPI_PICO )
					if( FX509List )
					{
						delete FX509List;
						FX509List = nullptr;
					}
#endif

//					Serial.println( "DISCONNECT" );

					if( C_OWNER.Enabled() )
						Enabled() = false;

					FIsConnected() = false;
				}
			}
		}

	protected:
		inline void StartSocket()
		{
			if( ! Enabled().GetValue() )
				return;

//			Serial.println( "StartSocket 1" );
			if( ! C_OWNER.GetIsStarted() )
				return;

//			Serial.println( "StartSocket 2" );
			if( FIsConnected() )
				return;

//			Serial.println( "StartSocket 3" );
			if( millis() - FLastConnectTime < ReconnectDelay().GetValue() )
				return;

//			Serial.println( "StartSocket 4" );
			if( CACert().GetValue() != "" )
#if defined( VISUINO_ESP8266 ) || defined( VISUINO_RPI_PICO )
//				FClient.setCACert( (uint8_t *)CACert().c_str(), CACert().GetValue().length() );
				if( FX509List )
					delete FX509List;

				FX509List = new X509List( (char *)CACert().c_str() );
//				Serial.println( CACert().c_str() );
				FClient.setTrustAnchors( FX509List );
#else
				FClient.setCACert( (char *)CACert().c_str() );
#endif

#if defined( VISUINO_ESP8266 ) || defined( VISUINO_RPI_PICO )
			if( Fingerprint().Enabled() )
			{
//				if ( Fingerprint().Key().GetCount() )
//				{
//				uint8_t *ABuffer = new uint8_t[ 16 ];
//				memcpy_P( ABuffer, EncryptionKey, 16 );
//				spiBurstWrite( RH_RF69_REG_3E_AESKEY1, ABuffer, 16 );
//				delete [] ABuffer;
                uint8_t const *AKey = Fingerprint().Key().Allocate();

				uint8_t ALocalKey[ 20 ];
				memcpy( ALocalKey, AKey, 20 );
                Fingerprint().Key().Release( AKey );

				FClient.setFingerprint( ALocalKey );
//				}
			}

			if( AllowSelfSigned() )
				FClient.allowSelfSignedCerts();

#else
			if( Certificate().GetValue() != "" )
			{
//#if defined( VISUINO_ESP8266 )
////				FClient.setCertificate( (uint8_t*)Certificate().c_str(), Certificate().GetValue().length() );
//#else
//				FIsConnected() = FClient.setCertificate( Certificate().c_str() );
				FClient.setCertificate( Certificate().c_str() );
//#endif
			}

			if( PrivateKey().GetValue() != "" )
			{
//#if defined( VISUINO_ESP8266 )
//				FClient.setPrivateKey( (uint8_t*)Certificate().c_str(), Certificate().GetValue().length() );
//#else
//				FIsConnected() = FClient.setPrivateKey( PrivateKey().c_str() );
				FClient.setPrivateKey( PrivateKey().c_str() );
//#endif
			}
#endif

#if ! defined( SEEEDUINO_WIO_TERMINAL )
			if( Insecure() )
				FClient.setInsecure();

#if ! ( defined( VISUINO_ESP8266 ) || defined( VISUINO_RPI_PICO ) )
			else
				FClient._use_insecure = false;
#endif
#endif


//			Serial.println( "StartSocket1" );
//			if( FIsConnected() )
//				return;

//			Serial.println( "StartSocket" );
			if( Host().GetValue().length() )
				FIsConnected() = FClient.connect( Host().c_str(), Port() );

			else if( IPAddress().GetUInt32Value() != 0 )
			{
//				IPAddress.printTo( Serial );
				FIsConnected() = FClient.connect( GetIPAddressValue( IPAddress() ), Port() );
			}

            FLastConnectTime = millis();
		}

	public:
		inline TCPCACertSecureClientSocket()
		{
			FLastConnectTime = millis() + ReconnectDelay().GetValue();
//			FRunning() = false;
//			FConnecting() = false;
			FIsConnected() = false;
		}


	};
//---------------------------------------------------------------------------
	template <
		typename T_Enabled,
		typename T_Key,
		typename T_Key_ApplyValues,
		typename T_Key_GetValue
	> class TArduinoSecureSocketFingerprint :
		public T_Enabled,
		public T_Key
	{
	public:
		_V_PROP_( Enabled )
		_V_PROP_( Key )

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"
