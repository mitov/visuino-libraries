////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>
#include <mailbox.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
	class TArduinoBasicMilkV_MailboxesModule
	{
	public:
		inline void SystemInit()
		{
			mailbox_init( false );
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_Enabled,
		uint8_t C_INDEX,
		typename T_INSTANCE_ReceiveMessage,
		typename T_OutputPin
	> class TArduinoBasicMilkV_Mailbox :
		public T_Enabled,
		public T_OutputPin
	{
	public:
		_V_PIN_( OutputPin )

	public:
		_V_PROP_( Enabled )

	public:
		inline void UpdateEnabled()
		{
			if( ! T_OutputPin::GetPinIsConnected() )
				return;

			if( Enabled() )
			{
				mailbox_register( C_INDEX, ReceiveMessageStatic );
				mailbox_enable_receive( C_INDEX );
			}

			else
			{
				mailbox_disable_receive( C_INDEX );
				mailbox_unregister( C_INDEX );
			}

		}

	public:
		void ReceiveMessage( MailboxMsg &msg )
		{
			const uint8_t *AInBuffer = (const uint8_t *)msg.data;
			uint32_t ASize = Func::GetVariableSizeValue( AInBuffer );
			T_OutputPin::SendPinValue( Mitov::TDataBlock( ASize, AInBuffer ));
		}

	protected:
		static void ReceiveMessageStatic( MailboxMsg msg )
		{
			T_INSTANCE_ReceiveMessage::Call( msg );
		}

	public:
		size_t write( uint8_t AData )
		{
			return write( &AData, 1 );
		}

		size_t write( uint8_t *AData, uint32_t ASize )
		{
			if( ! Enabled() )
				return 0;

			uint8_t ANumBytes = Func::GetVariableSize( ASize );
			uint8_t *ABufferData = new uint8_t[ ANumBytes + ASize ];

			uint32_t ADataIndex = 0;

        	Func::GetAddVariableSizeValue( ABufferData, ADataIndex, ASize );

			memcpy( ABufferData + ANumBytes, AData, ASize );

			delete [] ABufferData;

/*
			if( ! T_BASE::GetTXCharacteristic() )
				return 0;

//			if( ! C_OWNER.IsConnected() )
//				return;

            uint32_t ASizeToSend = ASize;
			while( ASizeToSend )
			{
				uint8_t ASendSize = MitovMin<uint32_t>( ASizeToSend, 20 );
				T_BASE::WriteData( AData, ASendSize );
//				FTXCharacteristic->writeValue( AData, ASendSize );
//				FTXCharacteristic->setValue( AData, ASendSize );
//				FTXCharacteristic->notify();
				AData += ASendSize;
				ASizeToSend -= ASendSize;
			}

            return ASize;
*/
		}

		void Print( const Mitov::String AValue )
		{
			if( ! Enabled() )
				return;

			write( (uint8_t *)AValue.c_str(), AValue.length());
			write( (uint8_t *)"\r\n", 2 );
		}

		void Print( float AValue )
		{
			if( ! Enabled() )
				return;

			char AText[ __VISUINO_FLOAT_TO_STR_LEN__ ];
			dtostrf( AValue,  1, 2, AText );
			Print( Mitov::String( AText ));
		}

		void Print( int32_t AValue )
		{
			if( ! Enabled() )
				return;

			char AText[ 16 ];
			ltoa( AValue, AText, 10 );
			Print( Mitov::String( AText ));
		}

		void Print( uint32_t AValue )
		{
			if( ! Enabled() )
				return;

			char AText[ 16 ];
			ltoa( AValue, AText, 10 );
			Print( Mitov::String( AText ));
		}

		template <typename T> inline void PrintChar( T AValue )
		{
			write( AValue );
		}

		inline size_t Write( uint8_t *AData, uint32_t ASize )
		{
			return write( AData, ASize );
		}

	    inline size_t Write(uint8_t AByte)
		{
			return write( AByte );
		}

	public:
		inline void SystemInit()
		{
			UpdateEnabled();
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"
