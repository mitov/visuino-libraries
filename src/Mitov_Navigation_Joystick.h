////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
	template <
		uint16_t C_0_PIN_DOWN,
		uint16_t C_0_PIN_LEFT,
		uint16_t C_0_PIN_PRESS,
		uint16_t C_0_PIN_RIGHT,
		uint16_t C_0_PIN_UP,
		typename T_DirectionOutputPins_Up,
		typename T_DirectionOutputPins_Down,
		typename T_DirectionOutputPins_Left,
		typename T_DirectionOutputPins_Right,
		typename T_PressOutputPin
	> class TArduino_Navigation_Joystick_Module :
		public T_DirectionOutputPins_Up,
		public T_DirectionOutputPins_Down,
		public T_DirectionOutputPins_Left,
		public T_DirectionOutputPins_Right,
		public T_PressOutputPin
	{
	public:
		_V_PIN_( DirectionOutputPins_Up )
		_V_PIN_( DirectionOutputPins_Down )
		_V_PIN_( DirectionOutputPins_Left )
		_V_PIN_( DirectionOutputPins_Right )
		_V_PIN_( PressOutputPin )


	protected:
		void SendData()
		{
			T_DirectionOutputPins_Up::SetPinValue( ! Digital.Read( C_0_PIN_UP ) );
			T_DirectionOutputPins_Down::SetPinValue( ! Digital.Read( C_0_PIN_DOWN ) );
			T_DirectionOutputPins_Left::SetPinValue( ! Digital.Read( C_0_PIN_LEFT ) );
			T_DirectionOutputPins_Right::SetPinValue( ! Digital.Read( C_0_PIN_RIGHT ) );
			T_PressOutputPin::SetPinValue( ! Digital.Read( C_0_PIN_PRESS ) );
		}

	public:
		inline void SystemStart()
		{
			pinMode( C_0_PIN_UP, INPUT_PULLUP );
			pinMode( C_0_PIN_LEFT, INPUT_PULLUP );
			pinMode( C_0_PIN_RIGHT, INPUT_PULLUP );
			pinMode( C_0_PIN_DOWN, INPUT_PULLUP );
			pinMode( C_0_PIN_PRESS, INPUT_PULLUP );

			SendData();
		}

		inline void SystemLoopBegin()
		{
			SendData();
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"