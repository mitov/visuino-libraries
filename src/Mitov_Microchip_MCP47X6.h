////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>
#include <Wire.h> //I2C Arduino Library
#include <Mitov_Microchip_MCP4728.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
	namespace TArduinoMicrochipMCP47X6VoltageReference
	{
		enum TArduinoMicrochipMCP47X6VoltageReference 
		{
			VDD,
			VREF,
			VREF_Buffered
		};
	}
//---------------------------------------------------------------------------
/*
	class TArduinoMicrochipMCP4716Impl
	{
	public:
		template <typename T_I2C> static void Write( T_I2C &C_I2C, uint16_t AValue )
		{
			C_I2C.write( uint8_t( AValue >> 8 ));
			C_I2C.write( uint8_t( AValue & 0xF0 ));
		}
	};
//---------------------------------------------------------------------------
	class TArduinoMicrochipMCP4726Impl
	{
	public:
		template <typename T_I2C> static void Write( T_I2C &C_I2C, uint16_t AValue )
		{
			C_I2C.write( uint8_t( AValue >> 8 ));
			C_I2C.write( uint8_t( AValue & 0xF0 ));
		}
	};
*/
//---------------------------------------------------------------------------
	template <
		typename T_I2C, T_I2C &C_I2C,
		uint8_t C_MASK,
		typename T_Address,
		typename T_ClockInputPin_o_IsConnected,
		typename T_Enabled,
		typename T_Gain,
		typename T_InitialValue,
		typename T_PowerDown,
		typename T_VoltageReference,
		typename T_WriteToEEPROM
	> class Microchip_MCP47X6 :
		public T_ClockInputPin_o_IsConnected,
		public T_Address,
		public T_Enabled,
		public T_Gain,
		public T_InitialValue,
		public T_PowerDown,
		public T_VoltageReference,
		public T_WriteToEEPROM
	{
	public:
		_V_PROP_( Enabled )
		_V_PROP_( Address )
		_V_PROP_( Gain )
		_V_PROP_( InitialValue )
		_V_PROP_( PowerDown )
		_V_PROP_( VoltageReference )
		_V_PROP_( WriteToEEPROM )

	public:
		_V_PROP_( ClockInputPin_o_IsConnected )

	protected:
		static const uint8_t	CMD_WRITEDAC		= 0x40;  // Writes data to the DAC
		static const uint8_t	CMD_WRITEDACEEPROM  = 0x60;  // Writes data to the DAC and the EEPROM (persisting the assigned value after reset)

	public:
		inline void InputPin_o_Receive( void *_Data )
		{
			InitialValue() = *(float * )_Data;
			if( ! ClockInputPin_o_IsConnected() )
				UpdateValue();

		}

		void ClockInputPin_o_Receive( void *_Data )
		{
			UpdateValue();
		}

	public:
		void UpdateValue() // Used for property setters!
		{
			if( ! Enabled() )
				return;

			uint16_t AValue = MitovConstrain( InitialValue().GetValue(), 0.0f, 1.0f ) * 0xFFFF + 0.5;
			C_I2C.beginTransmission( uint8_t( Address().GetValue() ));

			uint8_t	ACommand;
  			if( WriteToEEPROM() )
				ACommand = CMD_WRITEDACEEPROM;

			else
				ACommand = CMD_WRITEDAC;

			if( Gain() )
				ACommand |= 1;

			if( PowerDown().Enabled() )
			{
				if( PowerDown().Resistor() >= 640 )
					ACommand |= 0b110;

				else if( PowerDown().Resistor() >= 125 )
					ACommand |= 0b100;

				else
					ACommand |= 0b010;
			}

			switch( VoltageReference().GetValue() )
			{
				case TArduinoMicrochipMCP47X6VoltageReference::VREF: ACommand |= 0b00010000; break;
				case TArduinoMicrochipMCP47X6VoltageReference::VREF_Buffered: ACommand |= 0b00011000; break;
			}

			C_I2C.write( ACommand );

//			Serial.println( ACommand, HEX );
//			Serial.println( CurrentValue );
//			Serial.println( AValue );
//			T_IMPLEMENTATION::Write( C_I2C, AValue );
			C_I2C.write( uint8_t( AValue >> 8 ));
//			C_I2C.write( uint8_t( AValue & C_MASK ));
			C_I2C.write( uint8_t( AValue ));
//			C_I2C.write( uint8_t( AValue & 0xF0 ));
//			C_I2C.write( uint8_t(( AValue >> 4 ) & 0xFF ));
//			C_I2C.write( uint8_t(( AValue << 4 ) & 0xF0 ));
			C_I2C.endTransmission();
		}

	public:
		inline void SystemStart()
		{
			UpdateValue();
		}

	};
}

#include "Mitov_BuildChecks_End.h"