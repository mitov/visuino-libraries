////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>
#include <Mitov_GPS.h>
#include <Mitov_UBlox_M8_GPS.h>

#include "Mitov_BuildChecks_Begin.h"

#ifndef BUFFER_LENGTH
	#define BUFFER_LENGTH 256
#endif

namespace Mitov
{
//---------------------------------------------------------------------------
	namespace UBlox_M8_DDC_Const
	{
		const uint8_t AVAILABLE = 0xFD; // HIGH
		const uint8_t AVAILABLE_LOW = 0xFE;
		const uint8_t DATA = 0xFF;
	};
//---------------------------------------------------------------------------
	template <
		typename T_I2C, T_I2C &C_I2C,
		typename T_Address,
		typename T_ResetOutputPin,
		typename T_SerialOutputPin
	> class UBlox_M8_GPS_I2C_Impl :
		public T_Address,
		public T_ResetOutputPin,
		public T_SerialOutputPin
	{
	public:
		_V_PIN_( ResetOutputPin )
		_V_PIN_( SerialOutputPin )

	public:
		_V_PROP_( Address )

	protected:
/*
		inline uint8_t ReadRegister8( uint8_t ARegister )
		{
			C_I2C.beginTransmission( uint8_t( Address().GetValue() ));
			C_I2C.write( ARegister );
		}
*/
		inline uint16_t ReadRequest( uint16_t ASize )
		{
			C_I2C.beginTransmission( uint8_t( Address().GetValue() ));
			C_I2C.write( UBlox_M8_DDC_Const::DATA );
			C_I2C.endTransmission(false);
			return C_I2C.requestFrom( uint8_t( Address().GetValue() ), _VISUINO_I2C_SIZE_( ASize ) );
		}

		inline uint16_t ReadAvailable()
		{
			C_I2C.beginTransmission( uint8_t( Address().GetValue() ));
			C_I2C.write( UBlox_M8_DDC_Const::AVAILABLE );
			C_I2C.endTransmission(false);
			if( C_I2C.requestFrom( uint8_t( Address().GetValue() ), _VISUINO_I2C_SIZE_( 2 ) ) != 2 )
				return 0;
  
			uint8_t AAvailableHigh = C_I2C.read();
			uint8_t AAvailableLow  = C_I2C.read();

			if ( AAvailableHigh == 0xff && AAvailableLow == 0xff ) 
				return 0;


			uint8_t AResult = (( uint16_t( AAvailableHigh ) << 8 ) | AAvailableLow );
			if( AResult > 255 )
				return 255;

			return AResult;
		}

	protected:
		void Reset()
		{
			if( ! T_ResetOutputPin::GetPinIsConnected() )
				return;

			T_ResetOutputPin::SetPinValueLow();
			delayMicroseconds( 2 );
			T_ResetOutputPin::SetPinValueHigh();
		}

		template<typename T> inline void Read( T *AInstance )
		{
			uint16_t ACount = ReadAvailable();
//			Serial.println( BUFFER_LENGTH );
			while( ACount )
			{
				uint8_t ARequestSize = MitovMin<uint16_t>( ACount, BUFFER_LENGTH );
                ACount -= ARequestSize;
				if( ReadRequest( ARequestSize ) != ARequestSize )
					break;

				for( uint16_t i = 0; i < ARequestSize; ++ i )
				{
					uint8_t AByte = C_I2C.read();
					if( T_SerialOutputPin::GetPinIsConnected() )
						T_SerialOutputPin::SetPinValue( Mitov::TDataBlock( 1, &AByte ) );

					AInstance->ProcessChar( AByte );
				}
			}

//			Serial.println( "TEST" );
		}

	public:
		inline void ResetInputPin_o_Receive( void *_Data )
		{	
			Reset();
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"
