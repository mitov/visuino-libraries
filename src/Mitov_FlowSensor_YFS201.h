////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
	template <
		typename T_Enabled,
		typename T_FFirstTime,
		typename T_FZeroed,
		typename T_MinFrequency,
		typename T_OutputPin
	> class FlowSensorYFS201 :
		public T_Enabled,
		public T_FFirstTime,
		public T_FZeroed,
		public T_MinFrequency,
		public T_OutputPin
	{
	public:
		_V_PIN_( OutputPin )

    public:
		_V_PROP_( MinFrequency )
        _V_PROP_( Enabled )

	protected:
		_V_PROP_( FFirstTime )
		_V_PROP_( FZeroed )
		unsigned long	FStartTime = 0;

	public:
		void InputPin_o_Receive( void *_Data )
		{
			if( FFirstTime() )
			{
				FFirstTime() = false;
				FStartTime = micros();
				return;
			}

            if( ! Enabled() )
				return;

			unsigned long ANow = micros();
			unsigned long APeriod = ANow - FStartTime;
			FStartTime = ANow;

			if( APeriod == 0 )
				APeriod = 1;

			float AFrequency =  1000000.0f / APeriod;
			float AFlowRate = ( AFrequency * 2.25); //Take counted pulses in the last second and multiply by 2.25mL
			AFlowRate *= 60;         //Convert seconds to minutes, giving you mL / Minute
			AFlowRate /= 1000;       //Convert mL to Liters, giving you Liters / Minute
			T_OutputPin::SetPinValue( AFlowRate );
			FZeroed() = false;
		}

	public:
		inline void SystemLoopBegin() // For compatibility with Interrupt version
		{
			if( ! Enabled() )
				return;

			if( FZeroed() )
				return;

			if( FFirstTime() )
			{
				T_OutputPin::SetPinValue( 0 );
				FZeroed() = true;
			}

			unsigned long ACurrentMicros = micros();
			unsigned long ATimeDiff = ( ACurrentMicros - FStartTime );
            unsigned long ASamplePeriod = ( 1000000 / MinFrequency() ) + 0.5;
//			if( ATimeDiff > 1000000 )
			if( ATimeDiff > ASamplePeriod )
			{
				float AFrequency =  1000000.0f / ATimeDiff;
				if( AFrequency < MinFrequency() )
				{
					T_OutputPin::SetPinValue( 0 );
					FZeroed() = true;
				}

				else
				{
					float AFlowRate = ( AFrequency * 2.25); //Take counted pulses in the last second and multiply by 2.25mL
					AFlowRate *= 60;         //Convert seconds to minutes, giving you mL / Minute
					AFlowRate /= 1000;       //Convert mL to Liters, giving you Liters / Minute
					T_OutputPin::SetPinValue( AFrequency );
				}

//				FZeroed() = true;
//				FStartTime = ACurrentMicros;
			}
		}

	public:
		inline FlowSensorYFS201()
		{
			FFirstTime() = true;
			FZeroed() = false;
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_Enabled,
		typename T_FFirstTime,
		typename T_FZeroed,
		typename T_MinFrequency,
		typename T_OutputPin
	> class FlowSensorYFS201_Interrupt : 
//		public OpenWire::InterruptObject,
		public T_Enabled,
		public T_FFirstTime,
		public T_FZeroed,
		public T_MinFrequency,
		public T_OutputPin
	{
    public:
		_V_PIN_( OutputPin )

    public:
		_V_PROP_( MinFrequency )
        _V_PROP_( Enabled )

	protected:
		_V_PROP_( FFirstTime )
		_V_PROP_( FZeroed )
		unsigned long	FLastTime = 0;
		uint32_t FCounter = 0;

	public:
		inline void __ICACHE_RAM_ATTR__ InterruptHandler( bool )
		{
			++ FCounter;
		}

	public:
		inline void SystemLoopBegin()
		{
			unsigned long ACurrentMicros = micros();
			if( ! Enabled() )
			{
				noInterrupts(); // Protect the FReceivedPackets from being corrupted
				FCounter = 0;
				interrupts();
				FLastTime = ACurrentMicros;
				return;
			}				

			unsigned long ATimeDiff = ( ACurrentMicros - FLastTime );

			noInterrupts(); // Protect the FReceivedPackets from being corrupted
			uint32_t ACount = FCounter;
			if( ( ! ACount ) && ( ATimeDiff < 100000 ) )
			{
				interrupts();
				return;
			}

			FCounter = 0;

			interrupts();

			if( FFirstTime() )
				if( ! ACount )
				{
					if( ! FZeroed() )
					{
						T_OutputPin::SetPinValue( 0 );
						FZeroed() = true;
					}

					return;
				}

//			Serial.print( "ACount = " );
//			Serial.println( ACount );

			if( ! ACount )
				ACount = 1;

			else
				FLastTime = ACurrentMicros;

			float AFrequency = ( float( ACount ) / ATimeDiff ) * 1000000;
			if( AFrequency < MinFrequency() )
			{
				if( ! FZeroed() )
				{
					T_OutputPin::SetPinValue( 0 );
					FZeroed() = true;
				}
			}

			else
			{
				FFirstTime() = false;
				FZeroed() = false;
				float AFlowRate = ( AFrequency * 2.25); //Take counted pulses in the last second and multiply by 2.25mL
				AFlowRate *= 60;         //Convert seconds to minutes, giving you mL / Minute
				AFlowRate /= 1000;       //Convert mL to Liters, giving you Liters / Minute
				T_OutputPin::SetPinValue( AFrequency );
			}

//			Serial.println( AFrequency );

//			if( ACount )
//				FLastTime = ACurrentMicros;
		}

	public:
		inline FlowSensorYFS201_Interrupt()
		{
			FFirstTime() = true;
			FZeroed() = false;
		}
	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"
