////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
	template <
		typename T_InFahrenheit
	> class TexasInstruments_LM35_Thermometer_Impl :
		public T_InFahrenheit
	{
	public:
		_V_PROP_( InFahrenheit )

	public:
		template<typename T> inline float Compute( T *AOwner, float AValue )
		{
    		AValue *= AOwner->GetVoltageReference() * 100; // Voltage Vonvert

    		if( InFahrenheit() )
    			AValue = AValue * ( 9.0 / 5.0 ) + 32.0;

			return AValue;
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"

