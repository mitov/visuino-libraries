﻿////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
	namespace MFRC522Const
	{
		// Page 0: Command and status
		//						  0x00			// reserved for future use
		constexpr uint8_t CommandReg				= 0x01;	// starts and stops command execution
		constexpr uint8_t ComIEnReg				= 0x02;	// enable and disable interrupt request control bits
		constexpr uint8_t DivIEnReg				= 0x03;	// enable and disable interrupt request control bits
		constexpr uint8_t ComIrqReg				= 0x04;	// interrupt request bits
		constexpr uint8_t DivIrqReg				= 0x05;	// interrupt request bits
		constexpr uint8_t ErrorReg				= 0x06;	// error bits showing the error status of the last command executed 
		constexpr uint8_t Status1Reg				= 0x07;	// communication status bits
		constexpr uint8_t Status2Reg				= 0x08;	// receiver and transmitter status bits
		constexpr uint8_t FIFODataReg				= 0x09;	// input and output of 64 byte FIFO buffer
		constexpr uint8_t FIFOLevelReg			= 0x0A;	// number of bytes stored in the FIFO buffer
		constexpr uint8_t WaterLevelReg			= 0x0B;	// level for FIFO underflow and overflow warning
		constexpr uint8_t ControlReg				= 0x0C;	// miscellaneous control registers
		constexpr uint8_t BitFramingReg			= 0x0D;	// adjustments for bit-oriented frames
		constexpr uint8_t CollReg					= 0x0E;	// bit position of the first bit-collision detected on the RF interface
		//						  0x0F			// reserved for future use
		
		// Page 1: Command
		// 						  0x10			// reserved for future use
		constexpr uint8_t ModeReg					= 0x11;	// defines general modes for transmitting and receiving 
		constexpr uint8_t TxModeReg				= 0x12;	// defines transmission data rate and framing
		constexpr uint8_t RxModeReg				= 0x13;	// defines reception data rate and framing
		constexpr uint8_t TxControlReg			= 0x14;	// controls the logical behavior of the antenna driver pins TX1 and TX2
		constexpr uint8_t TxASKReg				= 0x15;	// controls the setting of the transmission modulation
		constexpr uint8_t TxSelReg				= 0x16;	// selects the internal sources for the antenna driver
		constexpr uint8_t RxSelReg				= 0x17;	// selects internal receiver settings
		constexpr uint8_t RxThresholdReg			= 0x18;	// selects thresholds for the bit decoder
		constexpr uint8_t DemodReg				= 0x19;	// defines demodulator settings
		// 						  0x1A			// reserved for future use
		// 						  0x1B			// reserved for future use
		constexpr uint8_t MfTxReg					= 0x1C;	// controls some MIFARE communication transmit parameters
		constexpr uint8_t MfRxReg					= 0x1D;	// controls some MIFARE communication receive parameters
		// 						  0x1E			// reserved for future use
		constexpr uint8_t SerialSpeedReg			= 0x1F;	// selects the speed of the serial UART interface
		
		// Page 2: Configuration
		// 						  0x20			// reserved for future use
		constexpr uint8_t CRCResultRegH			= 0x21;	// shows the MSB and LSB values of the CRC calculation
		constexpr uint8_t CRCResultRegL			= 0x22;
		// 						  0x23			// reserved for future use
		constexpr uint8_t ModWidthReg				= 0x24;	// controls the ModWidth setting?
		// 						  0x25			// reserved for future use
		constexpr uint8_t RFCfgReg				= 0x26;	// configures the receiver gain
		constexpr uint8_t GsNReg					= 0x27;	// selects the conductance of the antenna driver pins TX1 and TX2 for modulation 
		constexpr uint8_t CWGsPReg				= 0x28;	// defines the conductance of the p-driver output during periods of no modulation
		constexpr uint8_t ModGsPReg				= 0x29;	// defines the conductance of the p-driver output during periods of modulation
		constexpr uint8_t TModeReg				= 0x2A;	// defines settings for the internal timer
		constexpr uint8_t TPrescalerReg			= 0x2B;	// the lower 8 bits of the TPrescaler value. The 4 high bits are in TModeReg.
		constexpr uint8_t TReloadRegH				= 0x2C;	// defines the 16-bit timer reload value
		constexpr uint8_t TReloadRegL				= 0x2D;
		constexpr uint8_t TCounterValueRegH		= 0x2E;	// shows the 16-bit timer value
		constexpr uint8_t TCounterValueRegL		= 0x2F;
		
		// Page 3: Test Registers
		// 						  0x30			// reserved for future use
		constexpr uint8_t TestSel1Reg				= 0x31;	// general test signal configuration
		constexpr uint8_t TestSel2Reg				= 0x32;	// general test signal configuration
		constexpr uint8_t TestPinEnReg			= 0x33;	// enables pin output driver on pins D1 to D7
		constexpr uint8_t TestPinValueReg			= 0x34;	// defines the values for D1 to D7 when it is used as an I/O bus
		constexpr uint8_t TestBusReg				= 0x35;	// shows the status of the internal test bus
		constexpr uint8_t AutoTestReg				= 0x36;	// controls the digital self-test
		constexpr uint8_t VersionReg				= 0x37;	// shows the software version
		constexpr uint8_t AnalogTestReg			= 0x38;	// controls the pins AUX1 and AUX2
		constexpr uint8_t TestDAC1Reg				= 0x39;	// defines the test value for TestDAC1
		constexpr uint8_t TestDAC2Reg				= 0x3A;	// defines the test value for TestDAC2
		constexpr uint8_t TestADCReg				= 0x3B;		// shows the value of ADC I and Q channels
		// 						  0x3C			// reserved for production tests
		// 						  0x3D			// reserved for production tests
		// 						  0x3E			// reserved for production tests
		// 						  0x3F			// reserved for production tests

	// MFRC522 commands. Described in chapter 10 of the datasheet.
		constexpr uint8_t PCD_Idle				= 0x00;		// no action, cancels current command execution
		constexpr uint8_t PCD_Mem				= 0x01;		// stores 25 bytes into the internal buffer
		constexpr uint8_t PCD_GenerateRandomID	= 0x02;		// generates a 10-byte random ID number
		constexpr uint8_t PCD_CalcCRC			= 0x03;		// activates the CRC coprocessor or performs a self-test
		constexpr uint8_t PCD_Transmit			= 0x04;		// transmits data from the FIFO buffer
		constexpr uint8_t PCD_NoCmdChange		= 0x07;		// no command change, can be used to modify the CommandReg register bits without affecting the command, for example, the PowerDown bit
		constexpr uint8_t PCD_Receive			= 0x08;		// activates the receiver circuits
		constexpr uint8_t PCD_Transceive 		= 0x0C;		// transmits data from FIFO buffer to antenna and automatically activates the receiver after transmission
		constexpr uint8_t PCD_MFAuthent 		= 0x0E;		// performs the MIFARE standard authentication as a reader
		constexpr uint8_t PCD_SoftReset			= 0x0F;		// resets the MFRC522
        
		constexpr uint8_t MF_ACK				= 0xA;		// The MIFARE Classic uses a 4 bit ACK/NAK. Any other value than 0xA is NAK.
        constexpr uint8_t MF_KEY_SIZE			= 6;			// A Mifare Crypto1 key is 6 bytes.

		// Size of the MFRC522 FIFO
		constexpr uint8_t FIFO_SIZE = 64;		// The FIFO is 64 bytes.

		// Return codes from the functions in this class. Remember to update GetStatusCodeName() if you add more.
		// last value set to 0xff, then compiler uses less ram, it seems some optimisations are triggered
		enum StatusCode : uint8_t {
			STATUS_OK				,	// Success
			STATUS_BUSY     		,   // Is Busy
			STATUS_ERROR			,	// Error in communication
			STATUS_COLLISION		,	// Collission detected
			STATUS_TIMEOUT			,	// Timeout in communication.
			STATUS_NO_ROOM			,	// A buffer is not big enough.
			STATUS_INTERNAL_ERROR	,	// Internal error in the code. Should not happen ;-)
			STATUS_INVALID			,	// Invalid argument.
			STATUS_CRC_WRONG		,	// The CRC_A does not match
			STATUS_NO_CARD  		,	// There is no card
			STATUS_MIFARE_NACK		= 0xff	// A MIFARE PICC responded with NAK.
		};

	// Commands sent to the PICC.
		enum PICC_Command : uint8_t
		{
			// The commands used by the PCD to manage communication with several PICCs (ISO 14443-3, Type A, section 6.4)
			PICC_CMD_REQA			= 0x26,		// REQuest command, Type A. Invites PICCs in state IDLE to go to READY and prepare for anticollision or selection. 7 bit frame.
			PICC_CMD_WUPA			= 0x52,		// Wake-UP command, Type A. Invites PICCs in state IDLE and HALT to go to READY(*) and prepare for anticollision or selection. 7 bit frame.
			PICC_CMD_CT				= 0x88,		// Cascade Tag. Not really a command, but used during anti collision.
			PICC_CMD_SEL_CL1		= 0x93,		// Anti collision/Select, Cascade Level 1
			PICC_CMD_SEL_CL2		= 0x95,		// Anti collision/Select, Cascade Level 2
			PICC_CMD_SEL_CL3		= 0x97,		// Anti collision/Select, Cascade Level 3
			PICC_CMD_HLTA			= 0x50,		// HaLT command, Type A. Instructs an ACTIVE PICC to go to state HALT.
			PICC_CMD_RATS           = 0xE0,     // Request command for Answer To Reset.
			// The commands used for MIFARE Classic (from http://www.mouser.com/ds/2/302/MF1S503x-89574.pdf, Section 9)
			// Use PCD_MFAuthent to authenticate access to a sector, then use these commands to read/write/modify the blocks on the sector.
			// The read/write commands can also be used for MIFARE Ultralight.
			PICC_CMD_MF_AUTH_KEY_A	= 0x60,		// Perform authentication with Key A
			PICC_CMD_MF_AUTH_KEY_B	= 0x61,		// Perform authentication with Key B
			PICC_CMD_MF_READ		= 0x30,		// Reads one 16 byte block from the authenticated sector of the PICC. Also used for MIFARE Ultralight.
			PICC_CMD_MF_WRITE		= 0xA0,		// Writes one 16 byte block to the authenticated sector of the PICC. Called "COMPATIBILITY WRITE" for MIFARE Ultralight.
			PICC_CMD_MF_DECREMENT	= 0xC0,		// Decrements the contents of a block and stores the result in the internal data register.
			PICC_CMD_MF_INCREMENT	= 0xC1,		// Increments the contents of a block and stores the result in the internal data register.
			PICC_CMD_MF_RESTORE		= 0xC2,		// Reads the contents of a block into the internal data register.
			PICC_CMD_MF_TRANSFER	= 0xB0,		// Writes the contents of the internal data register to a block.
			// The commands used for MIFARE Ultralight (from http://www.nxp.com/documents/data_sheet/MF0ICU1.pdf, Section 8.6)
			// The PICC_CMD_MF_READ and PICC_CMD_MF_WRITE can also be used for MIFARE Ultralight.
			PICC_CMD_UL_WRITE		= 0xA2		// Writes one 4 byte page to the PICC.
		};

		// A struct used for passing the UID of a PICC.
		struct Uid
		{
			uint8_t		size;			// Number of bytes in the UID. 4, 7 or 10.
			uint8_t		uidByte[10];
			uint8_t		sak;			// The SAK (Select acknowledge) byte returned from the PICC after successful selection.
		};

		// ISO/IEC 14443-4 bit rates
		enum TagBitRates : uint8_t
		{
			BITRATE_106KBITS = 0x00,
			BITRATE_212KBITS = 0x01,
			BITRATE_424KBITS = 0x02,
			BITRATE_848KBITS = 0x03
		};

		// Structure to store ISO/IEC 14443-4 ATS
		struct Ats
		{
			uint8_t size;
			uint8_t fsc;                 // Frame size for proximity card

			struct 
			{
				bool transmitted;
				bool        sameD;	// Only the same D for both directions supported
				TagBitRates ds;		// Send D
				TagBitRates dr;		// Receive D
			} ta1;

			struct 
			{
				bool transmitted;
				uint8_t fwi;			// Frame waiting time integer
				uint8_t sfgi;			// Start-up frame guard time integer
			} tb1;

			struct 
			{
				bool transmitted;
				bool supportsCID;
				bool supportsNAD;
			} tc1;

			// Raw data from ATS
			uint8_t data[ FIFO_SIZE - 2 ]; // ATS cannot be bigger than FSD - 2 bytes (CRC), according to ISO 14443-4 5.2.2
		};

	} // MFRC522Const
//---------------------------------------------------------------------------
	template <
		typename T_Inverted,
		typename T_OpenDrain
	> class TArduino_RFID_MFRC522_Interrupt :	
		public T_Inverted,
		public T_OpenDrain
	{
	public:
		_V_PROP_( Inverted )
		_V_PROP_( OpenDrain )

	};
//---------------------------------------------------------------------------
	namespace TArduino_RFID_MFRC522_Transmitter_ExternalOutputPin_Mode
	{
		enum TArduino_RFID_MFRC522_Transmitter_ExternalOutputPin_Mode
		{
			ThreeState,
			Low,
			High,
			Test,
			Modulation,
			TransmitterStream,
			ReceiverStream
		};
	}
//---------------------------------------------------------------------------
	template <
		typename T_Enabled,
		typename T_InvertedWhenDisabled,
		typename T_InvertedWhenEnabled
	> class TArduino_RFID_MFRC522_Transmitter_Pin :
		public T_Enabled,
		public T_InvertedWhenDisabled,
		public T_InvertedWhenEnabled
	{
	public:
		_V_PROP_( Enabled )
		_V_PROP_( InvertedWhenDisabled )
		_V_PROP_( InvertedWhenEnabled )

	};
//---------------------------------------------------------------------------
	template <
		typename T_Enabled,
		typename T_InvertedWhenDisabled,
		typename T_InvertedWhenEnabled,
		typename T_Unmodulated
	> class TArduino_RFID_MFRC522_Transmitter_Pin2 :
		public T_Enabled,
		public T_InvertedWhenDisabled,
		public T_InvertedWhenEnabled,
		public T_Unmodulated
	{
	public:
		_V_PROP_( Enabled )
		_V_PROP_( InvertedWhenDisabled )
		_V_PROP_( InvertedWhenEnabled )
		_V_PROP_( Unmodulated )

	};
//---------------------------------------------------------------------------
	template <
		typename T_Enabled,
		typename T_ExternalModulation,
		typename T_Pin1,
		typename T_Pin2,
		typename T_ThreeState
	> class TArduino_RFID_MFRC522_Transmitter_Pins :
		public T_Enabled,
		public T_ExternalModulation,
		public T_Pin1,
		public T_Pin2,
		public T_ThreeState
	{
	public:
		_V_PROP_( Enabled )
		_V_PROP_( ExternalModulation )
		_V_PROP_( Pin1 )
		_V_PROP_( Pin2 )
		_V_PROP_( ThreeState )

	};
//---------------------------------------------------------------------------
	template <
		typename T_AdditionalResponseTime,
		typename T_CRC,
		typename T_ExternalOutputPinMode,
		typename T_ForceAmplitudeShiftKeying,
		typename T_Inverted,
		typename T_InvertedExternalInput,
		typename T_MillerModulationWidth,
		typename T_Outputs,
		typename T_RemainActive,
		typename T_Speed
	> class TArduino_RFID_MFRC522_Transmitter :
		public T_AdditionalResponseTime,
		public T_CRC,
		public T_ExternalOutputPinMode,
		public T_ForceAmplitudeShiftKeying,
		public T_Inverted,
		public T_InvertedExternalInput,
		public T_MillerModulationWidth,
		public T_Outputs,
		public T_RemainActive,
		public T_Speed
	{
	public:
		_V_PROP_( AdditionalResponseTime )
		_V_PROP_( CRC )
		_V_PROP_( ExternalOutputPinMode )
		_V_PROP_( ForceAmplitudeShiftKeying )
		_V_PROP_( Inverted )
		_V_PROP_( InvertedExternalInput )
		_V_PROP_( MillerModulationWidth )
		_V_PROP_( Outputs )
		_V_PROP_( RemainActive )
		_V_PROP_( Speed )

	};
//---------------------------------------------------------------------------	
	template <
		typename T_ContinuousWave,
		typename T_Modulation
	> class TArduino_RFID_MFRC522_Receiver_ConductanceValue :
		public T_ContinuousWave,
		public T_Modulation
	{
	public:
		_V_PROP_( ContinuousWave )
		_V_PROP_( Modulation )

	};
//---------------------------------------------------------------------------
	template <
		typename T_AutoSelect,
		typename T_FreezeDuringCommunication,
		typename T_UseInPhaseChannel
	> class TArduino_RFID_MFRC522_Receiver_DecodeChannel :
		public T_AutoSelect,
		public T_FreezeDuringCommunication,
		public T_UseInPhaseChannel
	{
	public:
		_V_PROP_( AutoSelect )
		_V_PROP_( FreezeDuringCommunication )
		_V_PROP_( UseInPhaseChannel )

	};
//---------------------------------------------------------------------------
	template <
		typename T_Demodulate,
		typename T_Enabled
	> class TArduino_RFID_MFRC522_Receiver_ExternalInput :
		public T_Demodulate,
		public T_Enabled
	{
	public:
		_V_PROP_( Demodulate )
		_V_PROP_( Enabled )

	};
//---------------------------------------------------------------------------
	template <
		typename T_Collision,
		typename T_Decode
	> class TArduino_RFID_MFRC522_Receiver_MinimalLevels :
		public T_Collision,
		public T_Decode
	{
	public:
		_V_PROP_( Collision )
		_V_PROP_( Decode )

	};
//---------------------------------------------------------------------------
	template <
		typename T_Burst,
		typename T_DataReceiving
	> class TArduino_RFID_MFRC522_Receiver_PhaseLockedLoopTime :
		public T_Burst,
		public T_DataReceiving
	{
	public:
		_V_PROP_( Burst )
		_V_PROP_( DataReceiving )

	};
//---------------------------------------------------------------------------
	template <
		typename T_Negative,
		typename T_Positive
	> class TArduino_RFID_MFRC522_Receiver_ConductanceValues :
		public T_Negative,
		public T_Positive
	{
	public:
		_V_PROP_( Negative )
		_V_PROP_( Positive )

	};
//---------------------------------------------------------------------------
	template <
		typename T_AutoDeactivate,
		typename T_CRC,
		typename T_ConductanceValues,
		typename T_DecodeChannel,
		typename T_Enabled,
		typename T_ExternalInput,
		typename T_FrameGuardDelay,
		typename T_IgnoreErrors,
		typename T_MinimalSignalLevels,
		typename T_PhaseLockedLoopTime,
		typename T_PowerDown,
		typename T_Speed
	> class TArduino_RFID_MFRC522_Receiver :
		public T_AutoDeactivate,
		public T_CRC,
		public T_ConductanceValues,
		public T_DecodeChannel,
		public T_Enabled,
		public T_ExternalInput,
		public T_FrameGuardDelay,
		public T_IgnoreErrors,
		public T_MinimalSignalLevels,
		public T_PhaseLockedLoopTime,
		public T_PowerDown,
		public T_Speed
	{
	public:
		_V_PROP_( AutoDeactivate )
		_V_PROP_( CRC )
		_V_PROP_( ConductanceValues )
		_V_PROP_( DecodeChannel )
		_V_PROP_( Enabled )
		_V_PROP_( ExternalInput )
		_V_PROP_( FrameGuardDelay )
		_V_PROP_( IgnoreErrors )
		_V_PROP_( MinimalSignalLevels )
		_V_PROP_( PhaseLockedLoopTime )
		_V_PROP_( PowerDown )
		_V_PROP_( Speed )

	};
//---------------------------------------------------------------------------
	template <
		typename T_Enabled,
		typename T_Gain
	> class TArduino_RFID_MFRC522_Receiver_Antenna :
		public T_Enabled,
		public T_Gain
	{
	public:
		_V_PROP_( Enabled )
		_V_PROP_( Gain )

	};
//---------------------------------------------------------------------------
	template <
		typename T_DetectedOutputPin,
		typename T_IDOutputPin,
//		typename T_StrongModulationOutputPin,
		typename T_TypeOutputPin
	> class TArduino_RFID_MFRC522_Card :
		public T_DetectedOutputPin,
		public T_IDOutputPin,
//		public T_StrongModulationOutputPin,
		public T_TypeOutputPin
	{
	public:
		_V_PIN_( DetectedOutputPin )
		_V_PIN_( IDOutputPin )
//		_V_PIN_( StrongModulationOutputPin )
		_V_PIN_( TypeOutputPin )

	};
//---------------------------------------------------------------------------
/*
	template <
		typename T_KeyAOutputPin,
		typename T_KeyBOutputPin
	> class TArduino_RFID_MFRC522_Card_Read_Block_Keys :
		public T_KeyAOutputPin,
		public T_KeyBOutputPin
	{
	public:
		_V_PIN_( KeyAOutputPin )
		_V_PIN_( KeyBOutputPin )

	};
*/
//---------------------------------------------------------------------------
	template <
		typename T_Key,
		typename T_Key_ApplyValues,
		typename T_Key_GetValue,
		typename T_TryA,
		typename T_TryB
	> class TArduino_RFID_MFRC522_Card_AlternativeKey :
		public T_Key,
		public T_Key_ApplyValues,
		public T_Key_GetValue,
		public T_TryA,
		public T_TryB
	{
	public:
		_V_PROP_( Key )
		_V_PROP_( TryA )
		_V_PROP_( TryB )

	public:
		void CopyKey( uint8_t * AKey, bool & ATryA, bool & ATryB )
		{
			Key().CopyData( AKey, 0, 6 );
			ATryA = TryA();
			ATryB = TryB();
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_AlternativeKeys_CopyKey,
		uint8_t COUNT_AlternativeKeys,
		typename T_Key,
		typename T_Key_ApplyValues,
		typename T_Key_GetValue,
		typename T_TryA,
		typename T_TryB
	> class TArduino_RFID_MFRC522_Card_Key :
		public T_Key,
		public T_Key_ApplyValues,
		public T_Key_GetValue,
		public T_TryA,
		public T_TryB
	{
	public:
		_V_PROP_( Key )
		_V_PROP_( TryA )
		_V_PROP_( TryB )

	public:
		template <typename T_OWNER> MFRC522Const::StatusCode TryAuthenticate( T_OWNER &C_OWNER, uint8_t ABlockNumber, bool &AUsedKeyB, uint8_t AKeyIndex )
		{
			// Authenticate using key A

			bool AFailed = false;
			AUsedKeyB = false;
			if( TryA() || TryB() )
			{
				uint8_t AKey[ 6 ] = { 0 };
				Key().CopyData( AKey, 0, 6 );
				
				if( TryA() )
				{
					MFRC522Const::StatusCode status = C_OWNER.PCD_Authenticate( MFRC522Const::PICC_CMD_MF_AUTH_KEY_A, ABlockNumber, AKey, &(C_OWNER.FUid ));
					if( status == MFRC522Const::STATUS_OK )
					{
						AKeyIndex = 0;
						return MFRC522Const::STATUS_OK;
					}

					AFailed = true;
				}

				if( TryB() )
				{
					if( AFailed )
						if( ! C_OWNER.RestartCard())
							return MFRC522Const::STATUS_NO_CARD;

					MFRC522Const::StatusCode status = C_OWNER.PCD_Authenticate( MFRC522Const::PICC_CMD_MF_AUTH_KEY_B, ABlockNumber, AKey, &(C_OWNER.FUid ));
					if( status == MFRC522Const::STATUS_OK )
					{
						AKeyIndex = 0;
						AUsedKeyB = true;
						return MFRC522Const::STATUS_OK;
					}

					AFailed = true;
				}
			}

			for( int i = 0; i < COUNT_AlternativeKeys; ++ i )
			{
				uint8_t AKey[ 6 ] = { 0 };
				bool ATryA;
				bool ATryB;

				T_AlternativeKeys_CopyKey::Call( i, AKey, ATryA, ATryB );

				if( ATryA )
				{
					if( AFailed )
						if( ! C_OWNER.RestartCard())
							return MFRC522Const::STATUS_NO_CARD;

					MFRC522Const::StatusCode status = C_OWNER.PCD_Authenticate( MFRC522Const::PICC_CMD_MF_AUTH_KEY_A, ABlockNumber, AKey, &( C_OWNER.FUid ));
					if( status == MFRC522Const::STATUS_OK )
					{
						AKeyIndex = i + 1;
						return MFRC522Const::STATUS_OK;
					}

					AFailed = true;
				}

				if( ATryB )
				{
					if( AFailed )
						if( ! C_OWNER.RestartCard())
							return MFRC522Const::STATUS_NO_CARD;

					MFRC522Const::StatusCode status = C_OWNER.PCD_Authenticate( MFRC522Const::PICC_CMD_MF_AUTH_KEY_B, ABlockNumber, AKey, &(C_OWNER.FUid ));
					if( status == MFRC522Const::STATUS_OK )
					{
						AKeyIndex = i + 1;
						AUsedKeyB = true;
						return MFRC522Const::STATUS_OK;
					}

					AFailed = true;
				}
			}

			return MFRC522Const::STATUS_ERROR;
		}

        void CopyKeyData( uint8_t AKeyIndex, uint8_t *ADestination )
		{
			if( AKeyIndex > 0 )
			{
				bool ATryA;
				bool ATryB;

				T_AlternativeKeys_CopyKey::Call( AKeyIndex - 1, ADestination, ATryA, ATryB );
				return;
			}

			Key().CopyData( ADestination, 0, 6 );
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
		typename T_Enabled,
		typename T_ErrorOutputPin,
		typename T_Key,
		typename T_SuccessOutputPin,
		typename T_Value,
		typename T_Value_ApplyValues,
		typename T_Value_GetValue
	> class TArduino_RFID_MFRC522_Card_ChangeID_Operation :
		public T_Enabled,
		public T_ErrorOutputPin,
		public T_Key,
		public T_SuccessOutputPin,
		public T_Value
	{
	public:
		_V_PIN_( ErrorOutputPin )
		_V_PIN_( SuccessOutputPin )

	public:
		_V_PROP_( Enabled )
		_V_PROP_( Key )
		_V_PROP_( Value )

	public:
		inline void ClockInputPin_o_Receive( void *_Data )
		{
			if( ! ( Enabled().GetValue() && C_OWNER.GetIsEnebaled() ) )
				return;

			if( ! C_OWNER.GetIsPresent() )
			{
				T_ErrorOutputPin::SetPinValue( "No Card." );
				return;
			}

			bool AUsedKeyB;
			uint8_t AKeyIndex;
            MFRC522Const::StatusCode status = Key().TryAuthenticate( C_OWNER, 0, AUsedKeyB, AKeyIndex );
			if( status != MFRC522Const::STATUS_OK )
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}

			uint8_t buffer[ 18 ];
			uint8_t size = sizeof( buffer );

			status = C_OWNER.MIFARE_ReadCheckCRC( 0, buffer, &size );
//				Serial.println( status );
//				Serial.println( C_OWNER.GetStatusCodeName( status ));

			if( status != MFRC522Const::STATUS_OK )
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}

/*
			for( int i = 0; i < 16; ++ i )
			{
				Serial.print( ", " );
				Serial.print( buffer[ i ], HEX );
			}

			Serial.println();
*/
			Value().CopyData( buffer, 0, 4 );

			// Write new UID to the data we just read, and calculate BCC byte
			uint8_t bcc = 0;
			for (uint8_t i = 0; i < 4; i++) 
				bcc ^= buffer[ i ];
	
			// Write BCC byte to buffer
			buffer[ 4 ] = bcc;
	
			// Stop encrypted traffic so we can send raw bytes
			C_OWNER.PCD_StopCrypto1();

			// Activate UID backdoor
			if( ! C_OWNER.MIFARE_OpenUidBackdoor() )
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}

//			Serial.println( "TEST1" );
	
			// Write modified block 0 back to card
			status = C_OWNER.MIFARE_Write( 0, buffer, 16 );
			if (status != MFRC522Const::STATUS_OK)
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}
	
			// Wake the card up again
			C_OWNER.PICC_IsAnyCardPresent();
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_CanReadFlagsOutputPin,
		typename T_CanReadKeyBOutputPin,
		typename T_CanWriteFlagsOutputPin,
		typename T_CanWriteKeyAOutputPin,
		typename T_CanWriteKeyBOutputPin
	> class TArduino_RFID_MFRC522_Card_Block_Info_KeyA :
		public T_CanReadFlagsOutputPin,
		public T_CanReadKeyBOutputPin,
		public T_CanWriteFlagsOutputPin,
		public T_CanWriteKeyAOutputPin,
		public T_CanWriteKeyBOutputPin
	{
	public:
		_V_PIN_( CanReadFlagsOutputPin )
		_V_PIN_( CanReadKeyBOutputPin )
		_V_PIN_( CanWriteFlagsOutputPin )
		_V_PIN_( CanWriteKeyAOutputPin )
		_V_PIN_( CanWriteKeyBOutputPin )

	};
//---------------------------------------------------------------------------
	template <
		typename T_CanReadFlagsOutputPin,
		typename T_CanWriteFlagsOutputPin,
		typename T_CanWriteKeyAOutputPin,
		typename T_CanWriteKeyBOutputPin,
		typename T_OutputPin
	> class TArduino_RFID_MFRC522_Card_Block_Info_KeyB :
		public T_CanReadFlagsOutputPin,
		public T_CanWriteFlagsOutputPin,
		public T_CanWriteKeyAOutputPin,
		public T_CanWriteKeyBOutputPin,
		public T_OutputPin
	{
	public:
		_V_PIN_( CanReadFlagsOutputPin )
		_V_PIN_( CanWriteFlagsOutputPin )
		_V_PIN_( CanWriteKeyAOutputPin )
		_V_PIN_( CanWriteKeyBOutputPin )
		_V_PIN_( OutputPin )

	};
//---------------------------------------------------------------------------
	template <
		typename T_CanDecrementOutputPin,
		typename T_CanIncrementOutputPin,
		typename T_CanReadOutputPin,
		typename T_CanWriteOutputPin
	> class TArduino_RFID_MFRC522_Card_Block_Info_KeyA_Read :
		public T_CanDecrementOutputPin,
		public T_CanIncrementOutputPin,
		public T_CanReadOutputPin,
		public T_CanWriteOutputPin
	{
	public:
		_V_PIN_( CanDecrementOutputPin )
		_V_PIN_( CanIncrementOutputPin )
		_V_PIN_( CanReadOutputPin )
		_V_PIN_( CanWriteOutputPin )

	};
//---------------------------------------------------------------------------
	template <
		typename T_CanDecrementOutputPin,
		typename T_CanIncrementOutputPin,
		typename T_CanReadOutputPin,
		typename T_CanWriteOutputPin,
		typename T_OutputPin
	> class TArduino_RFID_MFRC522_Card_Block_Info_KeyB_Read :
		public T_CanDecrementOutputPin,
		public T_CanIncrementOutputPin,
		public T_CanReadOutputPin,
		public T_CanWriteOutputPin,
		public T_OutputPin
	{
	public:
		_V_PIN_( CanDecrementOutputPin )
		_V_PIN_( CanIncrementOutputPin )
		_V_PIN_( CanReadOutputPin )
		_V_PIN_( CanWriteOutputPin )
		_V_PIN_( OutputPin )

	};
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
		typename T_Enabled,
		typename T_ErrorOutputPin,
		typename T_KeyA,
		typename T_KeyB,
		typename T_SuccessOutputPin,
		typename T_UserDataOutputPin
	> class TArduino_RFID_MFRC522_Card_Block_Info_Operation :
		public T_Enabled,
		public T_ErrorOutputPin,
		public T_KeyA,
		public T_KeyB,
		public T_SuccessOutputPin,
		public T_UserDataOutputPin
	{
	public:
		_V_PIN_( ErrorOutputPin )
		_V_PIN_( SuccessOutputPin )
		_V_PIN_( UserDataOutputPin )

	public:
		_V_PROP_( Enabled )
		_V_PROP_( KeyA )
		_V_PROP_( KeyB )

	public:
		inline void ClockInputPin_o_Receive( void *_Data )
		{
			if( ! ( Enabled().GetValue() && C_OWNER.GetIsEnebaled() ) )
				return;

			if( ! C_OWNER.GetIsPresent() )
			{
				T_ErrorOutputPin::SetPinValue( "No Card." );
				return;
			}

			bool AUsedKeyB;
			uint8_t AKeyIndex;
			MFRC522Const::StatusCode status = C_OWNER.TryAuthenticate( C_OWNER.GetSectorNumber() * 4 + 3, AUsedKeyB, AKeyIndex );
			if( status != MFRC522Const::STATUS_OK )
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}

			uint8_t buffer[ 18 ];
			uint8_t size = sizeof( buffer );

//				Serial.println( "READ" );
			status = C_OWNER.GetRFID().MIFARE_ReadCheckCRC( C_OWNER.GetSectorNumber() * 4 + 3, buffer, &size );
//				Serial.println( status );
//				Serial.println( C_OWNER.GetStatusCodeName( status ));

			if( status != MFRC522Const::STATUS_OK )
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}

			uint8_t c1  = buffer[7] >> 4;
			uint8_t c2  = buffer[8] & 0xF;
			uint8_t c3  = buffer[8] >> 4;
			uint8_t c1_ = buffer[6] & 0xF;
			uint8_t c2_ = buffer[6] >> 4;
			uint8_t c3_ = buffer[7] & 0xF;

			if( (c1 != (~c1_ & 0xF)) || (c2 != (~c2_ & 0xF)) || (c3 != (~c3_ & 0xF)) )
			{
				T_ErrorOutputPin::SetPinValue( "Invalid Flags." ); // Invert Error!
				return;
			}

			uint8_t g;

			switch( C_OWNER.GetBlockNumber() )
			{ 
				case 0: g = ((c1 & 1) << 2) | ((c2 & 1) << 1) | ((c3 & 1) << 0); break;
				case 1: g = ((c1 & 2) << 1) | ((c2 & 2) << 0) | ((c3 & 2) >> 1); break;
				default: g = ((c1 & 4) << 0) | ((c2 & 4) >> 1) | ((c3 & 4) >> 2); break;
			}

			switch( g )
			{
				case 0b000:
				{
					KeyA().CanIncrementOutputPin().SetPinValueHigh();
					KeyB().CanIncrementOutputPin().SetPinValueHigh();
					KeyA().CanDecrementOutputPin().SetPinValueHigh();
					KeyB().CanDecrementOutputPin().SetPinValueHigh();
					KeyA().CanReadOutputPin().SetPinValueHigh();
					KeyB().CanReadOutputPin().SetPinValueHigh();
					KeyA().CanWriteOutputPin().SetPinValueHigh();
					KeyB().CanWriteOutputPin().SetPinValueHigh();
					break;
				}

				case 0b001:
				{
					KeyA().CanIncrementOutputPin().SetPinValueLow();
					KeyB().CanIncrementOutputPin().SetPinValueLow();
					KeyA().CanDecrementOutputPin().SetPinValueHigh();
					KeyB().CanDecrementOutputPin().SetPinValueHigh();
					KeyA().CanReadOutputPin().SetPinValueHigh();
					KeyB().CanReadOutputPin().SetPinValueHigh();
					KeyA().CanWriteOutputPin().SetPinValueLow();
					KeyB().CanWriteOutputPin().SetPinValueLow();
					break;
				}

				case 0b010:
				{
					KeyA().CanIncrementOutputPin().SetPinValueLow();
					KeyB().CanIncrementOutputPin().SetPinValueLow();
					KeyA().CanDecrementOutputPin().SetPinValueLow();
					KeyB().CanDecrementOutputPin().SetPinValueLow();
					KeyA().CanReadOutputPin().SetPinValueHigh();
					KeyB().CanReadOutputPin().SetPinValueHigh();
					KeyA().CanWriteOutputPin().SetPinValueLow();
					KeyB().CanWriteOutputPin().SetPinValueLow();
					break;
				}

				case 0b011:
				{
					KeyA().CanIncrementOutputPin().SetPinValueLow();
					KeyB().CanIncrementOutputPin().SetPinValueLow();
					KeyA().CanDecrementOutputPin().SetPinValueLow();
					KeyB().CanDecrementOutputPin().SetPinValueLow();
					KeyA().CanReadOutputPin().SetPinValueHigh();
					KeyB().CanReadOutputPin().SetPinValueHigh();
					KeyA().CanWriteOutputPin().SetPinValueHigh();
					KeyB().CanWriteOutputPin().SetPinValueHigh();
					break;
				}

				case 0b100:
				{
					KeyA().CanIncrementOutputPin().SetPinValueLow();
					KeyB().CanIncrementOutputPin().SetPinValueLow();
					KeyA().CanDecrementOutputPin().SetPinValueLow();
					KeyB().CanDecrementOutputPin().SetPinValueLow();
					KeyA().CanReadOutputPin().SetPinValueHigh();
					KeyB().CanReadOutputPin().SetPinValueHigh();
					KeyA().CanWriteOutputPin().SetPinValueLow();
					KeyB().CanWriteOutputPin().SetPinValueHigh();
					break;
				}

				case 0b101:
				{
					KeyA().CanIncrementOutputPin().SetPinValueLow();
					KeyB().CanIncrementOutputPin().SetPinValueLow();
					KeyA().CanDecrementOutputPin().SetPinValueLow();
					KeyB().CanDecrementOutputPin().SetPinValueLow();
					KeyA().CanReadOutputPin().SetPinValueLow();
					KeyB().CanReadOutputPin().SetPinValueHigh();
					KeyA().CanWriteOutputPin().SetPinValueLow();
					KeyB().CanWriteOutputPin().SetPinValueLow();
					break;
				}

				case 0b110:
				{
					KeyA().CanIncrementOutputPin().SetPinValueLow();
					KeyB().CanIncrementOutputPin().SetPinValueHigh();
					KeyA().CanDecrementOutputPin().SetPinValueHigh();
					KeyB().CanDecrementOutputPin().SetPinValueHigh();
					KeyA().CanReadOutputPin().SetPinValueHigh();
					KeyB().CanReadOutputPin().SetPinValueHigh();
					KeyA().CanWriteOutputPin().SetPinValueLow();
					KeyB().CanWriteOutputPin().SetPinValueHigh();
					break;
				}

				case 0b111:
				{
					KeyA().CanIncrementOutputPin().SetPinValueLow();
					KeyB().CanIncrementOutputPin().SetPinValueLow();
					KeyA().CanDecrementOutputPin().SetPinValueLow();
					KeyB().CanDecrementOutputPin().SetPinValueLow();
					KeyA().CanReadOutputPin().SetPinValueLow();
					KeyB().CanReadOutputPin().SetPinValueLow();
					KeyA().CanWriteOutputPin().SetPinValueLow();
					KeyB().CanWriteOutputPin().SetPinValueLow();
					break;
				}

			}
/*
			uint8_t g[ 4 ];

			g[0] = ((c1 & 1) << 2) | ((c2 & 1) << 1) | ((c3 & 1) << 0);
			g[1] = ((c1 & 2) << 1) | ((c2 & 2) << 0) | ((c3 & 2) >> 1);
			g[2] = ((c1 & 4) << 0) | ((c2 & 4) >> 1) | ((c3 & 4) >> 2);
			g[3] = ((c1 & 8) >> 1) | ((c2 & 8) >> 2) | ((c3 & 8) >> 3);
*/
			T_UserDataOutputPin::SetPinValue( buffer[ 9 ] );

//			KeyA().OutputPin().SetPinValue( Mitov::TDataBlock( 6, buffer ));
			KeyB().OutputPin().SetPinValue( Mitov::TDataBlock( 6, buffer + 10 ));

			T_SuccessOutputPin::ClockPin();

//			Serial.println( c3, BIN );
//			Serial.println( g[3], BIN );
		}


	};
//---------------------------------------------------------------------------	
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
		typename T_Enabled,
		typename T_ErrorOutputPin,
		typename T_KeyA,
		typename T_KeyB,
		typename T_SuccessOutputPin,
		typename T_UserDataOutputPin
	> class TArduino_RFID_MFRC522_Card_Sector_Trailer_Info_Operation :
		public T_Enabled,
		public T_ErrorOutputPin,
		public T_KeyA,
		public T_KeyB,
		public T_SuccessOutputPin,
		public T_UserDataOutputPin
	{
	public:
		_V_PIN_( ErrorOutputPin )
		_V_PIN_( SuccessOutputPin )
		_V_PIN_( UserDataOutputPin )

	public:
		_V_PROP_( Enabled )
		_V_PROP_( KeyA )
		_V_PROP_( KeyB )

	public:
		inline void ClockInputPin_o_Receive( void *_Data )
		{
			if( ! ( Enabled().GetValue() && C_OWNER.GetIsEnebaled() ) )
				return;

			if( ! C_OWNER.GetIsPresent() )
			{
				T_ErrorOutputPin::SetPinValue( "No Card." );
				return;
			}

//			Serial.println( "TEST2" );
			bool AUsedKeyB;
			uint8_t AKeyIndex;
            MFRC522Const::StatusCode status = C_OWNER.TryAuthenticate( C_OWNER.GetSectorNumber() * 4 + 3, AUsedKeyB, AKeyIndex );
			if( status != MFRC522Const::STATUS_OK )
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}

//			Serial.println( "TEST3" );
			uint8_t buffer[ 18 ];
			uint8_t size = sizeof( buffer );

//				Serial.println( "READ" );
			status = C_OWNER.GetRFID().MIFARE_ReadCheckCRC( C_OWNER.GetSectorNumber() * 4 + 3, buffer, &size );
//				Serial.println( status );
//				Serial.println( C_OWNER.GetStatusCodeName( status ));

			if( status != MFRC522Const::STATUS_OK )
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}

			uint8_t c1  = buffer[7] >> 4;
			uint8_t c2  = buffer[8] & 0xF;
			uint8_t c3  = buffer[8] >> 4;
			uint8_t c1_ = buffer[6] & 0xF;
			uint8_t c2_ = buffer[6] >> 4;
			uint8_t c3_ = buffer[7] & 0xF;

			if( (c1 != (~c1_ & 0xF)) || (c2 != (~c2_ & 0xF)) || (c3 != (~c3_ & 0xF)) )
			{
//				Serial.println( "TEST1" );
				T_ErrorOutputPin::SetPinValue( "Invalid Flags." ); // Invert Error!
				return;
			}

			uint8_t g = ((c1 & 8) >> 1) | ((c2 & 8) >> 2) | ((c3 & 8) >> 3);

			switch( g )
			{
				case 0:
				{
					KeyA().CanWriteKeyAOutputPin().SetPinValueHigh();
					KeyA().CanReadFlagsOutputPin().SetPinValueHigh();
					KeyA().CanWriteFlagsOutputPin().SetPinValueLow();
					KeyA().CanReadKeyBOutputPin().SetPinValueHigh();
					KeyA().CanWriteKeyBOutputPin().SetPinValueLow();

					KeyB().CanWriteKeyAOutputPin().SetPinValueLow();
					KeyB().CanReadFlagsOutputPin().SetPinValueLow();
					KeyB().CanWriteFlagsOutputPin().SetPinValueLow();
					KeyB().CanWriteKeyBOutputPin().SetPinValueLow();
					break;
				}

				case 1:
				{
					KeyA().CanWriteKeyAOutputPin().SetPinValueHigh();
					KeyA().CanReadFlagsOutputPin().SetPinValueHigh();
					KeyA().CanWriteFlagsOutputPin().SetPinValueHigh();
					KeyA().CanReadKeyBOutputPin().SetPinValueHigh();
					KeyA().CanWriteKeyBOutputPin().SetPinValueHigh();

					KeyB().CanWriteKeyAOutputPin().SetPinValueLow();
					KeyB().CanReadFlagsOutputPin().SetPinValueLow();
					KeyB().CanWriteFlagsOutputPin().SetPinValueLow();
					KeyB().CanWriteKeyBOutputPin().SetPinValueLow();
					break;
				}

				case 2:
				{
					KeyA().CanWriteKeyAOutputPin().SetPinValueLow();
					KeyA().CanReadFlagsOutputPin().SetPinValueHigh();
					KeyA().CanWriteFlagsOutputPin().SetPinValueLow();
					KeyA().CanReadKeyBOutputPin().SetPinValueHigh();
					KeyA().CanWriteKeyBOutputPin().SetPinValueLow();

					KeyB().CanWriteKeyAOutputPin().SetPinValueLow();
					KeyB().CanReadFlagsOutputPin().SetPinValueLow();
					KeyB().CanWriteFlagsOutputPin().SetPinValueLow();
					KeyB().CanWriteKeyBOutputPin().SetPinValueLow();
					break;
				}

				case 3:
				{
					KeyA().CanWriteKeyAOutputPin().SetPinValueLow();
					KeyA().CanReadFlagsOutputPin().SetPinValueHigh();
					KeyA().CanWriteFlagsOutputPin().SetPinValueLow();
					KeyA().CanReadKeyBOutputPin().SetPinValueLow();
					KeyA().CanWriteKeyBOutputPin().SetPinValueLow();

					KeyB().CanWriteKeyAOutputPin().SetPinValueHigh();
					KeyB().CanReadFlagsOutputPin().SetPinValueHigh();
					KeyB().CanWriteFlagsOutputPin().SetPinValueHigh();
					KeyB().CanWriteKeyBOutputPin().SetPinValueHigh();
					break;
				}

				case 4:
				{
					KeyA().CanWriteKeyAOutputPin().SetPinValueLow();
					KeyA().CanReadFlagsOutputPin().SetPinValueHigh();
					KeyA().CanWriteFlagsOutputPin().SetPinValueLow();
					KeyA().CanReadKeyBOutputPin().SetPinValueLow();
					KeyA().CanWriteKeyBOutputPin().SetPinValueLow();

					KeyB().CanWriteKeyAOutputPin().SetPinValueHigh();
					KeyB().CanReadFlagsOutputPin().SetPinValueHigh();
					KeyB().CanWriteFlagsOutputPin().SetPinValueLow();
					KeyB().CanWriteKeyBOutputPin().SetPinValueHigh();
					break;
				}

				case 5:
				{
					KeyA().CanWriteKeyAOutputPin().SetPinValueLow();
					KeyA().CanReadFlagsOutputPin().SetPinValueHigh();
					KeyA().CanWriteFlagsOutputPin().SetPinValueLow();
					KeyA().CanReadKeyBOutputPin().SetPinValueLow();
					KeyA().CanWriteKeyBOutputPin().SetPinValueLow();

					KeyB().CanWriteKeyAOutputPin().SetPinValueLow();
					KeyB().CanReadFlagsOutputPin().SetPinValueHigh();
					KeyB().CanWriteFlagsOutputPin().SetPinValueHigh();
					KeyB().CanWriteKeyBOutputPin().SetPinValueLow();
					break;
				}

				case 6:
				case 7:
				{
					KeyA().CanWriteKeyAOutputPin().SetPinValueLow();
					KeyA().CanReadFlagsOutputPin().SetPinValueHigh();
					KeyA().CanWriteFlagsOutputPin().SetPinValueLow();
					KeyA().CanReadKeyBOutputPin().SetPinValueLow();
					KeyA().CanWriteKeyBOutputPin().SetPinValueLow();

					KeyB().CanWriteKeyAOutputPin().SetPinValueLow();
					KeyB().CanReadFlagsOutputPin().SetPinValueHigh();
					KeyB().CanWriteFlagsOutputPin().SetPinValueLow();
					KeyB().CanWriteKeyBOutputPin().SetPinValueLow();
					break;
				}

			}

			KeyB().OutputPin().SetPinValue( Mitov::TDataBlock( 6, buffer + 10 ));
			T_UserDataOutputPin::SetPinValue( buffer[ 9 ] );

			T_SuccessOutputPin::ClockPin();
		}
	};
//---------------------------------------------------------------------------
	template <
		typename T_Key,
		typename T_Key_ApplyValues,
		typename T_Key_GetValue,
		typename T_UseAuthenticationKey
	> class TArduino_RFID_MFRC522_Card_Block_Info_KeyA_Write_NewKey :
		public T_Key,
		public T_UseAuthenticationKey
	{
	public:
		_V_PROP_( Key )
		_V_PROP_( UseAuthenticationKey )

	};
//---------------------------------------------------------------------------
	template <
		typename T_Key,
		typename T_Key_ApplyValues,
		typename T_Key_GetValue,
		typename T_OnlyIfNeeded,
		typename T_UseAuthenticationKey
	> class TArduino_RFID_MFRC522_Card_Block_Info_KeyB_Write_NewKey :
		public T_Key,
		public T_OnlyIfNeeded,
		public T_UseAuthenticationKey
	{
	public:
		_V_PROP_( Key )
		_V_PROP_( OnlyIfNeeded )
		_V_PROP_( UseAuthenticationKey )

	};
//---------------------------------------------------------------------------
	template <
		typename T_CanDecrement,
		typename T_CanIncrement,
		typename T_CanRead,
		typename T_CanWrite,
		typename T_ErrorOutputPin,
		typename T_NewKey
	> class TArduino_RFID_MFRC522_Card_Block_Info_KeyA_Write :
		public T_CanDecrement,
		public T_CanIncrement,
		public T_CanRead,
		public T_CanWrite,
		public T_ErrorOutputPin,
		public T_NewKey
	{
	public:
		_V_PIN_( ErrorOutputPin )

	public:
		_V_PROP_( CanDecrement )
		_V_PROP_( CanIncrement )
		_V_PROP_( CanRead )
		_V_PROP_( CanWrite )
		_V_PROP_( NewKey )

	};
//---------------------------------------------------------------------------
	template <
		typename T_CanDecrement,
		typename T_CanIncrement,
		typename T_CanRead,
		typename T_CanWrite,
		typename T_ErrorOutputPin,
		typename T_NewKey
	> class TArduino_RFID_MFRC522_Card_Block_Info_KeyB_Write :
		public T_CanDecrement,
		public T_CanIncrement,
		public T_CanRead,
		public T_CanWrite,
		public T_ErrorOutputPin,
		public T_NewKey
	{
	public:
		_V_PIN_( ErrorOutputPin )

	public:
		_V_PROP_( CanDecrement )
		_V_PROP_( CanIncrement )
		_V_PROP_( CanRead )
		_V_PROP_( CanWrite )
		_V_PROP_( NewKey )

	};
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
		typename T_Enabled,
		typename T_ErrorOutputPin,
		typename T_KeyA,
		typename T_KeyB,
		typename T_SuccessOutputPin
//		typename T_UserData
	> class TArduino_RFID_MFRC522_Card_Write_Block_Info_Operation :
		public T_Enabled,
		public T_ErrorOutputPin,
		public T_KeyA,
		public T_KeyB,
		public T_SuccessOutputPin
//		public T_UserData
	{
	public:
		_V_PIN_( ErrorOutputPin )
		_V_PIN_( SuccessOutputPin )

	public:
		_V_PROP_( Enabled )
		_V_PROP_( KeyA )
		_V_PROP_( KeyB )
//		_V_PROP_( UserData )

	public:
		inline void ClockInputPin_o_Receive( void *_Data )
		{
			if( ! ( Enabled().GetValue() && C_OWNER.GetIsEnebaled() ) )
				return;

			if( ! C_OWNER.GetIsPresent() )
			{
				T_ErrorOutputPin::SetPinValue( "No Card." );
				return;
			}

//			Serial.println( "TEST1" );

			bool AUsedKeyB;
			uint8_t AKeyIndex;
            MFRC522Const::StatusCode status = C_OWNER.TryAuthenticate( C_OWNER.GetSectorNumber() * 4 + 3, AUsedKeyB, AKeyIndex );
			if( status != MFRC522Const::STATUS_OK )
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}
/*
	uint8_t buffer1[ 16 ] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x7, 0x80, 0x69, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
	MFRC522Const::StatusCode status1 = C_OWNER.MIFARE_Write( SectorNumber().GetValue() * 4 + 3, buffer1, 16 );
	Serial.println( status1 );
	Serial.println( C_OWNER.GetStatusCodeName( status1 ));
return;
*/
//			Serial.println( "TEST2" );

			uint8_t buffer[ 18 ];
			uint8_t size = sizeof( buffer );

//				Serial.println( "READ" );
			status = C_OWNER.GetRFID().MIFARE_ReadCheckCRC( C_OWNER.GetSectorNumber() * 4 + 3, buffer, &size );
//				Serial.println( status );
//				Serial.println( C_OWNER.GetStatusCodeName( status ));

			if( status != MFRC522Const::STATUS_OK )
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}
/*
			Serial.println( "TEST3" );

			for( int i = 0; i < 16; ++ i )
			{
				Serial.print( ", " );
				Serial.print( buffer[ i ], HEX );
			}

			Serial.println();
*/
			uint8_t c1  = buffer[7] >> 4;
			uint8_t c2  = buffer[8] & 0xF;
			uint8_t c3  = buffer[8] >> 4;
			uint8_t c1_ = buffer[6] & 0xF;
			uint8_t c2_ = buffer[6] >> 4;
			uint8_t c3_ = buffer[7] & 0xF;

			if( (c1 != (~c1_ & 0xF)) || (c2 != (~c2_ & 0xF)) || (c3 != (~c3_ & 0xF)) )
			{
//				Serial.println( "TEST1" );
				T_ErrorOutputPin::SetPinValue( "Invalid Flags." ); // Invert Error!
				return;
			}

			uint8_t g = ((c1 & 8) >> 1) | ((c2 & 8) >> 2) | ((c3 & 8) >> 3);

			bool ANeedsKeyB = ( ! (( g == 0b000 ) | ( g == 0b010 ) | ( g == 0b001 )) ) || AUsedKeyB;
			bool ACanWriteKeys = ( AUsedKeyB ) ? (( g == 0b100 ) | ( g == 0b011 )) : (( g == 0b000 ) | ( g == 0b001 ));
//			bool ACanWriteB = ( AUsedKeyB ) ? (( g == 0b100 ) | ( g == 0b011 )) : (( g == 0b000 ) | ( g == 0b001 ));

			if(	KeyA().CanRead() && KeyA().CanWrite() && KeyA().CanIncrement() && KeyA().CanDecrement() && 
				KeyB().CanRead() && KeyB().CanWrite() && KeyB().CanIncrement() && KeyB().CanDecrement() )
				g = 0b000;

			else if(	KeyA().CanRead() && ( ! KeyA().CanWrite()) && ( ! KeyA().CanIncrement()) && ( ! KeyA().CanDecrement()) && 
						KeyB().CanRead() && ( ! KeyB().CanWrite()) && ( ! KeyB().CanIncrement()) && ( ! KeyB().CanDecrement()) )
				g = 0b010;

			else if(	KeyA().CanRead() && ( ! KeyA().CanWrite() ) && ( ! KeyA().CanIncrement() ) && ( ! KeyA().CanDecrement()) && 
						KeyB().CanRead() &&     KeyB().CanWrite()   && ( ! KeyB().CanIncrement() ) && ( ! KeyB().CanDecrement() ))
				g = 0b100;

			else if(	KeyA().CanRead() && ( ! KeyA().CanWrite() ) && ( ! KeyA().CanIncrement() ) && KeyA().CanDecrement() && 
						KeyB().CanRead() &&     KeyB().CanWrite()   &&     KeyB().CanIncrement()   && KeyB().CanDecrement() )
				g = 0b110;

			else if(	KeyA().CanRead() && ( ! KeyA().CanWrite() ) && ( ! KeyA().CanIncrement() ) && KeyA().CanDecrement() && 
						KeyB().CanRead() && ( ! KeyB().CanWrite() ) && ( ! KeyB().CanIncrement() ) && KeyB().CanDecrement() )
				g = 0b001;

			else if(	( ! KeyA().CanRead() ) && ( ! KeyA().CanWrite() ) && ( ! KeyA().CanIncrement() ) && ( ! KeyA().CanDecrement() ) && 
						    KeyB().CanRead()   &&     KeyB().CanWrite()   && ( ! KeyB().CanIncrement() ) && ( ! KeyB().CanDecrement() ) )
				g = 0b011;

			else if(	( ! KeyA().CanRead() ) && ( ! KeyA().CanWrite() ) && ( ! KeyA().CanIncrement() ) && ( ! KeyA().CanDecrement() ) && 
						    KeyB().CanRead()   && ( ! KeyB().CanWrite() ) && ( ! KeyB().CanIncrement() ) && ( ! KeyB().CanDecrement() ) )
				g = 0b101;

			else if(	( ! KeyA().CanRead() ) && ( ! KeyA().CanWrite() ) && ( ! KeyA().CanIncrement() ) && ( ! KeyA().CanDecrement() ) && 
						( ! KeyB().CanRead() ) && ( ! KeyB().CanWrite() ) && ( ! KeyB().CanIncrement() ) && ( ! KeyB().CanDecrement() ) )
				g = 0b111;

			else
				g = 0b000;

			bool C1 = (( g & 0b100 ) != 0 );
			bool C2 = (( g & 0b010 ) != 0 );
			bool C3 = (( g & 0b001 ) != 0 );

			uint8_t AMask1 = 0b00000001 << C_OWNER.GetSectorNumber();
			uint8_t AMask2 = AMask1 << 4;
			uint8_t AMask3 = ~( AMask1 | AMask2 );

			buffer[ 6 ] = (( buffer[ 6 ] & AMask3 ) | ( C1 ? 0b00000000 : AMask1 ) | ( C2 ? 0b00000000 : AMask2 ));
			buffer[ 7 ] = (( buffer[ 7 ] & AMask3 ) | ( C3 ? 0b00000000 : AMask1 ) | ( C1 ? AMask2 : 0b00000000 ));
			buffer[ 8 ] = (( buffer[ 8 ] & AMask3 ) | ( C2 ? AMask1 : 0b00000000 ) | ( C3 ? AMask2 : 0b00000000 ));

			if( KeyA().NewKey().UseAuthenticationKey() )
				C_OWNER.CopyKeyData( AKeyIndex, buffer );

			else
				KeyA().NewKey().CopyData( buffer, 0, 6 );

			if( ( ! KeyB().NewKey().OnlyIfNeeded() ) || ANeedsKeyB )
			{
//				Serial.print( "ANeedsKeyB = " );
//				Serial.println( ANeedsKeyB );

				if( KeyB().NewKey().UseAuthenticationKey() )
					C_OWNER.CopyKeyData( AKeyIndex, buffer + 10 );

				else
					KeyB().NewKey().CopyData( buffer + 10, 0, 6 );

			}

			if( ! ACanWriteKeys )
			{
				KeyA().ErrorOutputPin().SetPinValue( "Ca't write key A." );
				if( ! KeyB().NewKey().OnlyIfNeeded() )
					KeyB().ErrorOutputPin().SetPinValue( "Ca't write key B." );

			}
/*
			for( int i = 0; i < 16; ++ i )
			{
				Serial.print( ", " );
				Serial.print( buffer[ i ], HEX );
			}

			Serial.println();
*/
//return;
			status = C_OWNER.GetRFID().MIFARE_Write( C_OWNER.GetSectorNumber() * 4 + 3, buffer, 16 );
			if( status != MFRC522Const::STATUS_OK ) 
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}
//			buffer[ 9 ] = MitovMax<uint8_t>( UserData(), 255 );

/*
			uint8_t c1 = ((g[0] & 4) >> 2) | ((g[1] & 2) << 1) | ((g[2] & 1) << 2) | ((g[3] & 1) << 3);
			uint8_t c2 = ((g[0] & 2) >> 1) | ((g[1] & 1) << 0) | ((g[2] & 1) << 1) | ((g[3] & 1) << 2);
			uint8_t c3 = ((g[0] & 1) << 0) | ((g[1] & 1) << 1) | ((g[2] & 1) << 2) | ((g[3] & 1) << 3);
			uint8_t c1_ = ~c1 & 0xF;
			uint8_t c2_ = ~c2 & 0xF;
			uint8_t c3_ = ~c3 & 0xF;
			buffer[7] = (c1 << 4) | c3_;
			buffer[8] = (c2) | (c3 << 4);
			buffer[6] = (c1_) | (c2_ << 4);
*/
			T_SuccessOutputPin::ClockPin();
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_CanReadFlags,
		typename T_CanReadKeyB,
		typename T_CanWriteFlags,
		typename T_CanWriteKeyA,
		typename T_CanWriteKeyB,
		typename T_ErrorOutputPin,
		typename T_NewKey
	> class TArduino_RFID_MFRC522_Card_Sector_Trailer_Info_KeyA_Write :
		public T_CanReadFlags,
		public T_CanReadKeyB,
		public T_CanWriteFlags,
		public T_CanWriteKeyA,
		public T_CanWriteKeyB,
		public T_ErrorOutputPin,
		public T_NewKey
	{
	public:
		_V_PIN_( ErrorOutputPin )

	public:
		_V_PROP_( CanReadFlags )
		_V_PROP_( CanReadKeyB )
		_V_PROP_( CanWriteFlags )
		_V_PROP_( CanWriteKeyA )
		_V_PROP_( CanWriteKeyB )
		_V_PROP_( NewKey )

	};
//---------------------------------------------------------------------------
	template <
		typename T_CanReadFlags,
		typename T_CanWriteFlags,
		typename T_CanWriteKeyA,
		typename T_CanWriteKeyB,
		typename T_ErrorOutputPin,
		typename T_NewKey
	> class TArduino_RFID_MFRC522_Card_Sector_Trailer_Info_KeyB_Write :
		public T_CanReadFlags,
		public T_CanWriteFlags,
		public T_CanWriteKeyA,
		public T_CanWriteKeyB,
		public T_ErrorOutputPin,
		public T_NewKey
	{
	public:
		_V_PIN_( ErrorOutputPin )

	public:
		_V_PROP_( CanReadFlags )
		_V_PROP_( CanWriteFlags )
		_V_PROP_( CanWriteKeyA )
		_V_PROP_( CanWriteKeyB )
		_V_PROP_( NewKey )

	};
//---------------------------------------------------------------------------
	template <
		typename T_Capabilities,
		typename T_ErrorOutputPin,
		typename T_NewKey
	> class TArduino_RFID_MFRC522_Card_Write_All_Sector_Infos_Key :
		public T_Capabilities,
		public T_ErrorOutputPin,
		public T_NewKey
	{
	public:
		_V_PIN_( ErrorOutputPin )

	public:
		_V_PROP_( Capabilities )
		_V_PROP_( NewKey )

	};
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
		typename T_Enabled,
		typename T_ErrorOutputPin,
		typename T_KeyA,
		typename T_KeyB,
		typename T_SuccessOutputPin
	> class TArduino_RFID_MFRC522_Card_Write_Sector_Trailer_Info_Operation :
		public T_ErrorOutputPin,
		public T_Enabled,
		public T_KeyA,
		public T_KeyB,
		public T_SuccessOutputPin
	{
	public:
		_V_PIN_( ErrorOutputPin )
		_V_PIN_( SuccessOutputPin )

	public:
		_V_PROP_( Enabled )
		_V_PROP_( KeyA )
		_V_PROP_( KeyB )

	public:
		inline void ClockInputPin_o_Receive( void *_Data )
		{
			if( ! ( Enabled().GetValue() && C_OWNER.GetIsEnebaled() ) )
				return;

			if( ! C_OWNER.GetIsPresent() )
			{
				T_ErrorOutputPin::SetPinValue( "No Card." );
				return;
			}

//			Serial.println( "TEST1" );

			bool AUsedKeyB;
			uint8_t AKeyIndex;
            MFRC522Const::StatusCode status = C_OWNER.TryAuthenticate( C_OWNER.GetSectorNumber() * 4 + 3, AUsedKeyB, AKeyIndex );
			if( status != MFRC522Const::STATUS_OK )
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}
/*
	uint8_t buffer1[ 16 ] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x7, 0x80, 0x69, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
	MFRC522Const::StatusCode status1 = C_OWNER.MIFARE_Write( SectorNumber().GetValue() * 4 + 3, buffer1, 16 );
	Serial.println( status1 );
	Serial.println( C_OWNER.GetStatusCodeName( status1 ));
return;
*/
//			Serial.println( "TEST2" );

			uint8_t buffer[ 18 ];
			uint8_t size = sizeof( buffer );

//				Serial.println( "READ" );
			status = C_OWNER.GetRFID().MIFARE_ReadCheckCRC( C_OWNER.GetSectorNumber() * 4 + 3, buffer, &size );
//				Serial.println( status );
//				Serial.println( C_OWNER.GetStatusCodeName( status ));

			if( status != MFRC522Const::STATUS_OK )
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}
/*
			Serial.println( "TEST3" );

			for( int i = 0; i < 16; ++ i )
			{
				Serial.print( ", " );
				Serial.print( buffer[ i ], HEX );
			}

			Serial.println();
*/
			uint8_t c1  = buffer[7] >> 4;
			uint8_t c2  = buffer[8] & 0xF;
			uint8_t c3  = buffer[8] >> 4;
			uint8_t c1_ = buffer[6] & 0xF;
			uint8_t c2_ = buffer[6] >> 4;
			uint8_t c3_ = buffer[7] & 0xF;

			if( (c1 != (~c1_ & 0xF)) || (c2 != (~c2_ & 0xF)) || (c3 != (~c3_ & 0xF)) )
			{
//				Serial.println( "TEST1" );
				T_ErrorOutputPin::SetPinValue( "Invalid Flags." ); // Invert Error!
				return;
			}

			uint8_t g = ((c1 & 8) >> 1) | ((c2 & 8) >> 2) | ((c3 & 8) >> 3);

			bool ANeedsKeyB = ( ! (( g == 0b000 ) | ( g == 0b010 ) | ( g == 0b001 )) ) || AUsedKeyB;
			bool ACanWriteKeys = ( AUsedKeyB ) ? (( g == 0b100 ) | ( g == 0b011 )) : (( g == 0b000 ) | ( g == 0b001 ));

			if(	    KeyA().CanWriteKeyA()   &&     KeyA().CanReadFlags()   && ( ! KeyA().CanWriteFlags() ) &&     KeyA().CanReadKeyB() &&     KeyA().CanWriteKeyB() &&
				( ! KeyB().CanWriteKeyA() ) && ( ! KeyB().CanReadFlags() ) && ( ! KeyB().CanWriteFlags() ) &&                             ( ! KeyB().CanWriteKeyB() ))
				g = 0b000;

			else if(	KeyA().CanWriteKeyA()   &&     KeyA().CanReadFlags()   &&     KeyA().CanWriteFlags()   &&     KeyA().CanReadKeyB() &&     KeyA().CanWriteKeyB() &&
					( ! KeyB().CanWriteKeyA() ) && ( ! KeyB().CanReadFlags() ) && ( ! KeyB().CanWriteFlags() ) &&                             ( ! KeyB().CanWriteKeyB() ))
				g = 0b001;

			else if(( ! KeyA().CanWriteKeyA() ) &&     KeyA().CanReadFlags()   && ( ! KeyA().CanWriteFlags() ) &&     KeyA().CanReadKeyB() && ( ! KeyA().CanWriteKeyB() ) &&
					( ! KeyB().CanWriteKeyA() ) && ( ! KeyB().CanReadFlags() ) && ( ! KeyB().CanWriteFlags() ) &&                             ( ! KeyB().CanWriteKeyB() ))
				g = 0b010;

			else if(( ! KeyA().CanWriteKeyA() ) &&     KeyA().CanReadFlags()   && ( ! KeyA().CanWriteFlags() ) && ( ! KeyA().CanReadKeyB() ) && ( ! KeyA().CanWriteKeyB() ) &&
					    KeyB().CanWriteKeyA()   &&     KeyB().CanReadFlags()   &&     KeyB().CanWriteFlags()   &&                                   KeyB().CanWriteKeyB() )
				g = 0b011;

			else if(( ! KeyA().CanWriteKeyA() ) &&     KeyA().CanReadFlags()   && ( ! KeyA().CanWriteFlags() ) && ( ! KeyA().CanReadKeyB() ) && ( ! KeyA().CanWriteKeyB() ) &&
					    KeyB().CanWriteKeyA()   &&     KeyB().CanReadFlags()   && ( ! KeyB().CanWriteFlags() ) &&                                   KeyB().CanWriteKeyB() )
				g = 0b100;

			else if(( ! KeyA().CanWriteKeyA() ) &&     KeyA().CanReadFlags()   && ( ! KeyA().CanWriteFlags() ) && ( ! KeyA().CanReadKeyB() ) && ( ! KeyA().CanWriteKeyB() ) &&
					( ! KeyB().CanWriteKeyA() ) &&     KeyB().CanReadFlags()   &&     KeyB().CanWriteFlags()   &&                               ( ! KeyB().CanWriteKeyB() ))
				g = 0b101;

			else if(( ! KeyA().CanWriteKeyA() ) &&     KeyA().CanReadFlags()   && ( ! KeyA().CanWriteFlags() ) && ( ! KeyA().CanReadKeyB() ) && ( ! KeyA().CanWriteKeyB() ) &&
					( ! KeyB().CanWriteKeyA() ) &&     KeyB().CanReadFlags()   && ( ! KeyB().CanWriteFlags() ) &&                               ( ! KeyB().CanWriteKeyB() ))
//				g = 0b110; // Both are the same!
				g = 0b111;

			else
				g = 0b001;

			bool C1 = (( g & 0b100 ) != 0 );
			bool C2 = (( g & 0b010 ) != 0 );
			bool C3 = (( g & 0b001 ) != 0 );

			buffer[ 6 ] = (( buffer[ 6 ] & 0b01110111 ) | ( C1 ? 0b00000000 : 0b00001000 ) | ( C2 ? 0b00000000 : 0b10000000 ));
			buffer[ 7 ] = (( buffer[ 7 ] & 0b01110111 ) | ( C3 ? 0b00000000 : 0b00001000 ) | ( C1 ? 0b10000000 : 0b00000000 ));
			buffer[ 8 ] = (( buffer[ 8 ] & 0b01110111 ) | ( C2 ? 0b00001000 : 0b00000000 ) | ( C3 ? 0b10000000 : 0b00000000 ));

			if( KeyA().NewKey().UseAuthenticationKey() )
				C_OWNER.CopyKeyData( AKeyIndex, buffer );

			else
				KeyA().NewKey().CopyData( buffer, 0, 6 );

			if( ( ! KeyB().NewKey().OnlyIfNeeded() ) || ANeedsKeyB )
			{
//				Serial.print( "ANeedsKeyB = " );
//				Serial.println( ANeedsKeyB );

				if( KeyB().NewKey().UseAuthenticationKey() )
					C_OWNER.CopyKeyData( AKeyIndex, buffer + 10 );

				else
					KeyB().NewKey().CopyData( buffer + 10, 0, 6 );

			}

			if( ! ACanWriteKeys )
			{
				KeyA().ErrorOutputPin().SetPinValue( "Ca't write key A." );
				if( ! KeyB().NewKey().OnlyIfNeeded() )
					KeyB().ErrorOutputPin().SetPinValue( "Ca't write key B." );

			}
/*
			for( int i = 0; i < 16; ++ i )
			{
				Serial.print( ", " );
				Serial.print( buffer[ i ], HEX );
			}

			Serial.println();
*/
//return;
			status = C_OWNER.GetRFID().MIFARE_Write( C_OWNER.GetSectorNumber() * 4 + 3, buffer, 16 );
			if( status != MFRC522Const::STATUS_OK ) 
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}

			T_SuccessOutputPin::ClockPin();
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_ErrorOutputPin,
		typename T_NewKey,
		typename T_NewKey_ApplyValues,
		typename T_NewKey_GetValue,
		typename T_UseAuthenticationKey
	> class TArduino_RFID_MFRC522_Card_Sector_UserData_KeyA_Write :
		public T_ErrorOutputPin,
		public T_NewKey,
		public T_UseAuthenticationKey
	{
	public:
		_V_PIN_( ErrorOutputPin )

	public:
		_V_PROP_( NewKey )
		_V_PROP_( UseAuthenticationKey )

	};
//---------------------------------------------------------------------------
	template <
		typename T_ErrorOutputPin,
		typename T_NewKey,
		typename T_NewKey_ApplyValues,
		typename T_NewKey_GetValue,
		typename T_OnlyIfNeeded,
		typename T_UseAuthenticationKey
	> class TArduino_RFID_MFRC522_Card_Sector_UserData_KeyB_Write :
		public T_ErrorOutputPin,
		public T_NewKey,
		public T_OnlyIfNeeded,
		public T_UseAuthenticationKey
	{
	public:
		_V_PIN_( ErrorOutputPin )

	public:
		_V_PROP_( NewKey )
		_V_PROP_( OnlyIfNeeded )
		_V_PROP_( UseAuthenticationKey )

	};
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
		typename T_Enabled,
		typename T_ErrorOutputPin,
		typename T_ID,
		typename T_ID_ApplyValues,
		typename T_ID_GetValue,
		typename T_SuccessOutputPin
	> class TArduino_RFID_MFRC522_Card_Unbrick_Operation :
		public T_Enabled,
		public T_ErrorOutputPin,
		public T_ID,
		public T_SuccessOutputPin
	{
	public:
		_V_PIN_( ErrorOutputPin )
		_V_PIN_( SuccessOutputPin )

	public:
		_V_PROP_( Enabled )
		_V_PROP_( ID )

	public:
		inline void ClockInputPin_o_Receive( void *_Data )
		{
			if( ! ( Enabled().GetValue() && C_OWNER.GetIsEnebaled() ) )
				return;

			uint8_t block0_buffer[] = {0x01, 0x02, 0x03, 0x04, 0x04, 0x08, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

			ID().CopyData( block0_buffer, 0, 4 );

			MFRC522Const::StatusCode status = C_OWNER.GetRFID().MIFARE_UnbrickUidSector( block0_buffer );
			if( status != MFRC522Const::STATUS_OK ) 
			{
//				Serial.println( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}

			T_SuccessOutputPin::ClockPin();
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_CanDecrement,
		typename T_CanIncrement,
		typename T_CanRead,
		typename T_CanWrite
	> class TArduino_RFID_MFRC522_Card_Write_All_Sector_Infos_Key_Block_Flags :
		public T_CanDecrement,
		public T_CanIncrement,
		public T_CanRead,
		public T_CanWrite
	{
	public:
		_V_PROP_( CanDecrement )
		_V_PROP_( CanIncrement )
		_V_PROP_( CanRead )
		_V_PROP_( CanWrite )

	};
//---------------------------------------------------------------------------
	template <
		typename T_CanReadFlags,
		typename T_CanReadKeyB,
		typename T_CanWriteFlags,
		typename T_CanWriteKeyA,
		typename T_CanWriteKeyB
	> class TArduino_RFID_MFRC522_Card_Write_Trailer_Sector_Infos_KeyA_Block_Flags :
		public T_CanReadFlags,
		public T_CanReadKeyB,
		public T_CanWriteFlags,
		public T_CanWriteKeyA,
		public T_CanWriteKeyB
	{
	public:
		_V_PROP_( CanReadFlags )
		_V_PROP_( CanReadKeyB )
		_V_PROP_( CanWriteFlags )
		_V_PROP_( CanWriteKeyA )
		_V_PROP_( CanWriteKeyB )

	};
//---------------------------------------------------------------------------
	template <
		typename T_CanReadFlags,
		typename T_CanWriteFlags,
		typename T_CanWriteKeyA,
		typename T_CanWriteKeyB
	> class TArduino_RFID_MFRC522_Card_Write_Trailer_Sector_Infos_KeyB_Block_Flags :
		public T_CanReadFlags,
		public T_CanWriteFlags,
		public T_CanWriteKeyA,
		public T_CanWriteKeyB
	{
	public:
		_V_PROP_( CanReadFlags )
		_V_PROP_( CanWriteFlags )
		_V_PROP_( CanWriteKeyA )
		_V_PROP_( CanWriteKeyB )

	};
//---------------------------------------------------------------------------
	template <
		typename T_Sector0,
		typename T_Sector1,
		typename T_Sector2,
		typename T_Trailer
	> class TArduino_RFID_MFRC522_Card_Write_All_Sector_Infos_Key_Flags_Basic :
		public T_Sector0,
		public T_Sector1,
		public T_Sector2,
		public T_Trailer
	{
	public:
		_V_PROP_( Sector0 )
		_V_PROP_( Sector1 )
		_V_PROP_( Sector2 )
		_V_PROP_( Trailer )

	};
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
		typename T_Enabled,
		typename T_ErrorOutputPin,
		typename T_KeyA,
		typename T_KeyB,
		typename T_SuccessOutputPin,
		typename T_UserData
	> class TArduino_RFID_MFRC522_Card_Write_All_Sector_Infos_Operation :
		public T_Enabled,
		public T_ErrorOutputPin,
		public T_KeyA,
		public T_KeyB,
		public T_SuccessOutputPin,
		public T_UserData
	{
	public:
		_V_PIN_( ErrorOutputPin )
		_V_PIN_( SuccessOutputPin )

	public:
		_V_PROP_( Enabled )
		_V_PROP_( KeyA )
		_V_PROP_( KeyB )
		_V_PROP_( UserData )

	public:
		inline void ClockInputPin_o_Receive( void *_Data )
		{
			if( ! ( Enabled().GetValue() && C_OWNER.GetIsEnebaled() ) )
				return;

			if( ! C_OWNER.GetIsPresent() )
			{
				T_ErrorOutputPin::SetPinValue( "No Card." );
				return;
			}

			bool AUsedKeyB;
			uint8_t AKeyIndex;
            MFRC522Const::StatusCode status = C_OWNER.TryAuthenticate( C_OWNER.GetSectorNumber() * 4 + 3, AUsedKeyB, AKeyIndex );
			if( status != MFRC522Const::STATUS_OK )
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}

			uint8_t buffer[ 18 ];
			uint8_t size = sizeof( buffer );

//				Serial.println( "READ" );
			status = C_OWNER.GetRFID().MIFARE_ReadCheckCRC( C_OWNER.GetSectorNumber() * 4 + 3, buffer, &size );
//				Serial.println( status );
//				Serial.println( C_OWNER.GetStatusCodeName( status ));

			bool ANeedsKeyB;
			bool ACanWriteKeys;
			if( status != MFRC522Const::STATUS_OK )
			{
				ANeedsKeyB = true;
				ACanWriteKeys = true;
			}

			else
			{
				uint8_t c1  = buffer[7] >> 4;
				uint8_t c2  = buffer[8] & 0xF;
				uint8_t c3  = buffer[8] >> 4;
				uint8_t c1_ = buffer[6] & 0xF;
				uint8_t c2_ = buffer[6] >> 4;
				uint8_t c3_ = buffer[7] & 0xF;

				if( (c1 != (~c1_ & 0xF)) || (c2 != (~c2_ & 0xF)) || (c3 != (~c3_ & 0xF)) )
				{
					ANeedsKeyB = true;
					ACanWriteKeys = true;
				}

				else
				{
					uint8_t g = ((c1 & 8) >> 1) | ((c2 & 8) >> 2) | ((c3 & 8) >> 3);

//					Serial.print( "g = " );
//					Serial.println( g, BIN );


					ANeedsKeyB = ( ! (( g == 0b000 ) | ( g == 0b010 ) | ( g == 0b001 )) ) || AUsedKeyB;
					ACanWriteKeys = ( AUsedKeyB ) ? (( g == 0b100 ) | ( g == 0b011 )) : (( g == 0b000 ) | ( g == 0b001 ));
				}
			}
/*
			for( int i = 0; i < 16; ++ i )
			{
				Serial.print( ", " );
				Serial.print( buffer[ i ], HEX );
			}

			Serial.println();
*/
			buffer[ 9 ] = UserData().GetValue(); // MitovMax<uint8_t>( UserData(), 255 );

            uint8_t g;
			if(	    KeyA().Capabilities().Trailer().CanWriteKeyA()   &&     KeyA().Capabilities().Trailer().CanReadFlags()   && ( ! KeyA().Capabilities().Trailer().CanWriteFlags() ) &&     KeyA().Capabilities().Trailer().CanReadKeyB() &&     KeyA().Capabilities().Trailer().CanWriteKeyB() &&
				( ! KeyB().Capabilities().Trailer().CanWriteKeyA() ) && ( ! KeyB().Capabilities().Trailer().CanReadFlags() ) && ( ! KeyB().Capabilities().Trailer().CanWriteFlags() ) &&                                                      ( ! KeyB().Capabilities().Trailer().CanWriteKeyB() ))
				g = 0b000;

			else if(	KeyA().Capabilities().Trailer().CanWriteKeyA()   &&     KeyA().Capabilities().Trailer().CanReadFlags()   &&     KeyA().Capabilities().Trailer().CanWriteFlags()   &&     KeyA().Capabilities().Trailer().CanReadKeyB() &&     KeyA().Capabilities().Trailer().CanWriteKeyB() &&
					( ! KeyB().Capabilities().Trailer().CanWriteKeyA() ) && ( ! KeyB().Capabilities().Trailer().CanReadFlags() ) && ( ! KeyB().Capabilities().Trailer().CanWriteFlags() ) &&													  ( ! KeyB().Capabilities().Trailer().CanWriteKeyB() ))
				g = 0b001;

			else if(( ! KeyA().Capabilities().Trailer().CanWriteKeyA() ) &&     KeyA().Capabilities().Trailer().CanReadFlags()   && ( ! KeyA().Capabilities().Trailer().CanWriteFlags() ) &&     KeyA().Capabilities().Trailer().CanReadKeyB() && ( ! KeyA().Capabilities().Trailer().CanWriteKeyB() ) &&
					( ! KeyB().Capabilities().Trailer().CanWriteKeyA() ) && ( ! KeyB().Capabilities().Trailer().CanReadFlags() ) && ( ! KeyB().Capabilities().Trailer().CanWriteFlags() ) &&													  ( ! KeyB().Capabilities().Trailer().CanWriteKeyB() ))
				g = 0b010;

			else if(( ! KeyA().Capabilities().Trailer().CanWriteKeyA() ) &&     KeyA().Capabilities().Trailer().CanReadFlags()   && ( ! KeyA().Capabilities().Trailer().CanWriteFlags() ) && ( ! KeyA().Capabilities().Trailer().CanReadKeyB() ) && ( ! KeyA().Capabilities().Trailer().CanWriteKeyB() ) &&
					    KeyB().Capabilities().Trailer().CanWriteKeyA()   &&     KeyB().Capabilities().Trailer().CanReadFlags()   &&     KeyB().Capabilities().Trailer().CanWriteFlags()   &&														    KeyB().Capabilities().Trailer().CanWriteKeyB() )
				g = 0b011;

			else if(( ! KeyA().Capabilities().Trailer().CanWriteKeyA() ) &&     KeyA().Capabilities().Trailer().CanReadFlags()   && ( ! KeyA().Capabilities().Trailer().CanWriteFlags() ) && ( ! KeyA().Capabilities().Trailer().CanReadKeyB() ) && ( ! KeyA().Capabilities().Trailer().CanWriteKeyB() ) &&
					    KeyB().Capabilities().Trailer().CanWriteKeyA()   &&     KeyB().Capabilities().Trailer().CanReadFlags()   && ( ! KeyB().Capabilities().Trailer().CanWriteFlags() ) &&															KeyB().Capabilities().Trailer().CanWriteKeyB() )
				g = 0b100;

			else if(( ! KeyA().Capabilities().Trailer().CanWriteKeyA() ) &&     KeyA().Capabilities().Trailer().CanReadFlags()   && ( ! KeyA().Capabilities().Trailer().CanWriteFlags() ) && ( ! KeyA().Capabilities().Trailer().CanReadKeyB() ) && ( ! KeyA().Capabilities().Trailer().CanWriteKeyB() ) &&
					( ! KeyB().Capabilities().Trailer().CanWriteKeyA() ) &&     KeyB().Capabilities().Trailer().CanReadFlags()   &&     KeyB().Capabilities().Trailer().CanWriteFlags()   &&														( ! KeyB().Capabilities().Trailer().CanWriteKeyB() ))
				g = 0b101;

			else if(( ! KeyA().Capabilities().Trailer().CanWriteKeyA() ) &&     KeyA().Capabilities().Trailer().CanReadFlags()   && ( ! KeyA().Capabilities().Trailer().CanWriteFlags() ) && ( ! KeyA().Capabilities().Trailer().CanReadKeyB() ) && ( ! KeyA().Capabilities().Trailer().CanWriteKeyB() ) &&
					( ! KeyB().Capabilities().Trailer().CanWriteKeyA() ) &&     KeyB().Capabilities().Trailer().CanReadFlags()   && ( ! KeyB().Capabilities().Trailer().CanWriteFlags() ) &&														( ! KeyB().Capabilities().Trailer().CanWriteKeyB() ))
//				g = 0b110; // Both are the same!
				g = 0b111;

			else
				g = 0b001;

//			Serial.print( "Trailer g = " );
//			Serial.println( g, BIN );

			bool C1 = (( g & 0b100 ) != 0 );
			bool C2 = (( g & 0b010 ) != 0 );
			bool C3 = (( g & 0b001 ) != 0 );

			buffer[ 6 ] = ( C1 ? 0b00000000 : 0b00001000 ) | ( C2 ? 0b00000000 : 0b10000000 );
			buffer[ 7 ] = ( C3 ? 0b00000000 : 0b00001000 ) | ( C1 ? 0b10000000 : 0b00000000 );
			buffer[ 8 ] = ( C2 ? 0b00001000 : 0b00000000 ) | ( C3 ? 0b10000000 : 0b00000000 );
/*
			Serial.println( "Trailer" );
			for( int i = 0; i < 16; ++ i )
			{
				Serial.print( ", " );
				Serial.print( buffer[ i ], HEX );
			}

			Serial.println();
*/
			// Sector 0
			if(	KeyA().Sector0().CanRead() && KeyA().Sector0().CanWrite() && KeyA().Sector0().CanIncrement() && KeyA().Sector0().CanDecrement() && 
				KeyB().Sector0().CanRead() && KeyB().Sector0().CanWrite() && KeyB().Sector0().CanIncrement() && KeyB().Sector0().CanDecrement() )
				g = 0b000;

			else if(	KeyA().Sector0().CanRead() && ( ! KeyA().Sector0().CanWrite()) && ( ! KeyA().Sector0().CanIncrement()) && ( ! KeyA().Sector0().CanDecrement()) && 
						KeyB().Sector0().CanRead() && ( ! KeyB().Sector0().CanWrite()) && ( ! KeyB().Sector0().CanIncrement()) && ( ! KeyB().Sector0().CanDecrement()) )
				g = 0b010;

			else if(	KeyA().Sector0().CanRead() && ( ! KeyA().Sector0().CanWrite() ) && ( ! KeyA().Sector0().CanIncrement() ) && ( ! KeyA().Sector0().CanDecrement()) && 
						KeyB().Sector0().CanRead() &&     KeyB().Sector0().CanWrite()   && ( ! KeyB().Sector0().CanIncrement() ) && ( ! KeyB().Sector0().CanDecrement() ))
				g = 0b100;

			else if(	KeyA().Sector0().CanRead() && ( ! KeyA().Sector0().CanWrite() ) && ( ! KeyA().Sector0().CanIncrement() ) && KeyA().Sector0().CanDecrement() && 
						KeyB().Sector0().CanRead() &&     KeyB().Sector0().CanWrite()   &&     KeyB().Sector0().CanIncrement()   && KeyB().Sector0().CanDecrement() )
				g = 0b110;

			else if(	KeyA().Sector0().CanRead() && ( ! KeyA().Sector0().CanWrite() ) && ( ! KeyA().Sector0().CanIncrement() ) && KeyA().Sector0().CanDecrement() && 
						KeyB().Sector0().CanRead() && ( ! KeyB().Sector0().CanWrite() ) && ( ! KeyB().Sector0().CanIncrement() ) && KeyB().Sector0().CanDecrement() )
				g = 0b001;

			else if(	( ! KeyA().Sector0().CanRead() ) && ( ! KeyA().Sector0().CanWrite() ) && ( ! KeyA().Sector0().CanIncrement() ) && ( ! KeyA().Sector0().CanDecrement() ) && 
						    KeyB().Sector0().CanRead()   &&     KeyB().Sector0().CanWrite()   && ( ! KeyB().Sector0().CanIncrement() ) && ( ! KeyB().Sector0().CanDecrement() ) )
				g = 0b011;

			else if(	( ! KeyA().Sector0().CanRead() ) && ( ! KeyA().Sector0().CanWrite() ) && ( ! KeyA().Sector0().CanIncrement() ) && ( ! KeyA().Sector0().CanDecrement() ) && 
						    KeyB().Sector0().CanRead()   && ( ! KeyB().Sector0().CanWrite() ) && ( ! KeyB().Sector0().CanIncrement() ) && ( ! KeyB().Sector0().CanDecrement() ) )
				g = 0b101;

			else if(	( ! KeyA().Sector0().CanRead() ) && ( ! KeyA().Sector0().CanWrite() ) && ( ! KeyA().Sector0().CanIncrement() ) && ( ! KeyA().Sector0().CanDecrement() ) && 
						( ! KeyB().Sector0().CanRead() ) && ( ! KeyB().Sector0().CanWrite() ) && ( ! KeyB().Sector0().CanIncrement() ) && ( ! KeyB().Sector0().CanDecrement() ) )
				g = 0b111;

			else
				g = 0b000;

//			Serial.print( "Sector 0 g = " );
//			Serial.println( g, BIN );

			C1 = (( g & 0b100 ) != 0 );
			C2 = (( g & 0b010 ) != 0 );
			C3 = (( g & 0b001 ) != 0 );

			constexpr uint8_t AMask1_0 = 0b00000001;
			constexpr uint8_t AMask2_0 = AMask1_0 << 4;
			constexpr uint8_t AMask3_0 = ~( AMask1_0 | AMask2_0 );

			buffer[ 6 ] = (( buffer[ 6 ] & AMask3_0 ) | ( C1 ? 0b00000000 : AMask1_0 ) | ( C2 ? 0b00000000 : AMask2_0 ));
			buffer[ 7 ] = (( buffer[ 7 ] & AMask3_0 ) | ( C3 ? 0b00000000 : AMask1_0 ) | ( C1 ? AMask2_0 : 0b00000000 ));
			buffer[ 8 ] = (( buffer[ 8 ] & AMask3_0 ) | ( C2 ? AMask1_0 : 0b00000000 ) | ( C3 ? AMask2_0 : 0b00000000 ));
/*
			Serial.println( "Sector 0" );
			for( int i = 0; i < 16; ++ i )
			{
				Serial.print( ", " );
				Serial.print( buffer[ i ], HEX );
			}

			Serial.println();
*/
			// Sector 1
			if(	KeyA().Sector1().CanRead() && KeyA().Sector1().CanWrite() && KeyA().Sector1().CanIncrement() && KeyA().Sector1().CanDecrement() && 
				KeyB().Sector1().CanRead() && KeyB().Sector1().CanWrite() && KeyB().Sector1().CanIncrement() && KeyB().Sector1().CanDecrement() )
				g = 0b000;

			else if(	KeyA().Sector1().CanRead() && ( ! KeyA().Sector1().CanWrite()) && ( ! KeyA().Sector1().CanIncrement()) && ( ! KeyA().Sector1().CanDecrement()) && 
						KeyB().Sector1().CanRead() && ( ! KeyB().Sector1().CanWrite()) && ( ! KeyB().Sector1().CanIncrement()) && ( ! KeyB().Sector1().CanDecrement()) )
				g = 0b010;

			else if(	KeyA().Sector1().CanRead() && ( ! KeyA().Sector1().CanWrite() ) && ( ! KeyA().Sector1().CanIncrement() ) && ( ! KeyA().Sector1().CanDecrement()) && 
						KeyB().Sector1().CanRead() &&     KeyB().Sector1().CanWrite()   && ( ! KeyB().Sector1().CanIncrement() ) && ( ! KeyB().Sector1().CanDecrement() ))
				g = 0b100;

			else if(	KeyA().Sector1().CanRead() && ( ! KeyA().Sector1().CanWrite() ) && ( ! KeyA().Sector1().CanIncrement() ) && KeyA().Sector1().CanDecrement() && 
						KeyB().Sector1().CanRead() &&     KeyB().Sector1().CanWrite()   &&     KeyB().Sector1().CanIncrement()   && KeyB().Sector1().CanDecrement() )
				g = 0b110;

			else if(	KeyA().Sector1().CanRead() && ( ! KeyA().Sector1().CanWrite() ) && ( ! KeyA().Sector1().CanIncrement() ) && KeyA().Sector1().CanDecrement() && 
						KeyB().Sector1().CanRead() && ( ! KeyB().Sector1().CanWrite() ) && ( ! KeyB().Sector1().CanIncrement() ) && KeyB().Sector1().CanDecrement() )
				g = 0b001;

			else if(	( ! KeyA().Sector1().CanRead() ) && ( ! KeyA().Sector1().CanWrite() ) && ( ! KeyA().Sector1().CanIncrement() ) && ( ! KeyA().Sector1().CanDecrement() ) && 
						    KeyB().Sector1().CanRead()   &&     KeyB().Sector1().CanWrite()   && ( ! KeyB().Sector1().CanIncrement() ) && ( ! KeyB().Sector1().CanDecrement() ) )
				g = 0b011;

			else if(	( ! KeyA().Sector1().CanRead() ) && ( ! KeyA().Sector1().CanWrite() ) && ( ! KeyA().Sector1().CanIncrement() ) && ( ! KeyA().Sector1().CanDecrement() ) && 
						    KeyB().Sector1().CanRead()   && ( ! KeyB().Sector1().CanWrite() ) && ( ! KeyB().Sector1().CanIncrement() ) && ( ! KeyB().Sector1().CanDecrement() ) )
				g = 0b101;

			else if(	( ! KeyA().Sector1().CanRead() ) && ( ! KeyA().Sector1().CanWrite() ) && ( ! KeyA().Sector1().CanIncrement() ) && ( ! KeyA().Sector1().CanDecrement() ) && 
						( ! KeyB().Sector1().CanRead() ) && ( ! KeyB().Sector1().CanWrite() ) && ( ! KeyB().Sector1().CanIncrement() ) && ( ! KeyB().Sector1().CanDecrement() ) )
				g = 0b111;

			else
				g = 0b000;

//			Serial.print( "Sector 1 g = " );
//			Serial.println( g, BIN );

			C1 = (( g & 0b100 ) != 0 );
			C2 = (( g & 0b010 ) != 0 );
			C3 = (( g & 0b001 ) != 0 );

			constexpr uint8_t AMask1_1 = 0b00000001 << 1;
			constexpr uint8_t AMask2_1 = AMask1_1 << 4;
			constexpr uint8_t AMask3_1 = ~( AMask1_1 | AMask2_1 );

			buffer[ 6 ] = (( buffer[ 6 ] & AMask3_1 ) | ( C1 ? 0b00000000 : AMask1_1 ) | ( C2 ? 0b00000000 : AMask2_1 ));
			buffer[ 7 ] = (( buffer[ 7 ] & AMask3_1 ) | ( C3 ? 0b00000000 : AMask1_1 ) | ( C1 ? AMask2_1 : 0b00000000 ));
			buffer[ 8 ] = (( buffer[ 8 ] & AMask3_1 ) | ( C2 ? AMask1_1 : 0b00000000 ) | ( C3 ? AMask2_1 : 0b00000000 ));
/*
			Serial.println( "Sector 1" );
			for( int i = 0; i < 16; ++ i )
			{
				Serial.print( ", " );
				Serial.print( buffer[ i ], HEX );
			}

			Serial.println();
*/
			// Sector 2
			if(	KeyA().Sector2().CanRead() && KeyA().Sector2().CanWrite() && KeyA().Sector2().CanIncrement() && KeyA().Sector2().CanDecrement() && 
				KeyB().Sector2().CanRead() && KeyB().Sector2().CanWrite() && KeyB().Sector2().CanIncrement() && KeyB().Sector2().CanDecrement() )
				g = 0b000;

			else if(	KeyA().Sector2().CanRead() && ( ! KeyA().Sector2().CanWrite()) && ( ! KeyA().Sector2().CanIncrement()) && ( ! KeyA().Sector2().CanDecrement()) && 
						KeyB().Sector2().CanRead() && ( ! KeyB().Sector2().CanWrite()) && ( ! KeyB().Sector2().CanIncrement()) && ( ! KeyB().Sector2().CanDecrement()) )
				g = 0b010;

			else if(	KeyA().Sector2().CanRead() && ( ! KeyA().Sector2().CanWrite() ) && ( ! KeyA().Sector2().CanIncrement() ) && ( ! KeyA().Sector2().CanDecrement()) && 
						KeyB().Sector2().CanRead() &&     KeyB().Sector2().CanWrite()   && ( ! KeyB().Sector2().CanIncrement() ) && ( ! KeyB().Sector2().CanDecrement() ))
				g = 0b100;

			else if(	KeyA().Sector2().CanRead() && ( ! KeyA().Sector2().CanWrite() ) && ( ! KeyA().Sector2().CanIncrement() ) && KeyA().Sector2().CanDecrement() && 
						KeyB().Sector2().CanRead() &&     KeyB().Sector2().CanWrite()   &&     KeyB().Sector2().CanIncrement()   && KeyB().Sector2().CanDecrement() )
				g = 0b110;

			else if(	KeyA().Sector2().CanRead() && ( ! KeyA().Sector2().CanWrite() ) && ( ! KeyA().Sector2().CanIncrement() ) && KeyA().Sector2().CanDecrement() && 
						KeyB().Sector2().CanRead() && ( ! KeyB().Sector2().CanWrite() ) && ( ! KeyB().Sector2().CanIncrement() ) && KeyB().Sector2().CanDecrement() )
				g = 0b001;

			else if(	( ! KeyA().Sector2().CanRead() ) && ( ! KeyA().Sector2().CanWrite() ) && ( ! KeyA().Sector2().CanIncrement() ) && ( ! KeyA().Sector2().CanDecrement() ) && 
						    KeyB().Sector2().CanRead()   &&     KeyB().Sector2().CanWrite()   && ( ! KeyB().Sector2().CanIncrement() ) && ( ! KeyB().Sector2().CanDecrement() ) )
				g = 0b011;

			else if(	( ! KeyA().Sector2().CanRead() ) && ( ! KeyA().Sector2().CanWrite() ) && ( ! KeyA().Sector2().CanIncrement() ) && ( ! KeyA().Sector2().CanDecrement() ) && 
						    KeyB().Sector2().CanRead()   && ( ! KeyB().Sector2().CanWrite() ) && ( ! KeyB().Sector2().CanIncrement() ) && ( ! KeyB().Sector2().CanDecrement() ) )
				g = 0b101;

			else if(	( ! KeyA().Sector2().CanRead() ) && ( ! KeyA().Sector2().CanWrite() ) && ( ! KeyA().Sector2().CanIncrement() ) && ( ! KeyA().Sector2().CanDecrement() ) && 
						( ! KeyB().Sector2().CanRead() ) && ( ! KeyB().Sector2().CanWrite() ) && ( ! KeyB().Sector2().CanIncrement() ) && ( ! KeyB().Sector2().CanDecrement() ) )
				g = 0b111;

			else
				g = 0b000;

//			Serial.print( "Sector 2 g = " );
//			Serial.println( g, BIN );

			C1 = (( g & 0b100 ) != 0 );
			C2 = (( g & 0b010 ) != 0 );
			C3 = (( g & 0b001 ) != 0 );

			constexpr uint8_t AMask1_2 = 0b00000001 << 2;
			constexpr uint8_t AMask2_2 = AMask1_2 << 4;
			constexpr uint8_t AMask3_2 = ~( AMask1_2 | AMask2_2 );

			buffer[ 6 ] = (( buffer[ 6 ] & AMask3_2 ) | ( C1 ? 0b00000000 : AMask1_2 ) | ( C2 ? 0b00000000 : AMask2_2 ));
			buffer[ 7 ] = (( buffer[ 7 ] & AMask3_2 ) | ( C3 ? 0b00000000 : AMask1_2 ) | ( C1 ? AMask2_2 : 0b00000000 ));
			buffer[ 8 ] = (( buffer[ 8 ] & AMask3_2 ) | ( C2 ? AMask1_2 : 0b00000000 ) | ( C3 ? AMask2_2 : 0b00000000 ));

			if( KeyA().UseAuthenticationKey() )
				C_OWNER.CopyKeyData( AKeyIndex, buffer );

			else
				KeyA().NewKey().CopyData( buffer, 0, 6 );

			if( ( ! KeyB().OnlyIfNeeded() ) || ANeedsKeyB )
			{
//				Serial.print( "ANeedsKeyB = " );
//				Serial.println( ANeedsKeyB );

				if( KeyB().UseAuthenticationKey() )
					C_OWNER.CopyKeyData( AKeyIndex, buffer + 10 );

				else
					KeyB().NewKey().CopyData( buffer + 10, 0, 6 );

			}
/*
			Serial.println( "Sector 2" );
			for( int i = 0; i < 16; ++ i )
			{
				Serial.print( ", " );
				Serial.print( buffer[ i ], HEX );
			}

			Serial.println();
*/
//return;
			status = C_OWNER.GetRFID().MIFARE_Write( C_OWNER.GetSectorNumber() * 4 + 3, buffer, 16 );
			if( status != MFRC522Const::STATUS_OK ) 
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}

			T_SuccessOutputPin::ClockPin();
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
		typename T_Enabled,
		typename T_ErrorOutputPin,
		typename T_KeyA,
		typename T_KeyB,
		typename T_SuccessOutputPin,
		typename T_UserData
	> class TArduino_RFID_MFRC522_Card_Write_Sector_UserData_Operation :
		public T_Enabled,
		public T_ErrorOutputPin,
		public T_KeyA,
		public T_KeyB,
		public T_SuccessOutputPin,
		public T_UserData
	{
	public:
		_V_PIN_( ErrorOutputPin )
		_V_PIN_( SuccessOutputPin )

	public:
		_V_PROP_( Enabled )
		_V_PROP_( KeyA )
		_V_PROP_( KeyB )
		_V_PROP_( UserData )

	public:
		inline void ClockInputPin_o_Receive( void *_Data )
		{
			if( ! ( Enabled().GetValue() && C_OWNER.GetIsEnebaled() ) )
				return;

			if( ! C_OWNER.GetIsPresent() )
			{
				T_ErrorOutputPin::SetPinValue( "No Card." );
				return;
			}

//			Serial.println( "TEST1" );

			bool AUsedKeyB;
			uint8_t AKeyIndex;
            MFRC522Const::StatusCode status = C_OWNER.TryAuthenticate( C_OWNER.GetSectorNumber() * 4 + 3, AUsedKeyB, AKeyIndex );
			if( status != MFRC522Const::STATUS_OK )
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}
/*
	uint8_t buffer1[ 16 ] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x7, 0x80, 0x69, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
	MFRC522Const::StatusCode status1 = C_OWNER.MIFARE_Write( SectorNumber().GetValue() * 4 + 3, buffer1, 16 );
	Serial.println( status1 );
	Serial.println( C_OWNER.GetStatusCodeName( status1 ));
return;
*/
//			Serial.println( "TEST2" );

			uint8_t buffer[ 18 ];
			uint8_t size = sizeof( buffer );

//				Serial.println( "READ" );
			status = C_OWNER.GetRFID().MIFARE_ReadCheckCRC( C_OWNER.GetSectorNumber() * 4 + 3, buffer, &size );
//				Serial.println( status );
//				Serial.println( C_OWNER.GetStatusCodeName( status ));

			if( status != MFRC522Const::STATUS_OK )
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}
/*
			Serial.println( "TEST3" );

			for( int i = 0; i < 16; ++ i )
			{
				Serial.print( ", " );
				Serial.print( buffer[ i ], HEX );
			}

			Serial.println();
*/
			uint8_t c1  = buffer[7] >> 4;
			uint8_t c2  = buffer[8] & 0xF;
			uint8_t c3  = buffer[8] >> 4;
			uint8_t c1_ = buffer[6] & 0xF;
			uint8_t c2_ = buffer[6] >> 4;
			uint8_t c3_ = buffer[7] & 0xF;

			if( (c1 != (~c1_ & 0xF)) || (c2 != (~c2_ & 0xF)) || (c3 != (~c3_ & 0xF)) )
			{
//				Serial.println( "TEST1" );
				T_ErrorOutputPin::SetPinValue( "Invalid Flags." ); // Invert Error!
				return;
			}

			uint8_t g = ((c1 & 8) >> 1) | ((c2 & 8) >> 2) | ((c3 & 8) >> 3);

			bool ANeedsKeyB = ( ! (( g == 0b000 ) | ( g == 0b010 ) | ( g == 0b001 )) ) || AUsedKeyB;
			bool ACanWriteKeys = ( AUsedKeyB ) ? (( g == 0b100 ) | ( g == 0b011 )) : (( g == 0b000 ) | ( g == 0b001 ));

			buffer[ 9 ] = UserData().GetValue(); // MitovMax<uint8_t>( UserData(), 255 );

			if( KeyA().UseAuthenticationKey() )
				C_OWNER.CopyKeyData( AKeyIndex, buffer );

			else
				KeyA().NewKey().CopyData( buffer, 0, 6 );

			if( ( ! KeyB().OnlyIfNeeded() ) || ANeedsKeyB )
			{
//				Serial.print( "ANeedsKeyB = " );
//				Serial.println( ANeedsKeyB );

				if( KeyB().UseAuthenticationKey() )
					C_OWNER.CopyKeyData( AKeyIndex, buffer + 10 );

				else
					KeyB().NewKey().CopyData( buffer + 10, 0, 6 );

			}

			if( ! ACanWriteKeys )
			{
				KeyA().ErrorOutputPin().SetPinValue( "Ca't write key A." );
				if( ! KeyB().OnlyIfNeeded() )
					KeyB().ErrorOutputPin().SetPinValue( "Ca't write key B." );

			}

/*
			Serial.println( "TEST4" );

			for( int i = 0; i < 16; ++ i )
			{
				Serial.print( ", " );
				Serial.print( buffer[ i ], HEX );
			}

			Serial.println();
*/
//return;

			status = C_OWNER.GetRFID().MIFARE_Write( C_OWNER.GetSectorNumber() * 4 + 3, buffer, 16 );
			if( status != MFRC522Const::STATUS_OK ) 
			{
//				Serial.println( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}

			T_SuccessOutputPin::ClockPin();
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
		typename T_Enabled,
		typename T_ErrorOutputPin,
		typename T_KeyA,
		typename T_KeyB,
		typename T_SuccessOutputPin
	> class TArduino_RFID_MFRC522_Card_Set_Sector_Key_Operation :
		public T_Enabled,
		public T_ErrorOutputPin,
		public T_KeyA,
		public T_KeyB,
		public T_SuccessOutputPin
	{
	public:
		_V_PIN_( ErrorOutputPin )
		_V_PIN_( SuccessOutputPin )

	public:
		_V_PROP_( Enabled )
		_V_PROP_( KeyA )
		_V_PROP_( KeyB )

	public:
		inline void ClockInputPin_o_Receive( void *_Data )
		{
			if( ! ( Enabled().GetValue() && C_OWNER.GetIsEnebaled() ) )
				return;

			if( ! C_OWNER.GetIsPresent() )
			{
				T_ErrorOutputPin::SetPinValue( "No Card." );
				return;
			}

//			Serial.println( "TEST1" );

			bool AUsedKeyB;
			uint8_t AKeyIndex;
            MFRC522Const::StatusCode status = C_OWNER.TryAuthenticate( C_OWNER.GetSectorNumber() * 4 + 3, AUsedKeyB, AKeyIndex );
			if( status != MFRC522Const::STATUS_OK )
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}

			uint8_t buffer[ 18 ];
			uint8_t size = sizeof( buffer );

//				Serial.println( "READ" );
			status = C_OWNER.GetRFID().MIFARE_ReadCheckCRC( C_OWNER.GetSectorNumber() * 4 + 3, buffer, &size );
//				Serial.println( status );
//				Serial.println( C_OWNER.GetStatusCodeName( status ));

			if( status != MFRC522Const::STATUS_OK )
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}
/*
			Serial.println( "TEST3" );

			for( int i = 0; i < 16; ++ i )
			{
				Serial.print( ", " );
				Serial.print( buffer[ i ], HEX );
			}

			Serial.println();
*/
			uint8_t c1  = buffer[7] >> 4;
			uint8_t c2  = buffer[8] & 0xF;
			uint8_t c3  = buffer[8] >> 4;
			uint8_t c1_ = buffer[6] & 0xF;
			uint8_t c2_ = buffer[6] >> 4;
			uint8_t c3_ = buffer[7] & 0xF;

			if( (c1 != (~c1_ & 0xF)) || (c2 != (~c2_ & 0xF)) || (c3 != (~c3_ & 0xF)) )
			{
//				Serial.println( "TEST1" );
				T_ErrorOutputPin::SetPinValue( "Invalid Flags." ); // Invert Error!
				return;
			}

			uint8_t g = ((c1 & 8) >> 1) | ((c2 & 8) >> 2) | ((c3 & 8) >> 3);

			bool ANeedsKeyB = ( ! (( g == 0b000 ) | ( g == 0b010 ) | ( g == 0b001 )) ) || AUsedKeyB;
			bool ACanWriteKeys = ( AUsedKeyB ) ? (( g == 0b100 ) | ( g == 0b011 )) : (( g == 0b000 ) | ( g == 0b001 ));

			if( KeyA().UseAuthenticationKey() )
				C_OWNER.CopyKeyData( AKeyIndex, buffer );

			else
				KeyA().NewKey().CopyData( buffer, 0, 6 );

			if( ( ! KeyB().OnlyIfNeeded() ) || ANeedsKeyB )
			{
//				Serial.print( "ANeedsKeyB = " );
//				Serial.println( ANeedsKeyB );

				if( KeyB().UseAuthenticationKey() )
					C_OWNER.CopyKeyData( AKeyIndex, buffer + 10 );

				else
					KeyB().NewKey().CopyData( buffer + 10, 0, 6 );

			}

			if( ! ACanWriteKeys )
			{
				KeyA().ErrorOutputPin().SetPinValue( "Ca't write key A." );
				if( ! KeyB().OnlyIfNeeded() )
					KeyB().ErrorOutputPin().SetPinValue( "Ca't write key B." );

			}
/*
			Serial.println( "TEST4" );

			for( int i = 0; i < 16; ++ i )
			{
				Serial.print( ", " );
				Serial.print( buffer[ i ], HEX );
			}

			Serial.println();

return;
*/
			status = C_OWNER.GetRFID().MIFARE_Write( C_OWNER.GetSectorNumber() * 4 + 3, buffer, 16 );
			if( status != MFRC522Const::STATUS_OK ) 
			{
//				Serial.println( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}

			T_SuccessOutputPin::ClockPin();
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
		typename T_Address,
		typename T_Enabled,
		typename T_ErrorOutputPin,
		typename T_SuccessOutputPin,
		typename T_Value
	> class TArduino_RFID_MFRC522_Card_Format_Value_Block_Operation :
		public T_Address,
		public T_Enabled,
		public T_ErrorOutputPin,
		public T_SuccessOutputPin,
		public T_Value
	{
	public:
		_V_PIN_( ErrorOutputPin )
		_V_PIN_( SuccessOutputPin )

	public:
		_V_PROP_( Address )
		_V_PROP_( Enabled )
		_V_PROP_( Value )

	public:
		inline void ClockInputPin_o_Receive( void *_Data )
		{
			if( ! ( Enabled().GetValue() && C_OWNER.GetIsEnebaled() ) )
				return;

			if( ! C_OWNER.GetIsPresent() )
			{
				T_ErrorOutputPin::SetPinValue( "No Card." );
				return;
			}

			bool AUsedKeyB;
			uint8_t AKeyIndex;
            MFRC522Const::StatusCode status = C_OWNER.TryAuthenticate( C_OWNER.GetSectorNumber() * 4 + C_OWNER.GetBlockNumber(), AUsedKeyB, AKeyIndex );
			if( status != MFRC522Const::STATUS_OK )
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}

			uint8_t buffer[ 16 ];

			int32_t AValue = Value();
			for( int i = 0; i < 4; ++ i )
			{
				uint8_t AByte = AValue;
				buffer[ i ] = AByte;
				buffer[ i + 4 ] = ~ AByte;
				buffer[ i + 4 + 4 ] = AByte;

				AValue >>= 8;
			}

			uint8_t AInverted = ~ uint8_t( Address().GetValue() );
			buffer[ 12 ] = Address().GetValue();
			buffer[ 13 ] = AInverted;
			buffer[ 14 ] = Address().GetValue();
			buffer[ 15 ] = AInverted;
/*
			for( int i = 0; i < 16; ++ i )
			{
				Serial.print( ", " );
				Serial.print( buffer[ i ], HEX );
			}

			Serial.println();
*/
			status = C_OWNER.GetRFID().MIFARE_Write( C_OWNER.GetSectorNumber() * 4 + C_OWNER.GetBlockNumber(), buffer, 16 );
			if( status != MFRC522Const::STATUS_OK ) 
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}
//			Serial.println( status );
//			Serial.println( C_OWNER.GetStatusCodeName( status ));

			T_SuccessOutputPin::ClockPin();
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
		typename T_Enabled,
		typename T_ErrorOutputPin,
		typename T_SuccessOutputPin,
		typename T_Value
	> class TArduino_RFID_MFRC522_Card_Increment_Value_Block_Operation :
		public T_Enabled,
		public T_ErrorOutputPin,
		public T_SuccessOutputPin,
		public T_Value
	{
	public:
		_V_PIN_( ErrorOutputPin )
		_V_PIN_( SuccessOutputPin )

	public:
		_V_PROP_( Enabled )
		_V_PROP_( Value )

		inline void ClockInputPin_o_Receive( void *_Data )
		{
			if( ! ( Enabled().GetValue() && C_OWNER.GetIsEnebaled() ) )
				return;

			if( ! C_OWNER.GetIsPresent() )
			{
				T_ErrorOutputPin::SetPinValue( "No Card." );
				return;
			}

			bool AUsedKeyB;
			uint8_t AKeyIndex;
            MFRC522Const::StatusCode status = C_OWNER.TryAuthenticate( C_OWNER.GetSectorNumber() * 4 + C_OWNER.GetBlockNumber(), AUsedKeyB, AKeyIndex );
			if( status != MFRC522Const::STATUS_OK )
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}

			if( Value().GetValue() > 0 )
				status = C_OWNER.GetRFID().MIFARE_Increment( C_OWNER.GetSectorNumber() * 4 + C_OWNER.GetBlockNumber(), Value().GetValue() );

			else
				status = C_OWNER.GetRFID().MIFARE_Decrement( C_OWNER.GetSectorNumber() * 4 + C_OWNER.GetBlockNumber(), - Value().GetValue() );


//				Serial.println( status );
//				Serial.println( C_OWNER.GetRFID().GetStatusCodeName( status ));

			if( status != MFRC522Const::STATUS_OK ) 
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}

			status = C_OWNER.GetRFID().MIFARE_Transfer( C_OWNER.GetSectorNumber() * 4 + C_OWNER.GetBlockNumber() );
			if( status != MFRC522Const::STATUS_OK ) 
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}

			T_SuccessOutputPin::ClockPin();
		}
	};
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
		typename T_Enabled,
		typename T_Key,
		typename T_SectorNumber
	> class TArduino_RFID_MFRC522_Card_Sector_Operation :
		public T_Key,
		public T_Enabled,
		public T_SectorNumber
	{
	public:
		typedef T_OWNER T_RFID;

	public:
		_V_PROP_( Enabled )
		_V_PROP_( Key )
		_V_PROP_( SectorNumber )

	public:
		inline bool GetIsEnebaled() { return C_OWNER.GetIsEnebaled() && Enabled().GetValue(); }
		inline bool GetIsPresent() { return C_OWNER.GetIsPresent(); }
		inline uint32_t GetSectorNumber() { return SectorNumber().GetValue(); }

		inline MFRC522Const::StatusCode TryAuthenticate( uint8_t ABlockNumber, bool &AUsedKeyB, uint8_t AKeyIndex ) 
		{ 
			return Key().TryAuthenticate( C_OWNER, ABlockNumber, AUsedKeyB, AKeyIndex ); 
		}

        inline void CopyKeyData( uint8_t AKeyIndex, uint8_t *ADestination )
		{
			Key().CopyKeyData( AKeyIndex, ADestination );
		}

		inline typename T_OWNER::T_RFID &GetRFID() { return C_OWNER.GetRFID(); }

	};
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
		typename T_BlockNumber,
		typename T_Enabled
	> class TArduino_RFID_MFRC522_Card_Block_Basic_Element :
		public T_BlockNumber,
		public T_Enabled
	{
	public:
		typedef T_OWNER T_RFID;

	public:
		_V_PROP_( BlockNumber )
		_V_PROP_( Enabled )

	public:
		inline bool GetIsEnebaled() { return C_OWNER.GetIsEnebaled() && Enabled().GetValue(); }
		inline bool GetIsPresent() { return C_OWNER.GetIsPresent(); }
		inline uint32_t GetSectorNumber() { return C_OWNER.GetSectorNumber(); }
		inline uint8_t GetBlockNumber() { return BlockNumber().GetValue(); }
		inline MFRC522Const::StatusCode TryAuthenticate( uint8_t ABlockNumber, bool &AUsedKeyB, uint8_t AKeyIndex ) { return C_OWNER.TryAuthenticate( ABlockNumber, AUsedKeyB, AKeyIndex ); }
        inline void CopyKeyData( uint8_t AKeyIndex, uint8_t *ADestination ) { C_OWNER.CopyKeyData( AKeyIndex, ADestination ); }

		inline typename T_OWNER::T_RFID &GetRFID() { return C_OWNER.GetRFID(); }

	};
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
		typename T_Enabled,
		typename T_ErrorOutputPin,
		typename T_OutputPin,
		typename T_SuccessOutputPin
	> class TArduino_RFID_MFRC522_Card_Data_Block_Element_Read :
		public T_Enabled,
		public T_ErrorOutputPin,
		public T_OutputPin,
		public T_SuccessOutputPin
	{
	public:
		_V_PIN_( ErrorOutputPin )
		_V_PIN_( OutputPin )
		_V_PIN_( SuccessOutputPin )

	public:
		_V_PROP_( Enabled )

	public:
		inline void ClockInputPin_o_Receive( void *_Data )
		{
			if( ! ( Enabled().GetValue() && C_OWNER.GetIsEnebaled() ) )
				return;

			if( ! C_OWNER.GetIsPresent() )
			{
				T_ErrorOutputPin::SetPinValue( "No Card." );
				return;
			}

//			Serial.println( C_OWNER.GetBlockNumber() );

			bool AUsedKeyB;
			uint8_t AKeyIndex;
            MFRC522Const::StatusCode status = C_OWNER.TryAuthenticate( C_OWNER.GetSectorNumber() * 4 + C_OWNER.GetBlockNumber(), AUsedKeyB, AKeyIndex );
			if( status != MFRC522Const::STATUS_OK )
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}

			uint8_t buffer[ 18 ];
			uint8_t size = sizeof( buffer );

//				Serial.println( "READ" );
			status = C_OWNER.GetRFID().MIFARE_ReadCheckCRC( C_OWNER.GetSectorNumber() * 4 + C_OWNER.GetBlockNumber(), buffer, &size );
//				Serial.println( status );
//				Serial.println( C_OWNER.GetStatusCodeName( status ));

			if( status != MFRC522Const::STATUS_OK )
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}

			T_OutputPin::SetPinValue( Mitov::TDataBlock( 16, buffer ));
//				T_OutputPin::SetPinValue( Mitov::TDataBlock( 18, buffer ));
//				T_OutputPin::SetPinValue( Mitov::TDataBlock( 2, ACRCBufferOut ));

			T_SuccessOutputPin::ClockPin();

//			MFRC522Const::StatusCode status = C_OWNER.PCD_Authenticate( MFRC522Const::PICC_CMD_MF_AUTH_KEY_A, SectorNumber().GetValue() * 4 + BlockNumber().GetValue(), &key, &(mfrc522.uid));
/*
			Serial.println(F("Authenticating using key A..."));
			MFRC522Const::StatusCode status = C_OWNER.PCD_Authenticate( MFRC522Const::PICC_CMD_MF_AUTH_KEY_A, SectorNumber().GetValue() * 4 + BlockNumber().GetValue(), &key, &(mfrc522.uid));
			if (status != MFRC522Const::STATUS_OK) 
			{
				Serial.print(F("PCD_Authenticate() failed: "));
//				Serial.println(mfrc522.GetStatusCodeName(status));
				return;
			}
*/
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
		typename T_AddressOutputPin,
		typename T_Enabled,
		typename T_ErrorOutputPin,
		typename T_OutputPin,
		typename T_SuccessOutputPin
	> class TArduino_RFID_MFRC522_Card_Value_Block_Element_Read :
		public T_AddressOutputPin,
		public T_Enabled,
		public T_ErrorOutputPin,
		public T_OutputPin,
		public T_SuccessOutputPin
	{
	public:
		_V_PIN_( AddressOutputPin )
		_V_PIN_( ErrorOutputPin )
		_V_PIN_( OutputPin )
		_V_PIN_( SuccessOutputPin )

	public:
		_V_PROP_( Enabled )

	public:
		inline void ClockInputPin_o_Receive( void *_Data )
		{
//            Serial.println( "TEST1" );
			if( ! ( Enabled().GetValue() && C_OWNER.GetIsEnebaled() ) )
				return;

//            Serial.println( "TEST2" );
			if( ! C_OWNER.GetIsPresent() )
			{
				T_ErrorOutputPin::SetPinValue( "No Card." );
				return;
			}

//            Serial.println( "TEST3" );
			bool AUsedKeyB;
			uint8_t AKeyIndex;
            MFRC522Const::StatusCode status = C_OWNER.TryAuthenticate( C_OWNER.GetSectorNumber() * 4 + C_OWNER.GetBlockNumber(), AUsedKeyB, AKeyIndex );
			if( status != MFRC522Const::STATUS_OK )
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}

			uint8_t buffer[ 18 ];
			uint8_t size = sizeof( buffer );

//				Serial.println( "READ" );
			status = C_OWNER.GetRFID().MIFARE_ReadCheckCRC( C_OWNER.GetSectorNumber() * 4 + C_OWNER.GetBlockNumber(), buffer, &size );
//				Serial.println( status );
//				Serial.println( C_OWNER.GetRFID().GetStatusCodeName( status ));

			if( status != MFRC522Const::STATUS_OK )
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}
/*
			for( int i = 0; i < 16; ++ i )
			{
				Serial.print( ", " );
				Serial.print( buffer[ i ], HEX );
			}

			Serial.println();
*/
			for( int i = 0; i < 4; ++ i )
			{
				uint8_t AValue = buffer[ i ];
				if( ( buffer[ i + 4 ] != uint8_t( ~ AValue )) || ( buffer[ i + 4 + 4 ] != AValue ))
				{
/*
					Serial.println( i );
					Serial.println( AValue, HEX );
					Serial.println( buffer[ i + 4 ], HEX );
					Serial.println( ~ AValue, HEX );
					Serial.println( buffer[ i + 4 + 4 ], HEX );
					Serial.println( buffer[ i + 4 ] != ~ AValue );
*/
					T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
					return;
				}
			}

			uint8_t AAddressValue = buffer[ 12 ];
			uint8_t AInvertedValue = ~ AAddressValue;
			if( ( buffer[ 13 ] != AInvertedValue ) || ( buffer[ 14 ] != AAddressValue ) || ( buffer[ 15 ] != AInvertedValue ) )
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}

			T_AddressOutputPin::SetPinValue( AAddressValue );

			int32_t AIntValue = ( int32_t( buffer[ 3 ] ) << 24 ) | ( int32_t( buffer[ 2 ] ) << 16 ) | ( int32_t( buffer[ 1 ] ) << 8 ) | buffer[ 0 ];
			T_OutputPin::SetPinValue( AIntValue );

			T_SuccessOutputPin::ClockPin();
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
		typename T_Enabled,
		typename T_ErrorOutputPin,
		typename T_SuccessOutputPin,
		typename T_Value,
		typename T_Value_ApplyValues,
		typename T_Value_GetValue
	> class TArduino_RFID_MFRC522_Card_Write_Data_Block_Operation :
		public T_Enabled,
		public T_ErrorOutputPin,
		public T_SuccessOutputPin,
		public T_Value
	{
	public:
		_V_PIN_( ErrorOutputPin )
		_V_PIN_( SuccessOutputPin )

	public:
		_V_PROP_( Enabled )
		_V_PROP_( Value )

	public:
		inline void ClockInputPin_o_Receive( void *_Data )
		{
			if( ! ( Enabled().GetValue() && C_OWNER.GetIsEnebaled() ) )
				return;

			if( ! C_OWNER.GetIsPresent() )
			{
				T_ErrorOutputPin::SetPinValue( "No Card." );
				return;
			}

			bool AUsedKeyB;
			uint8_t AKeyIndex;
            MFRC522Const::StatusCode status = C_OWNER.TryAuthenticate( C_OWNER.GetSectorNumber() * 4 + C_OWNER.GetBlockNumber(), AUsedKeyB, AKeyIndex );
			if( status != MFRC522Const::STATUS_OK )
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}

			uint8_t buffer[ 16 ] = { 0 };
			Value().CopyData( buffer, 0, 16 );

			status = C_OWNER.GetRFID().MIFARE_Write( C_OWNER.GetSectorNumber() * 4 + C_OWNER.GetBlockNumber(), buffer, 16 );
			if( status != MFRC522Const::STATUS_OK ) 
			{
				T_ErrorOutputPin::SetPinValue( C_OWNER.GetRFID().GetStatusCodeName( status ) );
				return;
			}
//			Serial.println( status );
//			Serial.println( C_OWNER.GetStatusCodeName( status ));

			T_SuccessOutputPin::ClockPin();
		}
	};
//---------------------------------------------------------------------------
	template <
		typename T_I2C, T_I2C &C_I2C,
		typename T_Address
	> class RFID_MFRC522_InterfaceI2C :
		public InterfaceI2CNoSetHighBit <
				T_I2C, C_I2C,
				T_Address
			>
	{
		typedef InterfaceI2CNoSetHighBit <
				T_I2C, C_I2C,
				T_Address
			> inherited;
            
	public:
		inline void PCD_ReadRepeatRegister(	uint8_t reg,	///< The register to read from. One of the PCD_Register enums.
									uint8_t count,			///< The number of bytes to read
									uint8_t *values,		///< Byte array to store the values in.
									uint8_t rxAlign		///< Only bit positions rxAlign..7 in values[0] are updated.
								)
		{
            inherited::StartReadRegisters( reg, count );
//			inherited::ReadRegisters( reg, values, count );

			uint8_t index = 0;							// Index in values array.
            
			if( rxAlign )
			{		// Only update bit positions rxAlign..7 in values[0]
				// Create bit mask for bit positions rxAlign..7
				uint8_t mask = (0xFF << rxAlign) & 0xFF;
				// Read value and tell that we want to read the same address again.
				uint8_t value = inherited::ReadUInt8();
				// Apply mask to both current value of values[0] and the new data in value.
				values[0] = (values[0] & ~mask) | (value & mask);
				++ index;
			}

			while( index < count )
				values[index ++] = inherited::ReadUInt8();	// Read value and tell that we want to read the same address again.

	        values[ index ] = inherited::ReadUInt8();			// Read the final byte. Send 0 to stop reading.
            inherited::EndTransaction();
		}

	};    
//---------------------------------------------------------------------------
	namespace TArduino_RFID_MFRC522_CRC_Preset
	{
		enum TArduino_RFID_MFRC522_CRC_Preset 
		{
			x0000,
			x6363,
			xA671,
			xFFFF
		};
	}
//---------------------------------------------------------------------------
	template <
		typename T_MostSignificantByteFirst,
		typename T_Preset
	> class TArduino_RFID_MFRC522_CRC :
		public T_MostSignificantByteFirst,
		public T_Preset
	{
	public:
		_V_PROP_( MostSignificantByteFirst )
		_V_PROP_( Preset )

	};
//---------------------------------------------------------------------------
	template <
		typename T_0_IMPLEMENTATION,
		bool C_1_SHIFT_REGISTERS,
		typename T_Antenna,
		typename T_CRC,
		typename T_Card,
		typename T_Enabled,
//		typename T_ExternalInput,
		typename T_FLocked,
		typename T_Interrupt,
		typename T_OverheatedOutputPin,
		typename T_ParityCheck,
		typename T_PowerDown,
		typename T_Receiver,
		typename T_ResetOutputPin,
		typename T_Transmitter,
		typename T_VersionOutputPin
	> class TArduino_RFID_MFRC522Basic :
		public T_0_IMPLEMENTATION,
		public T_Antenna,
		public T_CRC,
		public T_Card,
		public T_Enabled,
//		public T_ExternalInput,
		public T_FLocked,
		public T_Interrupt,
		public T_OverheatedOutputPin,
		public T_ParityCheck,
		public T_PowerDown,
		public T_Receiver,
		public T_ResetOutputPin,
		public T_Transmitter,
		public T_VersionOutputPin
	{
	public:
		typedef TArduino_RFID_MFRC522Basic T_RFID;

	public:
		_V_PIN_( OverheatedOutputPin )
		_V_PIN_( ResetOutputPin )
		_V_PIN_( VersionOutputPin )

	public:
		_V_PROP_( Antenna )
		_V_PROP_( CRC )
		_V_PROP_( Card )
		_V_PROP_( Enabled )
//		_V_PROP_( ExternalInput )
		_V_PROP_( Interrupt )
		_V_PROP_( ParityCheck )
		_V_PROP_( PowerDown )
		_V_PROP_( Receiver )
		_V_PROP_( Transmitter )

	public:
		_V_PROP_( FLocked )

		unsigned long FLastTime = 0;

		MFRC522Const::Uid	FUid;
		uint16_t	FAtqa;
		MFRC522Const::Ats FAts;

	public:
		inline T_RFID &GetRFID() { return *this; }
		inline bool GetIsPresent() { return FLocked().GetValue(); }
		inline bool GetIsEnebaled() { return Enabled().GetValue(); }

	protected:
		void UpdateTxModeReg()
		{
			uint8_t AValue = ( Transmitter().CRC() ) ? 0b10000000 : 0;

			if( Transmitter().Inverted() )
				AValue |= 0b00001000;

			if( Transmitter().Speed().GetValue() > 424 )
				AValue |= 0b00110000;

			else if( Transmitter().Speed().GetValue() > 212 )
				AValue |= 0b00100000;

			else if( Transmitter().Speed().GetValue() > 106 )
				AValue |= 0b00010000;

//			Serial.println( AValue, BIN );
			PCD_WriteRegister( MFRC522Const::TxModeReg, AValue );
		}

		void UpdateRxModeReg()
		{
			uint8_t AValue = ( Receiver().CRC() ) ? 0b10000000 : 0;
			if( Receiver().IgnoreErrors() )
				AValue |= 0b00001000;

			if( ! Receiver().AutoDeactivate() )
				AValue |= 0b00000100;

			if( Receiver().Speed().GetValue() > 424 )
				AValue |= 0b00110000;

			else if( Receiver().Speed().GetValue() > 212 )
				AValue |= 0b00100000;

			else if( Receiver().Speed().GetValue() > 106 )
				AValue |= 0b00010000;

//			Serial.println( AValue, BIN );
			PCD_WriteRegister( MFRC522Const::RxModeReg, AValue );
		}

		void UpdateMfTxReg()
		{
			uint8_t AValue = Transmitter().AdditionalResponseTime();
//			Serial.println( AValue, BIN );
			PCD_WriteRegister( MFRC522Const::MfTxReg, AValue );
		}

		void UpdateMfRxReg()
		{
			uint8_t AValue = ( ParityCheck() ) ? 0b00000000 : 0b00010000;
			PCD_WriteRegister( MFRC522Const::MfRxReg, AValue );
		}

		void UpdateModWidthReg()
		{
			uint8_t AValue = ( Transmitter().MillerModulationWidth() );

//			Serial.println( AValue, HEX );
			PCD_WriteRegister( MFRC522Const::ModWidthReg, AValue );
		}

		void UpdateTxASKReg()
		{
			uint8_t AValue = ( Transmitter().ForceAmplitudeShiftKeying() ) ? 0b01000000 : 0;
			PCD_WriteRegister( MFRC522Const::TxASKReg, AValue );
		}

		void UpdateModeReg()
		{
			uint8_t AValue = uint8_t( CRC().Preset() );

			if( ! Transmitter().InvertedExternalInput().GetValue() )
				AValue |= 0b00001000;

			if( ! Transmitter().RemainActive().GetValue() )
				AValue |= 0b00100000;

			// 00111101
//			PCD_WriteRegister( MFRC522Const::ModeReg, 0x3D );		// Default 0x3F. Set the preset value for the CRC coprocessor for the CalcCRC command to 0x6363 (ISO 14443-3 part 6.2.4)
			PCD_WriteRegister( MFRC522Const::ModeReg, AValue );		// Default 0x3F. Set the preset value for the CRC coprocessor for the CalcCRC command to 0x6363 (ISO 14443-3 part 6.2.4)
		}

		void UpdateTxControlReg()
		{
			uint8_t AValue = ( Transmitter().Outputs().Pin2().InvertedWhenEnabled() ) ? 0b10000000 : 0;
			if( Transmitter().Outputs().Pin2().InvertedWhenDisabled() )
				AValue |= 0b00100000;

			if( Transmitter().Outputs().Pin2().Unmodulated() )
				AValue |= 0b00001000;

			if( Transmitter().Outputs().Pin2().Enabled().GetValue() && Antenna().Enabled().GetValue() )
				AValue |= 0b00000010;

			if( Transmitter().Outputs().Pin1().InvertedWhenEnabled() )
				AValue |= 0b01000000;

			if( Transmitter().Outputs().Pin1().InvertedWhenDisabled() )
				AValue |= 0b00010000;

			if( Transmitter().Outputs().Pin1().Enabled().GetValue() && Antenna().Enabled().GetValue() )
				AValue |= 0b00000001;

			PCD_WriteRegister( MFRC522Const::TxControlReg, AValue );
		}

		void UpdateRFCfgReg()
		{
			uint8_t AValue;

			if( Antenna().Gain().GetValue() > 43 )
				AValue = 0b1110000;

			else if( Antenna().Gain().GetValue() > 38 )
				AValue = 0b1100000;

			else if( Antenna().Gain().GetValue() > 33 )
				AValue = 0b1010000;

			else if( Antenna().Gain().GetValue() > 23 )
				AValue = 0b1000000;

			else if( Antenna().Gain().GetValue() > 18 )
				AValue = 0b0010000;

			else
				AValue = 0b0000000;

			PCD_WriteRegister( MFRC522Const::RFCfgReg, AValue );
		}

		void UpdateTxSelReg()
		{
			uint8_t AValue;
			if( ! Transmitter().Outputs().Enabled().GetValue() )
			{
				if( Transmitter().Outputs().ThreeState() )
					AValue = 0b00000000;

				else
					AValue = 0b00110000;
			}

			else if( Transmitter().Outputs().ExternalModulation() )
				AValue = 0b00100000;

			else
				AValue = 0b00010000;

			AValue |= uint8_t( Transmitter().ExternalOutputPinMode().GetValue() );

			PCD_WriteRegister( MFRC522Const::TxSelReg, AValue );
		}

		void UpdateComIEnReg()
		{
			uint8_t AValue = ( Interrupt().Inverted().GetValue() ) ? 0b10000000 : 0b00000000;
			PCD_WriteRegister( MFRC522Const::ComIEnReg, AValue );
		}

		void UpdateDivIEnReg()
		{
			uint8_t AValue = ( Interrupt().OpenDrain().GetValue() ) ? 0b00000000 : 0b10000000;
			PCD_WriteRegister( MFRC522Const::DivIEnReg, AValue );
		}

		void UpdateGsNReg()
		{
			uint8_t AValue = ( Receiver().ConductanceValues().Negative().ContinuousWave().GetValue() << 4 ) | Receiver().ConductanceValues().Negative().Modulation().GetValue();
//			Serial.println( AValue, HEX );
			PCD_WriteRegister( MFRC522Const::GsNReg, AValue );
		}

		void UpdateCWGsPReg()
		{
			uint8_t AValue = Receiver().ConductanceValues().Positive().ContinuousWave().GetValue();
//			Serial.println( AValue );
			PCD_WriteRegister( MFRC522Const::CWGsPReg, AValue );
		}

		void UpdateModGsPReg()
		{
			uint8_t AValue = Receiver().ConductanceValues().Positive().Modulation().GetValue();
//			Serial.println( AValue );
			PCD_WriteRegister( MFRC522Const::ModGsPReg, AValue );
		}

		void UpdateRxSelReg()
		{
			uint8_t AValue = Receiver().FrameGuardDelay().GetValue();
			if( Receiver().ExternalInput().Enabled() )
			{
				if( Receiver().ExternalInput().Demodulate() )
					AValue |= 0b01000000;

				else
					AValue |= 0b01100000;

			}

			else
			{
				if( Receiver().Enabled().GetValue() )
				  AValue |= 0b10000000;

			}

//			Serial.println( AValue, BIN );
			PCD_WriteRegister( MFRC522Const::RxSelReg, AValue );
		}

		void UpdateRxThresholdReg()
		{
			uint8_t AValue = ( Receiver().MinimalSignalLevels().Decode().GetValue() << 4 ) | Receiver().MinimalSignalLevels().Collision().GetValue();
//			Serial.println( AValue, BIN );
			PCD_WriteRegister( MFRC522Const::RxThresholdReg, AValue );
		}

		void UpdateDemodReg()
		{
			uint8_t AValue = ( Receiver().PhaseLockedLoopTime().DataReceiving().GetValue() << 2 ) | Receiver().PhaseLockedLoopTime().Burst().GetValue();
			if( ! Receiver().DecodeChannel().AutoSelect() )
			{
				if( Receiver().DecodeChannel().UseInPhaseChannel() )
					AValue |= 0b00100000;

				else
					AValue |= 0b01100000;

			}

			else
			{
				if( Receiver().DecodeChannel().FreezeDuringCommunication() )
					AValue |= 0b01000000;

			}

//			Serial.println( AValue, BIN );
			PCD_WriteRegister( MFRC522Const::DemodReg, AValue );
		}

	protected:
		void WriteCommand( uint8_t ACommand )
		{
			if( PowerDown() )
				ACommand |= 0b00010000;

			if( Receiver().PowerDown().GetValue() )
				ACommand |= 0b00100000;

			PCD_WriteRegister( MFRC522Const::CommandReg, ACommand );
		}

		inline void PCD_WriteRegister( uint8_t reg, uint8_t value )
		{
			if( C_1_SHIFT_REGISTERS )
				reg <<= 1;
/*
			Serial.print( "W: " );
			Serial.print( reg >> 1, HEX );
			Serial.print( " " );
			Serial.println( value, BIN );
//			delay( 5 );
*/
			T_0_IMPLEMENTATION::WriteRegister8( reg, value );
		}

		inline uint8_t PCD_ReadRegister( uint8_t reg )
		{

			if( C_1_SHIFT_REGISTERS )
				reg <<= 1;
/*
			uint8_t AValue = T_0_IMPLEMENTATION::ReadRegister8( reg );
			Serial.print( "R: " );
			Serial.print( reg >> 1, HEX );
			Serial.print( " " );
			Serial.println( AValue, BIN );
//			delay( 5 );
			return AValue;
*/
			return T_0_IMPLEMENTATION::ReadRegister8( reg );
		}


		void PCD_ReadRegister(	uint8_t reg,	///< The register to read from. One of the PCD_Register enums.
								uint8_t count,			///< The number of bytes to read
								uint8_t *values,		///< Byte array to store the values in.
								uint8_t rxAlign		///< Only bit positions rxAlign..7 in values[0] are updated.
								)
		{
			if (count == 0)
				return;

			if( C_1_SHIFT_REGISTERS )
				reg <<= 1;

			T_0_IMPLEMENTATION::PCD_ReadRepeatRegister( reg, count, values, rxAlign );
/*
			Serial.print( "R: " );
			Serial.print( reg >> 1, HEX );
			Serial.print( " " );
			for( int i = 0; i < count; ++ i )
			{
				Serial.print( values[ i ], BIN );
				if( i < count - 1 )
					Serial.print( ", " );

			}

			Serial.println();

//			delay( 5 );
*/
		}

		/**
		 * Writes a number of bytes to the specified register in the MFRC522 chip.
		 * The interface is described in the datasheet section 8.1.2.
		 */
		void PCD_WriteRegister(	uint8_t reg,	///< The register to write to. One of the PCD_Register enums.
								uint8_t count,			///< The number of bytes to write to the register
								uint8_t *values		///< The values to write. Byte array.
							)
		{
			if( C_1_SHIFT_REGISTERS )
				reg <<= 1;

			T_0_IMPLEMENTATION::BeginTransaction();

			T_0_IMPLEMENTATION::Write8(reg);						// MSB == 0 is for writing. LSB is not used in address. Datasheet section 8.1.2.3.
			for( uint8_t index = 0; index < count; ++ index )
				T_0_IMPLEMENTATION::Write8( values[ index ] );

			T_0_IMPLEMENTATION::EndTransaction();
/*
			SPI.beginTransaction(SPISettings(MFRC522_SPICLOCK, MSBFIRST, SPI_MODE0));	// Set the settings to work with SPI bus
			digitalWrite(_chipSelectPin, LOW);		// Select slave
			SPI.transfer(reg);						// MSB == 0 is for writing. LSB is not used in address. Datasheet section 8.1.2.3.
			for( uint8_t index = 0; index < count; ++ index )
				SPI.transfer(values[index]);

			digitalWrite(_chipSelectPin, HIGH);		// Release slave again
			SPI.endTransaction(); // Stop using the SPI bus
*/
		} // End PCD_WriteRegister()

		/**
		 * Sets the bits given in mask in register reg.
		 */
		void PCD_SetRegisterBitMask(	uint8_t reg,	///< The register to update. One of the PCD_Register enums.
										uint8_t mask			///< The bits to set.
									)
		{
			uint8_t tmp = PCD_ReadRegister( reg );
			PCD_WriteRegister(reg, tmp | mask);			// set bit mask
		}

		/**
		 * Clears the bits given in mask from register reg.
		 */
		void PCD_ClearRegisterBitMask(	uint8_t reg,	///< The register to update. One of the PCD_Register enums.
												uint8_t mask			///< The bits to clear.
											  ) 
		{
			uint8_t tmp = PCD_ReadRegister(reg);
			PCD_WriteRegister(reg, tmp & (~mask));		// clear bit mask
		} // End PCD_ClearRegisterBitMask()

	public:
		void Reset()
		{
			// PERFORM RESRET HERE!!!
			if( T_ResetOutputPin::GetPinIsConnected() )
			{
				T_ResetOutputPin::SetPinValueLow();		// Make sure we have a clean LOW state.
				delayMicroseconds( 2 );				// 8.8.1 Reset timing requirements says about 100ns. Let us be generous: 2μsl
				T_ResetOutputPin::SetPinValueHigh();		// Exit power down mode. This triggers a hard reset.
				// Section 8.8.2 in the datasheet says the oscillator start-up time is the start up time of the crystal + 37,74μs. Let us be generous: 50ms.
				delay( 50 );
			}

			else
			{
//				Serial.println( "RESET" );
				WriteCommand( MFRC522Const::PCD_SoftReset );	// Issue the SoftReset command.
				// The datasheet does not mention how long the SoftRest command takes to complete.
				// But the MFRC522 might have been in soft power-down mode (triggered by bit 4 of CommandReg) 
				// Section 8.8.2 in the datasheet says the oscillator start-up time is the start up time of the crystal + 37,74μs. Let us be generous: 50ms.
				uint8_t count = 0;
				do {
					// Wait for the PowerDown bit in CommandReg to be cleared (max 3x50ms)
					delay(50);
				} while (( PCD_ReadRegister( MFRC522Const::CommandReg ) & ( 1 << 4 )) && ( ++ count ) < 3 );

//				Serial.println( "RESET DONE" );
			}
			
			if( T_VersionOutputPin::GetPinIsConnected() )
				T_VersionOutputPin::SetPinValue( PCD_ReadRegister( MFRC522Const::VersionReg ) & 0x0F );

			UpdateComIEnReg();
			UpdateDivIEnReg();
			UpdateTxModeReg();
			UpdateRxModeReg();
			UpdateMfRxReg();
			UpdateMfTxReg();

			UpdateModWidthReg();

			PCD_WriteRegister( MFRC522Const::TModeReg, 0x80 );			// TAuto=1; timer starts automatically at the end of the transmission in all communication modes at all speeds
			PCD_WriteRegister( MFRC522Const::TPrescalerReg, 0xA9 );		// TPreScaler = TModeReg[3..0]:TPrescalerReg, ie 0x0A9 = 169 => f_timer=40kHz, ie a timer period of 25μs.
			PCD_WriteRegister( MFRC522Const::TReloadRegH, 0x03 );		// Reload timer with 0x3E8 = 1000, ie 25ms before timeout.
			PCD_WriteRegister( MFRC522Const::TReloadRegL, 0xE8 );

			UpdateTxASKReg();
			UpdateModeReg();

			UpdateTxControlReg();

			UpdateRFCfgReg();
			UpdateTxSelReg();
			UpdateGsNReg();
			UpdateCWGsPReg();
			UpdateModGsPReg();
			UpdateRxSelReg();
			UpdateRxThresholdReg();
			UpdateDemodReg();

/*
			// Reset baud rates
			PCD_WriteRegister(TxModeReg, 0x00);
			PCD_WriteRegister(RxModeReg, 0x00);
			// Reset ModWidthReg
			PCD_WriteRegister(ModWidthReg, 0x26);

			// When communicating with a PICC we need a timeout if something goes wrong.
			// f_timer = 13.56 MHz / (2*TPreScaler+1) where TPreScaler = [TPrescaler_Hi:TPrescaler_Lo].
			// TPrescaler_Hi are the four low bits in TModeReg. TPrescaler_Lo is TPrescalerReg.
			PCD_WriteRegister(TModeReg, 0x80);			// TAuto=1; timer starts automatically at the end of the transmission in all communication modes at all speeds
			PCD_WriteRegister(TPrescalerReg, 0xA9);		// TPreScaler = TModeReg[3..0]:TPrescalerReg, ie 0x0A9 = 169 => f_timer=40kHz, ie a timer period of 25μs.
			PCD_WriteRegister(TReloadRegH, 0x03);		// Reload timer with 0x3E8 = 1000, ie 25ms before timeout.
			PCD_WriteRegister(TReloadRegL, 0xE8);
	
			PCD_WriteRegister(TxASKReg, 0x40);		// Default 0x00. Force a 100 % ASK modulation independent of the ModGsPReg register setting
			PCD_WriteRegister(ModeReg, 0x3D);		// Default 0x3F. Set the preset value for the CRC coprocessor for the CalcCRC command to 0x6363 (ISO 14443-3 part 6.2.4)
			PCD_AntennaOn();						// Enable the antenna driver pins TX1 and TX2 (they were disabled by the reset)
		*/
		}

		/**
		 * Executes the Transceive command.
		 * CRC validation can only be done if backData and backLen are specified.
		 * 
		 * @return STATUS_OK on success, STATUS_??? otherwise.
		 */
		inline MFRC522Const::StatusCode PCD_TransceiveData(	uint8_t *sendData,		///< Pointer to the data to transfer to the FIFO.
															uint8_t sendLen,		///< Number of bytes to transfer to the FIFO.
															uint8_t *backData,		///< nullptr or pointer to buffer if data should be read back after executing the command.
															uint8_t *backLen,		///< In: Max number of bytes to write to *backData. Out: The number of bytes returned.
															uint8_t *validBits = nullptr,	///< In/Out: The number of valid bits in the last byte. 0 for 8 valid bits. Default nullptr.
															uint8_t rxAlign = 0,		///< In: Defines the bit position in backData[0] for the first bit received. Default 0.
															bool checkCRC = false		///< In: True => The last two bytes of the response is assumed to be a CRC_A that must be validated.
										 ) 
		{
			uint8_t waitIRq = 0x30;		// RxIRq and IdleIRq
			return PCD_CommunicateWithPICC( MFRC522Const::PCD_Transceive, waitIRq, sendData, sendLen, backData, backLen, validBits, rxAlign, checkCRC);
		} // End PCD_TransceiveData()

		/**
		 * Use the CRC coprocessor in the MFRC522 to calculate a CRC_A.
		 *
		 * @return STATUS_OK on success, STATUS_??? otherwise.
		 */
		MFRC522Const::StatusCode PCD_CalculateCRC(	uint8_t *data,		///< In: Pointer to the data to transfer to the FIFO for CRC calculation.
														uint8_t length,	///< In: The number of bytes to transfer.
														uint8_t *result	///< Out: Pointer to result buffer. Result is written to result[0..1], low byte first.
							 )
		{
			WriteCommand( MFRC522Const::PCD_Idle );		// Stop any active command.
			PCD_WriteRegister( MFRC522Const::DivIrqReg, 0x04 );				// Clear the CRCIRq interrupt request bit
			PCD_WriteRegister( MFRC522Const::FIFOLevelReg, 0x80 );			// FlushBuffer = 1, FIFO initialization
			PCD_WriteRegister( MFRC522Const::FIFODataReg, length, data );	// Write data to the FIFO
			WriteCommand( MFRC522Const::PCD_CalcCRC );		// Start the calculation

			// Wait for the CRC calculation to complete. Check for the register to
			// indicate that the CRC calculation is complete in a loop. If the
			// calculation is not indicated as complete in ~90ms, then time out
			// the operation.
			const uint32_t deadline = millis() + 89;

			do {
				// DivIrqReg[7..0] bits are: Set2 reserved reserved MfinActIRq reserved CRCIRq reserved reserved
				uint8_t n = PCD_ReadRegister( MFRC522Const::DivIrqReg );
				if (n & 0x04)
				{									// CRCIRq bit set - calculation done
					WriteCommand( MFRC522Const::PCD_Idle );	// Stop calculating CRC for new content in the FIFO.
					// Transfer the result from the registers to the result buffer
					result[0] = PCD_ReadRegister( MFRC522Const::CRCResultRegL );
					result[1] = PCD_ReadRegister( MFRC522Const::CRCResultRegH );
					return MFRC522Const::STATUS_OK;
				}
				yield();
			}
			while (static_cast<uint32_t> (millis()) < deadline);

			// 89ms passed and nothing happened. Communication with the MFRC522 might be down.
			return MFRC522Const::STATUS_TIMEOUT;
		} // End PCD_CalculateCRC()

		/**
		 * Transfers data to the MFRC522 FIFO, executes a command, waits for completion and transfers data back from the FIFO.
		 * CRC validation can only be done if backData and backLen are specified.
		 *
		 * @return STATUS_OK on success, STATUS_??? otherwise.
		 */
		MFRC522Const::StatusCode PCD_CommunicateWithPICC(	uint8_t command,		///< The command to execute. One of the PCD_Command enums.
																uint8_t waitIRq,		///< The bits in the ComIrqReg register that signals successful completion of the command.
																uint8_t *sendData,		///< Pointer to the data to transfer to the FIFO.
																uint8_t sendLen,		///< Number of bytes to transfer to the FIFO.
																uint8_t *backData = nullptr,		///< nullptr or pointer to buffer if data should be read back after executing the command.
																uint8_t *backLen = nullptr,		///< In: Max number of bytes to write to *backData. Out: The number of bytes returned.
																uint8_t *validBits = nullptr,	///< In/Out: The number of valid bits in the last byte. 0 for 8 valid bits.
																uint8_t rxAlign = 0,		///< In: Defines the bit position in backData[0] for the first bit received. Default 0.
																bool checkCRC = false		///< In: True => The last two bytes of the response is assumed to be a CRC_A that must be validated.
											 ) 
		{
			// Prepare values for BitFramingReg
			uint8_t txLastBits = validBits ? *validBits : 0;
			uint8_t bitFraming = (rxAlign << 4) + txLastBits;		// RxAlign = BitFramingReg[6..4]. TxLastBits = BitFramingReg[2..0]
	
			WriteCommand( MFRC522Const::PCD_Idle );			// Stop any active command.
			PCD_WriteRegister( MFRC522Const::ComIrqReg, 0x7F );					// Clear all seven interrupt request bits
			PCD_WriteRegister( MFRC522Const::FIFOLevelReg, 0x80 );				// FlushBuffer = 1, FIFO initialization
			PCD_WriteRegister( MFRC522Const::FIFODataReg, sendLen, sendData );	// Write sendData to the FIFO
			PCD_WriteRegister( MFRC522Const::BitFramingReg, bitFraming );		// Bit adjustments
			WriteCommand( command );				// Execute the command

//			Serial.print( "command = " );
//			Serial.println( command );

			if (command == MFRC522Const::PCD_Transceive)
				PCD_SetRegisterBitMask( MFRC522Const::BitFramingReg, 0x80 );	// StartSend=1, transmission of data starts
	
			// In PCD_Init() we set the TAuto flag in TModeReg. This means the timer
			// automatically starts when the PCD stops transmitting.
			//
			// Wait here for the command to complete. The bits specified in the
			// `waitIRq` parameter define what bits constitute a completed command.
			// When they are set in the ComIrqReg register, then the command is
			// considered complete. If the command is not indicated as complete in
			// ~36ms, then consider the command as timed out.
			const uint32_t deadline = millis() + 36;
			bool completed = false;

			do 
			{
				uint8_t n = PCD_ReadRegister( MFRC522Const::ComIrqReg );	// ComIrqReg[7..0] bits are: Set1 TxIRq RxIRq IdleIRq HiAlertIRq LoAlertIRq ErrIRq TimerIRq
//				Serial.print( waitIRq, BIN );
//				Serial.print( " " );
//				Serial.println( n, BIN );
				if (n & waitIRq) 
				{					// One of the interrupts that signal success has been set.
//					Serial.println( "COMPLETED" );
					completed = true;
					break;
				}

				if (n & 0x01)						// Timer interrupt - nothing received in 25ms
					return MFRC522Const::STATUS_TIMEOUT;

				yield();
			}
			while (static_cast<uint32_t> (millis()) < deadline);

			// 36ms and nothing happened. Communication with the MFRC522 might be down.
			if (!completed)
				return MFRC522Const::STATUS_TIMEOUT;

			// Stop now if any errors except collisions were detected.
			uint8_t errorRegValue = PCD_ReadRegister( MFRC522Const::ErrorReg ); // ErrorReg[7..0] bits are: WrErr TempErr reserved BufferOvfl CollErr CRCErr ParityErr ProtocolErr

			if (errorRegValue & 0x13)	 // BufferOvfl ParityErr ProtocolErr
				return MFRC522Const::STATUS_ERROR;

			uint8_t _validBits = 0;

			// If the caller wants data back, get it from the MFRC522.
			if (backData && backLen) {
				uint8_t n = PCD_ReadRegister( MFRC522Const::FIFOLevelReg );	// Number of bytes in the FIFO
				if (n > *backLen)
					return MFRC522Const::STATUS_NO_ROOM;

				*backLen = n;											// Number of bytes returned
				PCD_ReadRegister( MFRC522Const::FIFODataReg, n, backData, rxAlign );	// Get received data from FIFO
				_validBits = PCD_ReadRegister( MFRC522Const::ControlReg ) & 0x07;		// RxLastBits[2:0] indicates the number of valid bits in the last received byte. If this value is 000b, the whole byte is valid.
				if (validBits)
					*validBits = _validBits;

			}

			// Tell about collisions
			if (errorRegValue & 0x08)		// CollErr
				return MFRC522Const::STATUS_COLLISION;

			// Perform CRC_A validation if requested.
			if (backData && backLen && checkCRC) {
				// In this case a MIFARE Classic NAK is not OK.
				if (*backLen == 1 && _validBits == 4)
					return MFRC522Const::STATUS_MIFARE_NACK;

				// We need at least the CRC_A value and all 8 bits of the last byte must be received.
				if (*backLen < 2 || _validBits != 0)
					return MFRC522Const::STATUS_CRC_WRONG;

				// Verify CRC_A - do our own calculation and store the control in controlBuffer.
				uint8_t controlBuffer[2];
				MFRC522Const::StatusCode status = PCD_CalculateCRC(&backData[0], *backLen - 2, &controlBuffer[0]);
				if (status != MFRC522Const::STATUS_OK)
					return status;

				if ((backData[*backLen - 2] != controlBuffer[0]) || (backData[*backLen - 1] != controlBuffer[1]))
					return MFRC522Const::STATUS_CRC_WRONG;

			}

			return MFRC522Const::STATUS_OK;
		} // End PCD_CommunicateWithPICC()

		/**
		 * Transmits REQA or WUPA commands.
		 * Beware: When two PICCs are in the field at the same time I often get STATUS_TIMEOUT - probably due do bad antenna design.
		 * 
		 * @return STATUS_OK on success, STATUS_??? otherwise.
		 */ 
		MFRC522Const::StatusCode PICC_REQA_or_WUPA(	uint8_t command, 		///< The command to send - PICC_CMD_REQA or PICC_CMD_WUPA
														uint8_t *bufferATQA,	///< The buffer to store the ATQA (Answer to request) in
														uint8_t *bufferSize	///< Buffer size, at least two bytes. Also number of bytes returned if STATUS_OK.
													) 
		{
			if (bufferATQA == nullptr || *bufferSize < 2)	// The ATQA response is 2 bytes long.
				return MFRC522Const::STATUS_NO_ROOM;

//			Serial.println( "TEST1" );

			PCD_WriteRegister( MFRC522Const::CollReg, 0 );
//			PCD_ClearRegisterBitMask(CollReg, 0x80);		// ValuesAfterColl=1 => Bits received after collision are cleared.
			uint8_t validBits = 7;									// For REQA and WUPA we need the short frame format - transmit only 7 bits of the last (and only) byte. TxLastBits = BitFramingReg[2..0]

			MFRC522Const::StatusCode status = PCD_TransceiveData(&command, 1, bufferATQA, bufferSize, &validBits);
//			Serial.print( "status = " );
//			Serial.println( status );
			if (status != MFRC522Const::STATUS_OK)
				return status;

//			Serial.println( "TEST 2" );
			if (*bufferSize != 2 || validBits != 0)		// ATQA must be exactly 16 bits.
				return MFRC522Const::STATUS_ERROR;

//			Serial.println( "TEST 3" );
			return MFRC522Const::STATUS_OK;
		}

		inline MFRC522Const::StatusCode PICC_RequestA(
				uint8_t *bufferATQA,	///< The buffer to store the ATQA (Answer to request) in
				uint8_t *bufferSize	///< Buffer size, at least two bytes. Also number of bytes returned if STATUS_OK.
			)
		{
			return PICC_REQA_or_WUPA( MFRC522Const::PICC_CMD_REQA, bufferATQA, bufferSize );
		}

		bool PICC_IsNewCardPresent() 
		{
/*
			// Reset baud rates
			PCD_WriteRegister( MFRC522Const::TxModeReg, 0x00 ); // ???????????????
			PCD_WriteRegister( MFRC522Const::RxModeReg, 0x00 ); // ???????????????
			// Reset ModWidthReg
			UpdateModWidthReg();
//			PCD_WriteRegister( MFRC522Const::ModWidthReg, 0x26 ); // 00100110
*/
			uint8_t bufferATQA[2] = { 0 };
			uint8_t bufferSize = sizeof(bufferATQA);

			MFRC522Const::StatusCode result = PICC_RequestA(bufferATQA, &bufferSize);
			if (result == MFRC522Const::STATUS_OK || result == MFRC522Const::STATUS_COLLISION) 
			{
				FAtqa = ((uint16_t)bufferATQA[1] << 8) | bufferATQA[0];
				FAts.size = 0;
				FAts.fsc = 32;	// default FSC value

				// Defaults for TA1
				FAts.ta1.transmitted = false;
				FAts.ta1.sameD = false;
				FAts.ta1.ds = MFRC522Const::BITRATE_106KBITS;
				FAts.ta1.dr = MFRC522Const::BITRATE_106KBITS;

				// Defaults for TB1
				FAts.tb1.transmitted = false;
				FAts.tb1.fwi = 0;	// TODO: Don't know the default for this!
				FAts.tb1.sfgi = 0;	// The default value of SFGI is 0 (meaning that the card does not need any particular SFGT)

				// Defaults for TC1
				FAts.tc1.transmitted = false;
				FAts.tc1.supportsCID = true;
				FAts.tc1.supportsNAD = false;

				memset(FAts.data, 0, MFRC522Const::FIFO_SIZE - 2);

//				tag.blockNumber = false;
				return true;
			}
//			Serial.println( uint8_t( result ));
			return ( result == MFRC522Const::STATUS_OK || result == MFRC522Const::STATUS_COLLISION );
		}

		/**
		 * Transmits Protocol and Parameter Selection Request (PPS) without parameter 1 
		 *
		 * @return STATUS_OK on success, STATUS_??? otherwise.
		 */
/*
		MFRC522Const::StatusCode PICC_PPS()
		{
			uint8_t ppsBuffer[4];
			uint8_t ppsBufferSize = 4;
			// Start byte: The start byte (PPS) consists of two parts:
			//  –The upper nibble(b8–b5) is set to’D'to identify the PPS. All other values are RFU.
			//  -The lower nibble(b4–b1), which is called the ‘card identifier’ (CID), defines the logical number of the addressed card.
			ppsBuffer[0] = 0xD0;	// CID is hardcoded as 0 in RATS
			ppsBuffer[1] = 0x00;	// PPS0 indicates whether PPS1 is present

			// Calculate CRC_A
			MFRC522Const::StatusCode result = PCD_CalculateCRC(ppsBuffer, 2, &ppsBuffer[2]);
			if (result != MFRC522Const::STATUS_OK)
				return result;

			// Transmit the buffer and receive the response, validate CRC_A.
			result = PCD_TransceiveData(ppsBuffer, 4, ppsBuffer, &ppsBufferSize, NULL, 0, true);
			if (result == MFRC522Const::STATUS_OK)
			{
				// Enable CRC for T=CL
				uint8_t txReg = PCD_ReadRegister(MFRC522Const::TxModeReg) | 0x80;
				uint8_t rxReg = PCD_ReadRegister(MFRC522Const::RxModeReg) | 0x80;

				PCD_WriteRegister(MFRC522Const::TxModeReg, txReg);
				PCD_WriteRegister(MFRC522Const::RxModeReg, rxReg);
			}

			return result;
		} // End PICC_PPS()
*/

		/**
		 * Transmits Protocol and Parameter Selection Request (PPS)
		 *
		 * @return STATUS_OK on success, STATUS_??? otherwise.
		 */
		MFRC522Const::StatusCode PICC_PPS( MFRC522Const::TagBitRates sendBitRate,	          ///< DS
											  MFRC522Const::TagBitRates receiveBitRate		  ///< DR
		) 
		{
			// TODO not used
			//uint8_t txReg = PCD_ReadRegister(TxModeReg) & 0x8F;
			//uint8_t rxReg = PCD_ReadRegister(RxModeReg) & 0x8F;

			uint8_t ppsBuffer[5];
			uint8_t ppsBufferSize = 5;
			// Start byte: The start byte (PPS) consists of two parts:
			//  –The upper nibble(b8–b5) is set to’D'to identify the PPS. All other values are RFU.
			//  -The lower nibble(b4–b1), which is called the ‘card identifier’ (CID), defines the logical number of the addressed card.
			ppsBuffer[0] = 0xD0;	// CID is hardcoded as 0 in RATS
			ppsBuffer[1] = 0x11;	// PPS0 indicates whether PPS1 is present

			// Bit 8 - Set to '0' as MFRC522 allows different bit rates for send and receive
			// Bit 4 - Set to '0' as it is Reserved for future use.
			//ppsBuffer[2] = (((sendBitRate & 0x03) << 4) | (receiveBitRate & 0x03)) & 0xE7;
			ppsBuffer[2] = (((sendBitRate & 0x03) << 2) | (receiveBitRate & 0x03)) & 0xE7;

			// Calculate CRC_A
			MFRC522Const::StatusCode result = PCD_CalculateCRC(ppsBuffer, 3, &ppsBuffer[3]);
			if (result != MFRC522Const::STATUS_OK)
				return result;
	
			// Transmit the buffer and receive the response, validate CRC_A.
			result = PCD_TransceiveData(ppsBuffer, 5, ppsBuffer, &ppsBufferSize, NULL, 0, true);
			if (result == MFRC522Const::STATUS_OK)
			{
				// Make sure it is an answer to our PPS
				// We should receive our PPS byte and 2 CRC bytes
				if ((ppsBufferSize == 3) && (ppsBuffer[0] == 0xD0)) 
				{
					uint8_t txReg = PCD_ReadRegister(MFRC522Const::TxModeReg) & 0x8F;
					uint8_t rxReg = PCD_ReadRegister(MFRC522Const::RxModeReg) & 0x8F;

					// Set bit rate and enable CRC for T=CL
					txReg = (txReg & 0x8F) | ((receiveBitRate & 0x03) << 4) | 0x80;
					rxReg = (rxReg & 0x8F) | ((sendBitRate & 0x03) << 4) | 0x80;
					rxReg &= 0xF0; //Enforce although this should be set already

					// From ConfigIsoType
					//rxReg |= 0x06;

					PCD_WriteRegister(MFRC522Const::TxModeReg, txReg);
					PCD_WriteRegister(MFRC522Const::RxModeReg, rxReg);

					// At 212kBps
					switch (sendBitRate) 
					{
						case MFRC522Const::BITRATE_212KBITS:
							//PCD_WriteRegister(ModWidthReg, 0x13);
							PCD_WriteRegister(MFRC522Const::ModWidthReg, 0x15);
							break;

						case MFRC522Const::BITRATE_424KBITS:
							PCD_WriteRegister(MFRC522Const::ModWidthReg, 0x0A);
							break;

						case MFRC522Const::BITRATE_848KBITS:
							PCD_WriteRegister(MFRC522Const::ModWidthReg, 0x05);
							break;

						default:
							PCD_WriteRegister(MFRC522Const::ModWidthReg, 0x26); // Default value
							break;
					}

					//PCD_WriteRegister(MFRC522Const::RxThresholdReg, 0x84); // ISO-14443.4 Type A (default)
					//PCD_WriteRegister(MFRC522Const::ControlReg, 0x10);
			
					delayMicroseconds(10);
				}

				else 
					return MFRC522Const::STATUS_ERROR;

			}

			return result;
		} // End PICC_PPS()

		/**
		 * Transmits a Request command for Answer To Select (ATS).
		 *
		 * @return STATUS_OK on success, STATUS_??? otherwise.
		 */
		MFRC522Const::StatusCode PICC_RequestATS( MFRC522Const::Ats *ats ) 
		{
			// TODO unused variable
			//byte count;
			MFRC522Const::StatusCode result;

			uint8_t bufferATS[ MFRC522Const::FIFO_SIZE ];
			uint8_t bufferSize = MFRC522Const::FIFO_SIZE;

			memset( bufferATS, 0, MFRC522Const::FIFO_SIZE );

			// Build command buffer
			bufferATS[0] = MFRC522Const::PICC_CMD_RATS;
	
			// The CID defines the logical number of the addressed card and has a range of 0 
			// through 14; 15 is reserved for future use (RFU).
			//
			// FSDI codes the maximum frame size (FSD) that the terminal can receive.
			//
			// FSDI        |  0  |  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  |  9-F
			// ------------+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----------
			// FSD (bytes) |  16 |  24 |  32 |  40 |  48 |  64 |  96 | 128 | 256 | RFU > 256
			//
			bufferATS[1] = 0x50; // FSD=64, CID=0

			// Calculate CRC_A
			result = PCD_CalculateCRC(bufferATS, 2, &bufferATS[2]);
			if (result != MFRC522Const::STATUS_OK)
				return result;

			// Transmit the buffer and receive the response, validate CRC_A.
			result = PCD_TransceiveData(bufferATS, 4, bufferATS, &bufferSize, NULL, 0, true);
			if (result != MFRC522Const::STATUS_OK)
				PICC_HaltA();

			// Set the ats structure data
			ats->size = bufferATS[0];

			// T0 byte:
			//
			// b8 | b7 | b6 | b5 | b4 | b3 | b2 | b1 | Meaning
			//----+----+----+----+----+----+----+----+---------------------------
			//  0 | ...| ...| ...| ...|... | ...| ...| Set to 0 (RFU)
			//  0 |  1 | x  |  x | ...|... | ...| ...| TC1 transmitted
			//  0 |  x | 1  |  x | ...|... | ...| ...| TB1 transmitted
			//  0 |  x | x  |  1 | ...|... | ...| ...| TA1 transmitted
			//  0 | ...| ...| ...|  x |  x |  x | x  | Maximum frame size (FSCI)
			//
			// FSCI        |  0  |  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  |  9-F
			// ------------+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----------
			// FSC (bytes) |  16 |  24 |  32 |  40 |  48 |  64 |  96 | 128 | 256 | RFU > 256
			//
			// Default FSCI is 2 (32 bytes)
			if (ats->size > 0x01)
			{
				// TC1, TB1 and TA1 where NOT transmitted
				ats->ta1.transmitted = bool(bufferATS[1] & 0x40);
				ats->tb1.transmitted = bool(bufferATS[1] & 0x20);
				ats->tc1.transmitted = bool(bufferATS[1] & 0x10);

				// Decode FSCI
				switch (bufferATS[1] & 0x0F)
				{
					case 0x00:
						ats->fsc = 16;
						break;
					case 0x01:
						ats->fsc = 24;
						break;
					case 0x02:
						ats->fsc = 32;
						break;
					case 0x03:
						ats->fsc = 40;
						break;
					case 0x04:
						ats->fsc = 48;
						break;
					case 0x05:
						ats->fsc = 64;
						break;
					case 0x06:
						ats->fsc = 96;
						break;
					case 0x07:
						ats->fsc = 128;
						break;
					case 0x08:
						// This value cannot be hold by a byte
						// The reason I ignore it is that MFRC255 FIFO is 64 bytes so this is not a possible value (or atleast it shouldn't)
						//ats->fsc = 256;
						break;
						// TODO: What to do with RFU (Reserved for future use)?
					default:
						break;
				}

				// TA1
				if (ats->ta1.transmitted)
				{
					ats->ta1.sameD = bool(bufferATS[2] & 0x80);
					ats->ta1.ds = MFRC522Const::TagBitRates( ( bufferATS[2] & 0x70 ) >> 4 );
					ats->ta1.dr = MFRC522Const::TagBitRates( bufferATS[2] & 0x07 );
				}
				else
				{
					// Default TA1
					ats->ta1.ds = MFRC522Const::BITRATE_106KBITS;
					ats->ta1.dr = MFRC522Const::BITRATE_106KBITS;
				}

				// TB1
				if (ats->tb1.transmitted)
				{
					uint8_t tb1Index = 2;

					if (ats->ta1.transmitted)
						tb1Index++;
			
					ats->tb1.fwi = (bufferATS[tb1Index] & 0xF0) >> 4;
					ats->tb1.sfgi = bufferATS[tb1Index] & 0x0F;
				}
				else
				{
					// Defaults for TB1
					ats->tb1.fwi = 0;	// TODO: Don't know the default for this!
					ats->tb1.sfgi = 0;	// The default value of SFGI is 0 (meaning that the card does not need any particular SFGT)
				}

				// TC1
				if (ats->tc1.transmitted)
				{
					uint8_t tc1Index = 2;

					if (ats->ta1.transmitted)
						tc1Index++;
					if (ats->tb1.transmitted)
						tc1Index++;

					ats->tc1.supportsCID = (bool)(bufferATS[tc1Index] & 0x02);
					ats->tc1.supportsNAD = (bool)(bufferATS[tc1Index] & 0x01);
				}
				else
				{
					// Defaults for TC1
					ats->tc1.supportsCID = true;
					ats->tc1.supportsNAD = false;
				}
			}
			else
			{
				// TC1, TB1 and TA1 where NOT transmitted
				ats->ta1.transmitted = false;
				ats->tb1.transmitted = false;
				ats->tc1.transmitted = false;

				// Default FSCI
				ats->fsc = 32;	// Defaults to FSCI 2 (32 bytes)

				// Default TA1
				ats->ta1.sameD = false;
				ats->ta1.ds = MFRC522Const::BITRATE_106KBITS;
				ats->ta1.dr = MFRC522Const::BITRATE_106KBITS;

				// Defaults for TB1
				ats->tb1.transmitted = false;
				ats->tb1.fwi = 0;	// TODO: Don't know the default for this!
				ats->tb1.sfgi = 0;	// The default value of SFGI is 0 (meaning that the card does not need any particular SFGT)

				// Defaults for TC1
				ats->tc1.transmitted = false;
				ats->tc1.supportsCID = true;
				ats->tc1.supportsNAD = false;
			}

			memcpy(ats->data, bufferATS, bufferSize - 2);

			return result;
		} // End PICC_RequestATS()

		/**
		 * Transmits SELECT/ANTICOLLISION commands to select a single PICC.
		 * Before calling this function the PICCs must be placed in the READY(*) state by calling PICC_RequestA() or PICC_WakeupA().
		 * On success:
		 * 		- The chosen PICC is in state ACTIVE(*) and all other PICCs have returned to state IDLE/HALT. (Figure 7 of the ISO/IEC 14443-3 draft.)
		 * 		- The UID size and value of the chosen PICC is returned in *uid along with the SAK.
		 *
		 * A PICC UID consists of 4, 7 or 10 bytes.
		 * Only 4 bytes can be specified in a SELECT command, so for the longer UIDs two or three iterations are used:
		 * 		UID size	Number of UID bytes		Cascade levels		Example of PICC
		 * 		========	===================		==============		===============
		 * 		single				 4						1				MIFARE Classic
		 * 		double				 7						2				MIFARE Ultralight
		 * 		triple				10						3				Not currently in use?
		 *
		 * @return STATUS_OK on success, STATUS_??? otherwise.
		 */
		MFRC522Const::StatusCode PICC_Select(	MFRC522Const::Uid *uid,			///< Pointer to Uid struct. Normally output, but can also be used to supply a known UID.
													uint8_t validBits = 0		///< The number of known UID bits supplied in *uid. Normally 0. If set you must also supply uid->size.
												 )
		{
			bool useCascadeTag;
			uint8_t cascadeLevel = 1;
			MFRC522Const::StatusCode result;
			uint8_t count;
			uint8_t checkBit;
			uint8_t index;
			uint8_t uidIndex;					// The first index in uid->uidByte[] that is used in the current Cascade Level.
			int8_t currentLevelKnownBits;		// The number of known UID bits in the current Cascade Level.
			uint8_t buffer[9];					// The SELECT/ANTICOLLISION commands uses a 7 byte standard frame + 2 bytes CRC_A
			uint8_t bufferUsed;				// The number of bytes used in the buffer, ie the number of bytes to transfer to the FIFO.
			uint8_t rxAlign;					// Used in BitFramingReg. Defines the bit position for the first bit received.
			uint8_t txLastBits;				// Used in BitFramingReg. The number of valid bits in the last transmitted byte.
			uint8_t *responseBuffer;
			uint8_t responseLength;

			// Description of buffer structure:
			//		Byte 0: SEL 				Indicates the Cascade Level: PICC_CMD_SEL_CL1, PICC_CMD_SEL_CL2 or PICC_CMD_SEL_CL3
			//		Byte 1: NVB					Number of Valid Bits (in complete command, not just the UID): High nibble: complete bytes, Low nibble: Extra bits.
			//		Byte 2: UID-data or CT		See explanation below. CT means Cascade Tag.
			//		Byte 3: UID-data
			//		Byte 4: UID-data
			//		Byte 5: UID-data
			//		Byte 6: BCC					Block Check Character - XOR of bytes 2-5
			//		Byte 7: CRC_A
			//		Byte 8: CRC_A
			// The BCC and CRC_A are only transmitted if we know all the UID bits of the current Cascade Level.
			//
			// Description of bytes 2-5: (Section 6.5.4 of the ISO/IEC 14443-3 draft: UID contents and cascade levels)
			//		UID size	Cascade level	Byte2	Byte3	Byte4	Byte5
			//		========	=============	=====	=====	=====	=====
			//		 4 bytes		1			uid0	uid1	uid2	uid3
			//		 7 bytes		1			CT		uid0	uid1	uid2
			//						2			uid3	uid4	uid5	uid6
			//		10 bytes		1			CT		uid0	uid1	uid2
			//						2			CT		uid3	uid4	uid5
			//						3			uid6	uid7	uid8	uid9

			// Sanity checks
			if( validBits > 80 )
				return MFRC522Const::STATUS_INVALID;

			// Prepare MFRC522
//			PCD_ClearRegisterBitMask( MFRC522Const::CollReg, 0x80 );		// ValuesAfterColl=1 => Bits received after collision are cleared.
			PCD_WriteRegister( MFRC522Const::CollReg, 0 );		// ValuesAfterColl=1 => Bits received after collision are cleared.

			// Repeat Cascade Level loop until we have a complete UID.
			bool uidComplete = false;
//			Serial.println( "START 88" );
			while (!uidComplete)
			{
				// Set the Cascade Level in the SEL byte, find out if we need to use the Cascade Tag in byte 2.
				switch (cascadeLevel)
				{
					case 1:
						buffer[0] = MFRC522Const::PICC_CMD_SEL_CL1;
						uidIndex = 0;
						useCascadeTag = validBits && uid->size > 4;	// When we know that the UID has more than 4 bytes
						break;

					case 2:
						buffer[0] = MFRC522Const::PICC_CMD_SEL_CL2;
						uidIndex = 3;
						useCascadeTag = validBits && uid->size > 7;	// When we know that the UID has more than 7 bytes
						break;

					case 3:
						buffer[0] = MFRC522Const::PICC_CMD_SEL_CL3;
						uidIndex = 6;
						useCascadeTag = false;						// Never used in CL3.
						break;

					default:
						return MFRC522Const::STATUS_INTERNAL_ERROR;
						break;
				}

				// How many UID bits are known in this Cascade Level?
				currentLevelKnownBits = validBits - (8 * uidIndex);
				if (currentLevelKnownBits < 0)
					currentLevelKnownBits = 0;

				// Copy the known bits from uid->uidByte[] to buffer[]
				index = 2; // destination index in buffer[]
				if (useCascadeTag)
					buffer[index++] = MFRC522Const::PICC_CMD_CT;

				uint8_t bytesToCopy = currentLevelKnownBits / 8 + (currentLevelKnownBits % 8 ? 1 : 0); // The number of bytes needed to represent the known bits for this level.
				if (bytesToCopy)
				{
					uint8_t maxBytes = useCascadeTag ? 3 : 4; // Max 4 bytes in each Cascade Level. Only 3 left if we use the Cascade Tag
					if (bytesToCopy > maxBytes)
						bytesToCopy = maxBytes;

					for (count = 0; count < bytesToCopy; ++ count )
						buffer[index++] = uid->uidByte[uidIndex + count];

				}
				// Now that the data has been copied we need to include the 8 bits in CT in currentLevelKnownBits
				if (useCascadeTag)
					currentLevelKnownBits += 8;

				// Repeat anti collision loop until we can transmit all UID bits + BCC and receive a SAK - max 32 iterations.
				bool selectDone = false;
				while (!selectDone)
				{
					// Find out how many bits and bytes to send and receive.
					if (currentLevelKnownBits >= 32)
					{ // All UID bits in this Cascade Level are known. This is a SELECT.
						//Serial.print(F("SELECT: currentLevelKnownBits=")); Serial.println(currentLevelKnownBits, DEC);
						buffer[1] = 0x70; // NVB - Number of Valid Bits: Seven whole bytes
						// Calculate BCC - Block Check Character
						buffer[6] = buffer[2] ^ buffer[3] ^ buffer[4] ^ buffer[5];
						// Calculate CRC_A
						result = PCD_CalculateCRC(buffer, 7, &buffer[7]);
						if (result != MFRC522Const::STATUS_OK)
							return result;

						txLastBits		= 0; // 0 => All 8 bits are valid.
						bufferUsed		= 9;
						// Store response in the last 3 bytes of buffer (BCC and CRC_A - not needed after tx)
						responseBuffer	= &buffer[6];
						responseLength	= 3;
					}

					else
					{ // This is an ANTICOLLISION.
						//Serial.print(F("ANTICOLLISION: currentLevelKnownBits=")); Serial.println(currentLevelKnownBits, DEC);
						txLastBits		= currentLevelKnownBits % 8;
						count			= currentLevelKnownBits / 8;	// Number of whole bytes in the UID part.
						index			= 2 + count;					// Number of whole bytes: SEL + NVB + UIDs
						buffer[1]		= (index << 4) + txLastBits;	// NVB - Number of Valid Bits
						bufferUsed		= index + (txLastBits ? 1 : 0);
						// Store response in the unused part of buffer
						responseBuffer	= &buffer[index];
						responseLength	= sizeof(buffer) - index;
					}

					// Set bit adjustments
					rxAlign = txLastBits;											// Having a separate variable is overkill. But it makes the next line easier to read.
					PCD_WriteRegister( MFRC522Const::BitFramingReg, (rxAlign << 4) + txLastBits);	// RxAlign = BitFramingReg[6..4]. TxLastBits = BitFramingReg[2..0]

//					Serial.println( "TEST1" );

					// Transmit the buffer and receive the response.
					result = PCD_TransceiveData( buffer, bufferUsed, responseBuffer, &responseLength, &txLastBits, rxAlign );
//					Serial.print( "result = " );
//					Serial.println( result );

					if( result == MFRC522Const::STATUS_COLLISION )
					{ // More than one PICC in the field => collision.
						uint8_t valueOfCollReg = PCD_ReadRegister( MFRC522Const::CollReg ); // CollReg[7..0] bits are: ValuesAfterColl reserved CollPosNotValid CollPos[4:0]
						if (valueOfCollReg & 0x20) // CollPosNotValid
							return MFRC522Const::STATUS_COLLISION; // Without a valid collision position we cannot continue

						uint8_t collisionPos = valueOfCollReg & 0x1F; // Values 0-31, 0 means bit 32.
						if( collisionPos == 0 )
							collisionPos = 32;

						if( collisionPos <= currentLevelKnownBits ) // No progress - should not happen
							return MFRC522Const::STATUS_INTERNAL_ERROR;

						// Choose the PICC with the bit set.
						currentLevelKnownBits	= collisionPos;
						count			= currentLevelKnownBits % 8; // The bit to modify
						checkBit		= (currentLevelKnownBits - 1) % 8;
						index			= 1 + (currentLevelKnownBits / 8) + (count ? 1 : 0); // First byte is index 0.
						buffer[index]	|= (1 << checkBit);
//						buffer[index]	|= (1 << count);
					}

					else if (result != MFRC522Const::STATUS_OK)
						return result;

					else
					{ // STATUS_OK
						if (currentLevelKnownBits >= 32) // This was a SELECT.
							selectDone = true; // No more anticollision
							// We continue below outside the while.

						else // This was an ANTICOLLISION.
							// We now have all 32 bits of the UID in this Cascade Level
							currentLevelKnownBits = 32;
							// Run loop again to do the SELECT.

					}
				} // End of while (!selectDone)

				// We do not check the CBB - it was constructed by us above.

				// Copy the found UID bytes from buffer[] to uid->uidByte[]
				index			= (buffer[2] == MFRC522Const::PICC_CMD_CT ) ? 3 : 2; // source index in buffer[]
				bytesToCopy		= (buffer[2] == MFRC522Const::PICC_CMD_CT ) ? 3 : 4;
				for (count = 0; count < bytesToCopy; count++)
					uid->uidByte[uidIndex + count] = buffer[index++];

//				Serial.println( "TEST 66" );

				// Check response SAK (Select Acknowledge)
				if (responseLength != 3 || txLastBits != 0) // SAK must be exactly 24 bits (1 byte + CRC_A).
					return MFRC522Const::STATUS_ERROR;

				// Verify CRC_A - do our own calculation and store the control in buffer[2..3] - those bytes are not needed anymore.
				result = PCD_CalculateCRC(responseBuffer, 1, &buffer[2]);
				if (result != MFRC522Const::STATUS_OK)
					return result;

				if ((buffer[2] != responseBuffer[1]) || (buffer[3] != responseBuffer[2]))
					return MFRC522Const::STATUS_CRC_WRONG;

				if (responseBuffer[0] & 0x04) // Cascade bit set - UID not complete yes
					cascadeLevel++;

				else
				{
					uidComplete = true;
					uid->sak = responseBuffer[0];
				}
			} // End of while (!uidComplete)

			// Set correct uid->size
			uid->size = 3 * cascadeLevel + 1;
	
			// IF SAK bit 6 = 1 then it is ISO/IEC 14443-4 (T=CL)
			// A Request ATS command should be sent
			// We also check SAK bit 3 is cero, as it stands for UID complete (1 would tell us it is incomplete)
			if ((uid->sak & 0x24) == 0x20) 
			{
				MFRC522Const::Ats ats;
				result = PICC_RequestATS( &ats );
				if (result == MFRC522Const::STATUS_OK) 
				{
					// Check the ATS
					if (ats.size > 0)
					{
						// TA1 has been transmitted?
						// PPS must be supported...
						if (ats.ta1.transmitted)
						{
							// TA1
							//  8 | 7 | 6 | 5 | 4 | 3 | 2 | 1 | Description
							// ---+---+---+---+---+---+---+---+------------------------------------------
							//  0 | - | - | - | 0 | - | - | - | Different D for each direction supported
							//  1 | - | - | - | 0 | - | - | - | Only same D for both direction supported
							//  - | x | x | x | 0 | - | - | - | DS (Send D)
							//  - | - | - | - | 0 | x | x | x | DR (Receive D)
							//
							// D to bitrate table
							//  3 | 2 | 1 | Value
							// ---+---+---+-----------------------------
							//  1 | - | - | 848 kBaud is supported
							//  - | 1 | - | 424 kBaud is supported
							//  - | - | 1 | 212 kBaud is supported
							//  0 | 0 | 0 | Only 106 kBaud is supported
							//
							// Note: 106 kBaud is always supported
							//
							// I have almost constant timeouts when changing speeds :(
							// default never used, so only delarate
							//TagBitRates ds = BITRATE_106KBITS; 
							//TagBitRates dr = BITRATE_106KBITS;
							MFRC522Const::TagBitRates ds;
							MFRC522Const::TagBitRates dr;
					
							//// TODO Not working at 848 or 424
							//if (ats.ta1.ds & 0x04) 
							//{
							//	ds = BITRATE_848KBITS;
							//} 
							//else if (ats.ta1.ds & 0x02)
							//{
							//	ds = BITRATE_424KBITS;
							//}
							//else if (ats.ta1.ds & 0x01)
							//{
							//	ds = BITRATE_212KBITS;
							//}
							//else 
							//{
							//	ds = BITRATE_106KBITS;
							//}

							if (ats.ta1.ds & 0x01)
								ds = MFRC522Const::BITRATE_212KBITS;

							else 
								ds = MFRC522Const::BITRATE_106KBITS;

							//// Not working at 848 or 424
							//if (ats.ta1.dr & 0x04) 
							//{
							//	dr = BITRATE_848KBITS;
							//} 
							//else if (ats.ta1.dr & 0x02)
							//{
							//	dr = BITRATE_424KBITS;
							//}
							//else if (ats.ta1.dr & 0x01)
							//{
							//	dr = BITRATE_212KBITS;
							//}
							//else 
							//{
							//	dr = BITRATE_106KBITS;
							//}

							if (ats.ta1.dr & 0x01)
								dr = MFRC522Const::BITRATE_212KBITS;

							else
								dr = MFRC522Const::BITRATE_106KBITS;

							PICC_PPS(ds, dr);
						}
					}
				}
			}

			return MFRC522Const::STATUS_OK;
		}

		/**
		 * Transmits a Wake-UP command, Type A. Invites PICCs in state IDLE and HALT to go to READY(*) and prepare for anticollision or selection. 7 bit frame.
		 * Beware: When two PICCs are in the field at the same time I often get STATUS_TIMEOUT - probably due do bad antenna design.
		 * 
		 * @return STATUS_OK on success, STATUS_??? otherwise.
		 */
		inline MFRC522Const::StatusCode PICC_WakeupA(	uint8_t *bufferATQA,	///< The buffer to store the ATQA (Answer to request) in
													uint8_t *bufferSize	///< Buffer size, at least two bytes. Also number of bytes returned if STATUS_OK.
												) 
		{
			return PICC_REQA_or_WUPA( MFRC522Const::PICC_CMD_WUPA, bufferATQA, bufferSize);
		} // End PICC_WakeupA()

		bool PICC_IsAnyCardPresent() 
		{
			uint8_t bufferATQA[2];
			uint8_t bufferSize = sizeof(bufferATQA);
  
			// Reset baud rates
//			PCD_WriteRegister(rfid.TxModeReg, 0x00);
//			PCD_WriteRegister(rfid.RxModeReg, 0x00);
			// Reset ModWidthReg
//			PCD_WriteRegister(rfid.ModWidthReg, 0x26);
  
			MFRC522Const::StatusCode result = PICC_WakeupA( bufferATQA, &bufferSize );
			return (result == MFRC522Const::STATUS_OK || result == MFRC522Const::STATUS_COLLISION);
		}

		/**
		 * Instructs a PICC in state ACTIVE(*) to go to state HALT.
		 *
		 * @return STATUS_OK on success, STATUS_??? otherwise.
		 */ 
		MFRC522Const::StatusCode PICC_HaltA() 
		{
			uint8_t buffer[4];
	
			// Build command buffer
			buffer[0] = MFRC522Const::PICC_CMD_HLTA;
			buffer[1] = 0;
			// Calculate CRC_A
			MFRC522Const::StatusCode result = PCD_CalculateCRC(buffer, 2, &buffer[2]);
			if (result != MFRC522Const::STATUS_OK)
				return result;
	
			// Send the command.
			// The standard says:
			//		If the PICC responds with any modulation during a period of 1 ms after the end of the frame containing the
			//		HLTA command, this response shall be interpreted as 'not acknowledge'.
			// We interpret that this way: Only STATUS_TIMEOUT is a success.
			result = PCD_TransceiveData(buffer, sizeof(buffer), nullptr, 0);
			if (result == MFRC522Const::STATUS_TIMEOUT)
				return MFRC522Const::STATUS_OK;

			if (result == MFRC522Const::STATUS_OK) // That is ironically NOT ok in this case ;-)
				return MFRC522Const::STATUS_ERROR;

			return result;
		} // End PICC_HaltA()

		/**
		 * Wrapper for MIFARE protocol communication.
		 * Adds CRC_A, executes the Transceive command and checks that the response is MF_ACK or a timeout.
		 * 
		 * @return STATUS_OK on success, STATUS_??? otherwise.
		 */
		MFRC522Const::StatusCode PCD_MIFARE_Transceive(	uint8_t *sendData,		///< Pointer to the data to transfer to the FIFO. Do NOT include the CRC_A.
															uint8_t sendLen,		///< Number of bytes in sendData.
															bool acceptTimeout = false	///< True => A timeout is also success
														) 
		{
			uint8_t cmdBuffer[18]; // We need room for 16 bytes data and 2 bytes CRC_A.
	
			// Sanity check
			if (sendData == nullptr || sendLen > 16)
				return MFRC522Const::STATUS_INVALID;
	
			// Copy sendData[] to cmdBuffer[] and add CRC_A
			memcpy(cmdBuffer, sendData, sendLen);
			MFRC522Const::StatusCode result = PCD_CalculateCRC(cmdBuffer, sendLen, &cmdBuffer[sendLen]);
			if (result != MFRC522Const::STATUS_OK)
				return result;

			sendLen += 2;
	
			// Transceive the data, store the reply in cmdBuffer[]
			uint8_t waitIRq = 0x30;		// RxIRq and IdleIRq
			uint8_t cmdBufferSize = sizeof(cmdBuffer);
			uint8_t validBits = 0;
			result = PCD_CommunicateWithPICC( MFRC522Const::PCD_Transceive, waitIRq, cmdBuffer, sendLen, cmdBuffer, &cmdBufferSize, &validBits );
			if (acceptTimeout && result == MFRC522Const::STATUS_TIMEOUT)
				return MFRC522Const::STATUS_OK;

			if (result != MFRC522Const::STATUS_OK)
				return result;

			// The PICC must reply with a 4 bit ACK
			if (cmdBufferSize != 1 || validBits != 4)
				return MFRC522Const::STATUS_ERROR;

			if (cmdBuffer[0] != MFRC522Const::MF_ACK)
				return MFRC522Const::STATUS_MIFARE_NACK;

			return MFRC522Const::STATUS_OK;
		} // End PCD_MIFARE_Transceive()

		void SendCardInfo()
		{
			if( Card().TypeOutputPin().GetPinIsConnected() )
			{
				// http://www.nxp.com/documents/application_note/AN10833.pdf
				// 3.2 Coding of Select Acknowledge (SAK)
				// ignore 8-bit (iso14443 starts with LSBit = bit 1)
				// fixes wrong type for manufacturer Infineon (http://nfc-tools.org/index.php?title=ISO14443A)
				switch ( FUid.sak & 0x7F )
				{
//							case 0x04:	Atext =  return PICC_TYPE_NOT_COMPLETE;	// UID not complete
					case 0x09:	Card().TypeOutputPin().SetPinValue( "MIFARE Mini, 320 bytes" ); break;
					case 0x08:	Card().TypeOutputPin().SetPinValue( "MIFARE 1KB" ); break;
					case 0x18:	Card().TypeOutputPin().SetPinValue( "MIFARE 4KB" ); break;
					case 0x00:	Card().TypeOutputPin().SetPinValue( "MIFARE Ultralight or Ultralight C" ); break;
					case 0x10:
					case 0x11:	Card().TypeOutputPin().SetPinValue( "MIFARE Plus" ); break;
					case 0x01:	Card().TypeOutputPin().SetPinValue( "MIFARE TNP3XXX" ); break;
					case 0x20:
						if ( FAtqa == 0x0344 )
						{
							Card().TypeOutputPin().SetPinValue( "MIFARE DESFire" ); 
							break;
						}

						Card().TypeOutputPin().SetPinValue( "PICC compliant with ISO/IEC 14443-4" ); break;
					case 0x40:	Card().TypeOutputPin().SetPinValue( "PICC compliant with ISO/IEC 18092 (NFC)" ); break;
					default:	Card().TypeOutputPin().SetPinValue( "Unknown" ); break;
				}
			} // End PICC_GetType()

/*
			if( Card().StrongModulationOutputPin().GetPinIsConnected() )
			{
				uint8_t buffer[ 18 ];
				uint8_t size = sizeof( buffer );

	//				Serial.println( "READ" );
				MFRC522Const::StatusCode status = MIFARE_ReadCheckCRC( 0, buffer, &size );
	//				Serial.println( status );
	//				Serial.println( C_OWNER.GetStatusCodeName( status ));

				if( status != MFRC522Const::STATUS_OK )
				{
//					Card().ErrorOutputPin().ClockPin();
					return;
				}

				Serial.println( buffer[ 11 ], BIN );
				Card().StrongModulationOutputPin().SetPinValue( ( buffer[ 11 ] & 0x20 ) != 0 );
			}
*/
			Card().IDOutputPin().SendPinValue( Mitov::TDataBlock( FUid.size, FUid.uidByte ));
		}

		/**
		 * Executes the MFRC522 MFAuthent command.
		 * This command manages MIFARE authentication to enable a secure communication to any MIFARE Mini, MIFARE 1K and MIFARE 4K card.
		 * The authentication is described in the MFRC522 datasheet section 10.3.1.9 and http://www.nxp.com/documents/data_sheet/MF1S503x.pdf section 10.1.
		 * For use with MIFARE Classic PICCs.
		 * The PICC must be selected - ie in state ACTIVE(*) - before calling this function.
		 * Remember to call PCD_StopCrypto1() after communicating with the authenticated PICC - otherwise no new communications can start.
		 * 
		 * All keys are set to FFFFFFFFFFFFh at chip delivery.
		 * 
		 * @return STATUS_OK on success, STATUS_??? otherwise. Probably STATUS_TIMEOUT if you supply the wrong key.
		 */
		MFRC522Const::StatusCode PCD_Authenticate( uint8_t command,		///< PICC_CMD_MF_AUTH_KEY_A or PICC_CMD_MF_AUTH_KEY_B
													uint8_t blockAddr, 	///< The block number. See numbering in the comments in the .h file.
													const uint8_t *key,	///< Pointer to the Crypteo1 key to use (6 bytes)
													MFRC522Const::Uid *uid			///< Pointer to Uid struct. The first 4 bytes of the UID is used.
													) 
		{
			uint8_t waitIRq = 0x10;		// IdleIRq
	
			// Build command buffer
			uint8_t sendData[12];
			sendData[0] = command;
			sendData[1] = blockAddr;
			for ( uint8_t i = 0; i < MFRC522Const::MF_KEY_SIZE; ++ i ) 
				sendData[2+i] = key[ i ];	// 6 key bytes

			// Use the last uid bytes as specified in http://cache.nxp.com/documents/application_note/AN10927.pdf
			// section 3.2.5 "MIFARE Classic Authentication".
			// The only missed case is the MF1Sxxxx shortcut activation,
			// but it requires cascade tag (CT) byte, that is not part of uid.
			for ( uint8_t i = 0; i < 4; ++ i ) 
				sendData[8+i] = uid->uidByte[i+uid->size-4]; // The last 4 bytes of the UID
	
			// Start the authentication.
			return PCD_CommunicateWithPICC( MFRC522Const::PCD_MFAuthent, waitIRq, &sendData[0], sizeof(sendData));
		} // End PCD_Authenticate()

		/**
		 * Reads 16 bytes (+ 2 bytes CRC_A) from the active PICC.
		 * 
		 * For MIFARE Classic the sector containing the block must be authenticated before calling this function.
		 * 
		 * For MIFARE Ultralight only addresses 00h to 0Fh are decoded.
		 * The MF0ICU1 returns a NAK for higher addresses.
		 * The MF0ICU1 responds to the READ command by sending 16 bytes starting from the page address defined by the command argument.
		 * For example; if blockAddr is 03h then pages 03h, 04h, 05h, 06h are returned.
		 * A roll-back is implemented: If blockAddr is 0Eh, then the contents of pages 0Eh, 0Fh, 00h and 01h are returned.
		 * 
		 * The buffer must be at least 18 bytes because a CRC_A is also returned.
		 * Checks the CRC_A before returning STATUS_OK.
		 * 
		 * @return STATUS_OK on success, STATUS_??? otherwise.
		 */
		MFRC522Const::StatusCode MIFARE_Read(	uint8_t blockAddr, 	///< MIFARE Classic: The block (0-0xff) number. MIFARE Ultralight: The first page to return data from.
													uint8_t *buffer,		///< The buffer to store the data in
													uint8_t *bufferSize	///< Buffer size, at least 18 bytes. Also number of bytes returned if STATUS_OK.
												) 
		{
			MFRC522Const::StatusCode result;
	
			// Sanity check
			if( buffer == nullptr || *bufferSize < 18 )
				return MFRC522Const::STATUS_NO_ROOM;
	
			// Build command buffer
			buffer[0] = MFRC522Const::PICC_CMD_MF_READ;
			buffer[1] = blockAddr;
			// Calculate CRC_A
			result = PCD_CalculateCRC( buffer, 2, &buffer[2] );
			if( result != MFRC522Const::STATUS_OK )
				return result;
	
			// Transmit the buffer and receive the response, validate CRC_A.
			return PCD_TransceiveData(buffer, 4, buffer, bufferSize, nullptr, 0, true);
		} // End MIFARE_Read()

		MFRC522Const::StatusCode MIFARE_ReadCheckCRC(	uint8_t blockAddr, 	///< MIFARE Classic: The block (0-0xff) number. MIFARE Ultralight: The first page to return data from.
													uint8_t *buffer,		///< The buffer to store the data in
													uint8_t *bufferSize	///< Buffer size, at least 18 bytes. Also number of bytes returned if STATUS_OK.
												) 
		{
			MFRC522Const::StatusCode status = MIFARE_Read( blockAddr, buffer, bufferSize );
//				Serial.println( status );
//				Serial.println( C_OWNER.GetStatusCodeName( status ));

			if( status != MFRC522Const::STATUS_OK )
				return status;

			uint8_t ACRCBufferOut[2];
	
			// Calculate CRC_A
			status = PCD_CalculateCRC( buffer, *bufferSize - 2, ACRCBufferOut );
			if( status != MFRC522Const::STATUS_OK )
				return status;

			if( ( ACRCBufferOut[ 0 ] != buffer[ *bufferSize - 2 ] ) || ( ACRCBufferOut[ 1 ] != buffer[ *bufferSize - 1 ] ))
				return MFRC522Const::STATUS_CRC_WRONG;

			return MFRC522Const::STATUS_OK;
		}

		/**
		 * Writes 16 bytes to the active PICC.
		 * 
		 * For MIFARE Classic the sector containing the block must be authenticated before calling this function.
		 * 
		 * For MIFARE Ultralight the operation is called "COMPATIBILITY WRITE".
		 * Even though 16 bytes are transferred to the Ultralight PICC, only the least significant 4 bytes (bytes 0 to 3)
		 * are written to the specified address. It is recommended to set the remaining bytes 04h to 0Fh to all logic 0.
		 * * 
		 * @return STATUS_OK on success, STATUS_??? otherwise.
		 */
		MFRC522Const::StatusCode MIFARE_Write(	uint8_t blockAddr, ///< MIFARE Classic: The block (0-0xff) number. MIFARE Ultralight: The page (2-15) to write to.
													uint8_t *buffer,	///< The 16 bytes to write to the PICC
													uint8_t bufferSize	///< Buffer size, must be at least 16 bytes. Exactly 16 bytes are written.
												) 
		{
			// Sanity check
			if (buffer == nullptr || bufferSize < 16)
				return MFRC522Const::STATUS_INVALID;
	
			// Mifare Classic protocol requires two communications to perform a write.
			// Step 1: Tell the PICC we want to write to block blockAddr.
			uint8_t cmdBuffer[2];
			cmdBuffer[0] = MFRC522Const::PICC_CMD_MF_WRITE;
			cmdBuffer[1] = blockAddr;
			MFRC522Const::StatusCode result = PCD_MIFARE_Transceive(cmdBuffer, 2); // Adds CRC_A and checks that the response is MF_ACK.
			if (result != MFRC522Const::STATUS_OK)
				return result;
	
			// Step 2: Transfer the data
			return PCD_MIFARE_Transceive(buffer, bufferSize); // Adds CRC_A and checks that the response is MF_ACK.
		} // End MIFARE_Write()

		/**
		 * MIFARE Transfer writes the value stored in the volatile memory into one MIFARE Classic block.
		 * For MIFARE Classic only. The sector containing the block must be authenticated before calling this function.
		 * Only for blocks in "value block" mode, ie with access bits [C1 C2 C3] = [110] or [001].
		 * 
		 * @return STATUS_OK on success, STATUS_??? otherwise.
		 */
		MFRC522Const::StatusCode MIFARE_Transfer(	uint8_t blockAddr ///< The block (0-0xff) number.
													) 
		{
			uint8_t cmdBuffer[2]; // We only need room for 2 bytes.
	
			// Tell the PICC we want to transfer the result into block blockAddr.
			cmdBuffer[0] = MFRC522Const::PICC_CMD_MF_TRANSFER;
			cmdBuffer[1] = blockAddr;
			return PCD_MIFARE_Transceive(	cmdBuffer, 2); // Adds CRC_A and checks that the response is MF_ACK.
		} // End MIFARE_Transfer()

		/**
		 * Helper function for the two-step MIFARE Classic protocol operations Decrement, Increment and Restore.
		 * 
		 * @return STATUS_OK on success, STATUS_??? otherwise.
		 */
		MFRC522Const::StatusCode MIFARE_TwoStepHelper(	uint8_t command,	///< The command to use
															uint8_t blockAddr,	///< The block (0-0xff) number.
															int32_t data		///< The data to transfer in step 2
															) 
		{
			uint8_t cmdBuffer[2]; // We only need room for 2 bytes.
	
			// Step 1: Tell the PICC the command and block address
			cmdBuffer[0] = command;
			cmdBuffer[1] = blockAddr;
			MFRC522Const::StatusCode result = PCD_MIFARE_Transceive(	cmdBuffer, 2); // Adds CRC_A and checks that the response is MF_ACK.
			if (result != MFRC522Const::STATUS_OK)
				return result;
	
			// Step 2: Transfer the data
			return PCD_MIFARE_Transceive(	(uint8_t *)&data, 4, true); // Adds CRC_A and accept timeout as success.
		} // End MIFARE_TwoStepHelper()

		/**
		 * MIFARE Decrement subtracts the delta from the value of the addressed block, and stores the result in a volatile memory.
		 * For MIFARE Classic only. The sector containing the block must be authenticated before calling this function.
		 * Only for blocks in "value block" mode, ie with access bits [C1 C2 C3] = [110] or [001].
		 * Use MIFARE_Transfer() to store the result in a block.
		 * 
		 * @return STATUS_OK on success, STATUS_??? otherwise.
		 */
		MFRC522Const::StatusCode MIFARE_Decrement(	uint8_t blockAddr, ///< The block (0-0xff) number.
														int32_t delta		///< This number is subtracted from the value of block blockAddr.
													) 
		{
			return MIFARE_TwoStepHelper( MFRC522Const::PICC_CMD_MF_DECREMENT, blockAddr, delta );
		} // End MIFARE_Decrement()

		/**
		 * MIFARE Increment adds the delta to the value of the addressed block, and stores the result in a volatile memory.
		 * For MIFARE Classic only. The sector containing the block must be authenticated before calling this function.
		 * Only for blocks in "value block" mode, ie with access bits [C1 C2 C3] = [110] or [001].
		 * Use MIFARE_Transfer() to store the result in a block.
		 * 
		 * @return STATUS_OK on success, STATUS_??? otherwise.
		 */
		MFRC522Const::StatusCode MIFARE_Increment(	uint8_t blockAddr, ///< The block (0-0xff) number.
														int32_t delta		///< This number is added to the value of block blockAddr.
													) 
		{
			return MIFARE_TwoStepHelper( MFRC522Const::PICC_CMD_MF_INCREMENT, blockAddr, delta );
		} // End MIFARE_Increment()

		/**
		 * Used to exit the PCD from its authenticated state.
		 * Remember to call this function after communicating with an authenticated PICC - otherwise no new communications can start.
		 */
		void PCD_StopCrypto1() 
		{
			// Clear MFCrypto1On bit
			PCD_ClearRegisterBitMask( MFRC522Const::Status2Reg, 0x08 ); // Status2Reg[7..0] bits are: TempSensClear I2CForceHS reserved reserved MFCrypto1On ModemState[2:0]
		} // End PCD_StopCrypto1()

		/**
		 * Performs the "magic sequence" needed to get Chinese UID changeable
		 * Mifare cards to allow writing to sector 0, where the card UID is stored.
		 *
		 * Note that you do not need to have selected the card through REQA or WUPA,
		 * this sequence works immediately when the card is in the reader vicinity.
		 * This means you can use this method even on "bricked" cards that your reader does
		 * not recognise anymore (see MFRC522::MIFARE_UnbrickUidSector).
		 * 
		 * Of course with non-bricked devices, you're free to select them before calling this function.
		 */
		bool MIFARE_OpenUidBackdoor() 
		{
			// Magic sequence:
			// > 50 00 57 CD (HALT + CRC)
			// > 40 (7 bits only)
			// < A (4 bits only)
			// > 43
			// < A (4 bits only)
			// Then you can write to sector 0 without authenticating
	
			PICC_HaltA(); // 50 00 57 CD
	
//			Serial.println( "TEST0" );

			uint8_t cmd = 0x40;
			uint8_t validBits = 7; /* Our command is only 7 bits. After receiving card response,
								  this will contain amount of valid response bits. */
			uint8_t response[32]; // Card's response is written here
			uint8_t received = sizeof(response);
			MFRC522Const::StatusCode status = PCD_TransceiveData(&cmd, uint8_t( 1 ), response, &received, &validBits, uint8_t( 0 ), false); // 40
			if(status != MFRC522Const::STATUS_OK)
				return false;

//			Serial.println( "TEST2" );

			if (received != 1 || response[0] != 0x0A)
				return false;
	
//			Serial.println( "TEST3" );

			cmd = 0x43;
			validBits = 8;
			status = PCD_TransceiveData(&cmd, uint8_t( 1 ), response, &received, &validBits, uint8_t( 0 ), false); // 43
			if(status != MFRC522Const::STATUS_OK)
				return false;

//			Serial.println( "TEST4" );

			if (received != 1 || response[0] != 0x0A)
				return false;
	
//			Serial.println( "TEST5" );

			// You can now write to sector 0 without authenticating!
			return true;
		} // End MIFARE_OpenUidBackdoor()

		/**
		 * Resets entire sector 0 to zeroes, so the card can be read again by readers.
		 */
		MFRC522Const::StatusCode MIFARE_UnbrickUidSector( uint8_t *block0_buffer ) 
		{
			MIFARE_OpenUidBackdoor();
	
			// Write modified block 0 back to card
			return MIFARE_Write( uint8_t( 0 ), block0_buffer, uint8_t( 16 ));
		}

		void CardConnectionLost()
		{
			Card().DetectedOutputPin().SetPinValue( false );
			FLocked() = false;
		}

		bool RestartCard()
		{
			PICC_HaltA();
			PCD_StopCrypto1();
			if( ! PICC_IsAnyCardPresent())
			{
				CardConnectionLost();
				return false;
			}

			MFRC522Const::Uid uid;
			if( PICC_Select( &uid ) != MFRC522Const::STATUS_OK )
			{
				CardConnectionLost();
				return false;
			}

			if( ( FUid.size != uid.size ) || memcmp( uid.uidByte, FUid.uidByte, uid.size ))
			{
				FUid = uid;
				SendCardInfo();
			}

			return true;
		}

		/**
		 * Returns a __FlashStringHelper pointer to a status code name.
		 *
		 * @return const __FlashStringHelper *
		 */
#ifdef _MITOV_RFID_MFRC522_ERROR_PIN_
		inline const char *GetStatusCodeName(uint8_t code	///< One of the StatusCode enums.
												) 
		{
			return "";
		}

#else // _MITOV_RFID_MFRC522_ERROR_PIN_
		const __FlashStringHelper *GetStatusCodeName(uint8_t code	///< One of the StatusCode enums.
												) 
		{
			switch (code) 
			{
				case MFRC522Const::STATUS_OK:				return F("Success.");										break;
				case MFRC522Const::STATUS_ERROR:			return F("Error in communication.");						break;
				case MFRC522Const::STATUS_COLLISION:		return F("Collission detected.");							break;
				case MFRC522Const::STATUS_TIMEOUT:		return F("Timeout in communication.");						break;
				case MFRC522Const::STATUS_NO_ROOM:		return F("A buffer is not big enough.");					break;
				case MFRC522Const::STATUS_INTERNAL_ERROR:	return F("Internal error in the code. Should not happen.");	break;
				case MFRC522Const::STATUS_INVALID:		return F("Invalid argument.");								break;
				case MFRC522Const::STATUS_CRC_WRONG:		return F("The CRC_A does not match.");						break;
				case MFRC522Const::STATUS_NO_CARD:		return F("No Card.");						break;
				case MFRC522Const::STATUS_MIFARE_NACK:	return F("A MIFARE PICC responded with NAK.");				break;
				default:					return F("Unknown error");									break;
			}
		} // End GetStatusCodeName()
#endif // _MITOV_RFID_MFRC522_ERROR_PIN_

	public:
		inline void SystemInit()
		{
//			Serial.begin( 9600 );
//			delay( 5000 );
			T_0_IMPLEMENTATION::Init();
			Reset();
/*
			for( int i = 0; i < 0x3F; ++ i )
			{
				Serial.print( i, HEX );
				Serial.print( " = " );
				Serial.println( PCD_ReadRegister( i << 1 ), BIN );
			}
*/
		}

		inline void SystemStart()
		{
			Card().DetectedOutputPin().SetPinValue( false );
		}

		inline void SystemLoopBegin()
		{
//			Serial.println( "SCAN" );
//			Serial.println( PCD_ReadRegister( MFRC522Const::RxSelReg ), BIN );
//			Serial.println( PCD_ReadRegister( MFRC522Const::MfTxReg ), BIN );
//			Serial.println( PCD_ReadRegister( MFRC522Const::RxThresholdReg ), BIN );
//			Serial.println( PCD_ReadRegister( MFRC522Const::MfRxReg ), BIN );

			uint8_t errorRegValue = PCD_ReadRegister( MFRC522Const::ErrorReg );

			bool AOverheated = errorRegValue && 0b01000000;

			T_OverheatedOutputPin::SetPinValue( AOverheated );

			if( AOverheated )
				PCD_ClearRegisterBitMask( MFRC522Const::Status2Reg, 0b10000000 ); // Clear the overheating status

			PICC_HaltA();
			PCD_StopCrypto1();
//			delay( 1000 );
			if( FLocked() )
			{
//Serial.println( "TEST1" );
//return;
				if( PICC_IsAnyCardPresent() )
				{
//					PICC_HaltA();
//					Serial.println( "TEST_3" );
					MFRC522Const::Uid uid;
					if( PICC_Select( &uid ) == MFRC522Const::STATUS_OK )
					{
						if( ( FUid.size != uid.size ) || memcmp( uid.uidByte, FUid.uidByte, uid.size ))
						{
	//						Serial.println( "TEST_4" );
							FUid = uid;
							SendCardInfo();
						}

						return;
					}
				}

				CardConnectionLost();
			}

			bool APresent = PICC_IsNewCardPresent();
//			if( ! APresent )
//				APresent = PICC_IsNewCardPresent();
//delay( 100 );
//return;
//			if( ! APresent )
//				APresent = PICC_IsNewCardPresent(); // Retry!

			if( APresent )
			{
				Card().DetectedOutputPin().SetPinValue( true );
				FLocked() = true;
//				Serial.println( "STEP 11" );
//				MFRC522Const::Uid uid;
//				memset( &uid, 0, sizeof( uid ));
				MFRC522Const::StatusCode AResult = PICC_Select( &FUid );
//				Serial.print( "RESULT: " );
//				Serial.println( AResult );
				if( AResult == MFRC522Const::STATUS_OK )
				{
//					PICC_HaltA();
					SendCardInfo();
//					Serial.println( PCD_ReadRegister( MFRC522Const::GsNReg ), HEX );
//					Serial.println( PCD_ReadRegister( MFRC522Const::CWGsPReg ) );
//					Serial.println( PCD_ReadRegister( MFRC522Const::ModGsPReg ) );
				}
			}
/*
			if( PICC_IsNewCardPresent())
			{
				Serial.println( "RRESENT" );
//				delay( 100 );
			}

			delay( 100 );
*/
//			delay( 100 );
		}

	public:
		inline TArduino_RFID_MFRC522Basic()
		{
			FLocked() = false;
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"