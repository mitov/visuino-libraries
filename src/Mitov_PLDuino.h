////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
	class PLDiono
	{
	public:
		enum Pin
		{
			RELAY1 = 2,
			RELAY2 = 3,
			RELAY3 = 4,
			RELAY4 = 5,
			RELAY5 = 6,
			RELAY6 = 7,

			UART_RXD = 14,
			UART_TXD = 15,

			SD_CS = 53,

			LED_PIN = 13,

			SPEAKER_PIN = 9,

			RTC_INT = 19,
			RTC_SDA = 20,
			RTC_SCL = 21,

			LCD_PWR 	= 27,
			LCD_DC 		= 49,
			LCD_CS 		= 47,
			LCD_MOSI 	= 51,
			LCD_CLK 	= 52,
			LCD_RST 	= 48,
			LCD_MISO 	= 50,
			LCD_BACKLIGHT = 46,

			TOUCH_IRQ = 18,
			TOUCH_DO  = 50,
			TOUCH_DIN = 51,
			TOUCH_CS  = 28,
			TOUCH_CLK = 52,

			ESP_PWR		= 26,
			ESP_CHPD	= 25,
			ESP_GPIO0	= 24,
			ESP_GPIO2	= 23,
			ESP_RST		= 22
		};

	public:
		static void Init()
		{
			// Setup digital input pins
			for(int i=0; i<8; ++i)
			{
				pinMode(30+i, INPUT);
				Digital.Write(30+i, true);
			}

			// Setup analog input pins
			for(int i=0; i<8; ++i)
			{
				pinMode(A0+i, INPUT);
				Digital.Write(A0+i, false);
			}

			// Setup digital output pins
			for(int i=0; i<8; ++i)
				pinMode(38+i, OUTPUT);

			// Setup relays
			pinMode(RELAY1, OUTPUT);
			pinMode(RELAY2, OUTPUT);
			pinMode(RELAY3, OUTPUT);
			pinMode(RELAY4, OUTPUT);
			pinMode(RELAY5, OUTPUT);
			pinMode(RELAY6, OUTPUT);

			// Setup LED
			pinMode(LED_PIN, OUTPUT);

			// Enable 5v-3.3v converter
			DDRH |= (1<<PH2); // BOE on PH2 = output
			PORTH |= (1<<PH2); // set BOE High
			delay(200);
			PORTH &= ~(1<<PH2); // BOE low
	    
			pinMode( LCD_CS, OUTPUT);
			Digital.Write( LCD_CS, true);
		}

		static void EnableMicroSD()
		{
			pinMode( TOUCH_CS, OUTPUT); Digital.Write( TOUCH_CS, true ); 
			pinMode( LCD_PWR, OUTPUT); Digital.Write( LCD_PWR, true );
		}

	};
//---------------------------------------------------------------------------
	template <
//		typename T_0_PINS,
		typename T_Channels_UpdatePinOutputValue,
		typename T_Enabled
//		typename T_Serial
	> class ArduinoPLDuinoWiFi :
		public T_Enabled
//		public T_Serial
	{
	public:
		_V_PROP_( Enabled )
//		_V_PROP_( Serial )

	public:
		void UpdateEnabled()
		{
			if( Enabled() )
			{
				pinMode( PLDiono::ESP_CHPD, OUTPUT); Digital.Write( PLDiono::ESP_CHPD, true);
				pinMode( PLDiono::ESP_GPIO0, OUTPUT); Digital.Write( PLDiono::ESP_GPIO0, true);
				pinMode( PLDiono::ESP_GPIO2, OUTPUT); Digital.Write( PLDiono::ESP_GPIO2, true);
				pinMode( PLDiono::ESP_RST, OUTPUT); Digital.Write( PLDiono::ESP_RST, true);
				pinMode( PLDiono::ESP_PWR, OUTPUT); Digital.Write( PLDiono::ESP_PWR, true);

				Reset();
			}

			else
			{
//				pinMode( PLDiono::ESP_CHPD, INPUT); Digital.Write( PLDiono::ESP_CHPD, true);
//				pinMode( PLDiono::ESP_GPIO0, INPUT); Digital.Write( PLDiono::ESP_GPIO0, true);
//				pinMode( PLDiono::ESP_GPIO2, INPUT); Digital.Write( PLDiono::ESP_GPIO2, true);
//				pinMode( PLDiono::ESP_RST, INPUT); Digital.Write( PLDiono::ESP_RST, true);
				pinMode( PLDiono::ESP_PWR, OUTPUT); Digital.Write( PLDiono::ESP_PWR, false);
			}

			T_Channels_UpdatePinOutputValue::Call();

/*
			T_0_PINS::UpdatePin_0();
			T_0_PINS::UpdatePin_2();
			T_0_PINS::UpdatePin_15();
*/
		}

	public:
		inline void ResetInputPin_o_Receive( void *_Data )
		{
			Reset();
		}

	protected:
		void Reset()
		{
			if( Enabled() )
			{
				Digital.Write( PLDiono::ESP_RST, false );
				Digital.Write( PLDiono::ESP_CHPD, false );
				delay(250);
				Digital.Write( PLDiono::ESP_CHPD, true );
				Digital.Write( PLDiono::ESP_RST, true );
			}

			T_Channels_UpdatePinOutputValue::Call();
/*
			T_0_PINS::UpdatePin_0();
			T_0_PINS::UpdatePin_2();
			T_0_PINS::UpdatePin_15();
*/
		}

	public:
		inline void SystemInit()
		{
			UpdateEnabled();
		}

		inline void SystemStart()
		{
		}

		inline void SystemLoopBegin() {} // Placeholder
	};
//---------------------------------------------------------------------------
	template <
		typename T_AutoConfig,
		typename T_DigitalInputPin_o_IsConnected,
		typename T_InitialValue,
		typename T_IsCombinedInOut,
		typename T_IsOutput,
		typename T_IsRawInput,
		typename T_OutputPin,
		uint8_t C_PIN_NUMBER
	> class TArduinoPLDuinoWiFiDigitalChannel :
		public T_AutoConfig,
		public T_DigitalInputPin_o_IsConnected,
		public T_InitialValue,
		public T_IsCombinedInOut,
		public T_IsOutput,
		public T_IsRawInput,
		public T_OutputPin
	{
	public:
		_V_PIN_( OutputPin )

	public:
		_V_PROP_( AutoConfig )
		_V_PROP_( InitialValue )
		_V_PROP_( IsCombinedInOut )
		_V_PROP_( IsOutput )
		_V_PROP_( IsRawInput )

	protected:
		_V_PROP_( DigitalInputPin_o_IsConnected )

	public:
		inline void UpdatePinOutputValue()
		{
			Digital.Write( C_PIN_NUMBER, InitialValue().GetValue() );
		}

	public:
        void UpdatePinDirections() // Used as Live Binding updater! Do not rename!
        {
            if( IsOutput() )
			{
				pinMode( C_PIN_NUMBER, OUTPUT );
//				if( ! IsAnalog().GetValue() )
					Digital.Write( C_PIN_NUMBER, InitialValue().GetValue() );
			}

            else
				pinMode( C_PIN_NUMBER, INPUT );
		}

	public:
		inline void SystemInit()
		{
            UpdatePinDirections();
            if( IsCombinedInOut().GetValue() || IsOutput().GetValue() || ( AutoConfig() && DigitalInputPin_o_IsConnected() ) )
            {
				if( InitialValue() )
					if( IsCombinedInOut() )
						pinMode( C_PIN_NUMBER, OUTPUT );

//				Serial.println( AValue );

				Digital.Write( C_PIN_NUMBER, InitialValue() );

				if( ! InitialValue().GetValue() )
					if( IsCombinedInOut() )
						pinMode( C_PIN_NUMBER, INPUT );

            }
		}

		inline void SystemStart()
		{
            if( IsCombinedInOut().GetValue() || ( ! ( IsOutput().GetValue() || ( AutoConfig().GetValue() && DigitalInputPin_o_IsConnected().GetValue() ) ) ))
                T_OutputPin::SetPinValue( Digital.Read( C_PIN_NUMBER ), true );

//	    	T_OutputPin::SetPinValue( Digital.Read( C_PIN_NUMBER ), false );
		}

		inline void SystemLoopBegin()
        {
            if( IsCombinedInOut().GetValue() || ( ! ( IsOutput().GetValue() || ( AutoConfig().GetValue() && DigitalInputPin_o_IsConnected().GetValue() ) ) ))
                T_OutputPin::SetPinValue( Digital.Read( C_PIN_NUMBER ), true );

/*
            if( ! IsOutput().GetValue() )
            {
    			bool AValue = Digital.Read( C_PIN_NUMBER );
//				if( AValue == FLastOutput )
//					return;

//				FLastOutput = AValue;
//	Serial.println( AData.Value );
	    		T_OutputPin::SetPinValue( AValue, true );
            }
*/
		}

	public:
		void DigitalInputPin_o_Receive( void *_Data )
		{
//            if( ( IsCombinedInOut().GetValue() || IsOutput().GetValue() ) && ( ! IsAnalog().GetValue() ))
            if( IsCombinedInOut().GetValue() || IsOutput().GetValue() || ( AutoConfig() && DigitalInputPin_o_IsConnected() ) )
            {
				bool AValue = *(bool *)_Data;

				if( AValue )
					if( IsCombinedInOut() )
						pinMode( C_PIN_NUMBER, OUTPUT );

//				Serial.println( AValue );
				Digital.Write( C_PIN_NUMBER, AValue );

				if( ! AValue )
					if( IsCombinedInOut() )
						pinMode( C_PIN_NUMBER, INPUT );

            }
/*
			if( IsRawInput() )
			{
				*((int*)_Data) = C_PIN_NUMBER;
				return;
			}

            if( ( IsCombinedInOut().GetValue() || IsOutput().GetValue() ) && ( ! IsAnalog().GetValue() ))
            {
				bool AValue = *(bool *)_Data;

				if( AValue )
				  if( IsCombinedInOut() )
					  pinMode( C_PIN_NUMBER, OUTPUT );

                Digital.Write( C_PIN_NUMBER, AValue );

				if( ! AValue )
					if( IsCombinedInOut() )
						pinMode( C_PIN_NUMBER, IsPullUp().GetValue() ? INPUT_PULLUP : ( IsPullDown().GetValue() ? INPUT_PULLDOWN : INPUT ) );

            }
*/
		}
	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"