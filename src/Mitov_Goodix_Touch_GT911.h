////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
	namespace GT911_Consts
	{
		const uint16_t REG_CHECKSUM = 0x80FF;
		const uint16_t REG_DATA = 0x8140;

// Configuration information (R/W)
		const uint16_t CONFIG_START             = 0x8047;
		const uint16_t CONFIG_VERSION           = 0x8047;
		const uint16_t X_OUTPUT_MAX_LOW         = 0x8048;
		const uint16_t X_OUTPUT_MAX_HIGH        = 0x8049;
		const uint16_t Y_OUTPUT_MAX_LOW         = 0x804A;
		const uint16_t Y_OUTPUT_MAX_HIGH        = 0x804B;
		const uint16_t TOUCH_NUMBER             = 0x804C;
		const uint16_t MODULE_SWITCH_1          = 0x804D;
		const uint16_t MODULE_SWITCH_2          = 0x804E;
		const uint16_t SHAKE_COUNT              = 0x804F;
		const uint16_t FILTER                   = 0x8050;
		const uint16_t LARGE_TOUCH              = 0x8051;
		const uint16_t NOISE_REDUCTION          = 0x8052;
		const uint16_t SCREEN_TOUCH_LEVEL       = 0x8053;
		const uint16_t SCREEN_RELEASE_LEVEL     = 0x8054;
		const uint16_t LOW_POWER_CONTROL        = 0x8055;
		const uint16_t REFRESH_RATE             = 0x8056;
		const uint16_t X_THRESHOLD              = 0x8057;
		const uint16_t Y_THRESHOLD              = 0x8058;
		const uint16_t X_SPEED_LIMIT            = 0x8059; //Reserve
		const uint16_t Y_SPEED_LIMIT            = 0x805A; //Reserve
		const uint16_t SPACE_TOP_BOTTOM         = 0x805B;
		const uint16_t SPACE_LEFT_RIGHT         = 0x805C;
		const uint16_t MINI_FILTER              = 0x805D;
		const uint16_t STRETCH_R0               = 0x805E;
		const uint16_t STRETCH_R1               = 0x805F;
		const uint16_t STRETCH_R2               = 0x8060;
		const uint16_t STRETCH_RM               = 0x8061;
		const uint16_t DRV_GROUPA_NUM           = 0x8062;
		const uint16_t DRV_GROUPB_NUM           = 0x8063;
		const uint16_t SENSOR_NUM               = 0x8064;
		const uint16_t FREQ_A_FACTOR            = 0x8065;
		const uint16_t FREQ_B_FACTOR            = 0x8066;
		const uint16_t PANEL_BIT_FREQ_L         = 0x8067;
		const uint16_t PANEL_BIT_FREQ_H         = 0x8068;
		const uint16_t PANEL_SENSOR_TIME_L      = 0x8069; //Reserve
		const uint16_t PANEL_SENSOR_TIME_H      = 0x806A;
		const uint16_t PANEL_TX_GAIN            = 0x806B;
		const uint16_t PANEL_RX_GAIN            = 0x806C;
		const uint16_t PANEL_DUMP_SHIFT         = 0x806D;
		const uint16_t DRV_FRAME_CONTROL        = 0x806E;
		const uint16_t CHARGING_LEVEL_UP        = 0x806F;
		const uint16_t MODULE_SWITCH3           = 0x8070;
		const uint16_t GESTURE_DIS              = 0x8071;
		const uint16_t GESTURE_LONG_PRESS_TIME  = 0x8072;
		const uint16_t X_Y_SLOPE_ADJUST         = 0x8073;
		const uint16_t GESTURE_CONTROL          = 0x8074;
		const uint16_t GESTURE_SWITCH1          = 0x8075;
		const uint16_t GESTURE_SWITCH2          = 0x8076;
		const uint16_t GESTURE_REFRESH_RATE     = 0x8077;
		const uint16_t GESTURE_TOUCH_LEVEL      = 0x8078;
		const uint16_t NEWGREENWAKEUPLEVEL      = 0x8079;
		const uint16_t FREQ_HOPPING_START       = 0x807A;
		const uint16_t FREQ_HOPPING_END         = 0x807B;
		const uint16_t NOISE_DETECT_TIMES       = 0x807C;
		const uint16_t HOPPING_FLAG             = 0x807D;
		const uint16_t HOPPING_THRESHOLD        = 0x807E;
		const uint16_t NOISE_THRESHOLD          = 0x807F; //Reserve
		const uint16_t NOISE_MIN_THRESHOLD      = 0x8080;
		const uint16_t HOPPING_SENSOR_GROUP     = 0x8082;
		const uint16_t HOPPING_SEG1_NORMALIZE   = 0x8083;
		const uint16_t HOPPING_SEG1_FACTOR      = 0x8084;
		const uint16_t MAIN_CLOCK_AJDUST        = 0x8085;
		const uint16_t HOPPING_SEG2_NORMALIZE   = 0x8086;
		const uint16_t HOPPING_SEG2_FACTOR      = 0x8087;
		const uint16_t HOPPING_SEG3_NORMALIZE   = 0x8089;
		const uint16_t HOPPING_SEG3_FACTOR      = 0x808A;
		const uint16_t HOPPING_SEG4_NORMALIZE   = 0x808C;
		const uint16_t HOPPING_SEG4_FACTOR      = 0x808D;
		const uint16_t HOPPING_SEG5_NORMALIZE   = 0x808F;
		const uint16_t HOPPING_SEG5_FACTOR      = 0x8090;
		const uint16_t HOPPING_SEG6_NORMALIZE   = 0x8092;
		const uint16_t KEY_1                    = 0x8093;
		const uint16_t KEY_2                    = 0x8094;
		const uint16_t KEY_3                    = 0x8095;
		const uint16_t KEY_4                    = 0x8096;
		const uint16_t KEY_AREA                 = 0x8097;
		const uint16_t KEY_TOUCH_LEVEL          = 0x8098;
		const uint16_t KEY_LEAVE_LEVEL          = 0x8099;
		const uint16_t KEY_SENS_1_2             = 0x809A;
		const uint16_t KEY_SENS_3_4             = 0x809B;
		const uint16_t KEY_RESTRAIN             = 0x809C;
		const uint16_t KEY_RESTRAIN_TIME        = 0x809D;
		const uint16_t GESTURE_LARGE_TOUCH      = 0x809E;
		const uint16_t HOTKNOT_NOISE_MAP        = 0x80A1;
		const uint16_t LINK_THRESHOLD           = 0x80A2;
		const uint16_t PXY_THRESHOLD            = 0x80A3;
		const uint16_t GHOT_DUMP_SHIFT          = 0x80A4;
		const uint16_t GHOT_RX_GAIN             = 0x80A5;
		const uint16_t FREQ_GAIN0               = 0x80A6;
		const uint16_t FREQ_GAIN1               = 0x80A7;
		const uint16_t FREQ_GAIN2               = 0x80A8;
		const uint16_t FREQ_GAIN3               = 0x80A9;
		const uint16_t COMBINE_DIS              = 0x80B3;
		const uint16_t SPLIT_SET                = 0x80B4;
		const uint16_t SENSOR_CH0               = 0x80B7;
		const uint16_t DRIVER_CH0               = 0x80D5;
		const uint16_t CONFIG_CHKSUM            = 0x80FF;
		const uint16_t CONFIG_FRESH             = 0x8100;
		const uint8_t CONFIG_SIZE               = 0xFF-0x46;
// Coordinate information
		const uint16_t PRODUCT_ID        = 0x8140;
		const uint16_t FIRMWARE_VERSION  = 0x8140;
		const uint16_t RESOLUTION        = 0x8140;
		const uint16_t VENDOR_ID         = 0x8140;
		const uint16_t IMFORMATION       = 0x8140;
		const uint16_t POINT_INFO        = 0x814E;
		const uint16_t POINT_1           = 0x814F;
		const uint16_t POINT_2           = 0x8157;
		const uint16_t POINT_3           = 0x815F;
		const uint16_t POINT_4           = 0x8167;
		const uint16_t POINT_5           = 0x816F;
//		const uint16_t POINTS_REG        {GT911_POINT_1, GT911_POINT_2, GT911_POINT_3, GT911_POINT_4, GT911_POINT_5}

	}
//---------------------------------------------------------------------------
	template <
		typename T_I2C, T_I2C &C_I2C,
		typename T_Address,
		typename T_Enabled,
		typename T_Height,
		int16_t C_InterruptInputPin,
		typename T_Mirror,
		typename T_Orientation,
		typename T_OutputPin,
		typename T_ResetOutputPin,
		typename T_Width
	> class TArduino_Goodix_Touch_GT911 :
		public T_Address,
		public T_Enabled,
		public T_Height,
		public T_Mirror,
		public T_Orientation,
		public T_OutputPin,
		public T_ResetOutputPin,
		public T_Width
	{
	public:
		_V_PIN_( OutputPin )
		_V_PIN_( ResetOutputPin )

	public:
		_V_PROP_( Address )
		_V_PROP_( Enabled )
		_V_PROP_( Height )
		_V_PROP_( Mirror )
		_V_PROP_( Orientation )
		_V_PROP_( Width )

	public:
		void UpdateResolution()
		{
			uint8_t configBuf[ GT911_Consts::CONFIG_SIZE ];
			readBytes( GT911_Consts::CONFIG_START, configBuf, GT911_Consts::CONFIG_SIZE );

			bool AModified = false;

			uint8_t ALowByte = Width().GetValue() & 0xFF;
			uint8_t AHighByte = ( Width().GetValue() >> 8 ) & 0xFF;

			if( configBuf[ GT911_Consts::X_OUTPUT_MAX_LOW - GT911_Consts::CONFIG_START ] != ALowByte )
			{
				configBuf[ GT911_Consts::X_OUTPUT_MAX_LOW - GT911_Consts::CONFIG_START ] = ALowByte;
				write( GT911_Consts::X_OUTPUT_MAX_LOW, ALowByte );
				AModified = true;
			}
			
			if( configBuf[ GT911_Consts::X_OUTPUT_MAX_HIGH - GT911_Consts::CONFIG_START ] != AHighByte )
			{
				configBuf[ GT911_Consts::X_OUTPUT_MAX_HIGH - GT911_Consts::CONFIG_START ] = AHighByte;
				write( GT911_Consts::X_OUTPUT_MAX_HIGH, AHighByte );
				AModified = true;
			}

			ALowByte = Height().GetValue() & 0xFF;
			AHighByte = ( Height().GetValue() >> 8 ) & 0xFF;

			if( configBuf[ GT911_Consts::Y_OUTPUT_MAX_LOW - GT911_Consts::CONFIG_START ] != ALowByte )
			{
				configBuf[ GT911_Consts::Y_OUTPUT_MAX_LOW - GT911_Consts::CONFIG_START ] = ALowByte;
				write( GT911_Consts::Y_OUTPUT_MAX_LOW, ALowByte );
				AModified = true;
			}
			
			if( configBuf[ GT911_Consts::Y_OUTPUT_MAX_HIGH - GT911_Consts::CONFIG_START ] != AHighByte )
			{
				configBuf[ GT911_Consts::Y_OUTPUT_MAX_HIGH - GT911_Consts::CONFIG_START ] = AHighByte;
				write( GT911_Consts::Y_OUTPUT_MAX_HIGH, AHighByte );
				AModified = true;
			}

//			configBuf[ GT911_Consts::X_OUTPUT_MAX_LOW - GT911_Consts::CONFIG_START ] = lowByte(_width);
//			configBuf[ GT911_Consts::X_OUTPUT_MAX_HIGH - GT911_Consts::CONFIG_START ] = highByte(_width);
//			configBuf[ GT911_Consts::Y_OUTPUT_MAX_LOW - GT911_Consts::CONFIG_START ] = lowByte(_height);
//			configBuf[ GT911_Consts::Y_OUTPUT_MAX_HIGH - GT911_Consts::CONFIG_START ] = highByte(_height);

			if( ! AModified )
				return;

			uint8_t checksum = 0;
			for( uint8_t i = 0; i < GT911_Consts::CONFIG_SIZE - 1; ++ i ) 
				checksum += configBuf[ i ];

			checksum = (~checksum) + 1;
			configBuf[ GT911_Consts::CONFIG_CHKSUM - GT911_Consts::CONFIG_START ] = checksum;

			writeBytes( GT911_Consts::CONFIG_START, configBuf, GT911_Consts::CONFIG_SIZE - 1 );

			write( GT911_Consts::CONFIG_CHKSUM, configBuf[ GT911_Consts::CONFIG_CHKSUM - GT911_Consts::CONFIG_START ]);
			write( GT911_Consts::CONFIG_FRESH, 1 );
		}

	protected:
		inline void i2cStart( uint16_t reg ) 
		{
			C_I2C.beginTransmission( uint8_t( Address().GetValue() ) );
			C_I2C.write( reg >> 8 );
			C_I2C.write( reg & 0xFF );
		}

		void write( uint16_t reg, uint8_t data )
		{
			i2cStart( reg );
			C_I2C.write(data);
			C_I2C.endTransmission();
		}

		uint8_t read( uint16_t reg ) 
		{
			i2cStart( reg );
			C_I2C.endTransmission();
			C_I2C.requestFrom( uint8_t( Address().GetValue() ), _VISUINO_I2C_SIZE_( 1 ));
			return C_I2C.read();
		}

		void writeBytes(uint16_t reg, const uint8_t *data, uint16_t size)
		{
		  i2cStart( reg );
		  for( uint16_t i = 0; i < size; ++ i )
			C_I2C.write(data[i]);

		  C_I2C.endTransmission();
		}

		void readBytes(uint16_t reg, uint8_t *data, uint16_t size)
		{
			i2cStart( reg );
			C_I2C.endTransmission();

			C_I2C.requestFrom( uint8_t( Address().GetValue() ), _VISUINO_I2C_SIZE_( size ));

			for( uint16_t i = 0; i < size; ++ i )
				data[ i ] = C_I2C.read();

		}

		void Reset()
		{
			if( T_ResetOutputPin::GetPinIsConnected() )
			{
//				reset();
				T_ResetOutputPin::SetPinValueLow();
				delay( 11 );
				if( C_InterruptInputPin >= 0 )
				{
					if( Address().GetValue() == 0x14 )
					{
						pinMode( C_InterruptInputPin, OUTPUT );
						Digital.Write( C_InterruptInputPin, false );
						delayMicroseconds( 110 );
						pinMode( C_InterruptInputPin, INPUT );
					}
				}

				delay( 6 );
				T_ResetOutputPin::SetPinValueHigh();

				delay(200);			
			}

//			readInfo();
		}

	public:
		inline void __ICACHE_RAM_ATTR__ InterruptHandler( bool AValue )
		{
		}

	public:
		inline void ResetInputPin_o_Receive( void *_Data )
		{
			Reset();
		}

	public:
		inline void SystemStart()
		{
//delay( 10000 );
			if( T_ResetOutputPin::GetPinIsConnected() )
			{
				delay( 300 );
				Reset();
			}

			UpdateResolution();
		}

		inline void SystemLoopBegin()
		{
			uint8_t AStatus = read( GT911_Consts::POINT_INFO );
//			Serial.println( AStatus, HEX );
			if( AStatus & 0b10000000 ) // Has good data
			{
//				Serial.println();
//				Serial.println( AStatus, HEX );

				uint8_t ACount = AStatus & 0x0F;
				if( ACount > 0 )
				{
					TArduino2DTouchPoint *APoints = new TArduino2DTouchPoint[ ACount ];

					for( uint8_t i = 0; i < ACount; ++ i ) 
					{
						uint8_t data[ 7 ];
						readBytes( GT911_Consts::POINT_1 + i * 8, (uint8_t *)&data, sizeof( data ) );

//						Serial.println( data[ 1 ] );
//						Serial.println( data[ 3 ] );

						APoints[ i ].ID = data[ 0 ];

						uint16_t X = data[ 1 ] + ( uint16_t( data[ 2 ] ) << 8 );
						uint16_t Y = data[ 3 ] + ( uint16_t( data[ 4 ] ) << 8 );

						switch( Orientation().GetValue() )
						{
							case goUp:
								Mirror().template ApplyMirror<uint16_t>( X, Y, Width().GetValue(), Height().GetValue() );
								break;

							case goDown:
								Mirror().template ApplyMirror<uint16_t>( X, Y, Width().GetValue(), Height().GetValue() );
								X = Width().GetValue() - 1 - X;
								Y = Height().GetValue() - 1 - Y;
								break;

							case goLeft:
								swap( X, Y );
								Y = Width().GetValue() - Y - 1;
//								X = Height().GetValue() - X - 1;
								Mirror().template ApplyMirror<uint16_t>( X, Y, Height().GetValue(), Width().GetValue() );
								break;

							case goRight:
								swap( X, Y );
								X = Height().GetValue() - X - 1;
								Mirror().template ApplyMirror<uint16_t>( X, Y, Height().GetValue(), Width().GetValue() );
								break;

						}

						APoints[ i ].X = X;
						APoints[ i ].Y = Y;
						APoints[ i ].Size = data[ 5 ] + ( uint16_t( data[ 6 ] ) << 8 );
						APoints[ i ].Status = TTouchStatus::Down;
/*
						GTPoint data;
						readBytes( GT911_Consts::POINT_1 + i * 8, (uint8_t *)&data, sizeof( GTPoint ) );
						APoints[ i ].X = data.x;
						APoints[ i ].Y = data.y;
						APoints[ i ].ID = data.trackId;
						APoints[ i ].Size = data.area;
						APoints[ i ].Status = TTouchStatus::Down;
*/
					}

					TArduinoTouchInfo ATouchData( ACount, APoints );

					T_OutputPin::SetPinValue( ATouchData );
					delete [] APoints;
				}
			}

			write( GT911_Consts::POINT_INFO, 0 );
//			delay( 100 );
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"
