////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
	template <
		typename T_AxesOutputPins_X,
		typename T_AxesOutputPins_Y,
		typename T_ButtonOutputPin
	> class ArduinoModuleJoystick : 
		public T_AxesOutputPins_X,
		public T_AxesOutputPins_Y,
		public T_ButtonOutputPin
	{
	public:
		_V_PIN_( AxesOutputPins_X )
		_V_PIN_( AxesOutputPins_Y )
		_V_PIN_( ButtonOutputPin )

	public:
		inline void SystemLoopBeginButton()
		{
	    	T_ButtonOutputPin::SetPinValue( ! Digital.Read( 8 ), true );
		}

		inline void SystemStartButton()
		{
	    	T_ButtonOutputPin::SetPinValue( ! Digital.Read( 8 ), false );
		}

		inline void SystemLoopBeginJoystick()
		{
	    	T_AxesOutputPins_X::SetPinValue( Analog.Read( 0 ), true );
	    	T_AxesOutputPins_Y::SetPinValue( Analog.Read( 1 ), true );
		}

		inline void SystemStartJoystick()
		{
	    	T_AxesOutputPins_X::SetPinValue( Analog.Read( 0 ), false );
	    	T_AxesOutputPins_Y::SetPinValue( Analog.Read( 1 ), false );
		}
	};
//---------------------------------------------------------------------------
	template <
		typename T_DirectionOutputPins_Up,
		typename T_DirectionOutputPins_Down,
		typename T_DirectionOutputPins_Left,
		typename T_DirectionOutputPins_Right,
		typename T_OutputPins_E,
		typename T_OutputPins_F
	> class ArduinoModuleJoystickButtons : 
		public T_DirectionOutputPins_Up,
		public T_DirectionOutputPins_Down,
		public T_DirectionOutputPins_Left,
		public T_DirectionOutputPins_Right,
		public T_OutputPins_E,
		public T_OutputPins_F
	{
	public:
		_V_PIN_( DirectionOutputPins_Up )
		_V_PIN_( DirectionOutputPins_Down )
		_V_PIN_( DirectionOutputPins_Left )
		_V_PIN_( DirectionOutputPins_Right )
		_V_PIN_( OutputPins_E )
		_V_PIN_( OutputPins_F )

	public:
		inline void SystemLoopBeginDirection()
		{
	    	T_DirectionOutputPins_Up::SetPinValue( ! Digital.Read( 2 ), true );
	    	T_DirectionOutputPins_Down::SetPinValue( ! Digital.Read( 4 ), true );
	    	T_DirectionOutputPins_Left::SetPinValue( ! Digital.Read( 5 ), true );
	    	T_DirectionOutputPins_Right::SetPinValue( ! Digital.Read( 3 ), true );			
		}

		inline void SystemStartDirection()
		{
	    	T_DirectionOutputPins_Up::SetPinValue( ! Digital.Read( 2 ), false );
	    	T_DirectionOutputPins_Down::SetPinValue( ! Digital.Read( 4 ), false );
	    	T_DirectionOutputPins_Left::SetPinValue( ! Digital.Read( 5 ), false );
	    	T_DirectionOutputPins_Right::SetPinValue( ! Digital.Read( 3 ), false );
		}

		inline void SystemLoopBeginButtons()
		{
	    	T_OutputPins_E::SetPinValue( ! Digital.Read( 6 ), true );
	    	T_OutputPins_F::SetPinValue( ! Digital.Read( 7 ), true );
		}

		inline void SystemStartButtons()
		{
	    	T_OutputPins_E::SetPinValue( ! Digital.Read( 6 ), false );
	    	T_OutputPins_F::SetPinValue( ! Digital.Read( 7 ), false );
		}

	};	
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"
