////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
	template <
		uint8_t	C_PIN_CLOCK,
		uint8_t	C_PIN_DATA,
		uint8_t	C_PIN_ENABLE,
		uint8_t	C_PIN_STORE,
		typename T_Motors_GetMaskValue
	> class TArduinoTSCINBUNYOmniDirectionalSmartCarShield
	{
	public:
		inline void UpdateOutputs()
		{
			if( ! T_Motors_GetMaskValue::Count())
				return;

//			Serial.println( "UpdateOutputs" );

			uint8_t AValue = 0;
			T_Motors_GetMaskValue::Call( AValue );

//			Serial.println( AValue, BIN );

			Digital.Write( C_PIN_STORE, LOW );
			shiftOut( C_PIN_DATA, C_PIN_CLOCK, MSBFIRST, AValue );
			Digital.Write( C_PIN_STORE, HIGH );
		}

	public:
		inline void SystemStart()
		{
			if( ! T_Motors_GetMaskValue::Count())
				return;

			pinMode( C_PIN_CLOCK, OUTPUT );
			pinMode( C_PIN_DATA, OUTPUT );
			pinMode( C_PIN_STORE, OUTPUT );
			pinMode( C_PIN_ENABLE, OUTPUT );
			Digital.Write( C_PIN_ENABLE, LOW );
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_OutputPins_Left,
		typename T_OutputPins_Center,
		typename T_OutputPins_Right
	> class TArduinoTSCINBUNYOmniDirectionalSmartCarLineFollowingSensors :
		public T_OutputPins_Left,
		public T_OutputPins_Center,
		public T_OutputPins_Right
	{
	public:
		_V_PIN_( OutputPins_Left )
		_V_PIN_( OutputPins_Center )
		_V_PIN_( OutputPins_Right )

	protected:
		void ReadSensor( bool AChangeOnly )
		{
			T_OutputPins_Left::SetPinValue( Analog.Read( 14 ), AChangeOnly );
			T_OutputPins_Center::SetPinValue( Analog.Read( 15 ), AChangeOnly );
			T_OutputPins_Right::SetPinValue( Analog.Read( 16 ), AChangeOnly );
		}

	public:
		inline void SystemStart()
		{
			ReadSensor( false );
		}

		inline void SystemLoopBegin()
		{
			ReadSensor( true );
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_Enabled,
		typename T_Reverse
	> class TArduinoTSCINBUNYOmniDirectionalSmartCarShieldMotor :
		public T_Enabled,
		public T_Reverse
	{
	public:
		_V_PROP_( Enabled )
		_V_PROP_( Reverse )

	};
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
		bool C_IS_RIGHT,
		typename T_Enabled,
//		typename T_FForward,
		typename T_Front,
		typename T_InitialValue,
		typename T_Rear,
		typename T_SlopeRun,
		typename T_SlopeStop
	> class TArduinoTSCINBUNYOmniDirectionalSmartCarShieldMotorChannel :
		public T_Enabled,
//		public T_FForward,
		public T_Front,
		public T_InitialValue,
		public T_Rear
	{
	public:
		_V_PROP_( Enabled )
		_V_PROP_( Front )
		_V_PROP_( InitialValue )
		_V_PROP_( Rear )

//	protected:
//		_V_PROP_( FForward )

	public:
		inline void InputPin_o_Receive( void *_Data )
		{
			float ASpeed = MitovConstrain( *(float *)_Data, 0.0f, 1.0f );
			if( InitialValue() == ASpeed )
				return;

			InitialValue() = ASpeed;
			UpdateOutputs();
		}

	public:
		void UpdateOutputs()
		{
			if( Enabled() )
			{
				float AInitialValue = InitialValue().GetValue();
				float AOutSpeed = fabs( AInitialValue - 0.5 ) * 2;
//				FForward() = AInitialValue > 0.5;
//				bool ADirection = AInitialValue > 0.5;

				Analog.Write( ( C_IS_RIGHT ) ? 5 : 6, AOutSpeed );
//				Digital.Write( PIN_DIRECTION, ADirection );
			}

			else
			{
				Analog.WriteRaw( ( C_IS_RIGHT ) ? 5 : 6, 0 );
//				Digital.Write( PIN_DIRECTION, false );
			}

			C_OWNER.UpdateOutputs();
		}

	public:
		inline void SystemStart()
		{
			UpdateOutputs();
		}

		inline void SystemLoopBeginElapsed( unsigned long AElapsedMicros ) {} // Placeholder for *_Slopped compatibility

	public:
		inline void GetMaskValue( uint8_t & AValue )
		{
			if( ! Enabled() )
				return;

			if( C_IS_RIGHT )
			{
				if( Front().Enabled() )
					AValue |= ( ( InitialValue().GetValue() > 0.5 ) != Front().Reverse().GetValue() ) ? 0b00000100 : 0b00000010;

				if( Rear().Enabled() )
					AValue |= ( ( InitialValue().GetValue() > 0.5 ) != Rear().Reverse().GetValue() ) ? 0b00001000 : 0b00000001;
					
			}

			else
			{
				if( Front().Enabled() )
					AValue |= ( ( InitialValue().GetValue() > 0.5 ) != Front().Reverse().GetValue() ) ? 0b01000000 : 0b10000000;

				if( Rear().Enabled() )
					AValue |= ( ( InitialValue().GetValue() > 0.5 ) != Rear().Reverse().GetValue() ) ? 0b00010000 : 0b00100000;
					
			}
		}

	public:
		TArduinoTSCINBUNYOmniDirectionalSmartCarShieldMotorChannel()
		{
			pinMode( ( C_IS_RIGHT ) ? 5 : 6, OUTPUT );
//			pinMode( PIN_DIRECTION, OUTPUT );
		}
	};

//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
		bool C_IS_RIGHT,
		typename T_Enabled,
//		typename T_FForward,
		typename T_Front,
		typename T_InitialValue,
		typename T_Rear,
		typename T_SlopeRun,
		typename T_SlopeStop
	> class TArduinoTSCINBUNYOmniDirectionalSmartCarShieldMotorChannel_Slopped :
		public T_Enabled,
//		public T_FForward,
		public T_Front,
		public T_InitialValue,
		public T_Rear,
		public T_SlopeRun,
		public T_SlopeStop
	{
	public:
		_V_PROP_( Enabled )
		_V_PROP_( Front )
		_V_PROP_( InitialValue )
		_V_PROP_( Rear )
		_V_PROP_( SlopeRun )
		_V_PROP_( SlopeStop )

	protected:
//		_V_PROP_( FForward )

	protected:
		float	FCurrentValue = 0.0f;
		unsigned long	FLastTime = 0;

	public:
		inline void InputPin_o_Receive( void *_Data )
		{
			float ASpeed = MitovConstrain( *(float *)_Data, 0.0f, 1.0f );
			if( InitialValue() == ASpeed )
				return;

			InitialValue() = ASpeed;
			UpdateOutputs();
		}

	public:
		void UpdateOutputs()
		{
			if( Enabled() )
			{
				float AInitialValue = InitialValue().GetValue();
				float AOutSpeed = fabs( AInitialValue - 0.5 ) * 2;
//				FForward() = AInitialValue > 0.5;
//				bool ADirection = AInitialValue > 0.5;

				Analog.Write( ( C_IS_RIGHT ) ? 5 : 6, AOutSpeed );
//				Digital.Write( PIN_DIRECTION, ADirection );
			}

			else
			{
				Analog.WriteRaw( ( C_IS_RIGHT ) ? 5 : 6, 0 );
//				Digital.Write( PIN_DIRECTION, false );
			}

			C_OWNER.UpdateOutputs();
		}

	public:
		inline void SystemStart()
		{
			FCurrentValue = InitialValue();
			UpdateOutputs();
//			pinMode( 3, OUTPUT );
//			Digital.Write( 3, true );
		}

		inline void SystemLoopBeginElapsed( unsigned long AElapsedMicros )
		{
			float ATargetValue = ( Enabled() ) ? InitialValue() : 0.5;
			if( FCurrentValue != ATargetValue )
			{
				float ASlope = SlopeRun();
				if( FCurrentValue > ATargetValue )
				{
					if( ATargetValue > 0.5 )
						ASlope = SlopeStop();
				}

				else
				{
					if( ATargetValue < 0.5 )
						ASlope = SlopeStop();
				}

				if( ASlope == 0 )
					FCurrentValue = ATargetValue;

				else
				{
					float ARamp = fabs( AElapsedMicros * ASlope / 1000000 );
					if( FCurrentValue < ATargetValue )
					{
						FCurrentValue += ARamp;
						if( FCurrentValue > ATargetValue )
							FCurrentValue = ATargetValue;

					}
					else
					{
						FCurrentValue -= ARamp;
						if( FCurrentValue < ATargetValue )
							FCurrentValue = ATargetValue;

					}
				}

				UpdateOutputs();
			}
		}

	public:
		inline void GetMaskValue( uint8_t & AValue )
		{
			if( C_IS_RIGHT )
			{
				if( Front().Enabled() )
					AValue |= ( ( InitialValue().GetValue() > 0.5 ) != Front().Reverse().GetValue() ) ? 0b00000100 : 0b00000010;

				if( Rear().Enabled() )
					AValue |= ( ( InitialValue().GetValue() > 0.5 ) != Rear().Reverse().GetValue() ) ? 0b00001000 : 0b00000001;
					
			}

			else
			{
				if( Front().Enabled() )
					AValue |= ( ( InitialValue().GetValue() > 0.5 ) != Front().Reverse().GetValue() ) ? 0b01000000 : 0b10000000;

				if( Rear().Enabled() )
					AValue |= ( ( InitialValue().GetValue() > 0.5 ) != Rear().Reverse().GetValue() ) ? 0b00010000 : 0b00100000;
					
			}
		}

	public:
		TArduinoTSCINBUNYOmniDirectionalSmartCarShieldMotorChannel_Slopped()
		{
			pinMode( ( C_IS_RIGHT ) ? 5 : 6, OUTPUT );
//			pinMode( PIN_DIRECTION, OUTPUT );
		}
	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"