////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>
#include <SPI.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
namespace RH_RF69Const
{
	// This is the address that indicates a broadcast
	//static const uint8_t RH_BROADCAST_ADDRESS = 0xFF;

	static const uint8_t MAX_PAYLOAD_LEN = 255;

	// Maximum encryptable payload length the RF69 can support
	static const uint8_t MAX_ENCRYPTABLE_PAYLOAD_LEN = 64;

	// The length of the headers we add.
	// The headers are inside the RF69's payload and are therefore encrypted if encryption is enabled
	//static const uint8_t HEADER_LEN = 4;

	// The crystal oscillator frequency of the RF69 module
	static constexpr float FXOSC = 32000000.0;

	// The Frequency Synthesizer step = FXOSC / 2^^19
	static constexpr float FSTEP  = (FXOSC / 524288);

	// Register names
	static const uint8_t REG_00_FIFO                                 = 0x00;
	static const uint8_t REG_01_OPMODE                               = 0x01;
	static const uint8_t REG_02_DATAMODUL                            = 0x02;
	static const uint8_t REG_03_BITRATEMSB                           = 0x03;
	static const uint8_t REG_04_BITRATELSB                           = 0x04;
	static const uint8_t REG_05_FDEVMSB                              = 0x05;
	static const uint8_t REG_06_FDEVLSB                              = 0x06;
	static const uint8_t REG_07_FRFMSB                               = 0x07;
	static const uint8_t REG_08_FRFMID                               = 0x08;
	static const uint8_t REG_09_FRFLSB                               = 0x09;
	static const uint8_t REG_0A_OSC1                                 = 0x0a;
	static const uint8_t REG_0B_AFCCTRL                              = 0x0b;
	static const uint8_t REG_0C_RESERVED                             = 0x0c;
	static const uint8_t REG_0D_LISTEN1                              = 0x0d;
	static const uint8_t REG_0E_LISTEN2                              = 0x0e;
	static const uint8_t REG_0F_LISTEN3                              = 0x0f;
	static const uint8_t REG_10_VERSION                              = 0x10;
	static const uint8_t REG_11_PALEVEL                              = 0x11;
	static const uint8_t REG_12_PARAMP                               = 0x12;
	static const uint8_t REG_13_OCP                                  = 0x13;
	static const uint8_t REG_14_RESERVED                             = 0x14;
	static const uint8_t REG_15_RESERVED                             = 0x15;
	static const uint8_t REG_16_RESERVED                             = 0x16;
	static const uint8_t REG_17_RESERVED                             = 0x17;
	static const uint8_t REG_18_LNA                                  = 0x18;
	static const uint8_t REG_19_RXBW                                 = 0x19;
	static const uint8_t REG_1A_AFCBW                                = 0x1a;
	static const uint8_t REG_1B_OOKPEAK                              = 0x1b;
	static const uint8_t REG_1C_OOKAVG                               = 0x1c;
	static const uint8_t REG_1D_OOKFIX                               = 0x1d;
	static const uint8_t REG_1E_AFCFEI                               = 0x1e;
	static const uint8_t REG_1F_AFCMSB                               = 0x1f;
	static const uint8_t REG_20_AFCLSB                               = 0x20;
	static const uint8_t REG_21_FEIMSB                               = 0x21;
	static const uint8_t REG_22_FEILSB                               = 0x22;
	static const uint8_t REG_23_RSSICONFIG                           = 0x23;
	static const uint8_t REG_24_RSSIVALUE                            = 0x24;
	static const uint8_t REG_25_DIOMAPPING1                          = 0x25;
	static const uint8_t REG_26_DIOMAPPING2                          = 0x26;
	static const uint8_t REG_27_IRQFLAGS1                            = 0x27;
	static const uint8_t REG_28_IRQFLAGS2                            = 0x28;
	static const uint8_t REG_29_RSSITHRESH                           = 0x29;
	static const uint8_t REG_2A_RXTIMEOUT1                           = 0x2a;
	static const uint8_t REG_2B_RXTIMEOUT2                           = 0x2b;
	static const uint8_t REG_2C_PREAMBLEMSB                          = 0x2c;
	static const uint8_t REG_2D_PREAMBLELSB                          = 0x2d;
	static const uint8_t REG_2E_SYNCCONFIG                           = 0x2e;
	static const uint8_t REG_2F_SYNCVALUE1                           = 0x2f;
	// another 7 sync word bytes follow, 30 through 36 inclusive
	static const uint8_t REG_37_PACKETCONFIG1                        = 0x37;
	static const uint8_t REG_38_PAYLOADLENGTH                        = 0x38;
	static const uint8_t REG_39_NODEADRS                             = 0x39;
	static const uint8_t REG_3A_BROADCASTADRS                        = 0x3a;
	static const uint8_t REG_3B_AUTOMODES                            = 0x3b;
	static const uint8_t REG_3C_FIFOTHRESH                           = 0x3c;
	static const uint8_t REG_3D_PACKETCONFIG2                        = 0x3d;
	static const uint8_t REG_3E_AESKEY1                              = 0x3e;
	// Another 15 AES key bytes follow
	static const uint8_t REG_4E_TEMP1                                = 0x4e;
	static const uint8_t REG_4F_TEMP2                                = 0x4f;
	static const uint8_t REG_58_TESTLNA                              = 0x58;
	static const uint8_t REG_5A_TESTPA1                              = 0x5a;
	static const uint8_t REG_5C_TESTPA2                              = 0x5c;
	static const uint8_t REG_6F_TESTDAGC                             = 0x6f;
	static const uint8_t REG_71_TESTAFC                              = 0x71;

	// These register masks etc are named wherever possible
	// corresponding to the bit and field names in the RFM69 Manual

	// REG_01_OPMODE
	static const uint8_t OPMODE_SEQUENCEROFF                         = 0x80;
	static const uint8_t OPMODE_LISTENON                             = 0x40;
	static const uint8_t OPMODE_LISTENABORT                          = 0x20;
	static const uint8_t OPMODE_MODE                                 = 0x1c;
	static const uint8_t OPMODE_MODE_SLEEP                           = 0x00;
	static const uint8_t OPMODE_MODE_STDBY                           = 0x04;
	static const uint8_t OPMODE_MODE_FS                              = 0x08;
	static const uint8_t OPMODE_MODE_TX                              = 0x0c;
	static const uint8_t OPMODE_MODE_RX                              = 0x10;

	// REG_02_DATAMODUL
	static const uint8_t DATAMODUL_DATAMODE                          = 0x60;
	static const uint8_t DATAMODUL_DATAMODE_PACKET                   = 0x00;
	static const uint8_t DATAMODUL_DATAMODE_CONT_WITH_SYNC           = 0x40;
	static const uint8_t DATAMODUL_DATAMODE_CONT_WITHOUT_SYNC        = 0x60;
	static const uint8_t DATAMODUL_MODULATIONTYPE                    = 0x18;
	static const uint8_t DATAMODUL_MODULATIONTYPE_FSK                = 0x00;
	static const uint8_t DATAMODUL_MODULATIONTYPE_OOK                = 0x08;
	static const uint8_t DATAMODUL_MODULATIONSHAPING                 = 0x03;
	static const uint8_t DATAMODUL_MODULATIONSHAPING_FSK_NONE        = 0x00;
	static const uint8_t DATAMODUL_MODULATIONSHAPING_FSK_BT1_0       = 0x01;
	static const uint8_t DATAMODUL_MODULATIONSHAPING_FSK_BT0_5       = 0x02;
	static const uint8_t DATAMODUL_MODULATIONSHAPING_FSK_BT0_3       = 0x03;
	static const uint8_t DATAMODUL_MODULATIONSHAPING_OOK_NONE        = 0x00;
	static const uint8_t DATAMODUL_MODULATIONSHAPING_OOK_BR          = 0x01;
	static const uint8_t DATAMODUL_MODULATIONSHAPING_OOK_2BR         = 0x02;

	// REG_11_PALEVEL
	static const uint8_t PALEVEL_PA0ON                               = 0x80;
	static const uint8_t PALEVEL_PA1ON                               = 0x40;
	static const uint8_t PALEVEL_PA2ON                               = 0x20;
	static const uint8_t PALEVEL_OUTPUTPOWER                         = 0x1f;

	// REG_23_RSSICONFIG
	static const uint8_t RSSICONFIG_RSSIDONE                         = 0x02;
	static const uint8_t RSSICONFIG_RSSISTART                        = 0x01;

	// REG_25_DIOMAPPING1
	static const uint8_t DIOMAPPING1_DIO0MAPPING                     = 0xc0;
	static const uint8_t DIOMAPPING1_DIO0MAPPING_00                  = 0x00;
	static const uint8_t DIOMAPPING1_DIO0MAPPING_01                  = 0x40;
	static const uint8_t DIOMAPPING1_DIO0MAPPING_10                  = 0x80;
	static const uint8_t DIOMAPPING1_DIO0MAPPING_11                  = 0xc0;

	static const uint8_t DIOMAPPING1_DIO1MAPPING                     = 0x30;
	static const uint8_t DIOMAPPING1_DIO1MAPPING_00                  = 0x00;
	static const uint8_t DIOMAPPING1_DIO1MAPPING_01                  = 0x10;
	static const uint8_t DIOMAPPING1_DIO1MAPPING_10                  = 0x20;
	static const uint8_t DIOMAPPING1_DIO1MAPPING_11                  = 0x30;

	static const uint8_t DIOMAPPING1_DIO2MAPPING                     = 0x0c;
	static const uint8_t DIOMAPPING1_DIO2MAPPING_00                  = 0x00;
	static const uint8_t DIOMAPPING1_DIO2MAPPING_01                  = 0x04;
	static const uint8_t DIOMAPPING1_DIO2MAPPING_10                  = 0x08;
	static const uint8_t DIOMAPPING1_DIO2MAPPING_11                  = 0x0c;

	static const uint8_t DIOMAPPING1_DIO3MAPPING                     = 0x03;
	static const uint8_t DIOMAPPING1_DIO3MAPPING_00                  = 0x00;
	static const uint8_t DIOMAPPING1_DIO3MAPPING_01                  = 0x01;
	static const uint8_t DIOMAPPING1_DIO3MAPPING_10                  = 0x02;
	static const uint8_t DIOMAPPING1_DIO3MAPPING_11                  = 0x03;

	// REG_26_DIOMAPPING2
	static const uint8_t DIOMAPPING2_DIO4MAPPING                     = 0xc0;
	static const uint8_t DIOMAPPING2_DIO4MAPPING_00                  = 0x00;
	static const uint8_t DIOMAPPING2_DIO4MAPPING_01                  = 0x40;
	static const uint8_t DIOMAPPING2_DIO4MAPPING_10                  = 0x80;
	static const uint8_t DIOMAPPING2_DIO4MAPPING_11                  = 0xc0;

	static const uint8_t DIOMAPPING2_DIO5MAPPING                     = 0x30;
	static const uint8_t DIOMAPPING2_DIO5MAPPING_00                  = 0x00;
	static const uint8_t DIOMAPPING2_DIO5MAPPING_01                  = 0x10;
	static const uint8_t DIOMAPPING2_DIO5MAPPING_10                  = 0x20;
	static const uint8_t DIOMAPPING2_DIO5MAPPING_11                  = 0x30;

	static const uint8_t DIOMAPPING2_CLKOUT                          = 0x07;
	static const uint8_t DIOMAPPING2_CLKOUT_FXOSC_                   = 0x00;
	static const uint8_t DIOMAPPING2_CLKOUT_FXOSC_2                  = 0x01;
	static const uint8_t DIOMAPPING2_CLKOUT_FXOSC_4                  = 0x02;
	static const uint8_t DIOMAPPING2_CLKOUT_FXOSC_8                  = 0x03;
	static const uint8_t DIOMAPPING2_CLKOUT_FXOSC_16                 = 0x04;
	static const uint8_t DIOMAPPING2_CLKOUT_FXOSC_32                 = 0x05;
	static const uint8_t DIOMAPPING2_CLKOUT_FXOSC_RC                 = 0x06;
	static const uint8_t DIOMAPPING2_CLKOUT_FXOSC_OFF                = 0x07;

	// REG_27_IRQFLAGS1
	static const uint8_t IRQFLAGS1_MODEREADY                         = 0x80;
	static const uint8_t IRQFLAGS1_RXREADY                           = 0x40;
	static const uint8_t IRQFLAGS1_TXREADY                           = 0x20;
	static const uint8_t IRQFLAGS1_PLLLOCK                           = 0x10;
	static const uint8_t IRQFLAGS1_RSSI                              = 0x08;
	static const uint8_t IRQFLAGS1_TIMEOUT                           = 0x04;
	static const uint8_t IRQFLAGS1_AUTOMODE                          = 0x02;
	static const uint8_t IRQFLAGS1_SYNADDRESSMATCH                   = 0x01;

	// REG_28_IRQFLAGS2
	static const uint8_t IRQFLAGS2_FIFOFULL                          = 0x80;
	static const uint8_t IRQFLAGS2_FIFONOTEMPTY                      = 0x40;
	static const uint8_t IRQFLAGS2_FIFOLEVEL                         = 0x20;
	static const uint8_t IRQFLAGS2_FIFOOVERRUN                       = 0x10;
	static const uint8_t IRQFLAGS2_PACKETSENT                        = 0x08;
	static const uint8_t IRQFLAGS2_PAYLOADREADY                      = 0x04;
	static const uint8_t IRQFLAGS2_CRCOK                             = 0x02;

	// REG_2E_SYNCCONFIG
	static const uint8_t SYNCCONFIG_SYNCON                           = 0x80;
	static const uint8_t SYNCCONFIG_FIFOFILLCONDITION_MANUAL         = 0x40;
	static const uint8_t SYNCCONFIG_SYNCSIZE                         = 0x38;
	static const uint8_t SYNCCONFIG_SYNCSIZE_1                       = 0x00;
	static const uint8_t SYNCCONFIG_SYNCSIZE_2                       = 0x08;
	static const uint8_t SYNCCONFIG_SYNCSIZE_3                       = 0x10;
	static const uint8_t SYNCCONFIG_SYNCSIZE_4                       = 0x18;
	static const uint8_t SYNCCONFIG_SYNCSIZE_5                       = 0x20;
	static const uint8_t SYNCCONFIG_SYNCSIZE_6                       = 0x28;
	static const uint8_t SYNCCONFIG_SYNCSIZE_7                       = 0x30;
	static const uint8_t SYNCCONFIG_SYNCSIZE_8                       = 0x38;
	static const uint8_t SYNCCONFIG_SYNCSIZE_SYNCTOL                 = 0x07;

	// REG_37_PACKETCONFIG1
	static const uint8_t PACKETCONFIG1_PACKETFORMAT_VARIABLE         = 0x80;
	static const uint8_t PACKETCONFIG1_DCFREE                        = 0x60;
	static const uint8_t PACKETCONFIG1_DCFREE_NONE                   = 0x00;
	static const uint8_t PACKETCONFIG1_DCFREE_MANCHESTER             = 0x20;
	static const uint8_t PACKETCONFIG1_DCFREE_WHITENING              = 0x40;
	static const uint8_t PACKETCONFIG1_DCFREE_RESERVED               = 0x60;
	static const uint8_t PACKETCONFIG1_CRC_ON                        = 0x10;
	static const uint8_t PACKETCONFIG1_CRCAUTOCLEAROFF               = 0x08;
	static const uint8_t PACKETCONFIG1_ADDRESSFILTERING              = 0x06;
	static const uint8_t PACKETCONFIG1_ADDRESSFILTERING_NONE         = 0x00;
	static const uint8_t PACKETCONFIG1_ADDRESSFILTERING_NODE         = 0x02;
	static const uint8_t PACKETCONFIG1_ADDRESSFILTERING_NODE_BC      = 0x04;
	static const uint8_t PACKETCONFIG1_ADDRESSFILTERING_RESERVED     = 0x06;

	// REG_3C_FIFOTHRESH
	static const uint8_t FIFOTHRESH_TXSTARTCONDITION_NOTEMPTY        = 0x80;
	static const uint8_t FIFOTHRESH_FIFOTHRESHOLD                    = 0x7f;

	// REG_3D_PACKETCONFIG2
	static const uint8_t PACKETCONFIG2_INTERPACKETRXDELAY            = 0xf0;
	static const uint8_t PACKETCONFIG2_RESTARTRX                     = 0x04;
	static const uint8_t PACKETCONFIG2_AUTORXRESTARTON               = 0x02;
	static const uint8_t PACKETCONFIG2_AESON                         = 0x01;

	// REG_4E_TEMP1
	static const uint8_t TEMP1_TEMPMEASSTART                         = 0x08;
	static const uint8_t TEMP1_TEMPMEASRUNNING                       = 0x04;

	// REG_5A_TESTPA1
	static const uint8_t TESTPA1_NORMAL                              = 0x55;
	static const uint8_t TESTPA1_BOOST                               = 0x5d;

	// REG_5C_TESTPA2
	static const uint8_t TESTPA2_NORMAL                              = 0x70;
	static const uint8_t TESTPA2_BOOST                               = 0x7c;

	// REG_6F_TESTDAGC
	static const uint8_t TESTDAGC_CONTINUOUSDAGC_NORMAL              = 0x00;
	static const uint8_t TESTDAGC_CONTINUOUSDAGC_IMPROVED_LOWBETAON  = 0x20;
	static const uint8_t TESTDAGC_CONTINUOUSDAGC_IMPROVED_LOWBETAOFF = 0x30;

	// It is important to keep the modulation index for FSK between 0.5 and 10
	// modulation index = 2 * Fdev / BR
	// Note that I have not had much success with FSK with Fd > ~5
	// You have to construct these by hand, using the data from the RF69 Datasheet :-(
	static const uint8_t CONFIG_FSK = (DATAMODUL_DATAMODE_PACKET | DATAMODUL_MODULATIONTYPE_FSK | DATAMODUL_MODULATIONSHAPING_FSK_NONE);
	static const uint8_t CONFIG_GFSK = (DATAMODUL_DATAMODE_PACKET | DATAMODUL_MODULATIONTYPE_FSK | DATAMODUL_MODULATIONSHAPING_FSK_BT1_0);
	static const uint8_t CONFIG_OOK = (DATAMODUL_DATAMODE_PACKET | DATAMODUL_MODULATIONTYPE_OOK | DATAMODUL_MODULATIONSHAPING_OOK_NONE);

	// Choices for REG_37_PACKETCONFIG1:
	static const uint8_t CONFIG_NOWHITE = (PACKETCONFIG1_PACKETFORMAT_VARIABLE | PACKETCONFIG1_DCFREE_NONE | PACKETCONFIG1_CRC_ON | PACKETCONFIG1_ADDRESSFILTERING_NONE);
	static const uint8_t CONFIG_WHITE = (PACKETCONFIG1_PACKETFORMAT_VARIABLE | PACKETCONFIG1_DCFREE_WHITENING | PACKETCONFIG1_ADDRESSFILTERING_NONE);
	static const uint8_t CONFIG_MANCHESTER = (PACKETCONFIG1_PACKETFORMAT_VARIABLE | PACKETCONFIG1_DCFREE_MANCHESTER | PACKETCONFIG1_ADDRESSFILTERING_NONE);

} // RH_RF69Const
//---------------------------------------------------------------------------
	enum TArduinoLoRaRFM69Modulation
	{
		mFrequencyShiftKeying_Gaussian_1_0,
		mFrequencyShiftKeying_Gaussian_0_5,
		mFrequencyShiftKeying_Gaussian_0_3,
		mOnOffKeying_NoShaping,
		mOnOffKeying_Cutoff_BitRate,
		mOnOffKeying_Cutoff_2_BitRate
	};
//---------------------------------------------------------------------------
	enum TArduinoLoRaRFM69DCCCutoffFrequency
	{
		rfmDCCCutoff16,
		rfmDCCCutoff8,
		rfmDCCCutoff4,
		rfmDCCCutoff2,
		rfmDCCCutoff1,
		rfmDCCCutoff0_5,
		rfmDCCCutoff0_25,
		rfmDCCCutoff0_125
	};
//---------------------------------------------------------------------------
	enum TArduinoLoRaRFM69ChannelFilterBandwidth
	{
		bfFSK_2_6_OOK_1_3,
		bfFSK_3_1_OOK_1_6,
		bfFSK_3_9_OOK_2_0,
		bfFSK_5_2_OOK_2_6,
		bfFSK_6_3_OOK_3_1,
		bfFSK_7_8_OOK_3_9,
		bfFSK_10_4_OOK_5_2,
		bfFSK_12_5_OOK_6_3,
		bfFSK_15_6_OOK_7_8,
		bfFSK_20_8_OOK_10_4,
		bfFSK_25_0_OOK_12_5,
		bfFSK_31_3_OOK_15_6,
		bfFSK_41_7_OOK_20_8,
		bfFSK_50_0_OOK_25_0,
		bfFSK_62_5_OOK_31_3,
		bfFSK_83_3_OOK_41_7,
		bfFSK_100_0_OOK_50_0,
		bfFSK_125_0_OOK_62_5,
		bfFSK_166_7_OOK_83_3,
		bfFSK_200_0_OOK_100_0,
		bfFSK_250_0_OOK_125_0,
		bfFSK_333_3_OOK_166_7,
		bfFSK_400_0_OOK_200_0,
		bfFSK_500_0_OOK_250_0
	};
//---------------------------------------------------------------------------
	enum TArduinoLoRaRFM69DCFreeEncoding
	{
		dcfeNone,
		dcfeManchester,
		dcfeWhitening
	};
//---------------------------------------------------------------------------
	template <
		typename T_BitRate,
		typename T_CRCEnabled,
		typename T_ChannelFilterBandwidth,
		typename T_DCCCutoffFrequency,
		typename T_DCFreeEncoding,
		typename T_FrequencyDeviation,
		typename T_Modulation
	> class TArduinoLoRaRFM69ModemSettings :
		public T_BitRate,
		public T_CRCEnabled,
		public T_ChannelFilterBandwidth,
		public T_DCCCutoffFrequency,
		public T_DCFreeEncoding,
		public T_FrequencyDeviation,
		public T_Modulation
	{
	public:
		_V_PROP_( BitRate )
		_V_PROP_( CRCEnabled )
		_V_PROP_( ChannelFilterBandwidth )
		_V_PROP_( DCCCutoffFrequency )
		_V_PROP_( DCFreeEncoding )
		_V_PROP_( FrequencyDeviation )
		_V_PROP_( Modulation )

	};
//---------------------------------------------------------------------------
	template <
		typename T_ResetOutputPin
	> class LoRaRFM69_PinReset :
		public T_ResetOutputPin
	{
	public:
		_V_PIN_( ResetOutputPin )

	protected:
		void Reset()
		{
			if( T_ResetOutputPin::GetPinIsConnected() )
			{
//				Serial.println( "RESET" );
				T_ResetOutputPin::SetPinValueHigh();
				delay( 100 );
				T_ResetOutputPin::SetPinValueLow();
				delay( 100 );
			}
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_0_RESET,
		typename T_SPI, T_SPI &C_SPI,
		typename T_2_INTERRUPT,
		typename T_Address,
		typename T_BroadcastAddress,
		typename T_ChipSelectOutputPin,
		typename T_EncryptionKey,
		typename T_EncryptionKey_ApplyValues,
        typename T_EncryptionKey_GetValue,
		typename T_FMode,
		typename T_FModeChange,
		typename T_Frequency,
		typename T_MaxSendQueue,
		typename T_ModemSettings,
		typename T_OutputPin,
		typename T_Power,
		typename T_PreambleLength,
		typename T_Promiscuous,
		typename T_SPISpeed,
		typename T_SentToOutputPin,
		typename T_SignalStrengthOutputPin,
        typename T_SyncWords,
        typename T_SyncWords_ApplyValues,
        typename T_SyncWords_GetValue
	> class MitovLoRaRFM69 :
		public T_0_RESET,
		public T_2_INTERRUPT,
		public T_Address,
		public T_BroadcastAddress,
		public T_ChipSelectOutputPin,
		public T_EncryptionKey,
		public T_FMode,
		public T_FModeChange,
		public T_Frequency,
		public T_MaxSendQueue,
		public T_ModemSettings,
		public T_OutputPin,
		public T_Power,
		public T_PreambleLength,
		public T_Promiscuous,
		public T_SPISpeed,
		public T_SentToOutputPin,
		public T_SignalStrengthOutputPin,
        public T_SyncWords
	{
	public:
		_V_PIN_( OutputPin )
		_V_PIN_( SentToOutputPin )
		_V_PIN_( SignalStrengthOutputPin )

	public:
		_V_PROP_( SPISpeed )
        _V_PROP_( SyncWords )
		_V_PROP_( EncryptionKey )

	protected:
		/// \brief Defines different operating modes for the transport hardware
		///
		/// These are the different values that can be adopted by the _mode variable and
		/// returned by the mode() member function,
		enum RHMode
		{
			RHModeInitialising,			///< Transport is initialising. Initial default value until init() is called..
			RHModeIdle,					///< Transport is idle.
			RHModeFrequencySynthesizer, ///< Frequency Synthesizer.
			RHModeTx,					///< Transport is in the process of transmitting a message.
			RHModeRx					///< Transport is in the process of receiving a message.
		};

	public:
		/// Sets the length of the preamble
		/// in bytes.
		/// Caution: this should be set to the same
		/// value on all nodes in your network. Default is 4.
		/// Sets the message preamble length in REG_0?_PREAMBLE?SB
		/// \param[in] bytes Preamble length in bytes.
		_V_PROP_( PreambleLength )

		/// The selected output power in dBm
		// +13dBm, same as power-on default
		_V_PROP_( Power )
		_V_PROP_( Frequency )
		_V_PROP_( Address )
		_V_PROP_( BroadcastAddress )
		_V_PROP_( MaxSendQueue )

//		ConstBytes	SyncWords;
//		uint8_t* SyncWords = nullptr;
//		uint8_t	SyncWords_Length = 0;

//		uint8_t* EncryptionKey = nullptr;

//		uint8_t* ModemConfig;

/*
		struct
		{
			uint16_t	BitRate = 0x80;
			float		FrequencyDeviation = 0.25;

			TArduinoLoRaRFM69Modulation	Modulation : 3;
			TArduinoLoRaRFM69DCCCutoffFrequency DCCCutoffFrequency : 3;
			TArduinoLoRaRFM69ChannelFilterBandwidth ChannelFilterBandwidth : 5;
			TArduinoLoRaRFM69DCFreeEncoding DCFreeEncoding : 2;
			bool	CRCEnabled : 1;

		} ModemSettings;
*/
		_V_PROP_( ModemSettings )
		_V_PROP_( Promiscuous )

	protected:
		_V_PROP_( FModeChange )
		_V_PROP_( FMode )

//		int	FInterruptPin;

		/// The value of the last received RSSI value, in some transport specific units
		volatile int8_t     FLastRssi = 0;

		Mitov::SimpleList<uint8_t *>	FSendQueue;

	public:
		// set the frequency (in Hz)
/*
		void SynthesizeFrequency(uint32_t freqHz)
		{
//			uint8_t oldMode = FMode;
//			if (oldMode == OPMODE_MODE_TX )
//				setModeRx();

			freqHz /= RF69_FSTEP; // divide down by FSTEP to get FRF
			writeReg(RH_RF69Const::REG_FRFMSB, freqHz >> 16);
			writeReg(RH_RF69Const::REG_FRFMID, freqHz >> 8);
			writeReg(RH_RF69Const::REG_FRFLSB, freqHz);
//			if (oldMode == RF69_MODE_RX)
			setModeFrequencySynthesizer();

//			setOpMode(oldMode);
		}
*/
	protected:
		uint8_t spiRead(uint8_t reg)
		{
			C_SPI.beginTransaction( SPISpeed().GetValue(), MSBFIRST, SPI_MODE0 );
			T_ChipSelectOutputPin::SetPinValueLow();
			C_SPI.transfer(reg & ~0x80); // Send the address with the write mask off
			uint8_t val = C_SPI.transfer(0); // The written value is ignored, reg value is read
			T_ChipSelectOutputPin::SetPinValueHigh();
			C_SPI.endTransaction();
			return val;
		}

		uint8_t spiWrite(uint8_t reg, uint8_t val)
		{
//Serial.print( "spiWrite: " ); Serial.print( reg, HEX ); Serial.print( " = " ); Serial.println( val, HEX );
			C_SPI.beginTransaction( SPISpeed().GetValue(), MSBFIRST, SPI_MODE0 );
			T_ChipSelectOutputPin::SetPinValueLow();
			uint8_t status = C_SPI.transfer(reg | 0x80); // Send the address with the write mask on
			C_SPI.transfer(val); // New value follows
			T_ChipSelectOutputPin::SetPinValueHigh();
			C_SPI.endTransaction();
			return status;
		}

		uint8_t spiBurstWrite( uint8_t reg, const uint8_t* src, uint8_t len, uint8_t ATotalLength )
		{
/*
			Serial.print( "spiBurstWrite: " ); Serial.print( reg, HEX ); Serial.print( " =" );
			for( int i = 0; i < len; ++i )
			{
				Serial.print( " " );
				Serial.print( src[ i ], HEX );
			}

			Serial.println( "" );
*/
			uint8_t status = 0;
			C_SPI.beginTransaction( SPISpeed().GetValue(), MSBFIRST, SPI_MODE0 );
			T_ChipSelectOutputPin::SetPinValueLow();
			status = C_SPI.transfer(reg | 0x80); // Send the start address with the write mask on

            if( len > ATotalLength )
                len = ATotalLength;

			while( len -- )
				C_SPI.transfer(*src++);

			T_ChipSelectOutputPin::SetPinValueHigh();
			C_SPI.endTransaction();
			return status;
		}

	protected:
		/// If current mode is Rx or Tx changes it to Idle. If the transmitter or receiver is running,
		/// disables them.
		void setModeIdle()
		{
			if ( FMode().GetValue() != RHModeIdle)
			{
//				Serial.println( "setModeIdle" );
				if ( Power() >= 18)
				{
					// If high power boost, return power amp to receive mode
					spiWrite( RH_RF69Const::REG_5A_TESTPA1, RH_RF69Const::TESTPA1_NORMAL );
					spiWrite( RH_RF69Const::REG_5C_TESTPA2, RH_RF69Const::TESTPA2_NORMAL );
				}

				setOpMode( RH_RF69Const::OPMODE_MODE_STDBY );
				FMode() = RHModeIdle;
			}
		}

/*
		void setModeFrequencySynthesizer()
		{
			if ( FMode != RHModeFrequencySynthesizer)
			{
				if ( Power >= 18)
				{
					// Set high power boost mode
					// Note that OCP defaults to ON so no need to change that.
					spiWrite( RH_RF69Const::REG_5A_TESTPA1, RH_RF69Const::TESTPA1_BOOST );
					spiWrite( RH_RF69Const::REG_5C_TESTPA2, RH_RF69Const::TESTPA2_BOOST );
				}

				setOpMode( OPMODE_MODE_FS );
				FMode = RHModeFrequencySynthesizer;
			}
		}
*/
		void setModeRx()
		{
			if ( FMode().GetValue() != RHModeRx)
			{
//				Serial.println( "setModeRx" );
				if ( Power() >= 18)
				{
					// If high power boost, return power amp to receive mode
					spiWrite( RH_RF69Const::REG_5A_TESTPA1, RH_RF69Const::TESTPA1_NORMAL );
					spiWrite( RH_RF69Const::REG_5C_TESTPA2, RH_RF69Const::TESTPA2_NORMAL );
				}

				spiWrite( RH_RF69Const::REG_25_DIOMAPPING1, RH_RF69Const::DIOMAPPING1_DIO0MAPPING_01 ); // Set interrupt line 0 PayloadReady
				setOpMode( RH_RF69Const::OPMODE_MODE_RX ); // Clears FIFO
				FMode() = RHModeRx;
			}
		}

		void setModeTx()
		{
			if ( FMode().GetValue() != RHModeTx)
			{
//				Serial.println( "setModeTx" );
				if ( Power() >= 18)
				{
					// Set high power boost mode
					// Note that OCP defaults to ON so no need to change that.
					spiWrite( RH_RF69Const::REG_5A_TESTPA1, RH_RF69Const::TESTPA1_BOOST );
					spiWrite( RH_RF69Const::REG_5C_TESTPA2, RH_RF69Const::TESTPA2_BOOST );
				}

				spiWrite( RH_RF69Const::REG_25_DIOMAPPING1, RH_RF69Const::DIOMAPPING1_DIO0MAPPING_00 ); // Set interrupt line 0 PacketSent
				setOpMode( RH_RF69Const::OPMODE_MODE_TX ); // Clears FIFO
				FMode() = RHModeTx;
			}
		}

/*
		uint8_t spiBurstRead(uint8_t reg, uint8_t* dest, uint8_t len)
		{
			uint8_t status = 0;
			C_SPI.beginTransaction( SPISpeed().GetValue(), MSBFIRST, SPI_MODE0 );
			T_ChipSelectOutputPin::SetPinValueLow();
			status = C_SPI.transfer(reg & ~0x80); // Send the start address with the write mask off
			while (len--)
				*dest++ = C_SPI.transfer(0);

			T_ChipSelectOutputPin::SetPinValueHigh();
			C_SPI.endTransaction();
			return status;
		}
*/
		void setOpMode(uint8_t mode)
		{
			uint8_t opmode = spiRead( RH_RF69Const::REG_01_OPMODE );
			opmode &= ~ RH_RF69Const::OPMODE_MODE;
			opmode |= ( mode & RH_RF69Const::OPMODE_MODE );
			spiWrite( RH_RF69Const::REG_01_OPMODE, opmode );

			FModeChange() = true;

			// Wait for mode to change.
//			while (!(spiRead( RH_RF69Const::REG_27_IRQFLAGS1) & IRQFLAGS1_MODEREADY ))
//				;
		}

	public:
		void __ICACHE_RAM_ATTR__ InterruptHandler( bool )
		{
			T_2_INTERRUPT::SetValue( true );
		}

	public:
		void UpdateSyncWords()
		{
			uint8_t syncconfig = spiRead( RH_RF69Const::REG_2E_SYNCCONFIG );
			if ( T_SyncWords::GetCount() )
			{
//				uint8_t *ABuffer = new uint8_t[ SyncWords._BytesSize ];
//				memcpy_P( ABuffer, SyncWords._Bytes, SyncWords._BytesSize );
//				spiBurstWrite( RH_RF69Const::REG_2F_SYNCVALUE1, ABuffer, SyncWords._BytesSize );
//				delete [] ABuffer;
                const uint8_t * ASyncWords = T_SyncWords::Allocate();
				spiBurstWrite( RH_RF69Const::REG_2F_SYNCVALUE1, ASyncWords, T_SyncWords::GetCount(), T_SyncWords::GetCount() );
                T_SyncWords::Release( ASyncWords );

				syncconfig |= RH_RF69Const::SYNCCONFIG_SYNCON;
			}

			else
				syncconfig &= ~ RH_RF69Const::SYNCCONFIG_SYNCON;

			syncconfig &= ~ RH_RF69Const::SYNCCONFIG_SYNCSIZE;
			if( T_SyncWords::GetCount() )
				syncconfig |= ( T_SyncWords::GetCount() - 1 ) << 3;

			spiWrite( RH_RF69Const::REG_2E_SYNCCONFIG, syncconfig );
		}

		void UpdatePreambleLength()
		{
			spiWrite( RH_RF69Const::REG_2C_PREAMBLEMSB, PreambleLength().GetValue() >> 8 );
			spiWrite( RH_RF69Const::REG_2D_PREAMBLELSB, PreambleLength().GetValue() & 0xFF );
		}

		void UpdateFrequency()
		{
			// Frf = FRF / FSTEP
			uint32_t frf = (Frequency() * 1000000.0) / RH_RF69Const::FSTEP;
//			Serial.println( frf );
			spiWrite( RH_RF69Const::REG_07_FRFMSB, (frf >> 16) & 0xFF );
			spiWrite( RH_RF69Const::REG_08_FRFMID, (frf >> 8) & 0xFF );
			spiWrite( RH_RF69Const::REG_09_FRFLSB, frf & 0xFF );
		}

		void UpdateEncryptionKey()
		{
			if ( T_EncryptionKey::GetCount() )
			{
//				uint8_t *ABuffer = new uint8_t[ 16 ];
//				memcpy_P( ABuffer, EncryptionKey, 16 );
//				spiBurstWrite( RH_RF69Const::REG_3E_AESKEY1, ABuffer, 16 );
//				delete [] ABuffer;
                const uint8_t *AEncryptionKey = T_EncryptionKey::Allocate();
				spiBurstWrite( RH_RF69Const::REG_3E_AESKEY1, AEncryptionKey, T_EncryptionKey::GetCount(), 16 );
                T_EncryptionKey::Release( AEncryptionKey );
				spiWrite( RH_RF69Const::REG_3D_PACKETCONFIG2, spiRead( RH_RF69Const::REG_3D_PACKETCONFIG2) | RH_RF69Const::PACKETCONFIG2_AESON );
			}

			else
				spiWrite( RH_RF69Const::REG_3D_PACKETCONFIG2, spiRead( RH_RF69Const::REG_3D_PACKETCONFIG2) & ~ RH_RF69Const::PACKETCONFIG2_AESON );
		}

		void UpdateTransmitPower()
		{
			uint8_t palevel;
			if ( Power().GetValue() < -18)
				Power() = -18;

			// See http://www.hoperf.com/upload/rfchip/RF69-V1.2.pdf section 3.3.6
			// for power formulas
			if (Power() <= 13)
			{
				// -18dBm to +13dBm
				palevel = RH_RF69Const::PALEVEL_PA0ON | ((Power() + 18) & RH_RF69Const::PALEVEL_OUTPUTPOWER );
			}
			else if (Power() >= 18)
			{
				// +18dBm to +20dBm
				// Need PA1+PA2
				// Also need PA boost settings change when tx is turned on and off, see setModeTx()
				palevel = RH_RF69Const::PALEVEL_PA1ON | RH_RF69Const::PALEVEL_PA2ON | ((Power() + 11) & RH_RF69Const::PALEVEL_OUTPUTPOWER );
			}
			else
			{
				// +14dBm to +17dBm
				// Need PA1+PA2
				palevel = RH_RF69Const::PALEVEL_PA1ON | RH_RF69Const::PALEVEL_PA2ON | ((Power() + 14) & RH_RF69Const::PALEVEL_OUTPUTPOWER );
			}

			spiWrite( RH_RF69Const::REG_11_PALEVEL, palevel );
		}

		void UpdateAddress()
		{
			spiWrite( RH_RF69Const::REG_39_NODEADRS, Address() );
			spiWrite( RH_RF69Const::REG_3A_BROADCASTADRS, BroadcastAddress() );
		}

		// Sets registers from a canned modem configuration structure
		void UpdateModemRegisters()
		{
			const uint8_t CModulations[] =
			{
				RH_RF69Const::DATAMODUL_DATAMODE_PACKET | RH_RF69Const::DATAMODUL_MODULATIONTYPE_FSK | RH_RF69Const::DATAMODUL_MODULATIONSHAPING_FSK_BT1_0,
				RH_RF69Const::DATAMODUL_DATAMODE_PACKET | RH_RF69Const::DATAMODUL_MODULATIONTYPE_FSK | RH_RF69Const::DATAMODUL_MODULATIONSHAPING_FSK_BT0_5,
				RH_RF69Const::DATAMODUL_DATAMODE_PACKET | RH_RF69Const::DATAMODUL_MODULATIONTYPE_FSK | RH_RF69Const::DATAMODUL_MODULATIONSHAPING_FSK_BT0_3,

				RH_RF69Const::DATAMODUL_DATAMODE_PACKET | RH_RF69Const::DATAMODUL_MODULATIONTYPE_OOK | RH_RF69Const::DATAMODUL_MODULATIONSHAPING_OOK_NONE,
				RH_RF69Const::DATAMODUL_DATAMODE_PACKET | RH_RF69Const::DATAMODUL_MODULATIONTYPE_OOK | RH_RF69Const::DATAMODUL_MODULATIONSHAPING_OOK_BR,
				RH_RF69Const::DATAMODUL_DATAMODE_PACKET | RH_RF69Const::DATAMODUL_MODULATIONTYPE_OOK | RH_RF69Const::DATAMODUL_MODULATIONSHAPING_OOK_2BR
			};

			const uint8_t ChannelFilterBandwidths[] =
			{
				0b10000 | 7,
				0b01000 | 7,
				0b00000 | 7,
				0b10000 | 6,
				0b01000 | 6,
				0b00000 | 6,
				0b10000 | 5,
				0b01000 | 5,
				0b00000 | 5,
				0b10000 | 4,
				0b01000 | 4,
				0b00000 | 4,
				0b10000 | 3,
				0b01000 | 3,
				0b00000 | 3,
				0b10000 | 2,
				0b01000 | 2,
				0b00000 | 2,
				0b10000 | 1,
				0b01000 | 1,
				0b00000 | 1,
				0b10000 | 0,
				0b01000 | 0,
				0b00000 | 0
			};

			spiWrite( RH_RF69Const::REG_02_DATAMODUL, CModulations[ ModemSettings().Modulation() ] );
			spiWrite( RH_RF69Const::REG_03_BITRATEMSB, ModemSettings().BitRate().GetValue() >> 8 );
			spiWrite( RH_RF69Const::REG_04_BITRATELSB, ModemSettings().BitRate() );

			uint32_t frf = ( ModemSettings().FrequencyDeviation() * 1000000.0) / RH_RF69Const::FSTEP;
			spiWrite( RH_RF69Const::REG_05_FDEVMSB, frf >> 8 );
			spiWrite( RH_RF69Const::REG_06_FDEVLSB, frf );

			uint8_t AValue = ModemSettings().DCCCutoffFrequency().GetValue() << 5 | ChannelFilterBandwidths[ ModemSettings().ChannelFilterBandwidth() ];
			spiWrite( RH_RF69Const::REG_19_RXBW, AValue );

			AValue = RH_RF69Const::PACKETCONFIG1_PACKETFORMAT_VARIABLE | uint8_t( ModemSettings().DCFreeEncoding().GetValue() ) << 5 | ( ModemSettings().CRCEnabled() ? RH_RF69Const::PACKETCONFIG1_CRC_ON : 0 );
			if( ! Promiscuous() )
				AValue |= RH_RF69Const::PACKETCONFIG1_ADDRESSFILTERING_NODE_BC;

			spiWrite( RH_RF69Const::REG_37_PACKETCONFIG1, AValue );
		}

	public:
		void send( uint8_t ATxHeaderTo, const uint8_t* data, uint8_t len )
		{
//				Serial.println( "TRY SEND" );
//				Serial.println( len );
//				Serial.println( FTestCount );
//				Serial.println( int( FMode ));
//			if (len > MAX_MESSAGE_LEN)
//				return;

//			waitPacketSent(); // Make sure we dont interrupt an outgoing message // _mode == RHModeTx

			if( FMode() == RHModeTx )
			{
				while( FSendQueue.size() > MaxSendQueue() )
					;

				uint8_t *ABuffer = new uint8_t[ len + 2 ];
				ABuffer[ 0 ] = ATxHeaderTo;
				ABuffer[ 1 ] = len;
				memcpy( ABuffer + 2, data, len );
				FSendQueue.push_back( ABuffer );
				return;
			}

			setModeIdle(); // Prevent RX while filling the fifo
//			Serial.println( "SEND" );

			C_SPI.beginTransaction( SPISpeed().GetValue(), MSBFIRST, SPI_MODE0 );
			T_ChipSelectOutputPin::SetPinValueLow();
			C_SPI.transfer( RH_RF69Const::REG_00_FIFO | 0x80 ); // Send the start address with the write mask on
			C_SPI.transfer( len + 1 ); // Include length of headers
			// First the 4 headers
			C_SPI.transfer( ATxHeaderTo );
//			C_SPI.transfer( Address );
//			C_SPI.transfer(_txHeaderId);
//			C_SPI.transfer(_txHeaderFlags);
			// Now the payload
			while( len-- )
				C_SPI.transfer( *data++ );

			T_ChipSelectOutputPin::SetPinValueHigh();
			C_SPI.endTransaction();

			setModeTx(); // Start the transmitter
		}

	public:
		inline void SystemInit()
		{
			T_ChipSelectOutputPin::SetPinValueHigh();

			T_0_RESET::Reset();

			setModeIdle();

			// Configure important RH_RF69 registers
			// Here we set up the standard packet format for use by the RH_RF69 library:
			// 4 bytes preamble
			// 2 SYNC words 2d, d4
			// 2 CRC CCITT octets computed on the header, length and data (this in the modem config data)
			// 0 to 60 bytes data
			// RSSI Threshold -114dBm
			// We dont use the RH_RF69s address filtering: instead we prepend our own headers to the beginning
			// of the RH_RF69 payload
			spiWrite( RH_RF69Const::REG_3C_FIFOTHRESH, RH_RF69Const::FIFOTHRESH_TXSTARTCONDITION_NOTEMPTY | 0x0f); // thresh 15 is default
			// RSSITHRESH is default
		//    spiWrite(REG_29_RSSITHRESH, 220); // -110 dbM
			// SYNCCONFIG is default. SyncSize is set later by setSyncWords()
		//    spiWrite(RH_RF69Const::REG_2E_SYNCCONFIG, SYNCCONFIG_SYNCON); // auto, tolerance 0
			// PAYLOADLENGTH is default
		//    spiWrite(RH_RF69Const::REG_38_PAYLOADLENGTH, FIFO_SIZE); // max size only for RX
			// PACKETCONFIG 2 is default
			spiWrite( RH_RF69Const::REG_6F_TESTDAGC, RH_RF69Const::TESTDAGC_CONTINUOUSDAGC_IMPROVED_LOWBETAOFF );
			// If high power boost set previously, disable it
			spiWrite( RH_RF69Const::REG_5A_TESTPA1, RH_RF69Const::TESTPA1_NORMAL );
			spiWrite( RH_RF69Const::REG_5C_TESTPA2, RH_RF69Const::TESTPA2_NORMAL );

			UpdateSyncWords();
			UpdateModemRegisters();
//			setModemConfig(GFSK_Rb250Fd250);
			UpdatePreambleLength();
			UpdateFrequency();
			UpdateEncryptionKey();
			UpdateTransmitPower();
			UpdateAddress();

//			Serial.println( "START1" );

/*
			// The following can be changed later by the user if necessary.
			// Set up default configuration
			uint8_t syncwords[] = { 0x2d, 0xd4 };
			setSyncWords(syncwords, sizeof(syncwords)); // Same as RF22's
			// Reasnably fast and reliable default speed and modulation
			setModemConfig(GFSK_Rb250Fd250);
			// 3 would be sufficient, but this is the same as RF22's
			setPreambleLength(4);
			// An innocuous ISM frequency, same as RF22's
			setFrequency(434.0);
			// No encryption
			setEncryptionKey( nullptr );
			// +13dBm, same as power-on default
			setTxPower(13);
*/
			if( T_OutputPin::GetPinIsConnected() )
				setModeRx();
		}

		inline void SystemLoopBegin()
		{
			uint8_t *APacket = nullptr;
			uint8_t ASentTo = 0;
			uint8_t APacketLength = 0;
			if( T_2_INTERRUPT::GetValue() )
			{
				T_2_INTERRUPT::SetValue( false );
				uint8_t irqflags2 = spiRead( RH_RF69Const::REG_28_IRQFLAGS2 );
				if ( FMode() == RHModeTx && ( irqflags2 & RH_RF69Const::IRQFLAGS2_PACKETSENT ))
				{
					// A transmitter message has been fully sent
	/*
					if( OutputPin.IsConnected() )
						setModeRx(); // Clears FIFO

					else
	*/
					setModeIdle(); // Clears FIFO

					//	Serial.println("PACKETSENT");
				}

				// Must look for PAYLOADREADY, not CRCOK, since only PAYLOADREADY occurs _after_ AES decryption
				// has been done
				if ( FMode() == RHModeRx && ( irqflags2 & RH_RF69Const::IRQFLAGS2_PAYLOADREADY ))
				{
					// A complete message has been received with good CRC
					FLastRssi = -( int8_t( spiRead( RH_RF69Const::REG_24_RSSIVALUE ) >> 1 ));
	//				_lastPreambleTime = millis();

					setModeIdle();
					// Save it in our buffer
					C_SPI.beginTransaction( SPISpeed().GetValue(), MSBFIRST, SPI_MODE0 );
					T_ChipSelectOutputPin::SetPinValueLow();
					C_SPI.transfer( RH_RF69Const::REG_00_FIFO ); // Send the start address with the write mask off

					uint8_t payloadlen = C_SPI.transfer( 0 ); // First byte is payload len (counting the headers)
		//			if ( ( payloadlen <= MAX_ENCRYPTABLE_PAYLOAD_LEN ) && ( payloadlen >= HEADER_LEN ) )
					if ((( EncryptionKey().GetCount() == 0 ) && ( payloadlen <= RH_RF69Const::MAX_PAYLOAD_LEN ) ) || ( payloadlen <= RH_RF69Const::MAX_ENCRYPTABLE_PAYLOAD_LEN ))
					{
						uint8_t ARxHeaderTo = C_SPI.transfer( 0 );
						// Check addressing
		//				if ( Promiscuous ||
		//					ARxHeaderTo == Address ||
		//					ARxHeaderTo == BroadcastAddress )
						{
							APacket = new uint8_t[ payloadlen ];
							ASentTo = ARxHeaderTo;
							APacketLength = payloadlen - 1; // Skip the ARxHeaderTo
							for ( int i = 0; i < APacketLength; ++ i )
								APacket[ i ] = C_SPI.transfer( 0 );
		/*
							uint8_t *ABuffer = new uint8_t[ 1 + (payloadlen - HEADER_LEN) + 3 ];
							FReceivedPackets.push_back( ABuffer );
							*ABuffer++ = payloadlen;
							for ( int i = 0; i < (payloadlen - HEADER_LEN) + 3; ++i )
								*ABuffer++ =C_SPI.transfer(0);
		*/
							/*
							// Get the rest of the headers
							_rxHeaderFrom  = C_SPI.transfer(0);
							_rxHeaderId    = C_SPI.transfer(0);
							_rxHeaderFlags = C_SPI.transfer(0);
							// And now the real payload
							for (_bufLen = 0; _bufLen < (payloadlen - HEADER_LEN); _bufLen++)
								_buf[_bufLen] = C_SPI.transfer(0);
		*/
		//					_rxGood++;
		//					_rxBufValid = true;
						}
					}

					T_ChipSelectOutputPin::SetPinValueHigh();
					C_SPI.endTransaction();
					// Any junk remaining in the FIFO will be cleared next time we go to receive mode.
	//				setModeRx();
				}
			}

			if( FModeChange() )
			{
				if( !( spiRead( RH_RF69Const::REG_27_IRQFLAGS1 ) & RH_RF69Const::IRQFLAGS1_MODEREADY ) )
					return;

				FModeChange() = false;
			}


			// Wait for mode to change.
//			while (!(spiRead(REG_27_IRQFLAGS1) & IRQFLAGS1_MODEREADY))
//				;

			if( APacket )
			{
//				Serial.println( "RECEIVED" );
//				Serial.println( ASize );
				T_SentToOutputPin::SetPinValue( ASentTo );
				T_OutputPin::SetPinValue( Mitov::TDataBlock( APacketLength, APacket ));
				delete [] APacket;
			}

			if( FMode() != RHModeTx )
				if( FSendQueue.size() )
				{
					uint8_t *ABuffer = FSendQueue[ 0 ];
					FSendQueue.pop_front();
					send( ABuffer[ 0 ], ABuffer + 2, ABuffer[ 1 ] );
					delete [] ABuffer;
				}

			if( FMode().GetValue() == RHModeIdle )
				if( T_OutputPin::GetPinIsConnected() )
					setModeRx();

			T_SignalStrengthOutputPin::SetPinValue( FLastRssi );
//			Serial.print( "FLastRssi: " );	Serial.println( FLastRssi );
		}

	public:
/*
		MitovBasicLoRaRFM69( BasicSPI &ASPI, int AInterruptPin, void (*AInterruptRoutine)() ) :
			C_SPI( ASPI ),
//			FInterruptPin( AInterruptPin ),
//			Modulation( mFrequencyShiftKeying_Gaussian_1_0 ),
			Promiscuous( false ),
			FModeChange( false ),
			FMode( RHModeInitialising )
		{
			ModemSettings.Modulation = mFrequencyShiftKeying_Gaussian_1_0;
			ModemSettings.DCCCutoffFrequency = rfmDCCCutoff4;
			ModemSettings.ChannelFilterBandwidth = bfFSK_500_0_OOK_250_0;
			ModemSettings.DCFreeEncoding = dcfeNone;
			ModemSettings.CRCEnabled = true;

			// Add by Adrien van den Bossche <vandenbo@univ-tlse2.fr> for Teensy
			// ARM M4 requires the below. else pin interrupt doesn't work properly.
			// On all other platforms, its innocuous, belt and braces
			pinMode( AInterruptPin, INPUT );

			int AInterruptNumber = digitalPinToInterrupt( AInterruptPin );
			attachInterrupt( AInterruptNumber, AInterruptRoutine, RISING );

			C_SPI.usingInterrupt( AInterruptNumber );

			T_ChipSelectOutputPin::SetPinValueHigh();
		}
*/
		inline MitovLoRaRFM69()
		{
			T_2_INTERRUPT::SetValue( false );
			FModeChange() = false;
			FMode() = RHModeInitialising;
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
		typename T_Address
	> class MitovLoRaSendPacket :
		public T_Address
	{
	public:
		_V_PROP_( Address )

	public:
		void Print( Mitov::String AValue )
		{
//			Serial.println( "Print" );
			AValue += "\r\n";
			C_OWNER.send( Address(), (uint8_t *)AValue.c_str(), AValue.length() );
		}

		void Print( float AValue )
		{
			char AText[ __VISUINO_FLOAT_TO_STR_LEN__ ];
			dtostrf( AValue,  1, 2, AText );
			Print( Mitov::String( AText ));
		}

		void Print( int32_t AValue )
		{
			char AText[ 16 ];
			ltoa( AValue, AText, 10 );
			Print( Mitov::String( AText ));
		}

		void Print( uint32_t AValue )
		{
			char AText[ 16 ];
			ltoa( AValue, AText, 10 );
			Print( Mitov::String( AText ));
		}

		inline void PrintChar( char AValue )
		{
			C_OWNER.send( Address(), (uint8_t*)&AValue, 1 );
		}

		inline void PrintChar( uint8_t AValue )
		{
			C_OWNER.send( Address(), &AValue, 1 );
		}

		inline void Write( uint8_t *AData, uint32_t ASize )
		{
			C_OWNER.send( Address(), AData, ASize );
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"
