////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
//     Developed by Swellington Santos                                        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
    namespace HTU31DConst
    {
        enum HTU31D_COMMAND
        {
            READ_T_DATA = 0x0,
            READ_RH_DATA = 0x10,
            START_CONV = 0x40,
            READ_SERIAL = 0x0A,
            HEATER_ON = 0x04,
            HEATER_OFF = 0x02,
            RESET = 0x1E
            // READ_DIAG = 0x08
        };
    }

    //---------------------------------------------------------------------------
    template <
        typename T_Averaging,
        typename T_Enabled,
        typename T_OutputPin>
    class TArduinoTemperatureHumidityHTU31DHumidity : 
        public T_Averaging,
        public T_Enabled,
        public T_OutputPin
    {
    public:
        _V_PIN_(OutputPin)

    public:
        _V_PROP_(Averaging)
        _V_PROP_(Enabled)
    };
    //---------------------------------------------------------------------------
    template <
        typename T_Averaging,
        typename T_Enabled,
        typename T_InFahrenheit,
        typename T_OutputPin>
    class TArduinoTemperatureHumidityHTU31DTemperature : 
        public T_Averaging,
        public T_Enabled,
        public T_InFahrenheit,
        public T_OutputPin
    {
    public:
        _V_PIN_(OutputPin)

    public:
        _V_PROP_(Averaging)
        _V_PROP_(InFahrenheit)
        _V_PROP_(Enabled)
    };
    //---------------------------------------------------------------------------
    template <
        typename T_I2C, T_I2C &C_I2C,
        typename T_Address,
        typename T_ClockInputPin_o_IsConnected,
        typename T_Enabled,
        typename T_FClocked,
        typename T_FInitialized,
        typename T_FRequesting,
        typename T_Heating,
        typename T_Humidity,
        typename T_Temperature>
    class TArduinoTemperatureHumidityHTU31D : 
        public T_Address,
        public T_ClockInputPin_o_IsConnected,
        public T_Enabled,
        public T_FClocked,
        public T_FInitialized,
        public T_FRequesting,
        public T_Heating,
        public T_Humidity,
        public T_Temperature
    {
    public:
        _V_PROP_(Address)
        _V_PROP_(Enabled)
        _V_PROP_(Humidity)
        _V_PROP_(Temperature)
        _V_PROP_(Heating)

    private:
        _V_PROP_(FClocked)
        _V_PROP_(FInitialized)
        _V_PROP_(FRequesting)

    private:
        uint32_t FLastTime = millis();

    public:
        inline void UpdateHeating()
        {
            if (Heating())
                Write8(HTU31DConst::HEATER_ON, 1);

            else
                Write8(HTU31DConst::HEATER_OFF, 1);

        }

    private:
        inline uint8_t SetBitsAtPosition(uint8_t b, uint8_t value, uint8_t position)
        {
            uint8_t mask = ~(0b11 << position);
            b &= mask;
            b |= (value << position);
            return b;
        }

        inline uint8_t GetAveragingData(uint8_t avg)
        {
            switch (avg)
            {
            case 8:
                return 0b11;
            case 4:
                return 0b10;
            case 2:
                return 0b01;
            default:
                return 0b0;
            }
        }

        void Write8(uint8_t ARegister, uint8_t AValue) const
        {
            C_I2C.beginTransmission(uint8_t(Address()));
            C_I2C.write(ARegister);
            C_I2C.write(AValue);
            C_I2C.endTransmission();
        }

        uint32_t Read24(uint8_t ARegister) const
        {
            C_I2C.beginTransmission(uint8_t(Address()));
            C_I2C.write(ARegister);
            C_I2C.endTransmission(false);
            C_I2C.requestFrom(uint8_t(Address()), _VISUINO_I2C_SIZE_(3));
            uint32_t AValue = C_I2C.read();
            AValue <<= 8;
            AValue |= C_I2C.read();
            AValue <<= 8;
            AValue |= C_I2C.read();
            return AValue;
        }

        uint8_t Read8(uint8_t ARegister) const
        {
            C_I2C.beginTransmission(uint8_t(Address()));
            C_I2C.write(ARegister);
            C_I2C.endTransmission(false);
            C_I2C.requestFrom(uint8_t(Address()), _VISUINO_I2C_SIZE_(1));
            return C_I2C.read();
        }

        bool ReadBuffer(uint8_t ARegister, uint8_t *buffer, uint8_t len) const
        {
            C_I2C.beginTransmission(uint8_t(Address()));
            C_I2C.write(ARegister);
            C_I2C.endTransmission(false);
            C_I2C.requestFrom(uint8_t(Address()), _VISUINO_I2C_SIZE_(len));

            if (C_I2C.available() == _VISUINO_I2C_SIZE_(len))
            {
                for (uint8_t i = 0; i < len; i++)
                    buffer[i] = C_I2C.read();
                return true;
            }

            return false;
        }

    private:
        uint8_t calc_crc(uint16_t value) const
        {
            uint32_t polynom = 0x988000;
            uint32_t msb = 0x800000;
            uint32_t mask = 0xFF8000;
            uint32_t result = (uint32_t)value << 8;
            while (msb != 0x80)
            {
                if (result & msb)
                    result = ((result ^ polynom) & mask) | (result & ~mask);
                msb >>= 1;
                mask >>= 1;
                polynom >>= 1;
            }
            return result;
        }

        void Reset()
        {
            Write8(HTU31DConst::RESET, 1);
            delay(15);
        }

        void PrepareTemperature(uint16_t raw, uint8_t crc)
        {
            if (calc_crc(raw) == crc)
            {
                float _temperature = float(-40 + 165 * (raw / 65535.0));
                if (Temperature().InFahrenheit())
                    Temperature().OutputPin().SetPinValue(Mitov::Func::ConvertCtoF(_temperature));
                else
                    Temperature().OutputPin().SetPinValue(_temperature);
            }
        }

        void PrepareHumidity(uint16_t raw, uint8_t crc)
        {
            if (calc_crc(raw) == crc)
            {
                float _humidity = float(100 * (raw / 65535.0));
                Humidity().SetPinValue(_humidity);
            }
        }

        void ReadSensor()
        {
            if (FClocked() && (!FRequesting().GetValue()))
            {
                uint8_t cmd = HTU31DConst::START_CONV;
                cmd = SetBitsAtPosition(cmd, GetAveragingData(Humidity().Averaging()), 3);
                cmd = SetBitsAtPosition(cmd, GetAveragingData(Temperature().Averaging()), 1);
                Write8(cmd, 1);
                FRequesting() = true;
                FLastTime = millis();
            }

            if (FRequesting().GetValue() && (millis() - FLastTime > 25))
            {
                if (Temperature().Enabled() && Temperature().OutputPin().GetPinIsConnected())
                {
                    if (Humidity().Enabled() && Humidity().OutputPin().GetPinIsConnected())
                    {
                        uint8_t ABuffer[6] = {0};
                        if (ReadBuffer(HTU31DConst::READ_T_DATA, ABuffer, 6))
                        {
                            uint16_t raw_temp = ABuffer[0];
                            raw_temp <<= 8;
                            raw_temp |= ABuffer[1];
                            PrepareTemperature(raw_temp, ABuffer[2]);

                            uint16_t raw_hum = ABuffer[3];
                            raw_hum <<= 8;
                            raw_hum |= ABuffer[4];
                            PrepareHumidity(raw_hum, ABuffer[5]);
                        }
                    }
                    else
                    {
                        uint32_t temp_data = Read24(HTU31DConst::READ_T_DATA);
                        uint16_t raw_temp = (temp_data >> 8) & 0xffff;
                        uint8_t temp_crc = temp_data & 0xff;
                        PrepareTemperature(raw_temp, temp_crc);
                    }
                }
                else if (Humidity().Enabled() && Humidity().OutputPin().GetPinIsConnected())
                {
                    uint32_t temp_data = Read24(HTU31DConst::READ_RH_DATA);
                    uint16_t raw_hum = (temp_data >> 8) & 0xffff;
                    uint8_t temp_crc = temp_data & 0xff;
                    PrepareHumidity(raw_hum, temp_crc);
                }
                FRequesting() = false;
                FClocked() = false;
            }
        }

    public:
        inline void ClockInputPin_o_Receive(void *_data)
        {
            FClocked() = true;
            ReadSensor();
        }

        inline void ResetInputPin_o_Receive(void *_data)
        {
            Reset();
        }

    public:
        inline void SystemStart()
        {
            Reset();
            FInitialized() = true;
            UpdateHeating();
        }

        inline void SystemLoopBegin() const
        {
            if (!Enabled())
                return;
            if (FInitialized().GetValue())
                if (FClocked())
                    ReadSensor();
        }

    public:
        inline TArduinoTemperatureHumidityHTU31D()
        {
            FClocked() = false;
            FRequesting() = false;
            FInitialized() = false;
        }
    };
    //---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"