////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
	template <
		typename T_Enabled,
		typename T_IsPoweredOutputPin,
		typename T_OverCurrentOutputPin
	> class ArduinoMacchinaPowerSupplyModule :
		public T_Enabled,
		public T_IsPoweredOutputPin,
		public T_OverCurrentOutputPin
	{
	public:
		_V_PIN_( IsPoweredOutputPin )
		_V_PIN_( OverCurrentOutputPin )

	public:
		_V_PROP_( Enabled )

	public:
		inline void UpdateConfig()
		{
			Digital.Write( I_SENSE_EN, Enabled());
		}

	protected:
		void UpdateOutputs( bool AChangeOnly )
		{
			if( T_IsPoweredOutputPin::GetPinIsConnected() )
			{
				T_IsPoweredOutputPin::SetPinValue( Digital.Read( I_SENSE ), AChangeOnly );
			}

			if( T_OverCurrentOutputPin::GetPinIsConnected() )
			{
				T_OverCurrentOutputPin::SetPinValue( Digital.Read( I_SENSE_INT ), AChangeOnly );
			}
		}

	public:
		inline void SystemStart()
		{
			UpdateOutputs( true );
		}

		inline void SystemLoopBegin()
		{
			UpdateOutputs( false );
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"