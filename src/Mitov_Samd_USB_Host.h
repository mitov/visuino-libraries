////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>

#ifndef Notify
  #define Notify(...) ((void)0)
#endif
#include <Usb.h>

#ifdef Notify
  #undef Notify
#endif

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
	class TArduinoSamdUSBHostModule
	{
	protected:
		USBHost FUSBHost;

	public:
		inline USBHost *GetUSBHost() { return &FUSBHost; }

	public:
		inline void SystemInit()
		{
#ifdef SEEEDUINO_WIO_TERMINAL
			FUSBHost.Init();
			Digital.Write(PIN_USB_HOST_ENABLE, false );
			Digital.Write(OUTPUT_CTR_5V, true );
#endif // SEEEDUINO_WIO_TERMINAL
		}

		inline void SystemLoopBegin()
		{
			FUSBHost.Task();
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"
