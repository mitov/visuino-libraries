////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>

#include "Mitov_BuildChecks_Begin.h"

#ifndef BUFFER_LENGTH
	#define BUFFER_LENGTH 256
#endif

namespace Mitov
{
//---------------------------------------------------------------------------
	template <
		typename T_ButtonsOutputPins_Up,
		typename T_ButtonsOutputPins_Down,
		typename T_ButtonsOutputPins_Left,
		typename T_ButtonsOutputPins_Right,
		typename T_ButtonsOutputPins_Select,
		typename T_JoystickOutputPins_X,
		typename T_JoystickOutputPins_Y
	> class TArduinoFeatherJoystickShield_Impl :
		public T_ButtonsOutputPins_Up,
		public T_ButtonsOutputPins_Down,
		public T_ButtonsOutputPins_Left,
		public T_ButtonsOutputPins_Right,
		public T_ButtonsOutputPins_Select,
		public T_JoystickOutputPins_X,
		public T_JoystickOutputPins_Y
	{
	public:
		_V_PIN_( ButtonsOutputPins_Up )
		_V_PIN_( ButtonsOutputPins_Down )
		_V_PIN_( ButtonsOutputPins_Left )
		_V_PIN_( ButtonsOutputPins_Right )
        _V_PIN_( ButtonsOutputPins_Select )

	public:
        _V_PIN_( JoystickOutputPins_X )
        _V_PIN_( JoystickOutputPins_Y )

	protected:
		static const uint8_t SEESAW_BUTTON_RIGHT = 6;
		static const uint8_t SEESAW_BUTTON_DOWN = 7;
		static const uint8_t SEESAW_BUTTON_LEFT = 9;
		static const uint8_t SEESAW_BUTTON_UP = 10;
		static const uint8_t SEESAW_BUTTON_SELECT = 14;

	protected:
		static const uint8_t C_PIN_ANALOG_X = 1;
		static const uint8_t C_PIN_ANALOG_Y = 0;

	protected:
		inline bool GetDigitalPinConnected()
		{
			return	T_ButtonsOutputPins_Up::GetPinIsConnected() ||
					T_ButtonsOutputPins_Down::GetPinIsConnected() ||
					T_ButtonsOutputPins_Left::GetPinIsConnected() ||
					T_ButtonsOutputPins_Right::GetPinIsConnected() ||
					T_ButtonsOutputPins_Select::GetPinIsConnected();
		}

		inline uint32_t GetButtonsMask()
		{

			return (uint32_t( 1 ) << SEESAW_BUTTON_RIGHT ) | 
							(uint32_t( 1 ) << SEESAW_BUTTON_DOWN ) | 
							(uint32_t( 1 ) << SEESAW_BUTTON_LEFT ) | 
							(uint32_t( 1 ) << SEESAW_BUTTON_UP ) | 
							(uint32_t( 1 ) << SEESAW_BUTTON_SELECT );

		}

		inline void ProcessButtons( uint32_t buttons )
		{
			T_ButtonsOutputPins_Up::SetPinValue( ! (buttons & (uint32_t( 1 ) << SEESAW_BUTTON_UP )));
			T_ButtonsOutputPins_Down::SetPinValue( ! (buttons & (uint32_t( 1 ) << SEESAW_BUTTON_DOWN )));
			T_ButtonsOutputPins_Left::SetPinValue( ! (buttons & (uint32_t( 1 ) << SEESAW_BUTTON_LEFT )));
			T_ButtonsOutputPins_Right::SetPinValue( ! (buttons & (uint32_t( 1 ) << SEESAW_BUTTON_RIGHT )));
			T_ButtonsOutputPins_Select::SetPinValue( ! (buttons & (uint32_t( 1 ) << SEESAW_BUTTON_SELECT )));
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_ButtonsOutputPins_Up,
		typename T_ButtonsOutputPins_Down,
		typename T_ButtonsOutputPins_Left,
		typename T_ButtonsOutputPins_Right,
		typename T_ButtonsOutputPins_Select,
		typename T_ButtonsOutputPins_Start,
		typename T_JoystickOutputPins_X,
		typename T_JoystickOutputPins_Y
	> class TArduinoSeesawMiniGamepad_Impl :
		public T_ButtonsOutputPins_Up,
		public T_ButtonsOutputPins_Down,
		public T_ButtonsOutputPins_Left,
		public T_ButtonsOutputPins_Right,
		public T_ButtonsOutputPins_Select,
		public T_ButtonsOutputPins_Start,
		public T_JoystickOutputPins_X,
		public T_JoystickOutputPins_Y
	{
	public:
		_V_PIN_( ButtonsOutputPins_Up )
		_V_PIN_( ButtonsOutputPins_Down )
		_V_PIN_( ButtonsOutputPins_Left )
		_V_PIN_( ButtonsOutputPins_Right )
        _V_PIN_( ButtonsOutputPins_Select )
        _V_PIN_( ButtonsOutputPins_Start )

	public:
        _V_PIN_( JoystickOutputPins_X )
        _V_PIN_( JoystickOutputPins_Y )

	protected:
		static const uint8_t SEESAW_BUTTON_RIGHT = 5;
		static const uint8_t SEESAW_BUTTON_DOWN = 1;
		static const uint8_t SEESAW_BUTTON_LEFT = 2;
		static const uint8_t SEESAW_BUTTON_UP = 6;
		static const uint8_t SEESAW_BUTTON_SELECT = 0;
		static const uint8_t SEESAW_BUTTON_START = 16;

	protected:
		static const uint8_t C_PIN_ANALOG_X = 14;
		static const uint8_t C_PIN_ANALOG_Y = 15;

	protected:
		inline bool GetDigitalPinConnected()
		{
			return	T_ButtonsOutputPins_Up::GetPinIsConnected() ||
					T_ButtonsOutputPins_Down::GetPinIsConnected() ||
					T_ButtonsOutputPins_Left::GetPinIsConnected() ||
					T_ButtonsOutputPins_Right::GetPinIsConnected() ||
					T_ButtonsOutputPins_Select::GetPinIsConnected() ||
					T_ButtonsOutputPins_Start::GetPinIsConnected();
		}

		inline uint32_t GetButtonsMask()
		{

			return (uint32_t( 1 ) << SEESAW_BUTTON_RIGHT ) | 
							(uint32_t( 1 ) << SEESAW_BUTTON_DOWN ) | 
							(uint32_t( 1 ) << SEESAW_BUTTON_LEFT ) | 
							(uint32_t( 1 ) << SEESAW_BUTTON_UP ) | 
							(uint32_t( 1 ) << SEESAW_BUTTON_SELECT ) |
							(uint32_t( 1 ) << SEESAW_BUTTON_START );

		}

		inline void ProcessButtons( uint32_t buttons )
		{
			T_ButtonsOutputPins_Up::SetPinValue( ! (buttons & (uint32_t( 1 ) << SEESAW_BUTTON_UP )));
			T_ButtonsOutputPins_Down::SetPinValue( ! (buttons & (uint32_t( 1 ) << SEESAW_BUTTON_DOWN )));
			T_ButtonsOutputPins_Left::SetPinValue( ! (buttons & (uint32_t( 1 ) << SEESAW_BUTTON_LEFT )));
			T_ButtonsOutputPins_Right::SetPinValue( ! (buttons & (uint32_t( 1 ) << SEESAW_BUTTON_RIGHT )));
			T_ButtonsOutputPins_Select::SetPinValue( ! (buttons & (uint32_t( 1 ) << SEESAW_BUTTON_SELECT )));
			T_ButtonsOutputPins_Start::SetPinValue( ! (buttons & (uint32_t( 1 ) << SEESAW_BUTTON_START )));
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_I2C, T_I2C &C_I2C,
		typename T_IMPLEMENTATION,
		typename T_Address,
		typename T_FState
	> class TArduinoSeesawGamepad :
		public T_IMPLEMENTATION,
		public T_Address,
		public T_FState
	{
	public:
		_V_PROP_( Address )

	protected:
		_V_PROP_( FState )

	protected:
		unsigned long	FLastTime;

	protected:
		static const uint8_t SEESAW_STATUS_BASE = 0x00;
		static const uint8_t SEESAW_GPIO_BASE = 0x01;

		// GPIO module function address registers
		static const uint8_t SEESAW_GPIO_DIRSET_BULK = 0x02;
		static const uint8_t SEESAW_GPIO_DIRCLR_BULK = 0x03;
		static const uint8_t SEESAW_GPIO_BULK = 0x04;
		static const uint8_t SEESAW_GPIO_BULK_SET = 0x05;
		static const uint8_t SEESAW_GPIO_BULK_CLR = 0x06;
		static const uint8_t SEESAW_GPIO_BULK_TOGGLE = 0x07;
		static const uint8_t SEESAW_GPIO_INTENSET = 0x08;
		static const uint8_t SEESAW_GPIO_INTENCLR = 0x09;
		static const uint8_t SEESAW_GPIO_INTFLAG = 0x0A;
		static const uint8_t SEESAW_GPIO_PULLENSET = 0x0B;
		static const uint8_t SEESAW_GPIO_PULLENCLR = 0x0C;

		static const uint8_t SEESAW_STATUS_HW_ID = 0x01;
		static const uint8_t SEESAW_STATUS_VERSION = 0x02;
		static const uint8_t SEESAW_STATUS_OPTIONS = 0x03;
		static const uint8_t SEESAW_STATUS_TEMP = 0x04;
		static const uint8_t SEESAW_STATUS_SWRST = 0x7F;

		static const uint8_t SEESAW_TIMER_BASE = 0x08;
		static const uint8_t SEESAW_ADC_BASE = 0x09;
		static const uint8_t SEESAW_DAC_BASE = 0x0A;
		static const uint8_t SEESAW_INTERRUPT_BASE = 0x0B;
		static const uint8_t SEESAW_DAP_BASE = 0x0C;
		static const uint8_t SEESAW_EEPROM_BASE = 0x0D;
		static const uint8_t SEESAW_NEOPIXEL_BASE = 0x0E;
		static const uint8_t SEESAW_TOUCH_BASE = 0x0F;
		static const uint8_t SEESAW_KEYPAD_BASE = 0x10;
		static const uint8_t SEESAW_ENCODER_BASE = 0x11;
		static const uint8_t SEESAW_SPECTRUM_BASE = 0x12;
		static const uint8_t SEESAW_SOIL_BASE = 0x13;

		static const uint8_t SEESAW_ADC_STATUS = 0x00;
		static const uint8_t SEESAW_ADC_INTEN = 0x02;
		static const uint8_t SEESAW_ADC_INTENCLR = 0x03;
		static const uint8_t SEESAW_ADC_WINMODE = 0x04;
		static const uint8_t SEESAW_ADC_WINTHRESH = 0x05;
		static const uint8_t SEESAW_ADC_CHANNEL_OFFSET = 0x07;

		enum TState
		{
			stReset,
			stNone,
			stAnalog0,
			stAnalog1,
			stDigital
		};

	protected:
		void StartRead( uint8_t ARegHigh, uint8_t ARegLow, TState ANexState )
		{
			C_I2C.beginTransmission( uint8_t( Address() ));
			C_I2C.write( ARegHigh );
			C_I2C.write( ARegLow );
			C_I2C.endTransmission();
			FState() = ANexState;
			FLastTime = micros();
		}

		bool Read( uint8_t *ABuffer, uint8_t ACount )
		{
			C_I2C.requestFrom( uint8_t( Address() ), _VISUINO_I2C_SIZE_( ACount ));
//				Serial.println( "Read" );
//				while( ! C_I2C.available() )
//					yield();

			for( int i = 0; i < ACount; ++ i )
				*ABuffer ++ = C_I2C.read();

//			C_I2C.endTransmission( true );
		}

		float GetAnalog()
		{
			uint8_t buf[ 2 ];
			Read( buf, 2 ); //, 500);
			return ( float((uint16_t( buf[0] ) << 8) | buf[1] ) / 1023.0f );
		}

		uint32_t GetDigital()
		{
			uint8_t buf[4];
			Read( buf, 4 );
			return ( uint32_t( buf[0] ) << 24 ) | ( uint32_t( buf[1] ) << 16 ) | ( uint32_t( buf[2] ) << 8 ) | buf[3];
		}

		void Write( uint8_t ARegHigh, uint8_t ARegLow, uint8_t *ABuffer, uint8_t ACount )
		{
			C_I2C.beginTransmission( uint8_t( Address() ));
			C_I2C.write( ARegHigh );
			C_I2C.write( ARegLow );

			while( ACount -- )
				C_I2C.write( *ABuffer ++ );

			C_I2C.endTransmission( true );
		}

	protected:
		void Reset()
		{
			C_I2C.beginTransmission( uint8_t( Address() ));
			C_I2C.write( SEESAW_STATUS_BASE );
			C_I2C.write( SEESAW_STATUS_SWRST );
			C_I2C.write( 0xFF );
			C_I2C.endTransmission();

			FState() = stReset;
            FLastTime = micros();
//			FState() = stNone;
//            FLastTime = micros() + 10000; // 10mS Reset Delay
//			delay( 10 );
		}

	public:
		inline void ResetInputPin_o_Receive( void *_Data )
		{	
			Reset();
		}

	public:
		inline void SystemStart()
		{
			Reset();

//			write8(SEESAW_STATUS_BASE, SEESAW_STATUS_SWRST, 0xFF);
//			UpdateConfig();
		}

		inline void SystemLoopBegin()
		{
			switch( FState() )
			{
				case stReset:
				{
					if( micros() - FLastTime < 10000 )
						return;

					if( T_IMPLEMENTATION::GetDigitalPinConnected() )
					{
						uint32_t button_mask = T_IMPLEMENTATION::GetButtonsMask();

						uint8_t cmd[] = {(uint8_t)(button_mask >> 24), (uint8_t)(button_mask >> 16),
										(uint8_t)(button_mask >> 8), (uint8_t)button_mask};

						Write(SEESAW_GPIO_BASE, SEESAW_GPIO_DIRCLR_BULK, cmd, 4 );
						Write(SEESAW_GPIO_BASE, SEESAW_GPIO_PULLENSET, cmd, 4 );
						Write(SEESAW_GPIO_BASE, SEESAW_GPIO_BULK_SET, cmd, 4 );
					}

					FState() = stNone;
					FLastTime = micros();
					break;
				}

				case stNone:
				{
					if( micros() - FLastTime < 1000 )
						return;

	//				Serial.println( "NONE" );
					if( T_IMPLEMENTATION::JoystickOutputPins_Y().GetPinIsConnected() )
						StartRead( SEESAW_ADC_BASE, SEESAW_ADC_CHANNEL_OFFSET + T_IMPLEMENTATION::C_PIN_ANALOG_Y, stAnalog0 );

					else if( T_IMPLEMENTATION::JoystickOutputPins_X().GetPinIsConnected() )
						StartRead( SEESAW_ADC_BASE, SEESAW_ADC_CHANNEL_OFFSET + T_IMPLEMENTATION::C_PIN_ANALOG_X, stAnalog1 );

					else if( GetDigitalPinConnected() )
						StartRead( SEESAW_GPIO_BASE, SEESAW_GPIO_BULK, stDigital );

					break;
				}

				case stAnalog0: // X
				{
					if( micros() - FLastTime < 500 )
						return;

//					Serial.println( "ANALOG0" );

					T_IMPLEMENTATION::JoystickOutputPins_Y().SetPinValue( 1.0f - GetAnalog() );
					if( T_IMPLEMENTATION::JoystickOutputPins_X().GetPinIsConnected() )
						StartRead( SEESAW_ADC_BASE, SEESAW_ADC_CHANNEL_OFFSET + T_IMPLEMENTATION::C_PIN_ANALOG_X, stAnalog1 );

					else if( GetDigitalPinConnected() )
						StartRead( SEESAW_GPIO_BASE, SEESAW_GPIO_BULK, stDigital );

					else
                        FState() = stNone;
//							StartRead( SEESAW_ADC_BASE, SEESAW_ADC_CHANNEL_OFFSET + T_IMPLEMENTATION::C_PIN_ANALOG_Y, stAnalog0 );

					break;
				}

				case stAnalog1: // Y
				{
					if( micros() - FLastTime < 500 )
						return;

//					Serial.println( "ANALOG1" );

					T_IMPLEMENTATION::JoystickOutputPins_X().SetPinValue( GetAnalog() );
					if( GetDigitalPinConnected() )
						StartRead( SEESAW_GPIO_BASE, SEESAW_GPIO_BULK, stDigital );

					else if( T_IMPLEMENTATION::JoystickOutputPins_Y().GetPinIsConnected() )
						StartRead( SEESAW_ADC_BASE, SEESAW_ADC_CHANNEL_OFFSET + T_IMPLEMENTATION::C_PIN_ANALOG_Y, stAnalog0 );

					else
                        FState() = stNone;
//							StartRead( SEESAW_ADC_BASE, SEESAW_ADC_CHANNEL_OFFSET + T_IMPLEMENTATION::C_PIN_ANALOG_X, stAnalog1 );

					break;
				}

				case stDigital:
				{
					if( micros() - FLastTime < 250 )
						return;

					T_IMPLEMENTATION::ProcessButtons( GetDigital() );

					if( T_IMPLEMENTATION::JoystickOutputPins_Y().GetPinIsConnected() )
						StartRead( SEESAW_ADC_BASE, SEESAW_ADC_CHANNEL_OFFSET + T_IMPLEMENTATION::C_PIN_ANALOG_Y, stAnalog0 );

					else if( T_IMPLEMENTATION::JoystickOutputPins_X().GetPinIsConnected() )
						StartRead( SEESAW_ADC_BASE, SEESAW_ADC_CHANNEL_OFFSET + T_IMPLEMENTATION::C_PIN_ANALOG_X, stAnalog1 );

					else
                        FState() = stNone;
//							StartRead( SEESAW_GPIO_BASE, SEESAW_GPIO_BULK, stDigital );

					break;
				}
			}

//			if( FState() )
//			StartRead()
//			while( !Serial )
//				delay( 1 );

//			Serial.println( "SystemLoopBegin" );
//			Serial.println( GetAnalog( 1 ));
//			ReadData( false );
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"
