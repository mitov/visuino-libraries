////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov_Display_SSD1306.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
	template <
		typename T_I2C, T_I2C &C_I2C,
		typename T_Address
	> class Display_odtSSD_I2C :
		public T_Address
	{
	public:
        _V_PROP_( Address )

	public:
		void SendCommandStart() // uint8_t ACommand )
		{
//			Serial.print( "C: " );
//			Serial.println( ACommand, HEX );

			C_I2C.beginTransmission( uint8_t( Address().GetValue() ));
			C_I2C.write( uint8_t( 0x00 )); // Co = 0, D/C = 0
//			C_I2C.write( ACommand );
		}

		void SendData( uint8_t AData )
		{
//			Serial.println( AData, HEX );
			C_I2C.write( AData );
		}

		void SendCommandEnd()
		{
			C_I2C.endTransmission();
		}

		void SendCommand( uint8_t ACommand )
		{
//			Serial0.print( "C: " );
//			Serial0.println( ACommand, HEX );

			C_I2C.beginTransmission( uint8_t( Address().GetValue() ));
			C_I2C.write( uint8_t( 0x00 )); // Co = 0, D/C = 0
			C_I2C.write( ACommand );
			C_I2C.endTransmission();
		}

		template <typename T> void SendBuffer( T *AInstance, uint8_t *ABuffer, uint16_t ASize )
		{
			if( AInstance->Type().GetValue() == odtSH1106 )
			{
				SendCommand(SSD1306Const::SSD1306_SETLOWCOLUMN | 0x0);  // low col = 0
				SendCommand(SSD1306Const::SSD1306_SETHIGHCOLUMN | 0x0);  // hi col = 0
				SendCommand(SSD1306Const::SSD1306_SETSTARTLINE | 0x0); // line #0

				// save I2C bitrate
			#ifdef TWBR
				uint8_t twbrbackup = TWBR;
				TWBR = 12; // upgrade to 400KHz!
			#endif

				uint8_t height=64;
				uint8_t width=132;
				height >>= 3;
				width >>= 3;
				// I2C
				uint8_t m_row = 0;
				uint8_t m_col = 2;
				int p = 0;
				for ( uint8_t i = 0; i < height; ++ i )
				{
					// send a bunch of data in one xmission
					SendCommand(0xB0 + i + m_row);//set page address
					SendCommand(m_col & 0xf);//set lower column address
					SendCommand(0x10 | (m_col >> 4));//set higher column address

					for( uint8_t j = 0; j < 8; ++ j )
					{
						C_I2C.beginTransmission( uint8_t( Address().GetValue()) );
						C_I2C.write( SSD1306Const::SSD1306_SETSTARTLINE );
						C_I2C.write( ABuffer + p, width );
						p += width;
						C_I2C.endTransmission();
					}
				}

			#ifdef TWBR
				TWBR = twbrbackup;
			#endif
			}

			else if(( AInstance->Type().GetValue() == odtSSD1306 ) && ( AInstance->GetWidthConst() == 72 ))
			{
			#ifdef TWBR
				uint8_t twbrbackup = TWBR;
				TWBR = 12; // upgrade to 400KHz!
			#endif
				const uint8_t *ABuffer1 = ABuffer;
				const uint8_t *ABuffer2 = ABuffer;

				for( uint8_t AGroup = 1; AGroup < 7; ++ AGroup )
				{
					SendCommand( SSD1306Const::SSD1306_SETSTARTLINE );

					C_I2C.beginTransmission( uint8_t( Address().GetValue()) );
					C_I2C.write( uint8_t( 0x00 )); // Co = 0, D/C = 0
					C_I2C.write( SSD1306Const::SSD1306_SETHIGHCOLUMN | ( 30 >> 4 )  );
					C_I2C.endTransmission();

					C_I2C.beginTransmission( uint8_t( Address().GetValue()) );
					C_I2C.write( uint8_t( 0x00 )); // Co = 0, D/C = 0
					C_I2C.write( uint8_t( 0x00 | ( 30 & 0b1111 ) ));
					C_I2C.endTransmission();

					C_I2C.beginTransmission( uint8_t( Address().GetValue()) );
					C_I2C.write( uint8_t( 0x00 )); // Co = 0, D/C = 0
					C_I2C.write( 0xB0 + AGroup );
					C_I2C.endTransmission();

					for( uint16_t i = 0; i < 5; ++ i )
					{
						C_I2C.beginTransmission( uint8_t( Address().GetValue() ));
						C_I2C.write( SSD1306Const::SSD1306_SETSTARTLINE );
						if( i < 4 )
						{

							if( AGroup == 6 )
								for( int k = 0 ; k < 16; ++ k )
									C_I2C.write( *ABuffer1 ++ >> 4 );

							else if( AGroup == 1 )
								for( int k = 0 ; k < 16; ++ k )
									C_I2C.write( *ABuffer2 ++ << 4 );

							else
								for( int k = 0 ; k < 16; ++ k )
									C_I2C.write( ( *ABuffer2 ++ << 4 ) | ( *ABuffer1 ++ >> 4 ) );

						}

						else
						{

							if( AGroup == 6 )
								for( int k = 0 ; k < 8; ++ k )
									C_I2C.write( *ABuffer1 ++ >> 4 );

							else if( AGroup == 1 )
								for( int k = 0 ; k < 8; ++ k )
									C_I2C.write( *ABuffer2 ++ << 4 );

							else
								for( int k = 0 ; k < 8; ++ k )
									C_I2C.write( ( *ABuffer2 ++ << 4 ) | ( *ABuffer1 ++ >> 4 ) );

						}

						C_I2C.endTransmission();
					}
				}

			#ifdef TWBR
				TWBR = twbrbackup;
			#endif
			}

			else
			{
				// save I2C bitrate
			#ifdef TWBR
				uint8_t twbrbackup = TWBR;
				TWBR = 12; // upgrade to 400KHz!
			#endif

				// I2C
	//			for( uint16_t i=0; i < ( WIDTH * HEIGHT /8); i += 16 )
				for( uint16_t i=0; i < ASize; i += 16 )
				{
					// send a bunch of data in one xmission
					C_I2C.beginTransmission( uint8_t( Address().GetValue() ));
					C_I2C.write( SSD1306Const::SSD1306_SETSTARTLINE );
					C_I2C.write( ABuffer + i, 16 );
					C_I2C.endTransmission();
				}

			#ifdef TWBR
				TWBR = twbrbackup;
			#endif
			}
		}

		inline void IntSystemInitStart()
		{
#ifdef __SAM3X8E__
			// Force 400 KHz I2C, rawr! (Uses pins 20, 21 for SDA, SCL)
			TWI1->TWI_CWGR = 0;
			TWI1->TWI_CWGR = ((VARIANT_MCK / (2 * 400000)) - 4) * 0x101;
#endif
		}

		inline void IntSystemInitEnd() {} // Placeholder
	};
//---------------------------------------------------------------------------
	template <
		typename T_I2C, T_I2C &C_I2C,
		typename T_Address
	> class Display_01space_SSD1306_72x40_I2C :
		public T_Address
	{
	public:
        _V_PROP_( Address )

	public:
		void SendCommandStart() // uint8_t ACommand )
		{
//			Serial.print( "C: " );
//			Serial.println( ACommand, HEX );

			C_I2C.beginTransmission( uint8_t( Address().GetValue() ));
			C_I2C.write( uint8_t( 0x00 )); // Co = 0, D/C = 0
//			C_I2C.write( ACommand );
		}

		void SendData( uint8_t AData )
		{
//			Serial0.println( AData, HEX );
			C_I2C.write( AData );
		}

		void SendCommandEnd()
		{
			C_I2C.endTransmission();
		}

		void SendCommand( uint8_t ACommand )
		{
//			Serial0.print( "C: " );
//			Serial0.println( ACommand, HEX );

			C_I2C.beginTransmission( uint8_t( Address().GetValue() ));
			C_I2C.write( uint8_t( 0x00 )); // Co = 0, D/C = 0
			C_I2C.write( ACommand );
			C_I2C.endTransmission();
		}

		template <typename T> void SendBuffer( T *AInstance, uint8_t *ABuffer, uint16_t ASize )
		{
		#ifdef TWBR
			uint8_t twbrbackup = TWBR;
			TWBR = 12; // upgrade to 400KHz!
		#endif
//			const uint8_t *ABuffer1 = ABuffer;
//			const uint8_t *ABuffer2 = ABuffer;

			for( uint8_t AGroup = 3; AGroup < 8; ++ AGroup )
			{
				SendCommand( SSD1306Const::SSD1306_SETSTARTLINE );

				C_I2C.beginTransmission( uint8_t( Address().GetValue()) );
				C_I2C.write( uint8_t( 0x00 )); // Co = 0, D/C = 0
				C_I2C.write( SSD1306Const::SSD1306_SETHIGHCOLUMN | ( 28 >> 4 )  );
				C_I2C.endTransmission();

				C_I2C.beginTransmission( uint8_t( Address().GetValue()) );
				C_I2C.write( uint8_t( 0x00 )); // Co = 0, D/C = 0
				C_I2C.write( uint8_t( 0x00 | ( 28 & 0b1111 ) ));
				C_I2C.endTransmission();

				C_I2C.beginTransmission( uint8_t( Address().GetValue()) );
				C_I2C.write( uint8_t( 0x00 )); // Co = 0, D/C = 0
				C_I2C.write( 0xB0 + AGroup );
				C_I2C.endTransmission();

				for( uint16_t i = 0; i < 5; ++ i )
				{
					C_I2C.beginTransmission( uint8_t( Address().GetValue() ));
					C_I2C.write( SSD1306Const::SSD1306_SETSTARTLINE );
					if( i < 4 )
					{
						C_I2C.write( ABuffer, 16 );
						ABuffer += 16;
					}

					else
					{
						C_I2C.write( ABuffer, 8 );
						ABuffer += 8;
					}

					C_I2C.endTransmission();
				}
			}

		#ifdef TWBR
			TWBR = twbrbackup;
		#endif
		}

		inline void IntSystemInitStart()
		{
#ifdef __SAM3X8E__
			// Force 400 KHz I2C, rawr! (Uses pins 20, 21 for SDA, SCL)
			TWI1->TWI_CWGR = 0;
			TWI1->TWI_CWGR = ((VARIANT_MCK / (2 * 400000)) - 4) * 0x101;
#endif
		}

		inline void IntSystemInitEnd() {} // Placeholder
	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"
