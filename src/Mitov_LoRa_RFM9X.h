////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>
#include <SPI.h>

#include "Mitov_BuildChecks_Begin.h"

//#define ___MITOV_LORA_RFM9X_DEBUG__

namespace Mitov
{
//---------------------------------------------------------------------------
namespace RH_RF9XConst
{
	// The crystal oscillator frequency of the module
	static constexpr float FXOSC = 32000000.0;

	// The Frequency Synthesizer step = FXOSC / 2^^19
	static constexpr float FSTEP = (FXOSC / 524288);

	// Register names (LoRa Mode, from table 85)
	static const uint8_t REG_00_FIFO                                = 0x00;
	static const uint8_t REG_01_OP_MODE                             = 0x01;
	static const uint8_t REG_02_RESERVED                            = 0x02;
	static const uint8_t REG_03_RESERVED                            = 0x03;
	static const uint8_t REG_04_RESERVED                            = 0x04;
	static const uint8_t REG_05_RESERVED                            = 0x05;
	static const uint8_t REG_06_FRF_MSB                             = 0x06;
	static const uint8_t REG_07_FRF_MID                             = 0x07;
	static const uint8_t REG_08_FRF_LSB                             = 0x08;
	static const uint8_t REG_09_PA_CONFIG                           = 0x09;
	static const uint8_t REG_0A_PA_RAMP                             = 0x0a;
	static const uint8_t REG_0B_OCP                                 = 0x0b;
	static const uint8_t REG_0C_LNA                                 = 0x0c;
	static const uint8_t REG_0D_FIFO_ADDR_PTR                       = 0x0d;
	static const uint8_t REG_0E_FIFO_TX_BASE_ADDR                   = 0x0e;
	static const uint8_t REG_0F_FIFO_RX_BASE_ADDR                   = 0x0f;
	static const uint8_t REG_10_FIFO_RX_CURRENT_ADDR                = 0x10;
	static const uint8_t REG_11_IRQ_FLAGS_MASK                      = 0x11;
	static const uint8_t REG_12_IRQ_FLAGS                           = 0x12;
	static const uint8_t REG_13_RX_NB_BYTES                         = 0x13;
	static const uint8_t REG_14_RX_HEADER_CNT_VALUE_MSB             = 0x14;
	static const uint8_t REG_15_RX_HEADER_CNT_VALUE_LSB             = 0x15;
	static const uint8_t REG_16_RX_PACKET_CNT_VALUE_MSB             = 0x16;
	static const uint8_t REG_17_RX_PACKET_CNT_VALUE_LSB             = 0x17;
	static const uint8_t REG_18_MODEM_STAT                          = 0x18;
	static const uint8_t REG_19_PKT_SNR_VALUE                       = 0x19;
	static const uint8_t REG_1A_PKT_RSSI_VALUE                      = 0x1a;
	static const uint8_t REG_1B_RSSI_VALUE                          = 0x1b;
	static const uint8_t REG_1C_HOP_CHANNEL                         = 0x1c;
	static const uint8_t REG_1D_MODEM_CONFIG1                       = 0x1d;
	static const uint8_t REG_1E_MODEM_CONFIG2                       = 0x1e;
	static const uint8_t REG_1F_SYMB_TIMEOUT_LSB                    = 0x1f;
	static const uint8_t REG_20_PREAMBLE_MSB                        = 0x20;
	static const uint8_t REG_21_PREAMBLE_LSB                        = 0x21;
	static const uint8_t REG_22_PAYLOAD_LENGTH                      = 0x22;
	static const uint8_t REG_23_MAX_PAYLOAD_LENGTH                  = 0x23;
	static const uint8_t REG_24_HOP_PERIOD                          = 0x24;
	static const uint8_t REG_25_FIFO_RX_BYTE_ADDR                   = 0x25;
	static const uint8_t REG_26_MODEM_CONFIG3                       = 0x26;

	static const uint8_t REG_40_DIO_MAPPING1                        = 0x40;
	static const uint8_t REG_41_DIO_MAPPING2                        = 0x41;
	static const uint8_t REG_42_VERSION                             = 0x42;


	// Register names (FSK/OOK Mode, from table 85)
	static const uint8_t REG_3E_IRQFLAGS1                           = 0x3E;

	// REG_3E_IRQFLAGS1
	static const uint8_t IRQFLAGS1_MODEREADY                         = 0x80;
	static const uint8_t IRQFLAGS1_RXREADY                           = 0x40;
	static const uint8_t IRQFLAGS1_TXREADY                           = 0x20;
	static const uint8_t IRQFLAGS1_PLLLOCK                           = 0x10;
	static const uint8_t IRQFLAGS1_RSSI                              = 0x08;
	static const uint8_t IRQFLAGS1_TIMEOUT                           = 0x04;
	static const uint8_t IRQFLAGS1_AUTOMODE                          = 0x02;
	static const uint8_t IRQFLAGS1_SYNADDRESSMATCH                   = 0x01;

	// REG_01_OP_MODE                             0x01
	static const uint8_t LONG_RANGE_MODE                       = 0x80;
	static const uint8_t ACCESS_SHARED_REG                     = 0x40;
	static const uint8_t MODE                                  = 0x07;
	static const uint8_t MODE_SLEEP                            = 0x00;
	static const uint8_t MODE_STDBY                            = 0x01;
	static const uint8_t MODE_FSTX                             = 0x02;
	static const uint8_t MODE_TX                               = 0x03;
	static const uint8_t MODE_FSRX                             = 0x04;
	static const uint8_t MODE_RXCONTINUOUS                     = 0x05;
	static const uint8_t MODE_RXSINGLE                         = 0x06;
	static const uint8_t MODE_CAD                              = 0x07;

	// REG_09_PA_CONFIG                           0x09
	static const uint8_t PA_SELECT                             = 0x80;
	static const uint8_t OUTPUT_POWER                          = 0x0f;

	// REG_0A_PA_RAMP                             0x0a
	static const uint8_t LOW_PN_TX_PLL_OFF                     = 0x10;
	static const uint8_t PA_RAMP                               = 0x0f;
	static const uint8_t PA_RAMP_3_4MS                         = 0x00;
	static const uint8_t PA_RAMP_2MS                           = 0x01;
	static const uint8_t PA_RAMP_1MS                           = 0x02;
	static const uint8_t PA_RAMP_500US                         = 0x03;
	static const uint8_t PA_RAMP_250US                         = 0x0;
	static const uint8_t PA_RAMP_125US                         = 0x05;
	static const uint8_t PA_RAMP_100US                         = 0x06;
	static const uint8_t PA_RAMP_62US                          = 0x07;
	static const uint8_t PA_RAMP_50US                          = 0x08;
	static const uint8_t PA_RAMP_40US                          = 0x09;
	static const uint8_t PA_RAMP_31US                          = 0x0a;
	static const uint8_t PA_RAMP_25US                          = 0x0b;
	static const uint8_t PA_RAMP_20US                          = 0x0c;
	static const uint8_t PA_RAMP_15US                          = 0x0d;
	static const uint8_t PA_RAMP_12US                          = 0x0e;
	static const uint8_t PA_RAMP_10US                          = 0x0f;

	// REG_0B_OCP                                 0x0b
	static const uint8_t OCP_ON                                = 0x20;
	static const uint8_t OCP_TRIM                              = 0x1f;

	// REG_0C_LNA                                 0x0c
	static const uint8_t LNA_GAIN                              = 0xe0;
	static const uint8_t LNA_BOOST                             = 0x03;
	static const uint8_t LNA_BOOST_DEFAULT                     = 0x00;
	static const uint8_t LNA_BOOST_150PC                       = 0x11;

	// REG_11_IRQ_FLAGS_MASK                      0x11
	static const uint8_t RX_TIMEOUT_MASK                       = 0x80;
	static const uint8_t RX_DONE_MASK                          = 0x40;
	static const uint8_t PAYLOAD_CRC_ERROR_MASK                = 0x20;
	static const uint8_t VALID_HEADER_MASK                     = 0x10;
	static const uint8_t TX_DONE_MASK                          = 0x08;
	static const uint8_t CAD_DONE_MASK                         = 0x04;
	static const uint8_t FHSS_CHANGE_CHANNEL_MASK              = 0x02;
	static const uint8_t CAD_DETECTED_MASK                     = 0x01;

	// REG_12_IRQ_FLAGS                           0x12
	static const uint8_t RX_TIMEOUT                            = 0x80;
	static const uint8_t RX_DONE                               = 0x40;
	static const uint8_t PAYLOAD_CRC_ERROR                     = 0x20;
	static const uint8_t VALID_HEADER                          = 0x10;
	static const uint8_t TX_DONE                               = 0x08;
	static const uint8_t CAD_DONE                              = 0x04;
	static const uint8_t FHSS_CHANGE_CHANNEL                   = 0x02;
	static const uint8_t CAD_DETECTED                          = 0x01;

	// REG_18_MODEM_STAT                          0x18
	static const uint8_t RX_CODING_RATE                        = 0xe0;
	static const uint8_t MODEM_STATUS_CLEAR                    = 0x10;
	static const uint8_t MODEM_STATUS_HEADER_INFO_VALID        = 0x08;
	static const uint8_t MODEM_STATUS_RX_ONGOING               = 0x04;
	static const uint8_t MODEM_STATUS_SIGNAL_SYNCHRONIZED      = 0x02;
	static const uint8_t MODEM_STATUS_SIGNAL_DETECTED          = 0x01;

	// REG_1C_HOP_CHANNEL                         0x1c
	static const uint8_t PLL_TIMEOUT                           = 0x80;
	static const uint8_t RX_PAYLOAD_CRC_IS_ON                  = 0x40;
	static const uint8_t FHSS_PRESENT_CHANNEL                  = 0x3f;

	// REG_1D_MODEM_CONFIG1                       0x1d
	static const uint8_t BW                                    = 0xc0;
	static const uint8_t BW_125KHZ                             = 0x00;
	static const uint8_t BW_250KHZ                             = 0x40;
	static const uint8_t BW_500KHZ                             = 0x80;
	static const uint8_t BW_RESERVED                           = 0xc0;
	static const uint8_t CODING_RATE                           = 0x38;
	static const uint8_t CODING_RATE_4_5                       = 0x00;
	static const uint8_t CODING_RATE_4_6                       = 0x08;
	static const uint8_t CODING_RATE_4_7                       = 0x10;
	static const uint8_t CODING_RATE_4_8                       = 0x18;
	static const uint8_t IMPLICIT_HEADER_MODE_ON               = 0x04;
	static const uint8_t RX_PAYLOAD_CRC_ON                     = 0x02;
	static const uint8_t LOW_DATA_RATE_OPTIMIZE                = 0x01;

	// REG_1E_MODEM_CONFIG2                       0x1e
	static const uint8_t SPREADING_FACTOR                      = 0xf0;
	static const uint8_t SPREADING_FACTOR_64CPS                = 0x60;
	static const uint8_t SPREADING_FACTOR_128CPS               = 0x70;
	static const uint8_t SPREADING_FACTOR_256CPS               = 0x80;
	static const uint8_t SPREADING_FACTOR_512CPS               = 0x90;
	static const uint8_t SPREADING_FACTOR_1024CPS              = 0xa0;
	static const uint8_t SPREADING_FACTOR_2048CPS              = 0xb0;
	static const uint8_t SPREADING_FACTOR_4096CPS              = 0xc0;
	static const uint8_t TX_CONTINUOUS_MOE                     = 0x08;
	static const uint8_t AGC_AUTO_ON                           = 0x04;
	static const uint8_t SYM_TIMEOUT_MSB                       = 0x03;
}
//---------------------------------------------------------------------------
	enum TRFM9XGainControl { gcAuto, gcG1, gcG2, gcG3, gcG4, gcG5, gcG6 };
	enum TRFM9XCodingRate { cr4_5, cr4_6, cr4_7, cr4_8 };
//---------------------------------------------------------------------------
	template <
		typename T_ResetOutputPin
	> class LoRaRFM9X_PinReset :
		public T_ResetOutputPin
	{
	public:
		_V_PIN_( ResetOutputPin )

	protected:
		void Reset()
		{
			if( T_ResetOutputPin::GetPinIsConnected() )
			{
//				Serial.println( "RESET" );
				T_ResetOutputPin::SetPinValueLow();
				delay( 100 );
				T_ResetOutputPin::SetPinValueHigh();
				delay( 100 );
			}
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_0_RESET,
		typename T_SPI, T_SPI &C_SPI,
		typename T_2_INTERRUPT,
		typename T_ChipSelectOutputPin,
		typename T_CodingRate,
		typename T_FMode,
		typename T_FModeChange,
		typename T_FReady,
		typename T_Frequency,
		typename T_Gain,
		typename T_LoRaTMMode,
		typename T_LowNoiseAmplifierHighFrequencyBoost,
		typename T_MaxSendQueue,
		typename T_MobileNode,
		typename T_OutputPin,
		typename T_Power,
		typename T_PreambleLength,
		typename T_SPISpeed,
		typename T_SignalBandwidth,
		typename T_SignalStrengthOutputPin,
		typename T_SpreadingFactorOrder,
		typename T_SymbolTimeout
	> class MitovLoRaRFM9X :
        public T_0_RESET,
		public T_2_INTERRUPT,
		public T_ChipSelectOutputPin,
		public T_CodingRate,
		public T_FMode,
		public T_FModeChange,
		public T_FReady,
		public T_Frequency,
		public T_Gain,
		public T_LoRaTMMode,
		public T_LowNoiseAmplifierHighFrequencyBoost,
		public T_MaxSendQueue,
		public T_MobileNode,
		public T_OutputPin,
		public T_Power,
		public T_PreambleLength,
		public T_SPISpeed,
		public T_SignalBandwidth,
		public T_SignalStrengthOutputPin,
		public T_SpreadingFactorOrder,
		public T_SymbolTimeout
	{
	public:
		_V_PIN_( OutputPin )
		_V_PIN_( SignalStrengthOutputPin )
		_V_PIN_( ChipSelectOutputPin )

	protected:
		/// \brief Defines different operating modes for the transport hardware
		///
		/// These are the different values that can be adopted by the _mode variable and
		/// returned by the mode() member function,
		enum RHMode
		{
			RHModeInitialising,			///< Transport is initialising. Initial default value until init() is called..
			RHModeIdle,					///< Transport is idle.
			RHModeFrequencySynthesizer, ///< Frequency Synthesizer.
			RHModeTx,					///< Transport is in the process of transmitting a message.
			RHModeRx					///< Transport is in the process of receiving a message.
		};

	public:
		_V_PROP_( PreambleLength )
		_V_PROP_( Power )
		_V_PROP_( Frequency )
		_V_PROP_( SignalBandwidth )
		_V_PROP_( MaxSendQueue )

	public:
		_V_PROP_( SymbolTimeout )
		_V_PROP_( LoRaTMMode )
		_V_PROP_( MobileNode )
		_V_PROP_( LowNoiseAmplifierHighFrequencyBoost )
		_V_PROP_( Gain )
		_V_PROP_( CodingRate )
		_V_PROP_( SPISpeed )
		_V_PROP_( SpreadingFactorOrder )

	protected:
		_V_PROP_( FReady )
		_V_PROP_( FModeChange )
		_V_PROP_( FMode )

		/// The value of the last received RSSI value, in some transport specific units
		volatile int8_t     FLastRssi = 0;

		Mitov::SimpleList<uint8_t *>	FSendQueue;

	public:
		void Print( Mitov::String AValue )
		{
			AValue += "\r\n";
			send( (uint8_t *)AValue.c_str(), AValue.length() );
		}

		void Print( float AValue )
		{
			char AText[ __VISUINO_FLOAT_TO_STR_LEN__ ];
			dtostrf( AValue,  1, 2, AText );
			Print( Mitov::String( AText ));
		}

		void Print( int32_t AValue )
		{
			char AText[ 16 ];
			ltoa( AValue, AText, 10 );
			Print( Mitov::String( AText ));
		}

		void Print( uint32_t AValue )
		{
			char AText[ 16 ];
			ltoa( AValue, AText, 10 );
			Print( Mitov::String( AText ));
		}

		inline void PrintChar( char AValue )
		{
			send( (uint8_t*)&AValue, 1 );
		}

		inline void PrintChar( uint8_t AValue )
		{
			send( &AValue, 1 );
		}

		inline void Write( uint8_t *AData, uint32_t ASize )
		{
			send( AData, ASize );
		}

	public:
		void send( const uint8_t* data, uint8_t len )
		{
			if( ! FReady() )
				return;

#ifdef ___MITOV_LORA_RFM9X_DEBUG__
			Serial.println( "TRY SEND" );
			Serial.println( len );
#endif
//				Serial.println( FTestCount );
//				Serial.println( int( FMode ));
//			if (len > RH_RF69_MAX_MESSAGE_LEN)
//				return;

//			waitPacketSent(); // Make sure we dont interrupt an outgoing message // _mode == RHModeTx

			if( FMode() == RHModeTx )
			{
				while( FSendQueue.size() > MaxSendQueue() )
					;

				uint8_t *ABuffer = new uint8_t[ len + 1 ];
				ABuffer[ 0 ] = len;
				memcpy( ABuffer + 1, data, len );
				FSendQueue.push_back( ABuffer );
				return;
			}

			setModeIdle(); // Prevent RX while filling the fifo
#ifdef ___MITOV_LORA_RFM9X_DEBUG__
			Serial.println( "SEND" );
#endif

			// Position at the beginning of the FIFO
			spiWrite(RH_RF9XConst::REG_0D_FIFO_ADDR_PTR, 0);

			TransferPacket( data, len );

			spiWrite( RH_RF9XConst::REG_22_PAYLOAD_LENGTH, len );

			setModeTx(); // Start the transmitter
		}

	public:
		void __ICACHE_RAM_ATTR__ InterruptHandler( bool )
		{
			T_2_INTERRUPT::SetValue( true );
		}

	protected:
		void IntSystemInit()
		{

//			ChipSelectOutputPin.SendValue( true );

			T_ChipSelectOutputPin::SetPinValueHigh();
			Reset();
//			Serial.println( "START1" );

//			spiRead( RH_RF9XConst::REG_42_VERSION );
//			delay( 10000 );

			// Set sleep mode, so we can also set LORA mode:
			spiWrite(RH_RF9XConst::REG_01_OP_MODE, RH_RF9XConst::MODE_SLEEP | RH_RF9XConst::LONG_RANGE_MODE);
			delay(10); // Wait for sleep mode to take over from say, CAD

			// Check we are in sleep mode, with LORA set
			if (spiRead(RH_RF9XConst::REG_01_OP_MODE) != (RH_RF9XConst::MODE_SLEEP | RH_RF9XConst::LONG_RANGE_MODE))
			{
		//	Serial.println(spiRead(RH_RF9XConst::REG_01_OP_MODE), HEX);
				return; // No device present?
			}

			FReady() = true;

//			Serial.println( spiRead( RH_RF9XConst::REG_1F_SYMB_TIMEOUT_LSB ), HEX );

//			Serial.println( "START2" );
			// Set up FIFO
			// We configure so that we can use the entire 256 byte FIFO for either receive
			// or transmit, but not both at the same time
			spiWrite(RH_RF9XConst::REG_0E_FIFO_TX_BASE_ADDR, 0);
			spiWrite(RH_RF9XConst::REG_0F_FIFO_RX_BASE_ADDR, 0);

			// Packet format is preamble + explicit-header + payload + crc
			// Explicit Header Mode
			// payload is TO + FROM + ID + FLAGS + message data
			// RX mode is implmented with RXCONTINUOUS
			// max message data length is 255 - 4 = 251 octets

			setModeIdle();

/*
			// Set up default configuration
			// No Sync Words in LORA mode.
			setModemConfig(Bw125Cr45Sf128); // Radio default
		//    setModemConfig(Bw125Cr48Sf4096); // slow and reliable?
			setPreambleLength(8); // Default is 8
			// An innocuous ISM frequency, same as RF22's
			setFrequency(434.0);
			// Lowish power
			setTxPower(13);
		//    setTxPower(20);
*/
			UpdateModemRegisters();
			UpdatePreambleLength();
			UpdateFrequency();
			UpdateTransmitPower();
		}

	public:
		inline void SystemLoopBegin()
		{
			if( ! FReady() )
				return;

			if( FModeChange() )
			{
//				Serial.println( "FModeChange" );
				if( ! LoRaTMMode() )
					if( !(spiRead(RH_RF9XConst::REG_3E_IRQFLAGS1) & RH_RF9XConst::IRQFLAGS1_MODEREADY) )
						return;

				FModeChange() = false;
			}

			uint8_t ALength = 0;
			uint8_t *APacket = nullptr;

			if( T_2_INTERRUPT::GetValue() )
			{
				T_2_INTERRUPT::SetValue( false );
			// Read the interrupt register
				uint8_t irq_flags = spiRead(RH_RF9XConst::REG_12_IRQ_FLAGS);

				if ( FMode() == RHModeRx && irq_flags & (RH_RF9XConst::RX_TIMEOUT | RH_RF9XConst::PAYLOAD_CRC_ERROR))
				{
					spiWrite(RH_RF9XConst::REG_12_IRQ_FLAGS, 0xff); // Clear all IRQ flags
					; //_rxBad++;
				}

				else if ( FMode() == RHModeRx && irq_flags & RH_RF9XConst::RX_DONE)
				{
					spiWrite(RH_RF9XConst::REG_12_IRQ_FLAGS, 0xff); // Clear all IRQ flags
					// Have received a packet
					ALength = spiRead(RH_RF9XConst::REG_13_RX_NB_BYTES);

					// Reset the fifo read ptr to the beginning of the packet
	//				spiWrite(RH_RF9XConst::REG_0D_FIFO_ADDR_PTR, spiRead(RH_RF9XConst::REG_10_FIFO_RX_CURRENT_ADDR));
	//				spiBurstRead(RH_RF9XConst::REG_00_FIFO, _buf, len);
	//				_bufLen = len;
	//				spiWrite(RH_RF9XConst::REG_12_IRQ_FLAGS, 0xff); // Clear all IRQ flags

					// Remember the RSSI of this packet
					// this is according to the doc, but is it really correct?
					// weakest receiveable signals are reported RSSI at about -66
					FLastRssi = spiRead(RH_RF9XConst::REG_1A_PKT_RSSI_VALUE) - 137;

		//			Serial.print( "LN: " ); Serial.println( ALength );
					// Reset the fifo read ptr to the beginning of the packet
					spiWrite(RH_RF9XConst::REG_0D_FIFO_ADDR_PTR, spiRead(RH_RF9XConst::REG_10_FIFO_RX_CURRENT_ADDR));

					C_SPI.beginTransaction( SPISpeed().GetValue(), MSBFIRST, SPI_MODE0 );
					T_ChipSelectOutputPin::SetPinValueLow();

					C_SPI.transfer(RH_RF9XConst::REG_00_FIFO); // Send the start address with the write mask off

					APacket = new uint8_t[ ALength ];

					for ( int i = 0; i < ALength; ++i )
						APacket[ i ] = C_SPI.transfer( 0 );

					T_ChipSelectOutputPin::SetPinValueHigh();
					C_SPI.endTransaction();

					// We have received a message.
	//				validateRxBuf();
	//				if (_rxBufValid)
						setModeIdle(); // Got one
				}

				else if ( FMode() == RHModeTx && irq_flags & RH_RF9XConst::TX_DONE)
				{
					spiWrite(RH_RF9XConst::REG_12_IRQ_FLAGS, 0xff); // Clear all IRQ flags
	//				_txGood++;
					setModeIdle();
				}
			}

			if( APacket )
			{
//				uint8_t ASentTo = APacket[ 0 ];
#ifdef ___MITOV_LORA_RFM9X_DEBUG__
				Serial.println( "RECEIVED" );
				Serial.println( ALength );
#endif
				T_OutputPin::SetPinValue( Mitov::TDataBlock( ALength, APacket ));
				delete [] APacket;
			}

			if( FMode() != RHModeTx )
			{
				if( FSendQueue.size() )
				{
					uint8_t *ABuffer = FSendQueue[ 0 ];
					FSendQueue.pop_front();
					send( ABuffer + 1, ABuffer[ 0 ] );
					delete [] ABuffer;
				}
			}

			if( FMode() == RHModeIdle )
				if( T_OutputPin::GetPinIsConnected() )
					setModeRx();

			T_SignalStrengthOutputPin::SetPinValue( FLastRssi );
		}

	protected:
		/// If current mode is Rx or Tx changes it to Idle. If the transmitter or receiver is running,
		/// disables them.
		void setModeIdle()
		{
			if( FMode() != RHModeIdle )
			{
				setOpMode( RH_RF9XConst::MODE_STDBY );
				FMode() = RHModeIdle;
			}
		}

		void setModeRx()
		{
			if( FMode() != RHModeRx )
			{
				spiWrite(RH_RF9XConst::REG_01_OP_MODE, RH_RF9XConst::MODE_RXCONTINUOUS);
				spiWrite(RH_RF9XConst::REG_40_DIO_MAPPING1, 0x00); // Interrupt on RxDone
				FMode() = RHModeRx;
			}
		}

		void setModeTx()
		{
			if( FMode() != RHModeTx )
			{
				spiWrite(RH_RF9XConst::REG_01_OP_MODE, RH_RF9XConst::MODE_TX);
				spiWrite(RH_RF9XConst::REG_40_DIO_MAPPING1, 0x40); // Interrupt on TxDone
				FMode() = RHModeTx;
			}
		}

		void setOpMode(uint8_t mode)
		{
			uint8_t opmode = spiRead(RH_RF9XConst::REG_01_OP_MODE);
			opmode &= ~RH_RF9XConst::MODE;
			opmode |= (mode & RH_RF9XConst::MODE);
			spiWrite(RH_RF9XConst::REG_01_OP_MODE, opmode);

			FModeChange() = true;

			// Wait for mode to change.
//			while (!(spiRead(RH_RF69_REG_27_IRQFLAGS1) & RH_RF69_IRQFLAGS1_MODEREADY))
//				;
		}

	public:
		void UpdatePreambleLength()
		{
//			spiWrite(RH_RF69_REG_2C_PREAMBLEMSB, PreambleLength >> 8);
//			spiWrite(RH_RF69_REG_2D_PREAMBLELSB, PreambleLength & 0xff);

			spiWrite(RH_RF9XConst::REG_20_PREAMBLE_MSB, PreambleLength() >> 8);
		    spiWrite(RH_RF9XConst::REG_21_PREAMBLE_LSB, PreambleLength() & 0xff);
		}

		void UpdateFrequency()
		{
			// Frf = FRF / FSTEP
/*
			uint32_t frf = (Frequency * 1000000.0) / RH_RF69_FSTEP;
//			Serial.println( frf );
			spiWrite(RH_RF69_REG_07_FRFMSB, (frf >> 16) & 0xff);
			spiWrite(RH_RF69_REG_08_FRFMID, (frf >> 8) & 0xff);
			spiWrite(RH_RF69_REG_09_FRFLSB, frf & 0xff);
*/
			uint32_t frf = (Frequency() * 1000000.0) / RH_RF9XConst::FSTEP;
			spiWrite(RH_RF9XConst::REG_06_FRF_MSB, (frf >> 16) & 0xff);
			spiWrite(RH_RF9XConst::REG_07_FRF_MID, (frf >> 8) & 0xff);
			spiWrite(RH_RF9XConst::REG_08_FRF_LSB, frf & 0xff);
		}

		void UpdateTransmitPower()
		{
			int8_t aPower = Power();
			if (aPower > 20)
				aPower = 20;

			if (aPower < 5)
				aPower = 5;

			// RFM95/96/97/98 does not have RFO pins connected to anything. ONly PA_BOOST
			// pin is connected, so must use PA_BOOST
			// Pout = 2 + OutputPower.
			// The documentation is pretty confusing on this topic: PaSelect says the max poer is 20dBm,
			// but OutputPower claims it would be 17dBm.
			// My measurements show 20dBm is correct
			spiWrite(RH_RF9XConst::REG_09_PA_CONFIG, RH_RF9XConst::PA_SELECT | (aPower-5));
		//    spiWrite(RH_RF9XConst::REG_09_PA_CONFIG, 0); // no power
		}

		// Sets registers from a canned modem configuration structure
		void UpdateModemRegisters()
		{
//    { 0x72,   0x74,    0x00}, // Bw125Cr45Sf128 (the chip default)

//			spiWrite(RH_RF9XConst::REG_1D_MODEM_CONFIG1,       config->reg_1d);
//			spiWrite(RH_RF9XConst::REG_1E_MODEM_CONFIG2,       config->reg_1e);

			uint8_t	AValue;

			if( Gain() == gcAuto )
				AValue = 0b11000000;

			else
				AValue = Gain() << 5;

			AValue |= (( LowNoiseAmplifierHighFrequencyBoost() ) ? 0b11 : 0 );

			spiWrite( RH_RF9XConst::REG_0C_LNA, AValue );

			static const float CSignalBandwidths[] =
			{
				7.8,
				10.4,
				15.6,
				20.8,
				31.25,
				41.7,
				62.5,
				125.0,
				250.0,
				500.0
			};

			AValue = 9;
			for( int i = 0; i < sizeof( CSignalBandwidths ) / sizeof( CSignalBandwidths[ 0 ] ); ++i )
				if( SignalBandwidth() <= CSignalBandwidths[ i ] )
				{
					AValue = i;
					break;
				}

//			Serial.println( AValue );

			AValue <<= 4;
			AValue |= ( CodingRate() + 1 ) << 1;

//			Serial.println( AValue, HEX );

//			spiWrite(RH_RF9XConst::REG_1D_MODEM_CONFIG1,       0x72 );
			spiWrite(RH_RF9XConst::REG_1D_MODEM_CONFIG1,       AValue );

			AValue = ( SpreadingFactorOrder() << 4 ) |
				     0b100 | // Header indicates CRC on
					 ( SymbolTimeout() >> 8 );

//			Serial.println( AValue, HEX );

//			spiWrite(RH_RF9XConst::REG_1E_MODEM_CONFIG2,       0x74 );
			spiWrite(RH_RF9XConst::REG_1E_MODEM_CONFIG2,       AValue );

			AValue =	(( Gain() == gcAuto ) ? 0 : 0b100 ) |
								(( MobileNode() ) ? 0b1000 : 0 );
			spiWrite(RH_RF9XConst::REG_26_MODEM_CONFIG3,       AValue );

			spiWrite( RH_RF9XConst::REG_1F_SYMB_TIMEOUT_LSB, uint8_t( SymbolTimeout() ));

		}

	protected:
		void Reset()
		{
			T_ChipSelectOutputPin::SetPinValueHigh();
            T_0_RESET::Reset();
		}

		uint8_t spiRead(uint8_t reg)
		{
			uint8_t val;
			T_ChipSelectOutputPin::SetPinValueLow();
			C_SPI.beginTransaction( SPISpeed().GetValue(), MSBFIRST, SPI_MODE0 );
			C_SPI.transfer( reg & 0x7F ); // Send the address with the write mask off
			val = C_SPI.transfer( 0 ); // The written value is ignored, reg value is read
			C_SPI.endTransaction();
			T_ChipSelectOutputPin::SetPinValueHigh();
//Serial.print( "spiRead: " ); Serial.print( reg, HEX ); Serial.print( " = " ); Serial.println( val, HEX );
			return val;
		}

		uint8_t spiWrite(uint8_t reg, uint8_t val)
		{
//Serial.print( "spiWrite: " ); Serial.print( reg, HEX ); Serial.print( " = " ); Serial.println( val, HEX );
			T_ChipSelectOutputPin::SetPinValueLow();
			C_SPI.beginTransaction( SPISpeed().GetValue(), MSBFIRST, SPI_MODE0 );
			uint8_t status = C_SPI.transfer(reg | 0x80); // Send the address with the write mask on
			C_SPI.transfer(val); // New value follows
			C_SPI.endTransaction();
			T_ChipSelectOutputPin::SetPinValueHigh();
			return status;
		}

	protected:
		void TransferPacket( const uint8_t* data, uint8_t len )
		{
//			Serial.println( "TransferPacket" );
//			Serial.println( len );
			C_SPI.beginTransaction( SPISpeed().GetValue(), MSBFIRST, SPI_MODE0 );
			T_ChipSelectOutputPin::SetPinValueLow();
			C_SPI.transfer( RH_RF9XConst::REG_00_FIFO | 0x80 ); // Send the start address with the write mask on
//			C_SPI.transfer( len + 1 ); // Include length of headers
			// First the 4 headers
//			C_SPI.transfer( ATxHeaderTo );
//			C_SPI.transfer( Address );
//			C_SPI.transfer(_txHeaderId);
//			C_SPI.transfer(_txHeaderFlags);
			// Now the payload
			while( len-- )
				C_SPI.transfer( *data++ );

			T_ChipSelectOutputPin::SetPinValueHigh();
			C_SPI.endTransaction();
		}

	public:
		inline void SystemInit()
		{
			IntSystemInit();
		}

	public:
		inline MitovLoRaRFM9X()
		{
			T_2_INTERRUPT::SetValue( false );
			FReady() = false;
			FModeChange() = false;
			FMode() = RHModeInitialising;
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"
