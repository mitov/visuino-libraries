////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
	enum TArduinoAD7606Oversampling { aosNone, aos2, aos4, aos8, aos16, aos32, aos64 };
//---------------------------------------------------------------------------
	template <
		typename T_BUSY_PIN, T_BUSY_PIN &C_BUSY_PIN,
		typename T_ChipSelectOutputPin,
		uint16_t C_DataInputPins0,
		uint16_t C_DataInputPins1,
		uint16_t C_DataInputPins10,
		uint16_t C_DataInputPins11,
		uint16_t C_DataInputPins12,
		uint16_t C_DataInputPins13,
		uint16_t C_DataInputPins14,
		uint16_t C_DataInputPins15,
		uint16_t C_DataInputPins2,
		uint16_t C_DataInputPins3,
		uint16_t C_DataInputPins4,
		uint16_t C_DataInputPins5,
		uint16_t C_DataInputPins6,
		uint16_t C_DataInputPins7,
		uint16_t C_DataInputPins8,
		uint16_t C_DataInputPins9,
		typename T_Enabled,
		typename T_MostSignificantFirst,
		typename T_OutputPins_0,
		typename T_OutputPins_1,
		typename T_OutputPins_2,
		typename T_OutputPins_3,
		typename T_OutputPins_4,
		typename T_OutputPins_5,
		typename T_OutputPins_6,
		typename T_OutputPins_7,
		typename T_Oversampling,
		typename T_OversamplingOutputPins_0,
		typename T_OversamplingOutputPins_1,
		typename T_OversamplingOutputPins_2,
		typename T_ReadOutputPin,
		typename T_ResetOutputPin,
		typename T_StartOutputPin
	> class AD7606Parallel16bit :
		public T_ChipSelectOutputPin,
		public T_Enabled,
		public T_MostSignificantFirst,
		public T_OutputPins_0,
		public T_OutputPins_1,
		public T_OutputPins_2,
		public T_OutputPins_3,
		public T_OutputPins_4,
		public T_OutputPins_5,
		public T_OutputPins_6,
		public T_OutputPins_7,
		public T_Oversampling,
		public T_OversamplingOutputPins_0,
		public T_OversamplingOutputPins_1,
		public T_OversamplingOutputPins_2,
		public T_ReadOutputPin,
		public T_ResetOutputPin,
		public T_StartOutputPin
	{
	public:
		_V_PIN_( OutputPins_0 )
		_V_PIN_( OutputPins_1 )
		_V_PIN_( OutputPins_2 )
		_V_PIN_( OutputPins_3 )
		_V_PIN_( OutputPins_4 )
		_V_PIN_( OutputPins_5 )
		_V_PIN_( OutputPins_6 )
		_V_PIN_( OutputPins_7 )

		_V_PIN_( ChipSelectOutputPin )
		_V_PIN_( OversamplingOutputPins_0 )
		_V_PIN_( OversamplingOutputPins_1 )
		_V_PIN_( OversamplingOutputPins_2 )
		_V_PIN_( ReadOutputPin )
		_V_PIN_( ResetOutputPin )
		_V_PIN_( StartOutputPin )

	public:
		_V_PROP_( Enabled )

		_V_PROP_( Oversampling )

	protected:
//		uint16_t FPins[ 16 + 5 ];

	public:
		void ClockInputPin_o_Receive( void *_Data )
		{
			if( ! Enabled() )
				return;

//			Serial.println( "READ" );
//			T_ChipSelectOutputPin::SetPinValueLow();
			T_StartOutputPin::SetPinValueLow();

			unsigned long ACurrentTime = micros();

			T_StartOutputPin::SetPinValueHigh();

//			Serial.println( "WAIT" );
			while( micros() - ACurrentTime < 4 ); // Wait 4 uS
//			Serial.println( "WAIT1" );
			while( C_BUSY_PIN.DigitalRead()); // Wait to finish reading
//			while( Digital.Read( C_BusyInputPin ) ); // Wait to finish reading
//			Serial.println( "WAIT2" );

			T_ChipSelectOutputPin::SetPinValueLow();

			for( int AChannel = 0; AChannel < 8; ++AChannel )
			{
				T_ReadOutputPin::SetPinValueLow();

				uint16_t AResult = 0;
//				for( int i = 0; i < 16; ++i )
//					if( Digital.Read( FPins[ i ] ))
//						AResult |= ( 1 << i );

				if( Digital.Read( C_DataInputPins0 ))
					AResult |= ( 1 << 0 );
					
				if( Digital.Read( C_DataInputPins1 ))
					AResult |= ( 1 << 1 );
					
				if( Digital.Read( C_DataInputPins2 ))
					AResult |= ( 1 << 2 );
					
				if( Digital.Read( C_DataInputPins3 ))
					AResult |= ( 1 << 3 );
					
				if( Digital.Read( C_DataInputPins4 ))
					AResult |= ( 1 << 4 );
					
				if( Digital.Read( C_DataInputPins5 ))
					AResult |= ( 1 << 5 );
					
				if( Digital.Read( C_DataInputPins6 ))
					AResult |= ( 1 << 6 );
					
				if( Digital.Read( C_DataInputPins7 ))
					AResult |= ( 1 << 7 );

				if( Digital.Read( C_DataInputPins8 ))
					AResult |= ( 1 << 8 );

				if( Digital.Read( C_DataInputPins9 ))
					AResult |= ( 1 << 9 );

				if( Digital.Read( C_DataInputPins10 ))
					AResult |= ( 1 << 10 );

				if( Digital.Read( C_DataInputPins11 ))
					AResult |= ( 1 << 11 );

				if( Digital.Read( C_DataInputPins12 ))
					AResult |= ( 1 << 12 );

				if( Digital.Read( C_DataInputPins13 ))
					AResult |= ( 1 << 13 );

				if( Digital.Read( C_DataInputPins14 ))
					AResult |= ( 1 << 14 );

				if( Digital.Read( C_DataInputPins15 ))
					AResult |= ( 1 << 15 );

				T_ReadOutputPin::SetPinValueHigh();

//				if( ! AChannel )
//					Serial.println( AResult, HEX );

				float AValue = float( int16_t( AResult )) / 0x7FFF;
//				OutputPins[ AChannel ].Notify( &AValue );
				switch( AChannel )
				{
					case 0: T_OutputPins_0::SetPinValue( AValue ); break;
					case 1: T_OutputPins_1::SetPinValue( AValue ); break;
					case 2: T_OutputPins_2::SetPinValue( AValue ); break;
					case 3: T_OutputPins_3::SetPinValue( AValue ); break;
					case 4: T_OutputPins_4::SetPinValue( AValue ); break;
					case 5: T_OutputPins_5::SetPinValue( AValue ); break;
					case 6: T_OutputPins_6::SetPinValue( AValue ); break;
					case 7: T_OutputPins_7::SetPinValue( AValue ); break;
				}
			}

			T_ChipSelectOutputPin::SetPinValueHigh();
		}

		void ResetInputPin_o_Receive( void *_Data )
		{
//			Serial.println( "RESET" );
			T_ResetOutputPin::SetPinValueHigh();
			delay( 1 );
			T_ResetOutputPin::SetPinValueLow();
		}

	public:
		inline void SystemStart()
		{
//			for( int i = 0; i < 16; ++ i )
//				pinMode( FPins[ i ], INPUT );

			pinMode( C_DataInputPins0,  INPUT );
			pinMode( C_DataInputPins1,  INPUT );
			pinMode( C_DataInputPins2,  INPUT );
			pinMode( C_DataInputPins3,  INPUT );
			pinMode( C_DataInputPins4,  INPUT );
			pinMode( C_DataInputPins5,  INPUT );
			pinMode( C_DataInputPins6,  INPUT );
			pinMode( C_DataInputPins7,  INPUT );
			pinMode( C_DataInputPins8,  INPUT );
			pinMode( C_DataInputPins9,  INPUT );
			pinMode( C_DataInputPins10, INPUT );
			pinMode( C_DataInputPins11, INPUT );
			pinMode( C_DataInputPins12, INPUT );
			pinMode( C_DataInputPins13, INPUT );
			pinMode( C_DataInputPins14, INPUT );
			pinMode( C_DataInputPins15, INPUT );

//			for( int i = 16; i < 16 + 4; ++ i )
//				pinMode( FPins[ i ], OUTPUT );

//			pinMode( C_CHIP_SELECT_PIN, OUTPUT );
//			pinMode( C_READ_PIN, OUTPUT );
//			pinMode( C_RESET_PIN, OUTPUT );
//			pinMode( C_START_PIN, OUTPUT );

			T_ChipSelectOutputPin::SetPinValueHigh();
  			T_ReadOutputPin::SetPinValueHigh();
			T_StartOutputPin::SetPinValueHigh();
			T_ResetOutputPin::SetPinValueLow();

            C_BUSY_PIN.SetMode( INPUT );
//			pinMode( C_BusyInputPin, INPUT );
			
//			for( int i = 0; i < 3; ++i )
//				OversamplingOutputPins[ i ].SendValue<bool>( Oversampling & ( 1 << i ) != 0 );
			
			T_OversamplingOutputPins_0::SetPinValue( ( Oversampling().GetValue() & ( 1 )) != 0 );
			T_OversamplingOutputPins_1::SetPinValue( ( Oversampling().GetValue() & ( 1 << 1 )) != 0 );
			T_OversamplingOutputPins_2::SetPinValue( ( Oversampling().GetValue() & ( 1 << 2 )) != 0 );

			ResetInputPin_o_Receive( nullptr );
		}

		inline void SystemLoopBegin()
		{
			ClockInputPin_o_Receive( nullptr );
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_BUSY_PIN, T_BUSY_PIN &C_BUSY_PIN,
		typename T_ChipSelectOutputPin,
		uint16_t C_DataInputPins0,
		uint16_t C_DataInputPins1,
		uint16_t C_DataInputPins10,
		uint16_t C_DataInputPins11,
		uint16_t C_DataInputPins12,
		uint16_t C_DataInputPins13,
		uint16_t C_DataInputPins14,
		uint16_t C_DataInputPins15,
		uint16_t C_DataInputPins2,
		uint16_t C_DataInputPins3,
		uint16_t C_DataInputPins4,
		uint16_t C_DataInputPins5,
		uint16_t C_DataInputPins6,
		uint16_t C_DataInputPins7,
		uint16_t C_DataInputPins8,
		uint16_t C_DataInputPins9,
		typename T_Enabled,
		typename T_MostSignificantFirst,
		typename T_OutputPins_0,
		typename T_OutputPins_1,
		typename T_OutputPins_2,
		typename T_OutputPins_3,
		typename T_OutputPins_4,
		typename T_OutputPins_5,
		typename T_OutputPins_6,
		typename T_OutputPins_7,
		typename T_Oversampling,
		typename T_OversamplingOutputPins_0,
		typename T_OversamplingOutputPins_1,
		typename T_OversamplingOutputPins_2,
		typename T_ReadOutputPin,
		typename T_ResetOutputPin,
		typename T_StartOutputPin
	> class AD7606Parallel8bit :
		public T_ChipSelectOutputPin,
		public T_Enabled,
		public T_MostSignificantFirst,
		public T_OutputPins_0,
		public T_OutputPins_1,
		public T_OutputPins_2,
		public T_OutputPins_3,
		public T_OutputPins_4,
		public T_OutputPins_5,
		public T_OutputPins_6,
		public T_OutputPins_7,
		public T_Oversampling,
		public T_OversamplingOutputPins_0,
		public T_OversamplingOutputPins_1,
		public T_OversamplingOutputPins_2,
		public T_ReadOutputPin,
		public T_ResetOutputPin,
		public T_StartOutputPin
	{
//	protected:
//		static const uint8_t RESET_PIN = 8;
//		static const uint8_t CHIP_SELECT_PIN = 9;
//		static const uint8_t READ_PIN = 10;
//		static const uint8_t START_PIN = 11;
//		static const uint8_t BUSY_PIN = 12;

	public:
		_V_PIN_( OutputPins_0 )
		_V_PIN_( OutputPins_1 )
		_V_PIN_( OutputPins_2 )
		_V_PIN_( OutputPins_3 )
		_V_PIN_( OutputPins_4 )
		_V_PIN_( OutputPins_5 )
		_V_PIN_( OutputPins_6 )
		_V_PIN_( OutputPins_7 )

		_V_PIN_( ChipSelectOutputPin )
		_V_PIN_( OversamplingOutputPins_0 )
		_V_PIN_( OversamplingOutputPins_1 )
		_V_PIN_( OversamplingOutputPins_2 )
		_V_PIN_( ReadOutputPin )
		_V_PIN_( ResetOutputPin )
		_V_PIN_( StartOutputPin )

	public:
		_V_PROP_( Enabled )
		_V_PROP_( MostSignificantFirst )

		_V_PROP_( Oversampling )

//	protected:
//		uint16_t FPins[ 8 + 5 ];

	protected:
		uint16_t WriteMostSignificant()
		{
			uint16_t AResult = 0;

			if( Digital.Read( C_DataInputPins0 ))
				AResult |= ( 1 << ( 8 + 0 ));
					
			if( Digital.Read( C_DataInputPins1 ))
				AResult |= ( 1 << ( 8 + 1 ));
					
			if( Digital.Read( C_DataInputPins2 ))
				AResult |= ( 1 << ( 8 + 2 ));
					
			if( Digital.Read( C_DataInputPins3 ))
				AResult |= ( 1 << ( 8 + 3 ));
					
			if( Digital.Read( C_DataInputPins4 ))
				AResult |= ( 1 << ( 8 + 4 ));
					
			if( Digital.Read( C_DataInputPins5 ))
				AResult |= ( 1 << ( 8 + 5 ));
					
			if( Digital.Read( C_DataInputPins6 ))
				AResult |= ( 1 << ( 8 + 6 ));
					
			if( Digital.Read( C_DataInputPins7 ))
				AResult |= ( 1 << ( 8 + 7 ));
					
			return AResult;
		}

		uint16_t WriteLeastSignificant()
		{
			uint16_t AResult = 0;

			if( Digital.Read( C_DataInputPins0 ))
				AResult |= ( 1 << 0 );
					
			if( Digital.Read( C_DataInputPins1 ))
				AResult |= ( 1 << 1 );
					
			if( Digital.Read( C_DataInputPins2 ))
				AResult |= ( 1 << 2 );
					
			if( Digital.Read( C_DataInputPins3 ))
				AResult |= ( 1 << 3 );
					
			if( Digital.Read( C_DataInputPins4 ))
				AResult |= ( 1 << 4 );
					
			if( Digital.Read( C_DataInputPins5 ))
				AResult |= ( 1 << 5 );
					
			if( Digital.Read( C_DataInputPins6 ))
				AResult |= ( 1 << 6 );
					
			if( Digital.Read( C_DataInputPins7 ))
				AResult |= ( 1 << 7 );

			return AResult;
		}

	public:
		void ClockInputPin_o_Receive( void *_Data )
		{
			if( ! Enabled() )
				return;

//			Serial.println( "READ" );
			T_StartOutputPin::SetPinValueLow();

			unsigned long ACurrentTime = micros();

			T_StartOutputPin::SetPinValueHigh();

//			Serial.println( "WAIT" );
			while( micros() - ACurrentTime < 4 ); // Wait 4 uS
//			Serial.println( "WAIT1" );
			while( C_BUSY_PIN.DigitalRead()); // Wait to finish reading
//			Serial.println( "WAIT2" );

			T_ChipSelectOutputPin::SetPinValueLow();

			for( int AChannel = 0; AChannel < 8; ++AChannel )
			{
				uint16_t AResult = 0;

				T_ReadOutputPin::SetPinValueLow();

				if( MostSignificantFirst() )
				{
					AResult |= WriteMostSignificant();
//					for( int i = 0; i < 8; ++i )
//						if( Digital.Read( FPins[ i ] ))
//							AResult |= ( 1 << ( 8 + i ));
				}

				else
				{
					AResult |= WriteLeastSignificant();
//					for( int i = 0; i < 8; ++i )
//						if( Digital.Read( FPins[ i ] ))
//							AResult |= ( 1 << i );
				}

				T_ReadOutputPin::SetPinValueHigh();

				delay( 1 );

				T_ReadOutputPin::SetPinValueLow();

				if( MostSignificantFirst() )
				{
					AResult |= WriteLeastSignificant();
//					for( int i = 0; i < 8; ++i )
//						if( Digital.Read( FPins[ i ] ))
//							AResult |= ( 1 << i );
				}

				else
				{
					AResult |= WriteMostSignificant();
//					for( int i = 0; i < 8; ++i )
//						if( Digital.Read( FPins[ i ] ))
//							AResult |= ( 1 << ( 8 + i ));
				}

				T_ReadOutputPin::SetPinValueHigh();

//				if( ! AChannel )
//					Serial.println( AResult, HEX );

				float AValue = float( int16_t( AResult )) / 0x7FFF;
//				OutputPins[ AChannel ].Notify( &AValue );
				switch( AChannel )
				{
					case 0: T_OutputPins_0::SetPinValue( AValue ); break;
					case 1: T_OutputPins_1::SetPinValue( AValue ); break;
					case 2: T_OutputPins_2::SetPinValue( AValue ); break;
					case 3: T_OutputPins_3::SetPinValue( AValue ); break;
					case 4: T_OutputPins_4::SetPinValue( AValue ); break;
					case 5: T_OutputPins_5::SetPinValue( AValue ); break;
					case 6: T_OutputPins_6::SetPinValue( AValue ); break;
					case 7: T_OutputPins_7::SetPinValue( AValue ); break;
				}
			}

            T_ChipSelectOutputPin::SetPinValueHigh();
		}

		void ResetInputPin_o_Receive( void *_Data )
		{
//			Serial.println( "RESET" );
            T_ResetOutputPin::SetPinValueHigh();
			delay( 1 );
            T_ResetOutputPin::SetPinValueLow();
		}

	public:
		inline void SystemStart()
		{
//			for( int i = 0; i < 8; ++ i )
//				pinMode( FPins[ i ], INPUT );

			pinMode( C_DataInputPins0, INPUT );
			pinMode( C_DataInputPins1, INPUT );
			pinMode( C_DataInputPins2, INPUT );
			pinMode( C_DataInputPins3, INPUT );
			pinMode( C_DataInputPins4, INPUT );
			pinMode( C_DataInputPins5, INPUT );
			pinMode( C_DataInputPins6, INPUT );
			pinMode( C_DataInputPins7, INPUT );

//			for( int i = 8; i < 8 + 4; ++ i )
//				pinMode( FPins[ i ], OUTPUT );

//			pinMode( C_CHIP_SELECT_PIN, OUTPUT );
//			pinMode( C_READ_PIN, OUTPUT );
//			pinMode( C_RESET_PIN, OUTPUT );
//			pinMode( C_START_PIN, OUTPUT );

            T_ChipSelectOutputPin::SetPinValueHigh();
            T_ReadOutputPin::SetPinValueHigh();
			T_StartOutputPin::SetPinValueHigh();
			T_ResetOutputPin::SetPinValueLow();

            C_BUSY_PIN.SetMode( INPUT );
//			pinMode( C_BusyInputPin, INPUT );
			
//			for( int i = 0; i < 3; ++i )
//				OversamplingOutputPins[ i ].SendValue<bool>( Oversampling & ( 1 << i ) != 0 );
			
			T_OversamplingOutputPins_0::SetPinValue( ( Oversampling().GetValue() & ( 1 )) != 0 );
			T_OversamplingOutputPins_1::SetPinValue( ( Oversampling().GetValue() & ( 1 << 1 )) != 0 );
			T_OversamplingOutputPins_2::SetPinValue( ( Oversampling().GetValue() & ( 1 << 2 )) != 0 );

			ResetInputPin_o_Receive( nullptr );
		}

		inline void SystemLoopBegin()
		{
			ClockInputPin_o_Receive( nullptr );
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"
