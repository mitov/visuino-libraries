////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>

#include "Mitov_BuildChecks_Begin.h"

#if defined( VISUINO_ESP32 )
SPIClass SPI2( VSPI );

#ifndef __BORLANDC__
//SPIClass SPI2_Instance( HSPI ); // Workaround ESP32 bug!
//#define SPI1 SPI2_Instance
#endif // __BORLANDC__

#endif

#include "Mitov_BuildChecks_End.h"
