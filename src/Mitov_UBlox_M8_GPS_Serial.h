////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>
#include <Mitov_GPS.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
	template <
		typename T_ResetOutputPin
	> class UBlox_M8_GPS_Serial_Impl :
		T_ResetOutputPin
	{
	public:
		_V_PIN_( ResetOutputPin )

	protected:
		void Reset()
		{
			if( ! T_ResetOutputPin::GetPinIsConnected() )
				return;

			T_ResetOutputPin::SetPinValueLow();
			delayMicroseconds( 2 );
			T_ResetOutputPin::SetPinValueHigh();
		}

	public:
		inline void ResetInputPin_o_Receive( void *_Data )
		{	
			Reset();
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"
