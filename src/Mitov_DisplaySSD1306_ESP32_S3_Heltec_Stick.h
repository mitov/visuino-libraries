////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>
#include <Mitov_Display_SSD1306.h>

#include "Mitov_BuildChecks_Begin.h"


namespace Mitov
{
//---------------------------------------------------------------------------
	class DisplaySSD1306Buffered_ESP32_S3_Heltec_Stick :
		public DisplaySSD1306Buffered<32, 64>
	{
	protected:
		inline void SetPower( bool AValue ) 
		{
			pinMode( 36, OUTPUT );
			Digital.Write( 36, ! AValue );
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"
