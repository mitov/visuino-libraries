////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>
#include <SPI.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
	template <
		typename T_SPI, T_SPI &C_SPI,
		typename T_ChipSelectOutputPin,
		typename T_SPISpeed
	> class InterfaceSPISpeed :
		public T_ChipSelectOutputPin,
		public T_SPISpeed
	{
	public:
		_V_PIN_( ChipSelectOutputPin )

	public:
		_V_PROP_( SPISpeed )

	public:
		inline void Init()
		{
			T_ChipSelectOutputPin::SetPinValueHigh();
		}

		inline void BeginTransaction() 
		{ 
			C_SPI.beginTransaction( SPISpeed().GetValue(), MSBFIRST, SPI_MODE0 ); 
			T_ChipSelectOutputPin::SetPinValueLow();
		}

		inline void EndTransaction() 
		{ 
			T_ChipSelectOutputPin::SetPinValueHigh();
			C_SPI.endTransaction(); 
		}

		inline uint8_t Transfer( uint8_t AValue ) { return C_SPI.transfer( AValue ); }

		void WriteRegisters_Progmem( uint8_t ARegister, const uint8_t *AValues, uint32_t ASize )
		{
			C_SPI.beginTransaction( SPISpeed().GetValue(), MSBFIRST, SPI_MODE0 );
			T_ChipSelectOutputPin::SetPinValueLow();
			C_SPI.transfer( ARegister );
			for( int i = 0; i < ASize; ++ i )
				C_SPI.transfer( pgm_read_byte( AValues ++ ) );

			T_ChipSelectOutputPin::SetPinValueHigh();
			C_SPI.endTransaction();
		}

		void WriteRegister8( uint8_t ARegister, uint8_t AValue )
		{
			C_SPI.beginTransaction( SPISpeed().GetValue(), MSBFIRST, SPI_MODE0 );
			T_ChipSelectOutputPin::SetPinValueLow();
//			C_SPI.transfer( reg & ( ~0x80 ) ); // write, bit 7 low
			C_SPI.transfer( ARegister ); // write, bit 7 low
			C_SPI.transfer( AValue );
//			C_SPI.transfer( reg | 0x80 | 0x40 );
			T_ChipSelectOutputPin::SetPinValueHigh();
			C_SPI.endTransaction();
		}

		void WriteRegister16( uint8_t ARegister, uint16_t AValue )
		{
			C_SPI.beginTransaction( SPISpeed().GetValue(), MSBFIRST, SPI_MODE0 );
			T_ChipSelectOutputPin::SetPinValueLow();
//			C_SPI.transfer( reg & ( ~0x80 ) ); // write, bit 7 low
			C_SPI.transfer( ARegister ); // write, bit 7 low
			C_SPI.transfer( uint8_t( AValue ));
			C_SPI.transfer( uint8_t( AValue >> 8 ));
//			C_SPI.transfer( reg | 0x80 | 0x40 );
			T_ChipSelectOutputPin::SetPinValueHigh();
			C_SPI.endTransaction();
		}

		void WriteRegister16_HighFirst( uint8_t ARegister, uint16_t AValue )
		{
			C_SPI.beginTransaction( SPISpeed().GetValue(), MSBFIRST, SPI_MODE0 );
			T_ChipSelectOutputPin::SetPinValueLow();

			C_SPI.transfer( ARegister ); // write, bit 7 low

			C_SPI.transfer( uint8_t( AValue >> 8 ));
			C_SPI.transfer( uint8_t( AValue ));
			T_ChipSelectOutputPin::SetPinValueHigh();

			C_SPI.endTransaction();

		}

		uint8_t ReadRegister8( uint8_t ARegister )
		{
			C_SPI.beginTransaction( SPISpeed().GetValue(), MSBFIRST, SPI_MODE0 );
			T_ChipSelectOutputPin::SetPinValueLow();
			C_SPI.transfer( ARegister | 0x80 ); // write, bit 7 low
			uint8_t AValue = C_SPI.transfer( 0 );
			T_ChipSelectOutputPin::SetPinValueHigh();
			C_SPI.endTransaction();

			return AValue;
		}

		uint16_t ReadRegister16( uint8_t ARegister )
		{
			C_SPI.beginTransaction( SPISpeed().GetValue(), MSBFIRST, SPI_MODE0 );
			T_ChipSelectOutputPin::SetPinValueLow();

			C_SPI.transfer( ARegister | 0x80 ); // write, bit 7 low

			uint16_t AResult = C_SPI.transfer( 0 );
			AResult |= uint16_t( C_SPI.transfer( 0 ) ) << 8;

			T_ChipSelectOutputPin::SetPinValueHigh();
			C_SPI.endTransaction();

			return AResult;
		}

		void StartReadRegisters( uint8_t ARegister, uint8_t ACount )
		{
			C_SPI.beginTransaction( SPISpeed().GetValue(), MSBFIRST, SPI_MODE0 );
			T_ChipSelectOutputPin::SetPinValueLow();
			C_SPI.transfer( ARegister | 0x80 /*| 0x40 */ ); // read, bit 7 high
		}

		uint8_t RequestRead8()
		{
			C_SPI.beginTransaction( SPISpeed().GetValue(), MSBFIRST, SPI_MODE0 );
			T_ChipSelectOutputPin::SetPinValueLow();
			uint8_t AValue = C_SPI.transfer( 0 );
			T_ChipSelectOutputPin::SetPinValueHigh();
			C_SPI.endTransaction();

			return AValue;
		}

		inline void Write8( uint8_t AValue )
		{
			C_SPI.transfer( AValue );
		}

		inline uint8_t ReadUInt8()
		{
			return C_SPI.transfer( 0xFF );
		}

		inline uint16_t ReadUInt16()
		{
			uint16_t AResult = C_SPI.transfer( 0xFF );

			return ( AResult | uint16_t( C_SPI.transfer( 0xFF ) ) << 8 );
		}

		inline uint16_t ReadUInt32()
		{
			uint32_t AResult = C_SPI.transfer( 0xFF );
			AResult |= uint32_t( C_SPI.transfer( 0xFF ) << 8 );
			AResult |= uint32_t( C_SPI.transfer( 0xFF ) << 16 );
			return ( AResult | uint32_t( C_SPI.transfer( 0xFF ) ) << 24 );
		}

		inline uint32_t ReadUInt32_HighFirst()
		{
			uint32_t AResult = C_SPI.transfer( 0xFF ) << 24;
			AResult |= uint32_t( C_SPI.transfer( 0xFF ) ) << 16;
			AResult |= uint32_t( C_SPI.transfer( 0xFF ) ) << 8;
			return ( AResult | C_SPI.transfer( 0xFF ) );
		}

		inline uint32_t RequestRead32()
		{
			return ReadUInt32();
		}

		inline uint32_t RequestRead32_HighFirst()
		{
			return ReadUInt32_HighFirst();
		}

		void EndReadRegisters()
		{
			T_ChipSelectOutputPin::SetPinValueHigh();
			C_SPI.endTransaction();
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"
