////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>
#include <Mitov_Stepper.h>
#include <Mitov_PWM_PCA9685.h>
#include <Mitov_Adafruit_MotorDriverShield_V1.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
	class TArduinoAdafruitMotorDriverShieldV2MotorConstants_Driver1_Impl
	{
	public:
		static const uint8_t C_PIN_MOTOR0_FORWARD = 9; // IN2
		static const uint8_t C_PIN_MOTOR0_BACK = 10; // IN1
		static const uint8_t C_PIN_MOTOR0_SPEED = 8;

		static const uint8_t C_PIN_MOTOR1_FORWARD = 12; // IN2
		static const uint8_t C_PIN_MOTOR1_BACK = 11; // IN1
		static const uint8_t C_PIN_MOTOR1_SPEED = 13;

	public:
		inline uint8_t GetStepperPins( uint8_t AIndex )
		{
			switch( AIndex )
			{
				case 0 : return 9;
				case 1 : return 10; 
				case 2 : return 12;
				default : return 11;
			}
		}

	};
//---------------------------------------------------------------------------
	class TArduinoAdafruitMotorDriverShieldV2MotorConstants_Driver2_Impl
	{
	public:
		static const uint8_t C_PIN_MOTOR0_FORWARD = 3; // IN2
		static const uint8_t C_PIN_MOTOR0_BACK = 4; // IN1
		static const uint8_t C_PIN_MOTOR0_SPEED = 2;

		static const uint8_t C_PIN_MOTOR1_FORWARD = 6; // IN2
		static const uint8_t C_PIN_MOTOR1_BACK = 5; // IN1
		static const uint8_t C_PIN_MOTOR1_SPEED = 7;

	public:
		inline uint8_t GetStepperPins( uint8_t AIndex )
		{
			switch( AIndex )
			{
				case 0 : return 3;
				case 1 : return 4; 
				case 2 : return 6;
				default : return 5;
			}
		}

	};
//---------------------------------------------------------------------------
	class AdafruitMotorDriverControl_V2_NoPWMChannels
	{
	};
//---------------------------------------------------------------------------
	template <
		typename T_I2C, T_I2C &C_I2C,
		typename T_Address,
		typename T_PWMChannels_UpdateValues,
		typename T_PWMFrequency,
		typename T_Sleep
	> class AdafruitMotorDriverControl_V2 :
		public PCA9685PWM_Basic<T_I2C, C_I2C, T_Address>,
		public T_PWMFrequency,
		public T_Sleep
	{
		typedef PCA9685PWM_Basic<T_I2C, C_I2C, T_Address> inherited;

	public:
		_V_PROP_( PWMFrequency )
		_V_PROP_( Sleep )
        
	public:
		void UpdateFrequency()
		{
			float Afreq = PWMFrequency() * 0.9;  // Correct for overshoot in the frequency setting (see issue #11).
			float prescaleval = 25000000; //OscilatorFrequency();
			prescaleval /= 4096;
			prescaleval /= Afreq;
			prescaleval -= 1;
			uint8_t prescale = floor(prescaleval + 0.5);

			uint8_t oldmode = inherited::Read8(PCA9685Const::PCA9685_MODE1);
			uint8_t newmode = ( oldmode & 0x7F ) | 0x10; // sleep
			inherited::Write8(PCA9685Const::PCA9685_MODE1, newmode); // go to sleep
			inherited::Write8(PCA9685Const::PCA9685_PRESCALE, prescale); // set the prescaler
//			Serial.println( prescale );
			inherited::Write8(PCA9685Const::PCA9685_MODE1, oldmode);
			delay(1);
			inherited::Write8(PCA9685Const::PCA9685_MODE1, oldmode | 0xA0);  //  This sets the MODE1 register to turn on auto increment.
													// This is why the beginTransmission below was not working.
		}

	protected:
		void UpdateConfigOnly()
		{
			uint8_t	AValue = 0b100000;

//			if( ExternalClock() )
//				AValue |= 0b1000000;

			if( Sleep() )
				AValue |= 0b10000;

//			Serial.print( "PCA9685_MODE1: " );
//			Serial.println( AValue, BIN );

			inherited::Write8( PCA9685Const::PCA9685_MODE1, AValue );
/* Do not update PCA9685_MODE2
			AValue = 0;

			if( OutputLogic().Inverted() )
				AValue |= 0b10000;

			if( UpdateOnAck() )
				AValue |= 0b1000;

			if( ! OpenDrain() )
				AValue |= 0b100;

			AValue |= OutputLogic().Mode();

//			Serial.print( "PCA9685_MODE2: " );
//			Serial.println( AValue, BIN );

			inherited::Write8( PCA9685Const::PCA9685_MODE2, AValue );
*/
		}

	public:
		inline void WriteChannelData( uint8_t AIndex, uint16_t AOn, uint16_t AOff )
		{
			if( AIndex > 1 )
				AIndex += 12;

			inherited::WriteChannelDataShared( AIndex, AOn, AOff );
		}

	public:
		inline bool Enabled() { return true; }
		inline bool ClockInputPin_o_IsConnected() { return false; }

		inline void SetModified() {}

		void SetChannelValue( uint8_t AIndex, bool AValue )
		{
			inherited::WriteChannelDataShared( AIndex, ( AValue ) ? 4096 : 0, 0 );
		}

		inline void AnalogWriteRaw( uint8_t AIndex, uint8_t AValue )
		{
			inherited::WriteChannelDataShared( AIndex, 0, 0 ); // We only use it to turn off!
		}

		inline void AnalogWrite( uint8_t AIndex, float AValue )
		{
			inherited::WriteChannelDataShared( AIndex, 0, AValue * 4095 + 0.5 );
		}

	public:
		inline void SystemInit()
		{
			inherited::Write8( PCA9685Const::PCA9685_MODE1, 0x0);
			UpdateConfigOnly();
			UpdateFrequency();
			for( int i = 0; i < 14; ++ i )
				AnalogWriteRaw( i, 0 );

		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"
