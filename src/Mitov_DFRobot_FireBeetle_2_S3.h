////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>
#include <Wire.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
	class TArduinoESP32_DFRobot_FireBeetle_2_S3_PowerModuleCommon
	{
	protected:
		static constexpr uint8_t C_ADDR = 0x36;

	protected:
		void writeReg( uint8_t reg, uint8_t AValue )
		{
			Wire.beginTransmission( C_ADDR );
			Wire.write(reg);
			Wire.write( AValue );
			Wire.endTransmission();
		}

	public:
		void SetCameraPower(float DVDD, float AVDDorDOVDD)
		{
			uint8_t ALDOData;
			uint8_t DLDOData;
			if(DVDD < 0.5 || AVDDorDOVDD < 0.5)
			{
				ALDOData = 0;
				DLDOData = 0;
			}
			
			else if(DVDD > 3.5 || AVDDorDOVDD > 3.5)
			{
				ALDOData = 31;
				DLDOData = 31;
			}
			
			else
			{
				ALDOData = (DVDD-0.5) * 10;
				DLDOData = (AVDDorDOVDD-0.5) *10;
			}

			writeReg( 0x10, 0x19 );
			delay(10);
			writeReg(0x16, ALDOData );//ALDO
			delay(10);
			writeReg(0x17, DLDOData );//DLDO
			delay(10);		
		}

		void SetShutdownKeyLevelTime( bool ALongOffTime )
		{
			writeReg( 0x1E, ( ALongOffTime ) ? 0b10 : 0 );
		}

	public:
		void SystemInit()
		{
			Wire.beginTransmission( C_ADDR );
			Wire.write(0x00);
			Wire.endTransmission();

			writeReg( 0x00, 0x04 );
		}

	};
//---------------------------------------------------------------------------
	namespace Instance
	{
		TArduinoESP32_DFRobot_FireBeetle_2_S3_PowerModuleCommon DFRobot_FireBeetle_2_S3_PowerModule;
	}
//---------------------------------------------------------------------------
	template <
		typename T_LongOffButtonDelay
	> class TArduinoESP32_DFRobot_FireBeetle_2_S3_PowerModule :
		public T_LongOffButtonDelay
	{
	public:
		_V_PROP_( LongOffButtonDelay )

	public:
		void UpdateLongOffButtonDelay()
		{
			Instance::DFRobot_FireBeetle_2_S3_PowerModule.SetShutdownKeyLevelTime( LongOffButtonDelay() );
		}

	public:
		inline void SystemInit()
		{
			Instance::DFRobot_FireBeetle_2_S3_PowerModule.SystemInit();
			Instance::DFRobot_FireBeetle_2_S3_PowerModule.SetShutdownKeyLevelTime( LongOffButtonDelay() );
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"
