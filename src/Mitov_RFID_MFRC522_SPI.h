﻿////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>
#include <Mitov_Arduino_Basic_SPI.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
	template <
		typename T_SPI, T_SPI &C_SPI,
		typename T_ChipSelectOutputPin,
		typename T_SPISpeed
	> class RFID_MFRC522_InterfaceSPI :
		public InterfaceSPISpeed <
				T_SPI, C_SPI,
				T_ChipSelectOutputPin,
				T_SPISpeed
			>
	{
		typedef InterfaceSPISpeed <
				T_SPI, C_SPI,
				T_ChipSelectOutputPin,
				T_SPISpeed
			> inherited;

	public:
		void PCD_ReadRepeatRegister(	uint8_t reg,	///< The register to read from. One of the PCD_Register enums.
								uint8_t count,			///< The number of bytes to read
								uint8_t *values,		///< Byte array to store the values in.
								uint8_t rxAlign		///< Only bit positions rxAlign..7 in values[0] are updated.
								)
		{
			//Serial.print(F("Reading ")); 	Serial.print(count); Serial.println(F(" bytes from register."));
			uint8_t address = 0x80 | reg;				// MSB == 1 is for reading. LSB is not used in address. Datasheet section 8.1.2.3.
			uint8_t index = 0;							// Index in values array.

			inherited::BeginTransaction();
//			SPI.beginTransaction(SPISettings(MFRC522_SPICLOCK, MSBFIRST, SPI_MODE0));	// Set the settings to work with SPI bus
//			digitalWrite(_chipSelectPin, LOW);		// Select slave
			-- count;								// One read is performed outside of the loop

//			SPI.transfer(address);					// Tell MFRC522 which address we want to read
			inherited::Write8( address );					// Tell MFRC522 which address we want to read
			if( rxAlign )
			{		// Only update bit positions rxAlign..7 in values[0]
				// Create bit mask for bit positions rxAlign..7
				uint8_t mask = (0xFF << rxAlign) & 0xFF;
				// Read value and tell that we want to read the same address again.
				uint8_t value = C_SPI.transfer( address );
//				uint8_t value = T_0_IMPLEMENTATION::ReadUInt8();
				// Apply mask to both current value of values[0] and the new data in value.
				values[0] = (values[0] & ~mask) | (value & mask);
				++ index;
			}

			while( index < count )
				values[ index ++ ] = C_SPI.transfer( address );	// Read value and tell that we want to read the same address again.
//				values[index ++] = T_0_IMPLEMENTATION::ReadUInt8();	// Read value and tell that we want to read the same address again.

	        values[ index ] = C_SPI.transfer( 0 );			// Read the final byte. Send 0 to stop reading.
//			values[index] = T_0_IMPLEMENTATION::ReadUInt8();			// Read the final uint8_t. Send 0 to stop reading.
//			digitalWrite(_chipSelectPin, HIGH);			// Release slave again
//			SPI.endTransaction(); // Stop using the SPI bus
			inherited::EndTransaction();
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"