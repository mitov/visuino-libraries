#ifdef __TEST_FOR_DEBUG_PRINTS__

#ifdef Serial
  #define __V__Serial__V__
  #pragma push_macro("Serial")
#endif

#define Serial UNGUARDED DEBUG PRINT!!!

#ifdef Serial0
  #define __V__Serial0__V__
  #pragma push_macro("Serial0")
#endif

#define Serial0 UNGUARDED DEBUG PRINT!!!

#ifdef Console
  #define __V__Console__V__
  #pragma push_macro("Console")
#endif

#define Console UNGUARDED DEBUG PRINT!!!

#ifdef USBSerial
  #define __V__USBSerial__V__
  #pragma push_macro("USBSerial")
#endif

#define USBSerial UNGUARDED DEBUG PRINT!!!
#endif