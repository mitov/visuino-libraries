////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>
#include <Mitov_BasicEthernet.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
	template <
		typename T_Sockets_StopSocket
	> class RaspberryPiNetworkModule : 
		public T_Sockets_StopSocket
	{
	public:
		inline bool Enabled() { return true; }

	public:
		inline bool GetIsStarted() { return true; }

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"
