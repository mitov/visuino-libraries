////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov_Arduino_SPI.h>

//---------------------------------------------------------------------------
namespace Mitov
{
//---------------------------------------------------------------------------
	template <
		typename T_SPI,	T_SPI &C_SPI,
		int8_t C_AUTO_MISO,
		int8_t C_AUTO_MOSI,
		int8_t C_AUTO_SCK,
		typename T_AutoConfig,
		typename T_MISO,
		typename T_MOSI,
		typename T_SCK
	> class Arduino_RP2040_SPI :
		public ArduinoBasicSPI<T_SPI, C_SPI>,
		public T_AutoConfig,
		public T_MISO,
		public T_MOSI,
		public T_SCK
	{
	public:
		_V_PROP_( AutoConfig )
		_V_PROP_( MISO )
		_V_PROP_( MOSI )
		_V_PROP_( SCK )

	public:
		inline void SystemInit()
		{
			if( AutoConfig().GetValue() && ( C_AUTO_MISO >= 0 ) )
				C_SPI.setRX( C_AUTO_MISO );

			else
				C_SPI.setRX( MISO() );

			if( AutoConfig().GetValue() && ( C_AUTO_MOSI >= 0 ) )
				C_SPI.setTX( C_AUTO_MOSI );

			else
				C_SPI.setTX( MOSI() );

			if( AutoConfig().GetValue() && ( C_AUTO_SCK >= 0 ) )
				C_SPI.setSCK( C_AUTO_SCK );

			else
				C_SPI.setSCK( SCK() );

			C_SPI.begin();
		}
	};
//---------------------------------------------------------------------------
	template <
		typename T_SPI,	T_SPI &C_SPI,
		typename T_MISO,
		typename T_MOSI,
		typename T_SCK
	> class Arduino_RP2040_Simple_SPI :
		public ArduinoBasicSPI<T_SPI, C_SPI>,
		public T_MISO,
		public T_MOSI,
		public T_SCK
	{
	public:
		_V_PROP_( MISO )
		_V_PROP_( MOSI )
		_V_PROP_( SCK )

	public:
		inline void SystemInit()
		{
			C_SPI.setRX( MISO() );
			C_SPI.setTX( MOSI() );
			C_SPI.setSCK( SCK() );

			C_SPI.begin();
		}
	};
//---------------------------------------------------------------------------
} // Mitov
