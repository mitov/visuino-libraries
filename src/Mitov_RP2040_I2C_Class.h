////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>
#include <Mitov_Basic_I2C.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
	template <
		TwoWire &C_I2C,
		int8_t C_AUTO_SCL,
		int8_t C_AUTO_SDA,
		typename T_Address,
		typename T_AutoConfig,
		typename T_INSTANCE_RequestData,
        typename T_INSTANCE_SendByte,
		typename T_IsSlave,
		typename T_OutputPin,
		typename T_RequestOutputPin,
		typename T_SCL,
		typename T_SDA
	> class Arduino_RP2040_I2C :
		public ArduinoPinMapI2C <
				C_I2C,
				T_Address,
				T_INSTANCE_RequestData,
                T_INSTANCE_SendByte,
				T_IsSlave,
				T_OutputPin,
				T_RequestOutputPin,
				T_SCL,
				T_SDA
			>,
		public T_AutoConfig
	{
		typedef ArduinoPinMapI2C <
				C_I2C,
				T_Address,
				T_INSTANCE_RequestData,
                T_INSTANCE_SendByte,
				T_IsSlave,
				T_OutputPin,
				T_RequestOutputPin,
				T_SCL,
				T_SDA
			> inherited;

	public:
		_V_PROP_( AutoConfig )

	public:
		inline void SystemInit()
		{
			if( AutoConfig().GetValue() && ( C_AUTO_SDA >= 0 ) )
				C_I2C.setSDA( C_AUTO_SDA );

			else
				C_I2C.setSDA( inherited::SDA().GetValue() );

			if( AutoConfig().GetValue() && ( C_AUTO_SCL >= 0 ) )
				C_I2C.setSCL( C_AUTO_SCL );

			else
				C_I2C.setSCL( inherited::SCL().GetValue() );

			if( inherited::IsSlave() )
				C_I2C.begin( uint8_t( inherited::Address().GetValue() ) );

			else
				C_I2C.begin();

			inherited::SystemInitInternal();
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"


