////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
	template <
		typename T_I2C, T_I2C &C_I2C,
		typename T_Address,
		typename T_Enabled,
		typename T_OutputPin
	> class TArduinoMicrochip_ADC_MCP3021 :
		public T_Address,
		public T_Enabled,
		public T_OutputPin
	{
	public:
		_V_PIN_( OutputPin )

	public:
		_V_PROP_( Enabled )
		_V_PROP_( Address )

	protected:
		void ReadSensor()
		{
			C_I2C.requestFrom( Address(), _VISUINO_I2C_SIZE_( 2 ));
			uint16_t AValue = uint16_t( C_I2C.read() ) << 6;
			AValue |= ( C_I2C.read() >> 2 );

			T_OutputPin::SetPinValue( float( AValue ) / 1024 );
		}

	public:
		inline void ClockInputPin_o_Receive( void *_Data )
		{
			ReadSensor();
		}

	public:
		inline void SystemStart()
		{
			ReadSensor();
		}

		inline void SystemLoopBegin()
		{
			ReadSensor();
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_I2C, T_I2C &C_I2C,
		typename T_Address,
		typename T_Enabled,
		typename T_OutputPin
	> class TArduinoMicrochip_ADC_MCP3221 :
		public T_Address,
		public T_Enabled,
		public T_OutputPin
	{
	public:
		_V_PIN_( OutputPin )

	public:
		_V_PROP_( Enabled )
		_V_PROP_( Address )

	protected:
		void ReadSensor()
		{
			C_I2C.requestFrom( Address(), _VISUINO_I2C_SIZE_( 2 ));
			uint16_t AValue = uint16_t( C_I2C.read() ) << 8;
			AValue |= C_I2C.read();

			T_OutputPin::SetPinValue( float( AValue ) / 4096 );
		}

	public:
		inline void ClockInputPin_o_Receive( void *_Data )
		{
			ReadSensor();
		}

	public:
		inline void SystemStart()
		{
			ReadSensor();
		}

		inline void SystemLoopBegin()
		{
			ReadSensor();
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"