////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov_FocalTech_FT6336U.h>

#include "Mitov_BuildChecks_Begin.h"

//---------------------------------------------------------------------------
namespace Mitov
{
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
        uint16_t C_INT_PIN,
		typename T_Enabled,
		typename T_FLastCount,
		typename T_Mirror,
		typename T_MonitorMode,
		typename T_Orientation,
		typename T_OutputPin,
		typename T_ReportPeriod
	> class TArduino_M5Stack_Stamp_Dial_Shield_Touch :
		public T_Enabled,
		public T_FLastCount,
		public T_Mirror,
		public T_MonitorMode,
		public T_Orientation,
		public T_OutputPin,
		public T_ReportPeriod
	{
	public:
		_V_PIN_( OutputPin )

	public:
		_V_PROP_( Enabled )
		_V_PROP_( Mirror )
		_V_PROP_( MonitorMode )
		_V_PROP_( Orientation )
		_V_PROP_( ReportPeriod )

	public:
		_V_PROP_( FLastCount )

	public:
        void Update_ReportPeriod_Active()
        {
            Write( FocalTech_FT6336U::REG_PERIODACTIVE, ReportPeriod().GetValue() );
        }

        void Update_ReportPeriod_Monitor()
        {
            Write( FocalTech_FT6336U::REG_PERIODMONITOR, MonitorMode().ReportPeriod().GetValue() );
        }

        void Update_EnableMonitor()
        {
            Write( FocalTech_FT6336U::REG_CTRL, ( MonitorMode().Enabled().GetValue() ) ? 1 : 0 );
        }

        void Update_InactivityPeriod()
        {
            Write( FocalTech_FT6336U::REG_TIMEENTERMONITOR, MonitorMode().InactivityPeriod() );
        }

    protected:
        static const uint8_t TOUCH_ADDR = 0x38;

    protected:
        TArduino2DTouchPoint FLastPoints[ 2 ];

    protected:
        uint8_t Read( uint8_t reg )
        {
            Wire1.beginTransmission( TOUCH_ADDR );
            Wire1.write(reg);
            Wire1.endTransmission();
            Wire1.requestFrom( TOUCH_ADDR, _VISUINO_I2C_SIZE_( 1 ) );
            return Wire1.read();
        }

        void Write( uint8_t reg, uint8_t value )
        {
            Wire1.beginTransmission( TOUCH_ADDR );
            Wire1.write(reg);
            Wire1.write( value );
            Wire1.endTransmission();
        }

        // Reading size bytes into data
        void BeginRead(uint8_t reg, uint8_t size )
        {
            Wire1.beginTransmission( TOUCH_ADDR );
            Wire1.write(reg);
            Wire1.endTransmission();
            Wire1.requestFrom(TOUCH_ADDR, _VISUINO_I2C_SIZE_( size ));
        }

        inline uint16_t Read16()
        {
            uint16_t AResult = uint16_t( Wire1.read() ) << 8;
            AResult |= Wire1.read();

            return AResult;
        }

        inline void ReportResult( TArduino2DTouchPoint *APoints, uint8_t ACount )
        {
            TArduinoTouchInfo ATouchData( ACount, APoints );
            T_OutputPin::SetPinValue( ATouchData );
        }

	public:
		inline void SystemInit()
		{
            pinMode( C_INT_PIN, INPUT_PULLUP );

            // By default, the FT6336 will pulse the INT line for every touch
            // event. But because it shares the Wire1 TwoWire/I2C with other
            // devices, we cannot easily create an interrupt service routine to
            // handle these events. So instead, we set the INT wire to polled mode,
            // so it simply goes low as long as there is at least one valid touch.
            Write( FocalTech_FT6336U::REG_G_MODE, 0x00 );

			Update_EnableMonitor();
            Update_InactivityPeriod();
            Update_ReportPeriod_Active();
            Update_ReportPeriod_Monitor();
		}

		inline void SystemLoopBegin()
		{
//            Serial.println( C_INT_PIN );
//            delay( 100 );
		    if( Digital.Read( C_INT_PIN ))
		    {
                Write( FocalTech_FT6336U::REG_G_MODE, 0x00 );

//                Serial.println( "TEST" );
		        bool ASend = false;
		        if( FLastCount() )
		        {
		            if( FLastPoints[ 0 ].Status != TTouchStatus::Up )
		            {
		                FLastPoints[ 0 ].Status = TTouchStatus::Up;
		                ASend = true;
		            }

                    if( FLastCount() > 1 )
		                if( FLastPoints[ 1 ].Status != TTouchStatus::Up )
		                {
		                    FLastPoints[ 1 ].Status = TTouchStatus::Up;
		                    ASend = true;
		                }

		        }

		        if( ASend )
		            ReportResult( FLastPoints, FLastCount() );

		        return;
		    }

            uint8_t ACount = Read( FocalTech_FT6336U::REG_TD_STATUS );

//            Serial.println( ACount );

            if( ! ACount )
		        return;

            if( ACount > 2 )
		        return;
    
            if( ACount == 2 )
                BeginRead( FocalTech_FT6336U::REG_P1_XH, 10 );

            else
                BeginRead( FocalTech_FT6336U::REG_P1_XH, 4 );
/*
            if( ACount == 2 )
                BeginRead( FocalTech_FT6336U::REG_P1_XH, 10 );

            else
                BeginRead( FocalTech_FT6336U::REG_P1_XH, 4 );
*/
            uint16_t AValue = Read16();
//            uint16_t AValue1 = AValue;

            TArduino2DTouchPoint APoints[ 2 ] = { 0 };
            APoints[ 0 ].X = AValue & 0x3FFF;

            switch( AValue >> ( 8 + 6 ) )
            {
                case 0b00: APoints[ 0 ].Status = TTouchStatus::Down; break;
                case 0b01: APoints[ 0 ].Status = TTouchStatus::Up; break;
                default: APoints[ 0 ].Status = TTouchStatus::Contact; break;
            }

            AValue = Read16();
/*
            if( ( AValue & 0x0FFF ) == 0 )
            {
                Serial.println( AValue1, BIN );
                Serial.println( AValue, BIN );
            }
*/
            int8_t ARealCount = 0;
            if( ( APoints[ 0 ].X != 128 ) && (( AValue & 0x0FFF ) != 0 ) && (( AValue & 0x0FFF ) < 240 ) )
            {
                APoints[ 0 ].Y = AValue & 0x0FFF;
                APoints[ 0 ].ID = AValue >> ( 8 + 4 );
                if( APoints[ 0 ].ID == 8  )
                    APoints[ 0 ].ID = 1;

                ARealCount = 1;
            }

            if( ACount == 2 )
            {
//                Serial.println( ACount );
                Read16(); // Skip 2 bytes

                AValue = Read16();
                APoints[ ARealCount ].X = AValue & 0x3FFF;

                switch( AValue >> ( 8 + 6 ) )
                {
                    case 0b00: APoints[ ARealCount ].Status = TTouchStatus::Down; break;
                    case 0b01: APoints[ ARealCount ].Status = TTouchStatus::Up; break;
                    default: APoints[ ARealCount ].Status = TTouchStatus::Contact; break;
                }

                AValue = Read16();
                if( ( APoints[ ARealCount ].X != 128 ) && (( AValue & 0x0FFF ) != 0 ) && (( AValue & 0x0FFF ) < 240 ) )
                {
                    APoints[ ARealCount ].Y = AValue & 0x0FFF;
                    APoints[ ARealCount ].ID = AValue >> ( 8 + 4 );
                    if( APoints[ ARealCount ].ID == 8  )
                        APoints[ ARealCount ].ID = 1;

                    ++ ARealCount;
                }
            }

            else
            {
                if( FLastPoints[ 1 ].Status != TTouchStatus::Up )
                {
//                    if( ( ( FLastPoints[ 0 ].X >> 2 ) == ( APoints[ 0 ].X >> 2 ) ) && ( ( FLastPoints[ 0 ].Y >> 2 ) == ( APoints[ 0 ].Y >> 2 ) ) )
                    if( FLastPoints[ 0 ].ID == APoints[ 0 ].ID )
                        APoints[ 1 ] = FLastPoints[ 1 ];

                    else
                        APoints[ 1 ] = FLastPoints[ 0 ];

                    ARealCount = 2;
                }

                else
                {
                    APoints[ 1 ].X = -1;
                    APoints[ 1 ].Y = -1;
                    APoints[ 1 ].ID = 3;
                }

                APoints[ 1 ].Status = TTouchStatus::Up;
            }

            if( FLastCount() == ARealCount )
                if( memcmp( FLastPoints, APoints, sizeof( APoints ) ) == 0 )
                    return;

            if( ARealCount )
                ReportResult( APoints, ARealCount );

            if( ( ! ARealCount ) && ACount )
                return;

            FLastCount() = ARealCount;
            memcpy( FLastPoints, APoints, sizeof( APoints ) );
		}

	};
//---------------------------------------------------------------------------
} // Mitov

#include "Mitov_BuildChecks_End.h"
