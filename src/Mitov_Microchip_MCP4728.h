////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
	template <
		typename T_Enabled,
		typename T_Resistor
	> class TArduinoMicrochipMCP4728ChannelPowerDown :
		public T_Enabled,
		public T_Resistor
	{
	public:
		_V_PROP_( Enabled )
		_V_PROP_( Resistor )

	};
//---------------------------------------------------------------------------
	template <
		typename T_Enabled,
		typename T_Value
	> class TArduinoMicrochipMCP4728ChannelInitialValue :
		public T_Enabled,
		public T_Value
	{
	public:
		_V_PROP_( Enabled )
		_V_PROP_( Value )

	};
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
		typename T_ExternalReference,
		typename T_FStarted,
		typename T_Gain,
		uint8_t C_INDEX,
		typename T_InitialValue,
		typename T_PowerDown,
		typename T_WriteToEEPROM
	> class TArduinoMicrochipMCP4728Channel :
		public T_ExternalReference,
		public T_FStarted,
		public T_Gain,
		public T_InitialValue,
		public T_PowerDown,
		public T_WriteToEEPROM
	{
	public:
		_V_PROP_( ExternalReference )
		_V_PROP_( Gain )
		_V_PROP_( InitialValue )
		_V_PROP_( PowerDown )
		_V_PROP_( WriteToEEPROM )

	protected:
		_V_PROP_( FStarted )

	protected:
		void SetValue( float AValue )
		{
			FStarted() = true;
			uint32_t AIntValue = AValue * 0x0FFF + 0.5;

			if( ! ExternalReference().GetValue() )
			  AIntValue |= (1 << 15);

			if( PowerDown().Enabled() )
			{
				if( PowerDown().Resistor() >= 500 )
					AIntValue |= ( 0b11 << 13 );

				else if( PowerDown().Resistor() >= 100 )
					AIntValue |= ( 0b10 << 13 );

				else
					AIntValue |= ( 0b01 << 13 );

			}

			if( Gain() )
				AIntValue |= (1 << 12);

			C_OWNER.SetChannelValue( C_INDEX, WriteToEEPROM(), AIntValue );
		}

	public:
		inline void GetWriteToEEPROM( bool & AValue )
		{
			AValue = WriteToEEPROM();
		}

	public:
		inline void InputPin_o_Receive( void *_Data )
		{
			SetValue( MitovConstrain<float>( *(float * )_Data, 0.0f, 1.0f ) );
		}

	public:
		inline void SystemStart()
		{
			if( ! FStarted().GetValue() )
				SetValue( InitialValue().Value() );

		}

	public:
		inline TArduinoMicrochipMCP4728Channel()
		{
			FStarted() = false;
		}

	};
//---------------------------------------------------------------------------
	namespace MCP4728Const
	{
		constexpr uint8_t	MULTI_IR_CMD		= 0x40; ///< Command to write to the input register only
//		constexpr uint8_t	MULTI_EEPROM_CMD	= 0x50; ///< Command to write to the input register and EEPROM
//		constexpr uint8_t	FAST_WRITE_CMD		= 0xC0; ///< Command to write all channels at once with
	}
//---------------------------------------------------------------------------
	template <
		typename T_I2C, T_I2C &C_I2C,
		typename T_Address,
		typename T_Channels_GetWriteToEEPROM,
		typename T_Enabled
	> class TArduinoMicrochipMCP4728 :
		public T_Address,
		public T_Enabled
	{
	public:
		_V_PROP_( Enabled )
		_V_PROP_( Address )

	protected:
		void WriteCommand( uint8_t ACommand, uint16_t AValue )
		{
//			Serial.println( ACommand, HEX );
//			Serial.println( AValue, HEX );
			C_I2C.beginTransmission( uint8_t( Address() ));
			C_I2C.write( ACommand );
			C_I2C.write( uint8_t( AValue >> 8 ));
			C_I2C.write( uint8_t( AValue ));
			C_I2C.endTransmission();
		}

	public:
		void SetChannelValue( uint8_t AIndex, bool AWriteToEEPROM, uint16_t AValue )
		{
			if( Enabled() )
				WriteCommand( MCP4728Const::MULTI_IR_CMD | ( AWriteToEEPROM ? 1 : 0 ) | ( AIndex << 1 ), AValue );

		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_I2C, T_I2C &C_I2C,
		typename T_Address,
		typename T_Channels_GetWriteToEEPROM,
		typename T_Enabled
	> class TArduinoMicrochipMCP4728_Clocked :
		public T_Address,
		public T_Enabled
	{
	public:
		_V_PROP_( Enabled )
		_V_PROP_( Address )

	protected:
		uint16_t	FValues[ 4 ] = { 0 };

	protected:
		void WriteCommand( uint8_t ACommand, uint16_t AValue )
		{
			C_I2C.beginTransmission( uint8_t( Address() ));
			C_I2C.write( ACommand );
			C_I2C.write( uint8_t( AValue >> 8 ));
			C_I2C.write( uint8_t( AValue ));
			C_I2C.endTransmission();
		}

	public:
		void SetChannelValue( uint8_t AIndex, bool AWriteToEEPROM, uint16_t AValue )
		{
			FValues[ AIndex ] = AValue;
//			WriteCommand( MCP4728Const::MULTI_IR_CMD | ( AWriteToEEPROM ? 1 : 0 ) | ( AIndex << 1 ), AValue );
		}

	public:
		void ClockInputPin_o_Receive( void *_Data )
		{

			if( ! Enabled() )
				return;

			for( int i = 0; i < 4; ++ i )
			{
				bool AWriteToEEPROM = false;
				T_Channels_GetWriteToEEPROM::Call( i, AWriteToEEPROM );

				WriteCommand( MCP4728Const::MULTI_IR_CMD | ( AWriteToEEPROM ? 1 : 0 ) | ( i << 1 ), FValues[ i ] );
			}
/*
//			Serial.println( "ClockInputPin_o_Receive" );
			CurrentValue = InitialValue();
			UpdateValue();
*/
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"