////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>
#include <Mitov_Graphics.h>
#include <Mitov_Display_GC9A01.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
	namespace  ST7796S
	{
		const uint8_t SWRESET		= 0x01;   ///< Software Reset (maybe, not documented)
		const uint8_t RDDID			= 0x04;     ///< Read display identification information
		const uint8_t RDDST			= 0x09;     ///< Read Display Status
		const uint8_t RDMODE		= 0x0A;     ///< Read Display Power Mode
		const uint8_t RDMADCTL		= 0x0B;   ///< Read Display MADCTL
		const uint8_t RDPIXFMT		= 0x0C;   ///< Read Display Pixel Format
		const uint8_t RDIMGFMT		= 0x0D;   ///< Read Display Image Format
		const uint8_t RDSELFDIAG	= 0x0F; ///< Read Display Self-Diagnostic Result
		const uint8_t SLPIN			= 0x10;     ///< Enter Sleep Mode
		const uint8_t SLPOUT		= 0x11;    ///< Sleep Out
		const uint8_t PTLON			= 0x12;     ///< Partial Mode ON
		const uint8_t NORON			= 0x13;     ///< Normal Display Mode ON
		const uint8_t INVOFF		= 0x20;    ///< Display Inversion OFF
		const uint8_t INVON			= 0x21;     ///< Display Inversion ON
		const uint8_t GAMMASET		= 0x26; ///< Gamma Set
		const uint8_t DISPOFF		= 0x28;   ///< Display OFF
		const uint8_t DISPON		= 0x29;    ///< Display ON
		const uint8_t CASET			= 0x2A;     ///< Column Address Set
		const uint8_t RASET			= 0x2B;     ///< Raw/Page Address Set // PASET ?
		const uint8_t RAMWR			= 0x2C;     ///< Memory Write
		const uint8_t RAMRD			= 0x2E; ///< Memory Read
		const uint8_t PTLAR			= 0x30;     ///< Partial Area
		const uint8_t VSCRDEF		= 0x33;   ///< Vertical Scrolling Definition
		const uint8_t TEOFF			= 0x34;     ///< Tearing Effect Line OFF
		const uint8_t TEON			= 0x35;      ///< Tearing Effect Line ON
		const uint8_t MADCTL		= 0x36;    ///< Memory Access Control
		const uint8_t VSCRSADD		= 0x37;  ///< Vertical Scrolling Start Address
		const uint8_t IDLEOFF		= 0x38;   ///< Idle mode OFF
		const uint8_t IDLEON		= 0x39;    ///< Idle mode ON
		const uint8_t COLMOD		= 0x3A;    ///< Pixel Format Set
		const uint8_t CONTINUE		= 0x3C;  ///< Write Memory Continue
		const uint8_t RAMRDC		= 0x3E;  // Read Memory  Continue
		const uint8_t TEARSET		= 0x44;   ///< Set Tear Scanline
		const uint8_t GETLINE		= 0x45;   ///< Get Scanline
		const uint8_t SETBRIGHT		= 0x51; ///< Write Display Brightness
		const uint8_t GETBRIGHT		= 0x52; ///< get Display Brightness
		const uint8_t SETCTRL		= 0x53;   ///< Write CTRL Display
		const uint8_t RDCTRLD		= 0x54;   ///< Read CTRL Display
		const uint8_t WRCABC		= 0x55;   ///< Write content adaptive brightness control
		const uint8_t RDCABC		= 0x56;   ///< Read content adaptive brightness control
		const uint8_t WRCABCMB		= 0x5E;   ///< Write CABC minimum brightness
		const uint8_t RDCABCMB		= 0x5F;   ///< Read CABC minimum brightness
//		const uint8_t POWER7		= 0xA7;   ///< Power Control 7
		const uint8_t RDFCHKSUM		= 0xAA;   ///< Read First Checksum 
		const uint8_t RDCCHKSUM		= 0xAB;   ///< Read Continue Checksum 
//		const uint8_t TEWC			= 0xBA;      ///< Tearing effect width control
//		const uint8_t POWER1		= 0xC1;   ///< Power Control 1
//		const uint8_t POWER2		= 0xC3;   ///< Power Control 2
//		const uint8_t POWER3		= 0xC4;   ///< Power Control 3
//		const uint8_t POWER4		= 0xC9;   ///< Power Control 4
//		const uint8_t RDID1			= 0xDA;     ///< Read ID 1
//		const uint8_t RDID2			= 0xDB;     ///< Read ID 2
//		const uint8_t RDID3			= 0xDC;     ///< Read ID 3
		const uint8_t IFMODE		= 0xB0;     ///< Interface Mode Control
		const uint8_t FRMCTR1		= 0xB1;     ///< Frame Rate Control (In Normal Mode/Full Colors)
		const uint8_t FRMCTR2		= 0xB2;     ///< Frame Rate Control (In Idle Mode/8 colors)
		const uint8_t FRMCTR3		= 0xB3;     ///< Frame Rate Control (In Partial Mode/Full colors)
		const uint8_t INVTR			= 0xB4;     ///< Display Inversion Control 
		const uint8_t BPC			= 0xB5;     ///< Blanking Porch Contro
		const uint8_t DFC			= 0xB6;     ///< Display Function Control
		const uint8_t EM			= 0xB7;     ///< Entry Mode Set
		const uint8_t PWR1			= 0xC0;     ///< Power Control 1
		const uint8_t PWR2			= 0xC1;     ///< Power Control 2
		const uint8_t PWR3			= 0xC2;     ///< Power Control 3
		const uint8_t VCMPCTL		= 0xC5;     ///< Vcom Contro
		const uint8_t VCM_Offset	= 0xC6;     ///< Vcom Offset Register
		const uint8_t NVMADW		= 0xD0;		///< NVM Address/Data
		const uint8_t NVMBPROG		= 0xD1;		///< NVM Byte Program Control
		const uint8_t NVMSTRD		= 0xD2;		///< NVM Status Read
		const uint8_t RDID4			= 0xD3;		///< Read ID4
		const uint8_t PGC			= 0xE0;		///< Positive Gamma Control
		const uint8_t NGC			= 0xE1;		///< Negative Gamma Control 
		const uint8_t DGC1			= 0xE2;		///< Digital Gamma Control1
		const uint8_t DGC2			= 0xE3;		///< Digital Gamma Control2
		const uint8_t DOCA			= 0xE8;		///< Display Output
		const uint8_t CSCON			= 0xF0;		///< CTRL Adjust
		const uint8_t SPIRC			= 0xFB;		///< SPI Read Control



/*
		const uint8_t FRAMERATE		= 0xE8; ///< Frame rate control
		const uint8_t SPI2DATA		= 0xE9;  ///< SPI 2DATA control
		const uint8_t INREGEN2		= 0xEF;  ///< Inter register enable 2
		const uint8_t GAMMA1		= 0xF0;    ///< Set gamma 1
		const uint8_t GAMMA2		= 0xF1;    ///< Set gamma 2
		const uint8_t GAMMA3		= 0xF2;    ///< Set gamma 3
		const uint8_t GAMMA4		= 0xF3;    ///< Set gamma 4
		const uint8_t IFACE			= 0xF6;     ///< Interface control
		const uint8_t INREGEN1		= 0xFE;  ///< Inter register enable 1
*/
		const uint8_t MADCTL_MY		= 0x80;  ///< Bottom to top
		const uint8_t MADCTL_MX		= 0x40;  ///< Right to left
		const uint8_t MADCTL_MV		= 0x20;  ///< Reverse Mode
		const uint8_t MADCTL_ML		= 0x10;  ///< LCD refresh Bottom to top
		const uint8_t MADCTL_RGB	= 0x00; ///< Red-Green-Blue pixel order
		const uint8_t MADCTL_BGR	= 0x08; ///< Blue-Green-Red pixel order
		const uint8_t MADCTL_MH		= 0x04;  ///< LCD refresh right to left

		const uint8_t VISUINO_CMD_DELAY_100 = 0x80;
//		const uint8_t VISUINO_CMD_DELAY = 0b11000000;
		const uint8_t VISUINO_CMD_END_OF_LIST = 0x00;

		const uint8_t PROGMEM InitCmd[] = 
		{
			0xCF, 3, 0x00, 0x83, 0X30,
			0xED, 4, 0x64, 0x03, 0X12, 0X81,
			0xE8, 3, 0x85, 0x01, 0x79,
			0xCB, 5, 0x39, 0x2C, 0x00, 0x34, 0x02,
			0xF7, 1, 0x20,
			0xEA, 2, 0x00, 0x00,
			PWR1, 1, 0x26,		 /*Power control*/
			PWR2, 1, 0x11,		 /*Power control */
			VCMPCTL, 2, 0x35, 0x3E, /*VCOM control*/
			0xC7, 1, 0xBE,		 /*VCOM control*/
			MADCTL, 1, 0x28,		 /*Memory Access Control*/
			COLMOD, 1, 0x05,		 /*Pixel Format Set*/
			FRMCTR1, 2, 0x00, 0x1B,
			0xF2, 1, 0x08,
			GAMMASET, 1, 0x01,
			PGC, 15, 0x1F, 0x1A, 0x18, 0x0A, 0x0F, 0x06, 0x45, 0X87, 0x32, 0x0A, 0x07, 0x02, 0x07, 0x05, 0x00,
			NGC, 15, 0x00, 0x25, 0x27, 0x05, 0x10, 0x09, 0x3A, 0x78, 0x4D, 0x05, 0x18, 0x0D, 0x38, 0x3A, 0x1F,
			CASET, 4, 0x00, 0x00, 0x00, 0xEF,
			RASET, 4, 0x00, 0x00, 0x01, 0x3f,
			RAMWR, 0,
			EM, 1, 0x07,
			DFC, 4, 0x0A, 0x82, 0x27, 0x00,
			SLPOUT, VISUINO_CMD_DELAY_100,
			DISPON, VISUINO_CMD_DELAY_100,

//			MADCTL, 1, 0x48, // TEST
//			INVON, 0, // TEST

			VISUINO_CMD_END_OF_LIST        // End of list
		};
	}
//---------------------------------------------------------------------------
	template <
		typename T_0_IMPLEMENTATION,
		typename T_0_ORIENTATION_IMPLEMENTATION,
		typename T_Mirror,
		typename T_Orientation
	> class DisplayST7796S_Impl :
		public T_0_ORIENTATION_IMPLEMENTATION,
        public T_0_IMPLEMENTATION,
		public T_Mirror,
		public T_Orientation
	{
		typedef DisplayST7796S_Impl T_SELF;

	public:
		_V_PROP_( Mirror )
		_V_PROP_( Orientation )

	public:
		inline int16_t width(void)
		{
			return T_0_ORIENTATION_IMPLEMENTATION::template GetWidth<T_SELF>( this );
		}

		inline int16_t height(void)
		{
			return T_0_ORIENTATION_IMPLEMENTATION::template GetHeight<T_SELF>( this );
		}

        static inline constexpr int16_t GetConstWidth() { return 320; }
        static inline constexpr int16_t GetConstHeight() { return 480; }
/*
		inline constexpr int16_t width(void) const
		{
			if( T_Orientation::GetValue() == goUp || T_Orientation::GetValue() == goDown )
				return 320;

			return 480;
		}

		inline constexpr int16_t height(void) const
		{
			if( T_Orientation::GetValue() == goUp || T_Orientation::GetValue() == goDown )
				return 480;

			return 320;
		}
*/
		TUniColor GetPixelColor( int16_t x, int16_t y )
		{
			if( ! T_0_IMPLEMENTATION::HasPixelRead() )
				return 0x0000;

			if((x < 0) ||(x >= width()) || (y < 0) || (y >= height()))
				return 0x0000;

			return T_0_IMPLEMENTATION::GetPixelColor( x, y );
		}

	public:
		inline void setAddrWindowInternal(int x1, int y1, int x2, int y2)
		{
//			Serial.println( "setAddrWindow" );
/*
			Serial.println( x1 );
			Serial.println( y1 );
			Serial.println( x2 );
			Serial.println( y2 );
*/
			uint32_t t;

//			uint16_t x2 = (x1 + w - 1), y2 = (y1 + h - 1);
			T_0_IMPLEMENTATION::WriteCommandData2( ST7796S::CASET, x1, x2 );  // HX8357D uses same registers!
			T_0_IMPLEMENTATION::WriteCommandData2( ST7796S::RASET, y1, y2 ); // HX8357D uses same registers!

			sendCommandInternal( ST7796S::RAMWR );
			T_0_IMPLEMENTATION::SetDataCommandHigh();
		}

		inline void setAddrWindow(int x1, int y1, int x2, int y2)
		{
//			T_0_IMPLEMENTATION::WriteStart();
			setAddrWindowInternal( x1, y1, x2, y2);
//			T_0_IMPLEMENTATION::WriteEnd();
		}

	protected:
		void SendCommandSequence( const uint8_t *APointer )
		{
			T_0_IMPLEMENTATION::WriteStart();
            uint8_t cmd;
			while (( cmd = pgm_read_byte( APointer ++ )) > 0 )
			{
				uint8_t x = pgm_read_byte( APointer ++ );
				uint8_t numArgs = x & 0b00111111;
				sendCommands( cmd, APointer, numArgs );
				APointer += numArgs;
				if( x & 0b10000000 )
					delay( 100 );

			}

			T_0_IMPLEMENTATION::WriteEnd();
		}

        inline void sendCommandInternal( uint8_t commandByte )
		{
			T_0_IMPLEMENTATION::SetDataCommandLow();
			T_0_IMPLEMENTATION::Write8( commandByte );
		}

        inline void sendCommand( uint8_t commandByte )
        {
			T_0_IMPLEMENTATION::WriteStart();
			sendCommandInternal( commandByte );
			T_0_IMPLEMENTATION::WriteEnd();
		}

		void sendCommands( uint8_t commandByte, const uint8_t *dataBytes, uint8_t numDataBytes )
		{
/*
			Serial.print( "W: " );
			Serial.print( commandByte, HEX );
			Serial.print( " " );
			Serial.println( numDataBytes );
*/
//			SPI_BEGIN_TRANSACTION();
//			SPI_CS_LOW();
//			T_0_IMPLEMENTATION::WriteStart();

//			SPI_DC_LOW();          // Command mode
			sendCommandInternal( commandByte );

			T_0_IMPLEMENTATION::SetDataCommandHigh();
//			SPI_DC_HIGH();
			for (int i = 0; i < numDataBytes; ++ i )
				T_0_IMPLEMENTATION::Write8( pgm_read_byte( dataBytes++ ));

//			SPI_CS_HIGH();
//			SPI_END_TRANSACTION();
//			T_0_IMPLEMENTATION::WriteEnd();
		}

	public:
		static inline TUniColor NonTransparent( TUniColor color )
		{
			return 0xFF000000 | ::Mitov::Func::SwapRB( color );
		}

	public:
		void UpdateOrientation()
		{
//			Serial.println( "TEST" );
			sendCommand( ST7796S::MADCTL );

//			T_IMPLEMENTATION::writedata( C_PIXEL_MODE );
			switch( T_0_ORIENTATION_IMPLEMENTATION::ConvertOrientation( Orientation().GetValue() ) )
			{
//				case goUp:		T_0_IMPLEMENTATION::w_data( ST7796S::MADCTL_MX | ST7796S::MADCTL_BGR ); break;
//				case goUp:		T_0_IMPLEMENTATION::w_data( (( Mirror().Horizontal() ) ? 0 : ST7796S::MADCTL_MX ) | ST7796S::MADCTL_BGR ); break;
				case goUp:		T_0_IMPLEMENTATION::w_data( (( Mirror().Horizontal() ) ? 0 : ST7796S::MADCTL_MX ) | (( Mirror().Vertical() ) ? ST7796S::MADCTL_MY : 0 ) | ST7796S::MADCTL_BGR ); break;

//				case goLeft:	T_0_IMPLEMENTATION::w_data( ST7796S::MADCTL_MV | ST7796S::MADCTL_BGR ); break;
				case goLeft:	T_0_IMPLEMENTATION::w_data( (( Mirror().Horizontal() ) ? ST7796S::MADCTL_MY : 0 ) | (( Mirror().Vertical() ) ? ST7796S::MADCTL_MX : 0 ) | ST7796S::MADCTL_MV | ST7796S::MADCTL_BGR ); break;

//				case goDown: 	T_0_IMPLEMENTATION::w_data( ST7796S::MADCTL_MY | ST7796S::MADCTL_BGR ); break;
				case goDown: 	T_0_IMPLEMENTATION::w_data( (( Mirror().Horizontal() ) ? ST7796S::MADCTL_MX : 0 ) | (( Mirror().Vertical() ) ? 0 : ST7796S::MADCTL_MY ) | ST7796S::MADCTL_BGR ); break;

//				case goRight: 	T_0_IMPLEMENTATION::w_data( ST7796S::MADCTL_MX | ST7796S::MADCTL_MY | ST7796S::MADCTL_MV | ST7796S::MADCTL_BGR ); break;
				case goRight: 	T_0_IMPLEMENTATION::w_data( (( Mirror().Vertical() ) ? 0 : ST7796S::MADCTL_MX ) | (( Mirror().Horizontal() ) ? 0 : ST7796S::MADCTL_MY ) | ST7796S::MADCTL_MV | ST7796S::MADCTL_BGR ); break;
			}
		}

	public:
		inline void SystemInit()
		{
/*
			Serial.begin( 9600 );
//			delay( 2000 );

			for(int i = 0; i < 5; ++ i )
			{
				Serial.println( "START" );
				delay( 2000 );
			}
*/
			T_0_IMPLEMENTATION::SetDataCommandHigh();
			T_0_IMPLEMENTATION::SetShipSeletHigh();

//			Serial.println( "RESET" );
//			delay( 2000 );
			T_0_IMPLEMENTATION::ExecuteReset( this, 100, 100, 200 );

//			sendCommand( ST7796S::SWRESET ); // Engage software reset
//			delay( 150 );

//			Serial.println( "INIT" );
//			delay( 2000 );
			SendCommandSequence( ST7796S::InitCmd );

//			st7796s_set_orientation( 0x36, 0x48);

//			Serial.println( "READY" );
//			delay( 2000 );

//			ClearScreen( *this, NonTransparent( inherited::BackgroundColor().GetValue() ));
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_0_IMPLEMENTATION,
		typename T_0_ORIENTATION_IMPLEMENTATION,
		typename T_BackgroundColor,
		typename T_Elements_Render,
		typename T_FCursorX,
		typename T_FCursorY,
		typename T_Inverted,
		typename T_Mirror,
		typename T_Orientation,
		typename T_Text
	> class DisplayST7796S :
		public TFTDisplay565<
				DisplayST7796S<
					T_0_IMPLEMENTATION,
					T_0_ORIENTATION_IMPLEMENTATION,
					T_BackgroundColor,
					T_Elements_Render,
					T_FCursorX,
					T_FCursorY,
					T_Inverted,
					T_Mirror,
					T_Orientation,
					T_Text
				>,
				DisplayST7796S_Impl <
						T_0_IMPLEMENTATION,
						T_0_ORIENTATION_IMPLEMENTATION,
                        T_Mirror,
						T_Orientation
					>,
				T_BackgroundColor,
				T_FCursorX,
				T_FCursorY,
				T_Text
			>,
/*
		public GraphicsImpl<
				DisplayGC9A01<
					T_0_IMPLEMENTATION,
					T_0_ORIENTATION_IMPLEMENTATION,
					T_BackgroundColor,
					T_Elements_Render,
					T_FCursorX,
					T_FCursorY,
					T_Orientation,
					T_Text
				>,
				T_BackgroundColor,
				T_Text
			>,
*/
		public T_Inverted,
		public T_Orientation
    {
		typedef DisplayST7796S T_SELF;
/*
		typedef GraphicsImpl<
				T_SELF,
				T_BackgroundColor,
				T_Text
			> inherited;
*/
		typedef DisplayST7796S_Impl <
						T_0_IMPLEMENTATION,
						T_0_ORIENTATION_IMPLEMENTATION,
						T_Mirror,
						T_Orientation
				> T_SELF_IMPLEMENTATION;

		typedef TFTDisplay565<
				T_SELF,
				T_SELF_IMPLEMENTATION,
				T_BackgroundColor,
				T_FCursorX,
				T_FCursorY,
				T_Text
			> inherited;

	public:
		_V_PROP_( Inverted )

	public:
		inline void UpdateInverted()
		{
			T_SELF_IMPLEMENTATION::sendCommand( ( Inverted() ) ? ST7796S::INVOFF : ST7796S::INVON );
		}

	public:
		static inline void ClearScreen( T_SELF &AImplementation, TUniColor color )
		{
			inherited::fillRectImplementation( AImplementation, AImplementation.width(), AImplementation.height(), 0, 0, AImplementation.width(), AImplementation.height(), color );
		}

	public:
		static inline void Elements_CallChain()
		{
			T_Elements_Render::Call();
		}

	public:
		inline void SystemStart()
		{
			inherited::UpdateOrientation();
			UpdateInverted();
			ClearScreen( *this, T_SELF_IMPLEMENTATION::NonTransparent( inherited::BackgroundColor().GetValue() ));

			inherited::RenderElements();
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"
