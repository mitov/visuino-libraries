////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
	namespace TArduinoHoneywellMPRSensorRangeType
	{
		enum TArduinoHoneywellMPRSensorRangeType
		{
			t0001BA,
			t01_6BA,
			t02_5BA,
    
			t0060MG,
			t0100MG,
			t0160MG,
			t0250MG,
			t0400MG,
			t0600MG,

			t0001BG,
			t01_6BG,
			t02_5BG,
    
			t0100KA,
			t0160KA,
			t0250KA,

			t0006KG,
			t0010KG,
			t0016KG,
			t0025KG,
			t0040KG,
			t0060KG,
			t0100KG,
			t0160KG,
			t0250KG,

			t0015PA, 
			t0025PA,
			t0030PA,

			t0001PG,
			t0005PG,
			t0015PG,
			t0030PG,   
    
			t0300YG
		};
	}
//---------------------------------------------------------------------------
	template <
		typename T_FDataReady
	> class TArduinoHoneywellMPRSensor_ReadyFlag_Impl :
		public T_FDataReady
	{
	protected:
		_V_PROP_( FDataReady )

	protected:
		inline bool GetDataReady()
		{
			return FDataReady().GetValue();
		}

		inline bool GetDataReadyConnected()
		{
			return ! T_FDataReady::GetIsConstant();
		}

		inline void ClearDataReady()
		{
			FDataReady() = false;
		}

	public:
		inline void ReadyInputPin_o_Receive( void *_Data )
		{
			FDataReady() = *(bool *)_Data;
		}

	public:
		inline TArduinoHoneywellMPRSensor_ReadyFlag_Impl()
		{
			FDataReady() = false;
		}
	};
//---------------------------------------------------------------------------
	template <
		typename T_PIN, T_PIN &C_T_PIN
	> class TArduinoHoneywellMPRSensor_DirectPin_Impl
	{
	protected:
		inline bool GetDataReady()
		{
			return C_T_PIN.DigitalRead();
		}

		inline constexpr bool GetDataReadyConnected()
		{
			return true;
		}

		inline void ClearDataReady()
		{
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_IMPLEMENTATION,
		typename T_DataReadyImpl,
		typename T_ClockInputPin_o_IsConnected,
		typename T_Enabled,
		typename T_ErrorOutputPins_Integrity,
		typename T_ErrorOutputPins_MathSaturation,
		typename T_FReading,
		typename T_OutputPin,
		typename T_RangeType
	> class TArduinoHoneywellMPRSensor_Basic :
		public T_IMPLEMENTATION,
		public T_DataReadyImpl,
		public T_ClockInputPin_o_IsConnected,
		public T_Enabled,
		public T_ErrorOutputPins_Integrity,
		public T_ErrorOutputPins_MathSaturation,
		public T_FReading,
		public T_OutputPin,
		public T_RangeType
	{
	public:
		_V_PIN_( OutputPin )

		_V_PIN_( ErrorOutputPins_Integrity )
		_V_PIN_( ErrorOutputPins_MathSaturation )

	public:
		_V_PROP_( Enabled )
		_V_PROP_( RangeType )

	protected:
		_V_PROP_( FReading )

	protected:
		_V_PROP_( ClockInputPin_o_IsConnected )

	protected:
		static const uint32_t OUTPUT_MAX = 0xE66666;
		static const uint32_t OUTPUT_MIN = 0x19999A;

	protected:
		inline float GetPSIMax()
		{
			switch( RangeType() )
			{
				case TArduinoHoneywellMPRSensorRangeType::t0001BA : return 1.0 * 100000; // BAR
				case TArduinoHoneywellMPRSensorRangeType::t01_6BA : return 1.6 * 100000; // BAR
				case TArduinoHoneywellMPRSensorRangeType::t02_5BA : return 2.5 * 100000; // BAR

				case TArduinoHoneywellMPRSensorRangeType::t0060MG : return 60.0 * 100; // mBar
				case TArduinoHoneywellMPRSensorRangeType::t0100MG : return 100.0 * 100; // mBar
				case TArduinoHoneywellMPRSensorRangeType::t0160MG : return 160.0 * 100; // mBar
				case TArduinoHoneywellMPRSensorRangeType::t0250MG : return 250.0 * 100; // mBar
				case TArduinoHoneywellMPRSensorRangeType::t0400MG : return 400.0 * 100; // mBar
				case TArduinoHoneywellMPRSensorRangeType::t0600MG : return 600.0 * 100; // mBar

				case TArduinoHoneywellMPRSensorRangeType::t0001BG : return 1.0 * 100000; // BAR
				case TArduinoHoneywellMPRSensorRangeType::t01_6BG : return 1.6 * 100000; // BAR
				case TArduinoHoneywellMPRSensorRangeType::t02_5BG : return 2.5 * 100000; // BAR

				case TArduinoHoneywellMPRSensorRangeType::t0100KA : return 100.0 * 1000; // KP
				case TArduinoHoneywellMPRSensorRangeType::t0160KA : return 160.0 * 1000; // KP
				case TArduinoHoneywellMPRSensorRangeType::t0250KA : return 250.0 * 1000; // KP

				case TArduinoHoneywellMPRSensorRangeType::t0006KG : return 6.0 * 1000; // KP
				case TArduinoHoneywellMPRSensorRangeType::t0010KG : return 10.0 * 1000; // KP
				case TArduinoHoneywellMPRSensorRangeType::t0016KG : return 16.0 * 1000; // KP
				case TArduinoHoneywellMPRSensorRangeType::t0025KG : return 25.0 * 1000; // KP
				case TArduinoHoneywellMPRSensorRangeType::t0040KG : return 40.0 * 1000; // KP
				case TArduinoHoneywellMPRSensorRangeType::t0060KG : return 60.0 * 1000; // KP
				case TArduinoHoneywellMPRSensorRangeType::t0100KG : return 100.0 * 1000; // KP
				case TArduinoHoneywellMPRSensorRangeType::t0160KG : return 160.0 * 1000; // KP
				case TArduinoHoneywellMPRSensorRangeType::t0250KG : return 250.0 * 1000; // KP

				case TArduinoHoneywellMPRSensorRangeType::t0015PA : return 15.0 * 6894.7572931783; // PSI
				case TArduinoHoneywellMPRSensorRangeType::t0025PA : return 25.0 * 6894.7572931783; // PSI
				case TArduinoHoneywellMPRSensorRangeType::t0030PA : return 30.0 * 6894.7572931783; // PSI

				case TArduinoHoneywellMPRSensorRangeType::t0001PG : return 1.0 * 6894.7572931783; // PSI
				case TArduinoHoneywellMPRSensorRangeType::t0005PG : return 5.0 * 6894.7572931783; // PSI
				case TArduinoHoneywellMPRSensorRangeType::t0015PG : return 15.0 * 6894.7572931783; // PSI
				case TArduinoHoneywellMPRSensorRangeType::t0030PG : return 30.0 * 6894.7572931783; // PSI
    
				case TArduinoHoneywellMPRSensorRangeType::t0300YG : return 300.0 * 133.322; //  mmHg
			}
		}

		inline void StartReading()
		{
//			Serial.println( "Start" );
			T_IMPLEMENTATION::BeginTransaction();
			T_IMPLEMENTATION::Write8( 0xAA );
			T_IMPLEMENTATION::Write8( 0x00 );
			T_IMPLEMENTATION::Write8( 0x00 );
			T_IMPLEMENTATION::EndTransaction();
			FReading() = true;
		}

		void ReadData()
		{
			uint32_t AValue = T_IMPLEMENTATION::RequestRead32_HighFirst();

			bool AMathSaturation = ( AValue & uint32_t( 0b00000001 ) << 24 );
			bool AIntegrityFail = ( AValue & uint32_t( 0b00000100 ) << 24 );

			T_ErrorOutputPins_Integrity::SetPinValue( AIntegrityFail );
			T_ErrorOutputPins_MathSaturation::SetPinValue( AMathSaturation );

			if( T_OutputPin::GetPinIsConnected() )
				if( ! ( AMathSaturation || AIntegrityFail ))
				{
//					Serial.println( AValue, BIN );
					AValue &= 0xFFFFFF; // Mask the Status

//					float psi = ( AValue - OUTPUT_MIN ) * ( GetPSIMax() - 0.0 ); // _PSI_min );
//					psi /= (float)( OUTPUT_MAX - OUTPUT_MIN );
//					psi += _PSI_min;

					float psi = ( AValue - OUTPUT_MIN) * ( GetPSIMax() - 0.0 ); //_minPsi);
					psi /= (float)( OUTPUT_MAX - OUTPUT_MIN );
//					psi = (psi / (OUTPUT_MAX - OUTPUT_MIN)) + 0.0; //_minPsi;

					T_OutputPin::SetPinValue( psi );
				}

			T_DataReadyImpl::ClearDataReady();
			FReading() = false;
			if( ! ClockInputPin_o_IsConnected().GetValue() )
				StartReading();

		}

	public:
		inline void ClockInputPin_o_Receive( void *_Data )
		{
			if( ! FReading().GetValue() )
				StartReading();

		}

	public:
		inline void SystemLoopBegin()
		{
			if( FReading() )
			{
				if( T_DataReadyImpl::GetDataReadyConnected() )
				{
					if( T_DataReadyImpl::GetDataReady() )
						ReadData();

				}

				else
				{
					uint8_t AValue = T_IMPLEMENTATION::RequestRead8();
//					Serial.println( AValue, BIN );
//					bool AMathSaturation = ( AValue & 0b00000001 );
//					bool AIntegrityFail = ( AValue & 0b00000100 );
					if( ! ( AValue & 0b00100000 ))
						ReadData();

				}
			}

			else if( ! ClockInputPin_o_IsConnected().GetValue() )
				StartReading();

		}

	public:
		inline TArduinoHoneywellMPRSensor_Basic()
		{
			FReading() = false;
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_IMPLEMENTATION,
		typename T_ClockInputPin_o_IsConnected,
		typename T_Enabled,
		typename T_ErrorOutputPins_Integrity,
		typename T_ErrorOutputPins_MathSaturation,
		typename T_FDataReady,
		typename T_FReading,
		typename T_OutputPin,
		typename T_RangeType
	> class TArduinoHoneywellMPRSensor :
		public TArduinoHoneywellMPRSensor_Basic <
				T_IMPLEMENTATION,
				TArduinoHoneywellMPRSensor_ReadyFlag_Impl<T_FDataReady>,
				T_ClockInputPin_o_IsConnected,
				T_Enabled,
				T_ErrorOutputPins_Integrity,
				T_ErrorOutputPins_MathSaturation,
				T_FReading,
				T_OutputPin,
				T_RangeType
		>
	{
	};
//---------------------------------------------------------------------------
	template <
		typename T_IMPLEMENTATION,
		typename T_PIN, T_PIN &C_T_PIN,
		typename T_ClockInputPin_o_IsConnected,
		typename T_Enabled,
		typename T_ErrorOutputPins_Integrity,
		typename T_ErrorOutputPins_MathSaturation,
		typename T_FDataReady,
		typename T_FReading,
		typename T_OutputPin,
		typename T_RangeType
	> class TArduinoHoneywellMPRSensor_DirectPin :
		public TArduinoHoneywellMPRSensor_Basic <
				T_IMPLEMENTATION,
				TArduinoHoneywellMPRSensor_DirectPin_Impl<T_PIN, C_T_PIN>,
				T_ClockInputPin_o_IsConnected,
				T_Enabled,
				T_ErrorOutputPins_Integrity,
				T_ErrorOutputPins_MathSaturation,
				T_FReading,
				T_OutputPin,
				T_RangeType
		>
	{
	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"
