////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
	class TArduinoGeneralVisionNeuroShield
	{
	public:
		inline void SystemInit()
		{
   			pinMode( 5, OUTPUT );
			Digital.Write( 5, true );
			delay( 100 );
			Digital.Write( 5, false );
			delay( 100 );
		}
	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"
