////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>

#include "Mitov_BuildChecks_Begin.h"
//---------------------------------------------------------------------------
namespace Mitov
{
namespace Types
{
//---------------------------------------------------------------------------
	class ArduinoAnalogInputChannel_AsDigital_Heltec_13
	{
	public:
		inline bool DigitalRead()
		{
			return Digital.Read( 13 );
		}

	public:
		inline ArduinoAnalogInputChannel_AsDigital_Heltec_13()
		{
			pinMode( 13, INPUT );
		}

	};
} // Types
//---------------------------------------------------------------------------
namespace Instances
{
Types::ArduinoAnalogInputChannel_AsDigital_Heltec_13 ArduinoAnalogInputChannel_AsDigital_Heltec_13;
} // Instances
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"
