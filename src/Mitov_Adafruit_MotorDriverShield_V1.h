////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>
#include <Mitov_Stepper.h>
#include <Mitov_TexasInstruments_74HC595.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
		typename T_CONSTANTS
	> class TArduinoAdafruitMotorDriverShieldV1Stepper_Impl :
		public T_CONSTANTS
	{
	public:
		inline void SetPinValue( uint8_t AIndex, uint8_t AValue )
		{
			C_OWNER.SetChannelValue( T_CONSTANTS::GetStepperPins( AIndex ), AValue );
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
//		uint8_t C_BACK,
		uint8_t C_CHANNEL,
//		uint8_t C_FORWARD,
//		uint8_t C_SPEED,
		typename T_Enabled,
		typename T_InitialValue,
		typename T_SlopeRun,
		typename T_SlopeStop
	> class AdafruitShieldV1MotorDriverChannel :
		public T_Enabled,
		public T_InitialValue
	{
	public:
		_V_PROP_( Enabled )
		_V_PROP_( InitialValue )

	public:
		inline void InputPin_o_Receive( void *_Data )
		{
			float ASpeed = MitovConstrain( *(float *)_Data, 0.0f, 1.0f );
			if( InitialValue() == ASpeed )
				return;

			InitialValue() = ASpeed;
			UpdateOutputs();
		}

	public:
		void UpdateOutputs()
		{
			if( Enabled() )
			{
				float AOutSpeed = fabs( InitialValue() - 0.5 ) * 2;
				bool ADirection = InitialValue() > 0.5;

				C_OWNER.SetChannelValue( ( C_CHANNEL ) ? C_OWNER.C_PIN_MOTOR1_BACK : C_OWNER.C_PIN_MOTOR0_BACK, ADirection );
				ADirection = ! ADirection;

				C_OWNER.SetChannelValue( ( C_CHANNEL ) ? C_OWNER.C_PIN_MOTOR1_FORWARD : C_OWNER.C_PIN_MOTOR0_FORWARD, ADirection );

				C_OWNER.AnalogWrite( ( C_CHANNEL ) ? C_OWNER.C_PIN_MOTOR1_SPEED : C_OWNER.C_PIN_MOTOR0_SPEED, AOutSpeed );
			}

			else
			{
				C_OWNER.SetChannelValue( ( C_CHANNEL ) ? C_OWNER.C_PIN_MOTOR1_BACK : C_OWNER.C_PIN_MOTOR0_BACK, false );
				C_OWNER.SetChannelValue( ( C_CHANNEL ) ? C_OWNER.C_PIN_MOTOR1_FORWARD : C_OWNER.C_PIN_MOTOR0_FORWARD, false );
				C_OWNER.AnalogWriteRaw( ( C_CHANNEL ) ? C_OWNER.C_PIN_MOTOR1_SPEED : C_OWNER.C_PIN_MOTOR0_SPEED, 0 );
			}

		}

	public:
		inline void SystemStart()
		{
			pinMode( ( C_CHANNEL ) ? C_OWNER.C_PIN_MOTOR1_SPEED : C_OWNER.C_PIN_MOTOR0_SPEED, OUTPUT );
			UpdateOutputs();
		}

		inline void SystemLoopBeginElapsed( unsigned long AElapsedMicros ) {} // Placeholder for *_Slopped compatibility
	};
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
//		uint8_t C_BACK,
		uint8_t C_CHANNEL,
//		uint8_t C_FORWARD,
//		uint8_t C_SPEED,
		typename T_Enabled,
		typename T_InitialValue,
		typename T_SlopeRun,
		typename T_SlopeStop
	> class AdafruitShieldV1MotorDriverChannel_Slopped :
		public T_Enabled,
		public T_InitialValue,
		public T_SlopeRun,
		public T_SlopeStop
	{
	public:
		_V_PROP_( Enabled )
		_V_PROP_( InitialValue )
		_V_PROP_( SlopeRun )
		_V_PROP_( SlopeStop )

	protected:
		float	FCurrentValue = 0.0f;

	public:
		inline void InputPin_o_Receive( void *_Data )
		{
			float ASpeed = MitovConstrain( *(float *)_Data, 0.0f, 1.0f );
			if( InitialValue() == ASpeed )
				return;

			InitialValue() = ASpeed;
			UpdateOutputs();
		}

	public:
		void UpdateOutputs()
		{
			if( Enabled() )
			{
				float AOutSpeed = fabs( FCurrentValue - 0.5 ) * 2;
				bool ADirection = FCurrentValue > 0.5;

				C_OWNER.SetChannelValue( ( C_CHANNEL ) ? C_OWNER.C_PIN_MOTOR1_BACK : C_OWNER.C_PIN_MOTOR0_BACK, ADirection ); // IN1
				ADirection = ! ADirection;

				C_OWNER.SetChannelValue( ( C_CHANNEL ) ? C_OWNER.C_PIN_MOTOR1_FORWARD : C_OWNER.C_PIN_MOTOR0_FORWARD, ADirection ); // IN2

				C_OWNER.AnalogWrite( ( C_CHANNEL ) ? C_OWNER.C_PIN_MOTOR1_SPEED : C_OWNER.C_PIN_MOTOR0_SPEED, AOutSpeed );
			}
			else
			{
				C_OWNER.SetChannelValue( ( C_CHANNEL ) ? C_OWNER.C_PIN_MOTOR1_BACK : C_OWNER.C_PIN_MOTOR0_BACK, false );
				C_OWNER.SetChannelValue( ( C_CHANNEL ) ? C_OWNER.C_PIN_MOTOR1_FORWARD : C_OWNER.C_PIN_MOTOR0_FORWARD, false );
				C_OWNER.AnalogWriteRaw( ( C_CHANNEL ) ? C_OWNER.C_PIN_MOTOR1_SPEED : C_OWNER.C_PIN_MOTOR0_SPEED, 0 );
			}

		}

	public:
		inline void SystemStart()
		{
			pinMode( ( C_CHANNEL ) ? C_OWNER.C_PIN_MOTOR1_SPEED : C_OWNER.C_PIN_MOTOR0_SPEED, OUTPUT );
			FCurrentValue = InitialValue();

			UpdateOutputs();
		}

		inline void SystemLoopBeginElapsed( unsigned long AElapsedMicros )
		{
			float ATargetValue = ( Enabled() ) ? InitialValue() : 0.5;

			if( FCurrentValue != ATargetValue )
			{
				float ASlope = SlopeRun();
				if( FCurrentValue > ATargetValue )
				{
					if( ATargetValue > 0.5 )
						ASlope = SlopeStop();
				}

				else
				{
					if( ATargetValue < 0.5 )
						ASlope = SlopeStop();
				}

				if( ASlope == 0 )
					FCurrentValue = ATargetValue;

				else
				{
					float ARamp = fabs( AElapsedMicros * ASlope / 1000000 );
					if( FCurrentValue < ATargetValue )
					{
						FCurrentValue += ARamp;
						if( FCurrentValue > ATargetValue )
							FCurrentValue = ATargetValue;

					}

					else
					{
						FCurrentValue -= ARamp;
						if( FCurrentValue < ATargetValue )
							FCurrentValue = ATargetValue;

					}
				}

				UpdateOutputs();
			}
		}

	};
//---------------------------------------------------------------------------
	class TArduinoAdafruitMotorDriverShieldV1MotorConstants_Driver1_Impl
	{
	public:
		static const uint8_t C_PIN_MOTOR0_FORWARD = 2; // IN2
		static const uint8_t C_PIN_MOTOR0_BACK = 3; // IN1
		static const uint8_t C_PIN_MOTOR0_SPEED = 0; //11;

		static const uint8_t C_PIN_MOTOR1_FORWARD = 1; // IN2
		static const uint8_t C_PIN_MOTOR1_BACK = 4; // IN1
		static const uint8_t C_PIN_MOTOR1_SPEED = 1; //3;


	public:
		inline uint8_t GetStepperPins( uint8_t AIndex )
		{
			switch( AIndex )
			{
				case 0 : return 2;
				case 1 : return 3; 
				case 2 : return 0;
				default : return 6;
			}
		}

	};
//---------------------------------------------------------------------------
	class TArduinoAdafruitMotorDriverShieldV1MotorConstants_Driver2_Impl
	{
	public:
		static const uint8_t C_PIN_MOTOR0_FORWARD = 5; // IN2
		static const uint8_t C_PIN_MOTOR0_BACK = 7; // IN1
		static const uint8_t C_PIN_MOTOR0_SPEED = 2; //6;

		static const uint8_t C_PIN_MOTOR1_FORWARD = 0; // IN2
		static const uint8_t C_PIN_MOTOR1_BACK = 6; // IN1
		static const uint8_t C_PIN_MOTOR1_SPEED = 3;// 5;

	public:
		inline uint8_t GetStepperPins( uint8_t AIndex )
		{
			switch( AIndex )
			{
				case 0 : return 1;
				case 1 : return 4; 
				case 2 : return 5;
				default : return 7;
			}
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER, 
		typename T_CONSTANTS
	> class AdafruitMotorDriverDirectControl :
		public T_CONSTANTS
	{
	public:
		inline void SetChannelValue( uint8_t AIndex, bool AValue )
		{
			C_OWNER.SetChannelValue( T_CONSTANTS::GetStepperPins( AIndex ), AValue );
		}

		inline void AnalogWriteRaw( uint8_t AIndex, uint8_t AValue )
		{
			C_OWNER.AnalogWriteRaw( AIndex, AValue );
		}

		inline void AnalogWrite( uint8_t AIndex, float AValue )
		{
			C_OWNER.AnalogWrite( AIndex, AValue );
		}

		void SetModified()
		{
			C_OWNER.SetModified();
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
		typename T_CONSTANTS
	> class AdafruitMotorDriverDCMotors :
		public T_CONSTANTS
	{
	public:
		inline void SetChannelValue( uint8_t AIndex, bool AValue )
		{
			C_OWNER.SetChannelValue( AIndex, AValue );
		}

		inline void AnalogWriteRaw( uint8_t AIndex, uint8_t AValue )
		{
			C_OWNER.AnalogWriteRaw( AIndex, AValue );
		}

		inline void AnalogWrite( uint8_t AIndex, float AValue )
		{
			C_OWNER.AnalogWrite( AIndex, AValue );
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_ClockOutputPin,
		typename T_DataOutputPin,
		typename T_EnableOutputPin,
		typename T_Enabled,
		typename T_FModified,
		typename T_LatchOutputPin,
		typename T_MotorSpeedOutputPins_0,
		typename T_MotorSpeedOutputPins_1,
		typename T_MotorSpeedOutputPins_2,
		typename T_MotorSpeedOutputPins_3
	> class AdafruitMotorDriverControl_V1 :
		public T_ClockOutputPin,
		public T_DataOutputPin,
		public T_EnableOutputPin,
		public T_Enabled,
		public T_FModified,
		public T_LatchOutputPin,
		public T_MotorSpeedOutputPins_0,
		public T_MotorSpeedOutputPins_1,
		public T_MotorSpeedOutputPins_2,
		public T_MotorSpeedOutputPins_3
	{

	public:
		_V_PIN_( ClockOutputPin )
		_V_PIN_( DataOutputPin )
		_V_PIN_( EnableOutputPin )

		_V_PIN_( MotorSpeedOutputPins_0 )
		_V_PIN_( MotorSpeedOutputPins_1 )
		_V_PIN_( MotorSpeedOutputPins_2 )
		_V_PIN_( MotorSpeedOutputPins_3 )

	public:
		_V_PROP_( Enabled )
        
	protected:
        _V_PROP_( FModified )

	protected:
		uint8_t	FBits = 0;

	public:
		inline void UpdateEnabled()
		{
			T_EnableOutputPin::SetPinValue( ! Enabled().GetValue() );
		}

	protected:
		void UpdateAll()
		{
			if( ! FModified().GetValue() )
				return;

			FModified() = false;

			T_LatchOutputPin::SetPinValue( false );
			T_DataOutputPin::SetPinValue( false );

			for ( uint8_t i=0; i<8; ++i )
			{
				T_ClockOutputPin::SetPinValue( false );
				T_DataOutputPin::SetPinValue( FBits & ( 1 << ( 7-i )));
				T_ClockOutputPin::SetPinValue( true );
			}

			T_LatchOutputPin::SetPinValue( true );
		}

	public:
		void SetModified()
		{
			FModified() = true;
		}

		void SetChannelValue( uint8_t AIndex, bool AValue )
		{
			if( AValue )
				FBits |= ( (uint8_t)1 ) << AIndex;

			else
				FBits &= ~( ((uint8_t)1 ) << AIndex );

            SetModified();
		}

		inline void AnalogWrite( uint8_t AIndex, float AValue )
		{
			switch( AIndex )
			{
				case 0 :
					T_MotorSpeedOutputPins_0::SetPinValue( AValue );
					break;

				case 1 :
					T_MotorSpeedOutputPins_1::SetPinValue( AValue );
					break;

				case 2 :
					T_MotorSpeedOutputPins_2::SetPinValue( AValue );
					break;

				case 3 :
					T_MotorSpeedOutputPins_3::SetPinValue( AValue );
					break;
			}
		}

		inline void AnalogWriteRaw( uint8_t AIndex, uint8_t AValue )
		{
			AnalogWrite( AIndex, 0 );
		}

	public:
		inline void SystemStart()
		{
			UpdateAll();
			T_EnableOutputPin::SetPinValue( ! Enabled().GetValue() );
		}

		inline void SystemLoopUpdateHardware()
		{
			UpdateAll();
		}

	public:
		inline AdafruitMotorDriverControl_V1()
		{
			FModified() = true;
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"
