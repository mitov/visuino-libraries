////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>
#include <Mitov_NeoPixel.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
	template <
		typename T_Enabled,
		typename T_InitialValue
	> class UltrasonicRanger_RGB_Led :
		public T_Enabled,
		public T_InitialValue
	{
	public:
		_V_PROP_( Enabled )
		_V_PROP_( InitialValue )

	};
//---------------------------------------------------------------------------
	template <
		typename T_Brightness,
		typename T_FModified,
		typename T_Left,
		uint16_t C_OutputPin,
		typename T_Right
	> class UltrasonicRanger_RGB_Leds :
		public Mitov::NeoPixel_Adapter <
				Mitov::NeoPixel_GRBAdapter<2>, 
				T_Brightness 
			>,
		public T_FModified,
		public T_Left,
		public T_Right
	{
        typedef Mitov::NeoPixel_Adapter <
                Mitov::NeoPixel_GRBAdapter<2>, 
				T_Brightness 
			> inherited;
            
	public:
		_V_PROP_( Left )
		_V_PROP_( Right )

	protected:
		_V_PROP_( FModified )

	public:
		inline void Left_InputPin_o_Receive( void *_Data )
		{
			TColor AColor = *(TColor *)_Data;
			if( AColor == Left().InitialValue().GetReference() )
				return;

			Left().InitialValue() = AColor;
			FModified() = true;
		}

		inline void Right_InputPin_o_Receive( void *_Data )
		{
			TColor AColor = *(TColor *)_Data;
			if( AColor == Right().InitialValue().GetReference() )
				return;

			Right().InitialValue() = AColor;
			FModified() = true;
		}

	public:
		inline void SystemLoopEnd()
		{
			if( ! FModified().GetValue() )
				return;

			if( Left().Enabled() )
				inherited::IntSetPixelColor( 0, TColor( Left().InitialValue() ));

			else
				inherited::IntSetPixelColor( 0, 0, 0, 0 );

			if( Right().Enabled() )
				inherited::IntSetPixelColor( 1, TColor( Right().InitialValue() ));

			else
				inherited::IntSetPixelColor( 1, 0, 0, 0 );

		}

	public:
		inline UltrasonicRanger_RGB_Leds() :
			inherited( C_OutputPin )
		{
			FModified() = true;
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"
