////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
	class TArduino_MKR_WAN_1310_LoRa_Reset
	{
	public:
		inline void Reset() 
		{
		  pinMode(LORA_IRQ_DUMB, OUTPUT);
		  digitalWrite(LORA_IRQ_DUMB, LOW);

		  // Hardware reset
		  pinMode(LORA_BOOT0, OUTPUT);
		  digitalWrite(LORA_BOOT0, LOW);

		  pinMode(LORA_RESET, OUTPUT);
		  digitalWrite(LORA_RESET, HIGH);
		  delay(200);
		  digitalWrite(LORA_RESET, LOW);
		  delay(200);
		  digitalWrite(LORA_RESET, HIGH);
		  delay(50);
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"
