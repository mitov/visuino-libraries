////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
	template <
		typename T_HighestCellVoltageOutputPin,
		typename T_LowestCellVoltageOutputPin,
		typename T_Enabled,
		typename T_Temperature
	> class TArduinoCanBusRenaultKangooBattery :
		public T_HighestCellVoltageOutputPin,
		public T_LowestCellVoltageOutputPin,
		public T_Enabled,
		public T_Temperature
	{
	public:
		_V_PIN_( HighestCellVoltageOutputPin )
		_V_PIN_( LowestCellVoltageOutputPin )

	public:
		_V_PROP_( Enabled )
		_V_PROP_( Temperature )

	public:
		inline void InputPin_o_Receive( void *_Data )
		{
			if( ! Enabled().GetValue() )
				return;

			Mitov::TCanBusPacket &ADataBlock = *(Mitov::TCanBusPacket*)_Data;

			if( ADataBlock.GetID() != 155 )
				return;

			if( ADataBlock.IsRequest() )
				return;

			if( ADataBlock.GetSize() < 7 )
				return;

			const uint8_t *AData = ADataBlock.Read();

			float ATemperature = float( AData[ 3 ] >> 1 ) - 40;
			if( Temperature().InFahrenheit() )
				ATemperature = Func::ConvertCtoF( ATemperature );

			Temperature().OutputPin().SetPinValue( ATemperature );
			T_HighestCellVoltageOutputPin::SetPinValue( ( 1000 + float((( uint16_t( AData[ 3 ] & 0x1 ) << 8 ) | AData[ 4 ])) * 10 ) / 1000 );
			T_LowestCellVoltageOutputPin::SetPinValue( ( 1000 + float((uint16_t(AData[5]) << 1) | (AData[6] >> 7)) * 10 ) / 1000 );
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_Highest,
		typename T_Lowest
	> class TArduinoCanBusRenaultKangooBatterySourceCellVoltage :
		public T_Highest,
		public T_Lowest
	{
	public:
		_V_PROP_( Highest )
		_V_PROP_( Lowest )

	};
//---------------------------------------------------------------------------
	template <
		typename T_InFahrenheit,
		typename T_InitialValue
	> class TArduinoCanBusRenaultKangooBatterySourceTemperature :
		public T_InFahrenheit,
		public T_InitialValue
	{
	public:
		_V_PROP_( InFahrenheit )
		_V_PROP_( InitialValue )

    };
//---------------------------------------------------------------------------
	template <
		typename T_CellVoltage,
		typename T_Enabled,
		typename T_FModified,
		typename T_OutputPin,
		typename T_Temperature
	> class TArduinoCanBusRenaultKangooBatterySource :
		public T_CellVoltage,
		public T_Enabled,
		public T_FModified,
		public T_OutputPin,
		public T_Temperature
	{
	public:
		_V_PIN_( OutputPin )

	public:
		_V_PROP_( CellVoltage )
		_V_PROP_( Enabled )
		_V_PROP_( Temperature )

	protected:
		_V_PROP_( FModified )

	protected:
		void TrySendValue()
		{
			if( ! FModified().GetValue() )
				return;

            FModified() = false;

			uint8_t ABuffer[ 7 ] = { 0x80, 0x55, 0xF8, 0x00, 0x00, 0x00, 0x00 };

			int32_t ATemperature = ( Temperature().InFahrenheit() ) ?
					Func::Round( Func::ConvertFtoC( Temperature().InitialValue().GetValue() ) + 40 )
				:
					Func::Round( Temperature().InitialValue().GetValue() + 40 );

			if( ATemperature > 127 )
				ATemperature = 127;

			else if( ATemperature < 0 )
				ATemperature = 0;

			ABuffer[ 3 ] = uint8_t( ATemperature << 1 );


//			T_HighestCellVoltageOutputPin::SetPinValue( ( 1000 + float((( uint16_t( AData[ 3 ] & 0x1 ) << 8 ) | AData[ 4 ])) * 10 ) / 1000 );
			int32_t AHighestCellVoltage = Func::Round( CellVoltage().Highest().InitialValue().GetValue() * 100 - 100 );
			if( AHighestCellVoltage < 0 )
				AHighestCellVoltage = 0;

			else if( AHighestCellVoltage > 511 )
				AHighestCellVoltage = 511;

			ABuffer[ 3 ] |= uint8_t( AHighestCellVoltage >> 8 );
			ABuffer[ 4 ] = uint8_t( AHighestCellVoltage );

//			T_LowestCellVoltageOutputPin::SetPinValue( ( 1000 + float((uint16_t(AData[5]) << 1) | (AData[6] >> 7)) * 10 ) / 1000 );
			int32_t ALowestCellVoltage = Func::Round( CellVoltage().Lowest().InitialValue().GetValue() * 100 - 100 );
			if( ALowestCellVoltage < 0 )
				ALowestCellVoltage = 0;

			else if( ALowestCellVoltage > 511 )
				ALowestCellVoltage = 511;

			ABuffer[ 5 ] = uint8_t( ALowestCellVoltage >> 1 );
			ABuffer[ 6 ] = uint8_t( ( ALowestCellVoltage << 7 ) & 0x80 );

			TCanBusPacket APacket( 155, false, false, 7, ABuffer );
			T_OutputPin::SetPinValue( APacket );
		}

	public:
		inline void HighestVoltage_InputPin_o_Receive( void *_Data )
		{
			FModified() = true;
			CellVoltage().Highest().InputPin_o_Receive( _Data );
		}

		inline void LowestVoltage_InputPin_o_Receive( void *_Data )
		{
			FModified() = true;
			CellVoltage().Lowest().InputPin_o_Receive( _Data );
		}

		inline void Temperature_InputPin_o_Receive( void *_Data )
		{
			FModified() = true;
			Temperature().InputPin_o_Receive( _Data );
		}

		inline void ClockInputPin_o_Receive( void *_Data )
		{
            TrySendValue();
        }

	public:
		inline void SystemStart()
		{
            TrySendValue();
		}

		inline void SystemLoopBegin()
		{
            TrySendValue();
		}

	public:
		inline TArduinoCanBusRenaultKangooBatterySource()
		{
            FModified() = true;
        }

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"
