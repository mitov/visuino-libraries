////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>
#include "Mitov_BasicGenerator.h"

namespace Mitov
{
//---------------------------------------------------------------------------
	template <
		typename T_InLux
	> class TArduinoLightSensorTEMT6000_Impl :
		public T_InLux
	{
	public:
		_V_PROP_( InLux )

	protected:
		template<typename T> inline float Compute( T *AOwner, float AValue )
		{
			if( ! InLux().GetValue() )
				return AValue;

			return AValue * AOwner->GetVoltageReference() * ( 1000.0f / 5.0f ); // Readings are in lux scale
		}
	};
//---------------------------------------------------------------------------
}

