////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>
#include <Mitov_Display_ILI9341.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
//	TArduinoTFTLCDShieldTouchCoordinateMode = ( cmScreen, cmNormalized, cmRaw );
//---------------------------------------------------------------------------
	template <
		typename T_MaxRaw,
		typename T_MinRaw,
		typename T_Raw
	> class TArduinoTFTLCDShieldTouchCoordinate :
		public T_MaxRaw,
		public T_MinRaw,
		public T_Raw
	{
	public:
		_V_PROP_( Raw )
//		_V_PROP_( Mode )
		_V_PROP_( MinRaw )
		_V_PROP_( MaxRaw )

	};
//---------------------------------------------------------------------------
	template <
		typename T_OWNER, T_OWNER &C_OWNER,
		typename T_MinPressure,
		typename T_PressureOutputPin,
		typename T_XCoordinate,
		typename T_XOutputPin,
		typename T_YCoordinate,
		typename T_YOutputPin
	> class TFTShieldTouchScreen :
		public T_XOutputPin,
		public T_YOutputPin,
		public T_PressureOutputPin,
		public T_MinPressure,
		public T_XCoordinate,
		public T_YCoordinate
	{
	protected:
		static const int NUMSAMPLES = 5;

		static const int C_YP = A1;  // must be an analog pin, use "An" notation!
		static const int C_XM = A2;  // must be an analog pin, use "An" notation!
		static const int C_YM = 7;   // can be a digital pin
		static const int C_XP = 6;   // can be a digital pin


	public:
		_V_PIN_( XOutputPin )
		_V_PIN_( YOutputPin )
		_V_PIN_( PressureOutputPin )

	public:
		_V_PROP_( MinPressure )
		_V_PROP_( XCoordinate )
		_V_PROP_( YCoordinate )

	public:
		void ClockInputPin_o_Receive( void *_Data )
		{
			int32_t x, y;
			int samples[NUMSAMPLES];
			uint8_t i;

			bool	valid = true;

//			delay( 10 );

			pinMode(C_YP, INPUT);
			pinMode(C_YM, INPUT);

//			delay( 1 );

			Digital.Write( C_YP, false);
			Digital.Write( C_YM, false);

			pinMode(C_XP, OUTPUT);
			pinMode(C_XM, OUTPUT);

			Digital.Write( C_XP, true );
			Digital.Write( C_XM, false );

			for (i=0; i<NUMSAMPLES; i++)
				samples[i] = Analog.ReadRaw(C_YP);
  
			Func::insert_sort(samples, NUMSAMPLES);
			if( abs( samples[ 1 ] - samples[ 3 ] ) > 10 )
				valid = false;

			x = ( Analog.GetRange() - samples[NUMSAMPLES/2]);

			pinMode(C_XP, INPUT);
			pinMode(C_XM, INPUT);
//		   *portOutputRegister(xp_port) &= ~xp_pin;
			Digital.Write(C_XP, false);
   
			pinMode( C_YP, OUTPUT);
//		   *portOutputRegister(yp_port) |= yp_pin;
			Digital.Write(C_YP, true);
			pinMode(C_YM, OUTPUT);
  
			for (i=0; i<NUMSAMPLES; i++)
				samples[i] = Analog.ReadRaw(C_XM);
		   
			Func::insert_sort(samples, NUMSAMPLES);
			if( abs( samples[ 1 ] - samples[ 3 ] ) > 10 )
				valid = false;

			y = ( Analog.GetRange() - samples[NUMSAMPLES/2]);

			// Set X+ to ground
			pinMode( C_XP, OUTPUT);
//			*portOutputRegister(xp_port) &= ~xp_pin;
			Digital.Write( C_XP, false);
  
			// Set Y- to VCC
//			*portOutputRegister(ym_port) |= ym_pin;
			Digital.Write( C_YM, true); 
  
			// Hi-Z X- and Y+
//			*portOutputRegister(yp_port) &= ~yp_pin;
			Digital.Write( C_YP, false);
			pinMode(C_YP, INPUT);
  
/*
			for (i=0; i<NUMSAMPLES; i++)
				samples[i] = Analog.ReadRaw(C_XM);

			int z1 = samples[NUMSAMPLES/2]; //Analog.ReadRaw(C_XM); 

			for (i=0; i<NUMSAMPLES; i++)
				samples[i] = Analog.ReadRaw(C_YP);

			int z2 = samples[NUMSAMPLES/2];
*/
			int z1 = Analog.ReadRaw( C_XM ); 
			int z2 = Analog.ReadRaw( C_YP );

/*
			float _rxplate = 300;
			if (_rxplate != 0) 
			{
				// now read the x 
				float rtouch;
				rtouch = z2;
				rtouch /= z1;
				rtouch -= 1;
				rtouch *= x;
				rtouch *= _rxplate;
				rtouch /= 1024;
     
				z = rtouch;
			} 
			else
*/
//			z = (Analog.GetRange()-(z2-z1));


//			Serial.println( z );
			float z;
			if( ! valid )
				z = 0;

			else
			{
//				float ARange = Analog.GetRange() - z2;
				z = ( Analog.GetRange()-(z2-z1)) / Analog.GetRange();
			}

			if( z > MinPressure() )
			{
//				Serial.println( y );
				if( ! XCoordinate().Raw() )
				{
//					case cmNormalized:
//						x = MapRange( x, XCoordinate.MinRaw, XCoordinate.MaxRaw, 0.0f, 1.0f ); break;

//					case cmScreen:
					{
						x = Func::MapRange<float>( MitovMin<float>( XCoordinate().MaxRaw(), MitovMax<float>( XCoordinate().MinRaw(), x )), YCoordinate().MinRaw(), YCoordinate().MaxRaw(), 0, C_OWNER.height() );
//						y = Func::MapRange<float>( MitovMin( YCoordinate.MaxRaw, MitovMax( YCoordinate.MinRaw, y )), YCoordinate.MinRaw, YCoordinate.MaxRaw, 0, FOwner.height() );
						switch( C_OWNER.Orientation() )
						{
							case goDown :
							case goLeft :
								x = C_OWNER.height() - x;
						}
					}
				}

				if( ! YCoordinate().Raw() )
				{
//					case cmNormalized:
//						y = MapRange( y, YCoordinate.MinRaw, YCoordinate.MaxRaw, 0.0f, 1.0f ); break;

//					case cmScreen:
					{
						y = Func::MapRange<float>( MitovMin<float>( YCoordinate().MaxRaw(), MitovMax<float>( YCoordinate().MinRaw(), y )), XCoordinate().MinRaw(), XCoordinate().MaxRaw(), 0, C_OWNER.width() );
						switch( C_OWNER.Orientation() )
						{
							case goDown :
							case goLeft :
								y = C_OWNER.width() - y;
						}
					}
				}

				switch( C_OWNER.Orientation() )
				{
					case goLeft :
					case goRight :
						T_XOutputPin::SetPinValue( x );
						T_YOutputPin::SetPinValue( y );
						break;

					case goUp :
					case goDown :
						T_XOutputPin::SetPinValue( y );
						T_YOutputPin::SetPinValue( x );
						break;
				}
			}

			T_PressureOutputPin::SetPinValue( z );
			pinMode(C_XP, OUTPUT);
			pinMode(C_XM, OUTPUT);

			pinMode(C_YP, OUTPUT);
			pinMode(C_YM, OUTPUT);

//			delay( 10 );

//			Serial.println( z );
		}

	public:
		inline void SystemLoopBegin()
		{
			ClockInputPin_o_Receive( nullptr );
		}
	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"