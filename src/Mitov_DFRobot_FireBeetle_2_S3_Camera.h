﻿////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>
#include <Mitov_ESP32Camera.h>
#include <Mitov_DFRobot_FireBeetle_2_S3.h>

#include "Mitov_BuildChecks_Begin.h"

//---------------------------------------------------------------------------
namespace Mitov
{
//---------------------------------------------------------------------------
	namespace TArduinoESP32_DFRobot_FireBeetle_2_S3_CameraType
	{
		enum TArduinoESP32_DFRobot_FireBeetle_2_S3_CameraType
		{
			OV2640, //1.2,2.8
			OV7725 //1.8,3.3    
		};
	}
//---------------------------------------------------------------------------
	template <
		typename T_Brightness,
		typename T_ColorBar,
		typename T_Contrast,
		typename T_CountBuffers,
		typename T_EnableDownsize,
		typename T_Enabled,
		typename T_Exposure,
		typename T_Gain,
		typename T_GammaCorrection,
		typename T_GrabMode,
		typename T_ImageSize,
		typename T_JpegQuality,
		typename T_LensCorrection,
		typename T_Mirror,
		typename T_OutputPin,
		typename T_PixelCorrection,
		typename T_PixelFormat,
		typename T_Quality,
		typename T_Saturation,
		typename T_SpecialEffect,
		typename T_Type,
		typename T_UsePSRAM,
		typename T_WhiteBalance
   > class ESP32Camera_DFRobot_FireBeetle_2 :
		public ESP32Camera <
				T_Brightness,
				0, // C_I2C_CHANNEL
				42, // C_PIN_HREF
				5, // C_PIN_PCLK
				-1, // C_PIN_POWER_DOWN
				-1, // C_PIN_RESET
				2, // C_PIN_SIOC
				1, // C_PIN_SIOD
				6, // C_PIN_VSYNC
				45, // C_PIN_XCLK
				39, // C_PIN_Y2
				40, // C_PIN_Y3
				41, // C_PIN_Y4
				4, // C_PIN_Y5
				7, // C_PIN_Y6
				8, // C_PIN_Y7
				46, // C_PIN_Y8
				48, // C_PIN_Y9
				T_ColorBar,
				T_Contrast,
				T_CountBuffers,
				T_EnableDownsize,
				T_Enabled,
				T_Exposure,
				T_Gain,
				T_GammaCorrection,
				T_GrabMode,
				T_ImageSize,
				T_JpegQuality,
				T_LensCorrection,
				T_Mirror,
				T_OutputPin,
				T_PixelCorrection,
				T_PixelFormat,
				T_Quality,
				T_Saturation,
				T_SpecialEffect,
				Mitov::ConstantProperty<99, bool, true >, // UsePSRAM
				T_WhiteBalance
			>,
		public T_Type
	{
		typedef ESP32Camera <
				T_Brightness,
				0, // C_I2C_CHANNEL
				42, // C_PIN_HREF
				5, // C_PIN_PCLK
				-1, // C_PIN_POWER_DOWN
				-1, // C_PIN_RESET
				2, // C_PIN_SIOC
				1, // C_PIN_SIOD
				6, // C_PIN_VSYNC
				45, // C_PIN_XCLK
				39, // C_PIN_Y2
				40, // C_PIN_Y3
				41, // C_PIN_Y4
				4, // C_PIN_Y5
				7, // C_PIN_Y6
				8, // C_PIN_Y7
				46, // C_PIN_Y8
				48, // C_PIN_Y9
				T_ColorBar,
				T_Contrast,
				T_CountBuffers,
				T_EnableDownsize,
				T_Enabled,
				T_Exposure,
				T_Gain,
				T_GammaCorrection,
				T_GrabMode,
				T_ImageSize,
				T_JpegQuality,
				T_LensCorrection,
				T_Mirror,
				T_OutputPin,
				T_PixelCorrection,
				T_PixelFormat,
				T_Quality,
				T_Saturation,
				T_SpecialEffect,
				Mitov::ConstantProperty<99, bool, true >, // UsePSRAM
				T_WhiteBalance
			> inherited;

	public:
		_V_PROP_( Type )

	public:
		inline void SystemStart()
		{
			if( Type() == TArduinoESP32_DFRobot_FireBeetle_2_S3_CameraType::OV2640 )
				Instance::DFRobot_FireBeetle_2_S3_PowerModule.SetCameraPower( 1.2, 2.8 );

			else
				Instance::DFRobot_FireBeetle_2_S3_PowerModule.SetCameraPower( 1.8, 3.3 );

			inherited::SystemStart();
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"