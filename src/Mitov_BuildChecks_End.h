#ifdef __TEST_FOR_DEBUG_PRINTS__

#undef USBSerial

#ifdef __V__USBSerial__V__
  #pragma pop_macro("USBSerial")
#endif

#undef Serial

#ifdef __V__Serial__V__
  #pragma pop_macro("Serial")
#endif

#undef Serial0

#ifdef __V__Serial0__V__
  #pragma pop_macro("Serial0")
#endif

#undef Console

#ifdef __V__Console__V__
  #pragma pop_macro("Console")
#endif

#endif