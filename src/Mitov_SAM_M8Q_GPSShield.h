////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>
#include <Mitov_GPS.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
	template <
		typename T_ResetOutputPin,
		typename T_SerialOutputPin
	> class SAM_M8Q_GPSShield_Serial_Impl :
		public T_ResetOutputPin,
		public T_SerialOutputPin
	{
	public:
		_V_PIN_( ResetOutputPin )
		_V_PIN_( SerialOutputPin )

	protected:
		void Reset()
		{
			if( ! T_ResetOutputPin::GetPinIsConnected() )
				return;

			T_ResetOutputPin::SetPinValueLow();
			delayMicroseconds( 2 );
			T_ResetOutputPin::SetPinValueHigh();
		}

		template<typename T> inline void Read( T *AInstance )
		{
			int AData = Serial1.read();
			if( AData < 0 )
				return;

			if( T_SerialOutputPin::GetPinIsConnected() )
			{
				uint8_t AByte = AData;
				T_SerialOutputPin::SetPinValue( Mitov::TDataBlock( 1, &AByte ) );
			}

			AInstance->ProcessChar( AData );
		}

	public:
		inline void ResetInputPin_o_Receive( void *_Data )
		{	
			Reset();
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"
