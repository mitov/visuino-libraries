////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
	template <
		typename T_OutputPins_Up,
		typename T_OutputPins_Down,
		typename T_OutputPins_Left,
		typename T_OutputPins_Press
	> class Makerfocus_ESP8266_OLED_Joystick :
		public T_OutputPins_Up,
		public T_OutputPins_Down,
		public T_OutputPins_Left,
		public T_OutputPins_Press
	{
	public:
		_V_PIN_( OutputPins_Up )
		_V_PIN_( OutputPins_Down )
		_V_PIN_( OutputPins_Left )
		_V_PIN_( OutputPins_Press )

	public:
		inline void SystemInit()
		{
			if( T_OutputPins_Up::GetPinIsConnected() )
				pinMode( 12, INPUT_PULLUP );

			if( T_OutputPins_Down::GetPinIsConnected() )
				pinMode( 13, INPUT_PULLUP );

			if( T_OutputPins_Left::GetPinIsConnected() )
				pinMode( 0, INPUT_PULLUP );

			if( T_OutputPins_Press::GetPinIsConnected() )
				pinMode( 14, INPUT_PULLUP );

		}

		inline void SystemLoopBegin()
		{
			if( T_OutputPins_Up::GetPinIsConnected() )
				T_OutputPins_Up::SetPinValue( ! Digital.Read( 12 ) );

			if( T_OutputPins_Down::GetPinIsConnected() )
				T_OutputPins_Down::SetPinValue( ! Digital.Read( 13 ) );

			if( T_OutputPins_Left::GetPinIsConnected() )
				T_OutputPins_Left::SetPinValue( ! Digital.Read( 0 ) );

			if( T_OutputPins_Press::GetPinIsConnected() )
				T_OutputPins_Press::SetPinValue( ! Digital.Read( 14 ) );

		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"

