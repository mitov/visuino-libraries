////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2025 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Mitov.h>

#include "Mitov_BuildChecks_Begin.h"

namespace Mitov
{
//---------------------------------------------------------------------------
namespace PS2ControllerConst
{
	const uint8_t enter_config[] PROGMEM ={0x01, 0x43, 0x00, 0x01 }; //, 0x5A, 0x5A, 0x5A, 0x5A, 0x5A};
//	const uint8_t set_mode[] PROGMEM ={0x01, 0x44, 0x00, /* enabled */ 0x01, /* locked */ 0x03, 0x00, 0x00, 0x00, 0x00};
	const uint8_t set_mode[] PROGMEM ={0x01, 0x44, 0x00, /* enabled */ 0x01, /* locked */ 0x03};
//	const uint8_t set_bytes_large[] PROGMEM ={0x01,0x4F,0x00,0xFF,0xFF,0x03,0x00,0x00,0x00};
	const uint8_t exit_config[] PROGMEM ={0x01, 0x43, 0x00, 0x00};
//	const uint8_t enable_rumble[] PROGMEM ={0x01, 0x4D, 0x00, /* motor 1 on */ 0x00, /* motor 2 on*/ 0x01, 0xff, 0xff, 0xff, 0xff};
	const uint8_t enable_rumble[] PROGMEM ={0x01, 0x4D, 0x00, /* motor 1 on */ 0x00, /* motor 2 on*/ 0x01 };
	const uint8_t type_read[] PROGMEM ={0x01, 0x45, 0x00 }; //, 0x5A, 0x5A, 0x5A, 0x5A, 0x5A, 0x5A};
	const uint8_t set_pressures[] PROGMEM = {0x01, 0x4F, 0x00, 0xFF, 0xFF, 0x03, 0x00, 0x00, 0x00};

	const uint8_t star_read[3] PROGMEM = {0x01,0x42,0};

//	const uint8_t poll[] PROGMEM = {0x01, 0x42, 0x00, 0xFF, 0xFF};
//	const uint8_t poll[] PROGMEM = {0x01, 0x42, 0x00 };

	constexpr unsigned long COMMAND_RETRY_INTERVAL = 10;
	constexpr unsigned long COMMAND_TIMEOUT = 250;
	constexpr unsigned long MODE_SWITCH_DELAY = 500;
	constexpr uint8_t BUFFER_SIZE = 32;
	constexpr uint8_t PSX_ANALOG_BTN_DATA_SIZE = 12;

	constexpr uint8_t NEGCON_I_II_BUTTON_THRESHOLD = 128U;

	constexpr uint8_t NEGCON_L_BUTTON_THRESHOLD = 240U;

	constexpr uint8_t INTER_CMD_BYTE_DELAY = 50;

	constexpr uint8_t CTRL_BYTE_DELAY = 3;
/*
	enum PsxControllerType
	{
		PSCTRL_UNKNOWN = 0,			//!< No idea
		PSCTRL_DUALSHOCK,			//!< DualShock or compatible
		PSCTRL_DSWIRELESS,			//!< Sony DualShock Wireless
		PSCTRL_GUITHERO,			//!< Guitar Hero controller
	};

	enum PsxControllerProtocol
	{
		PSPROTO_UNKNOWN = 0,		//!< No idea
		PSPROTO_DIGITAL,			//!< Original controller (SCPH-1010) protocol (8 digital buttons + START + SELECT)
		PSPROTO_DUALSHOCK,			//!< DualShock (has analog axes)
		PSPROTO_DUALSHOCK2,			//!< DualShock 2 (has analog axes and buttons)
		PSPROTO_FLIGHTSTICK,		//!< Green-mode (like DualShock but missing SELECT, L3 and R3)
		PSPROTO_NEGCON,				//!< Namco neGcon (has 1 analog X axis and analog Square, Circle and L1 buttons)
		PSPROTO_JOGCON,				//!< Namco Jogcon (Wheel is mapped to analog X axis, half a rotation in each direction)
		PSPROTO_GUNCON
	};
*/
}

//---------------------------------------------------------------------------
	template <
		typename T_SPI, T_SPI &C_SPI,
		typename T_0_IMPLEMENTATION,
		typename T_ChipSelectOutputPin,
		typename T_Enabled,
		typename T_ErrorOutputPin,
//		typename T_FConnected,
		typename T_FState,
		typename T_SPISpeed
	> class PS2Controller :
		public T_0_IMPLEMENTATION,
		public T_ChipSelectOutputPin,
		public T_Enabled,
		public T_ErrorOutputPin,
//		public T_FConnected,
		public T_FState,
		public T_SPISpeed
	{
	public:
		_V_PIN_( ChipSelectOutputPin )
		_V_PIN_( ErrorOutputPin )

	public:
		_V_PROP_( Enabled )
		_V_PROP_( SPISpeed )

	protected:
//		_V_PROP_( FConnected )
		_V_PROP_( FState )

		unsigned long	FLastTime = 0;

	protected:
		enum TState
		{
            stNotConnected,
            stConnected,
			stConnecting,
			stEnterConfig,
			stEnableAnalogSticks,
			stEnableAnalogButtons,
			stEnableRumble,
			stExitConfigMode,
			stUpdateAnalogModeEnterConfigMode,
			stUpdateAnalogMode
//            st
        };

	public:
		inline void Update_AnalogMode()
		{
            FState() = stUpdateAnalogModeEnterConfigMode;
		}

	protected:
		inline bool begin()
		{
			// Some disposable readings to let the controller know we are here
			uint8_t inputBuffer[ PS2ControllerConst::BUFFER_SIZE ];
			for( uint8_t i = 0; i < 5; ++ i )
			{
				read( inputBuffer );
				delay( 1 );
			}

			return read( inputBuffer );
		}

		inline void ReadController()
		{
//			Serial.println( "ReadController" );
//			uint8_t PS2data[ 21 ];
//			bool AError = read_gamepad( PS2data, T_0_IMPLEMENTATION::GetSmallMotor(), T_0_IMPLEMENTATION::GetLargeMotor() );
//			Serial.println( "ReadController2" );

//		  return ((~buttons & button) > 0);

//			T_0_IMPLEMENTATION::Process( PS2data, AError );


//			Serial.println( "ReadController" );
//			Fps2x.read_gamepad( FSmallMotor(), FLargeMotor );
//			T_LeftStickOutputPins_X::SetPinValue( ReadAnalog( PSS_LX ));
//			T_LeftStickOutputPins_Y::SetPinValue( ReadAnalog( PSS_LY ));
//			Serial.println( ReadAnalog( PSS_LY )); //PSAB_PAD_UP )); // PSS_LX ));
			switch( FState() )
			{
				case stNotConnected :
				{
	//                Serial.println( "BEGIN" );
					if( ! begin() )
					{
						T_ErrorOutputPin::SetPinValueHigh();
						FState() = stNotConnected;
	//					      Serial.println ( "Cannot begin" );
						break;
					}

					FLastTime = millis();
					FState() = stConnecting;
					break;
				}
	//					Serial.println ( "Controller found!" );
				case stConnecting :
				{
	//				Serial.println( "CONNECTING" );

					if( millis() - FLastTime < 300 )
						break;

	//				delay( 300 );
					if( ! enterConfigMode())
					{
						FState() = stNotConnected;
						T_ErrorOutputPin::SetPinValueHigh();
						break;
					}

					FState() = stEnterConfig;
					FLastTime = millis();
					break;
				}

				case stEnterConfig :
				{
	//				Serial.println( "stEnterConfig" );

					if( millis() - FLastTime < PS2ControllerConst::MODE_SWITCH_DELAY )
						break;

	//						PsxControllerType ctype = getControllerType();
	//                        Serial.println( uint8_t( ctype ));
	//				PGM_BYTES_P cname = reinterpret_cast<PGM_BYTES_P> (pgm_read_ptr (&(controllerTypeStrings[ctype < PSCTRL_MAX ? static_cast<uint8_t> (ctype) : PSCTRL_MAX])));
	//				Serial.print ( "Controller Type is: " );
	//				Serial.println (PSTR_TO_F (cname));

					FState() = stEnableAnalogSticks;
					if( T_0_IMPLEMENTATION::GetUsesJoystick() )
					{
						if( ! enableAnalogSticks())
						{
							T_ErrorOutputPin::SetPinValueHigh();
							FState() = stNotConnected;
	//								Serial.println ( "Cannot enable analog sticks" );
							break;
						}

						FLastTime = millis();
					}

					else
						FLastTime = millis() - PS2ControllerConst::MODE_SWITCH_DELAY;

					break;
				}

				case stUpdateAnalogModeEnterConfigMode :
				{
	//				Serial.println( "stUpdateAnalogModeEnterConfigMode" );

					if( ! enterConfigMode())
					{
						FState() = stNotConnected;
						T_ErrorOutputPin::SetPinValueHigh();
						break;
					}

					FState() = stUpdateAnalogMode;
					FLastTime = millis();
                    break;
				}

				case stUpdateAnalogMode :
				{
	//				Serial.println( "stUpdateAnalogMode" );

					if( millis() - FLastTime < PS2ControllerConst::MODE_SWITCH_DELAY )
						break;

	//						PsxControllerType ctype = getControllerType();
	//                        Serial.println( uint8_t( ctype ));
	//				PGM_BYTES_P cname = reinterpret_cast<PGM_BYTES_P> (pgm_read_ptr (&(controllerTypeStrings[ctype < PSCTRL_MAX ? static_cast<uint8_t> (ctype) : PSCTRL_MAX])));
	//				Serial.print ( "Controller Type is: " );
	//				Serial.println (PSTR_TO_F (cname));

					FState() = stEnableRumble;
					if( T_0_IMPLEMENTATION::GetUsesJoystick() )
					{
						if( ! enableAnalogSticks())
						{
							T_ErrorOutputPin::SetPinValueHigh();
							FState() = stNotConnected;
	//								Serial.println ( "Cannot enable analog sticks" );
							break;
						}

						FLastTime = millis();
					}

					else
						FLastTime = millis() - PS2ControllerConst::MODE_SWITCH_DELAY;

					break;
				}

				case stEnableAnalogSticks :
				{
	//				Serial.println( "stEnableAnalogSticks" );

					if( millis() - FLastTime < PS2ControllerConst::MODE_SWITCH_DELAY )
						break;

					FState() = stEnableAnalogButtons;
					if( T_0_IMPLEMENTATION::GetUsesAnalogButtons() )
					{
						if( ! enableAnalogButtons())
						{
							T_ErrorOutputPin::SetPinValueHigh();
							FState() = stNotConnected;
	//								Serial.println ( "Cannot enable analog buttons" );
							break;
						}

						FLastTime = millis();
					}

					else
						FLastTime = millis() - PS2ControllerConst::MODE_SWITCH_DELAY;

					break;
				}

				case stEnableAnalogButtons :
				{
	//				Serial.println( "stEnableAnalogButtons" );

					if( millis() - FLastTime < PS2ControllerConst::MODE_SWITCH_DELAY )
						break;

					FState() = stEnableRumble;
					if( T_0_IMPLEMENTATION::GetUsesMotors() )
					{
						if( ! enableRumble())
						{
							T_ErrorOutputPin::SetPinValueHigh();
							FState() = stNotConnected;
	//								Serial.println ( "Cannot enable analog buttons" );
							break;
						}

						FLastTime = millis();
					}

					else
						FLastTime = millis() - PS2ControllerConst::MODE_SWITCH_DELAY;

					break;
				}

				case stEnableRumble :
				{
	//				Serial.println( "stEnableRumble" );

					if( millis() - FLastTime < PS2ControllerConst::MODE_SWITCH_DELAY )
						break;

					if( ! exitConfigMode())
					{
	//								Serial.println ( "Cannot exitConfigMode" );

						T_ErrorOutputPin::SetPinValueHigh();
						FState() = stNotConnected;
						break;
					}

					FState() = stExitConfigMode;
					T_ErrorOutputPin::SetPinValueLow();
                    break;
				}

				case stExitConfigMode :
				{
	//				Serial.println( "stExitConfigMode" );

					if( millis() - FLastTime > PS2ControllerConst::MODE_SWITCH_DELAY )
						FState() = stConnected;

                    break;
				}

				case stConnected :
				{
					uint8_t inputBuffer[ PS2ControllerConst::BUFFER_SIZE ];
					if( ! read( inputBuffer ) )
					{
						FState() = stNotConnected;
						T_ErrorOutputPin::SetPinValueHigh();
	//                    Serial.println( "ERROR" );
					}

	/*
					for( int i = 0; i < PS2ControllerConst::BUFFER_SIZE; ++ i )
					{
						Serial.print( inputBuffer[ i ], HEX );
						Serial.print( ", " );
					}

					Serial.println();
	*/
					T_0_IMPLEMENTATION::Process( inputBuffer, FState() != stConnected );
                    break;
				}
            }
		}

/*
	public:
		inline bool	ReadDigital( unsigned int AIndex )
		{
//			return Fps2x.Button( AIndex );
		}

		inline float ReadAnalog( unsigned int AIndex )
		{
//			return ((float)Fps2x.Analog( AIndex )) / 255;
		}
*/
	public:
		inline void ClockInputPin_o_Receive( void *_Data )
		{
			ReadData( false );
		}

	protected:
		inline void attention()
		{
			T_ChipSelectOutputPin::SetPinValueLow();
			C_SPI.beginTransaction( SPISpeed().GetValue(), LSBFIRST, SPI_MODE3 );
			delayMicroseconds( PS2ControllerConst::CTRL_BYTE_DELAY );
		}

		inline void noAttention()
		{
			C_SPI.endTransaction();
			T_ChipSelectOutputPin::SetPinValueHigh();
			delayMicroseconds( PS2ControllerConst::CTRL_BYTE_DELAY );
		}

		void ReadData( bool AChangeOnly )
		{
			if( ! Enabled() )
				return;

			ReadController();
//			T_Sensors_Process::Call( AChangeOnly );
		}

		inline bool isValidReply( const uint8_t *status ) const
		{
			//~ return status[0] != 0xFF || status[1] != 0xFF || status[2] != 0xFF;
			return status[1] != 0xFF && (status[2] == 0x5A || status[2] == 0x00);
			//~ return /* status[0] == 0xFF && */ status[1] != 0xFF && status[2] == 0x5A;
		}

		uint8_t getReplyLength( const uint8_t *buf ) const
		{
			return (buf[1] & 0x0F) * 2;
		}

		uint8_t shiftInOut( const uint8_t out ) const
		{
			return SPI.transfer (out);
		}

		void shiftInOut( bool AFromProgMem, const uint8_t *out, uint8_t *in, const uint8_t len ) const
		{
			for( uint8_t i = 0; i < len; ++ i )
			{
				if( AFromProgMem )
					in[i] = shiftInOut( pgm_read_byte( out + i ) );

				else
					in[i] = shiftInOut( out[i] );

				delayMicroseconds( PS2ControllerConst::INTER_CMD_BYTE_DELAY );   // Very important!
			}
		}

		void shiftInOutSimple( uint8_t *in, const uint8_t len ) const
		{
			for( uint8_t i = 0; i < len; ++ i )
			{
				in[i] = shiftInOut( 0x5A );
				delayMicroseconds( PS2ControllerConst::INTER_CMD_BYTE_DELAY );   // Very important!
			}
		}

		bool autoShift( bool AFromProgMem, const uint8_t *out, const uint8_t len, uint8_t *in )
		{
			attention();

			// All commands have at least 3 bytes, so shift out those first
			shiftInOut( AFromProgMem, out, in, 3 );
			if( isValidReply( in ))
			{
				// Reply is good, get full length
				uint8_t replyLen = getReplyLength( in );

				// Shift out rest of command
				if( len > 3 )
					shiftInOut( AFromProgMem, out + 3, in + 3, len - 3);

				uint8_t left = replyLen - len + 3;
				//~ Serial.print ("len = ");
				//~ Serial.print (replyLen);
				//~ Serial.print (", left = ");
				//~ Serial.println (left);
				if( left == 0)
				{
					// The whole reply was gathered
					noAttention();
					return true;
				}

				if( len + left <= PS2ControllerConst::BUFFER_SIZE )
				{
					// Part of reply is still missing and we have space for it
					shiftInOutSimple( in + len, left);
					noAttention();
					return true;
				}

				// Reply incomplete but not enough space provided
			}

			noAttention();
			return false;
		}

		bool exitConfigMode()
		{
            bool ARet = false;
			unsigned long start = millis();
			do
			{
				//~ shiftInOut (poll, in, sizeof( poll));
				//~ shiftInOut (exit_config, in, sizeof( exit_config));
				uint8_t inputBuffer[ PS2ControllerConst::BUFFER_SIZE ];
				ARet = autoShift( true, PS2ControllerConst::exit_config, sizeof( PS2ControllerConst::exit_config ), inputBuffer );

				if( ARet )
					ARet = ! isConfigReply( inputBuffer );

				if( ! ARet )
					delay( PS2ControllerConst::COMMAND_RETRY_INTERVAL );

			}
			while( ! ARet && millis() - start <= PS2ControllerConst::COMMAND_TIMEOUT );

//			delay( PS2ControllerConst::MODE_SWITCH_DELAY );

			return ARet;
		}

		inline bool isConfigReply( const uint8_t *status ) const
		{
			return (status[1] & 0xF0) == 0xF0;
		}

		inline bool isFlightstickReply( const uint8_t *status ) const
		{
			return (status[1] & 0xF0) == 0x50;
		}

		inline bool isDualShockReply( const uint8_t *status ) const
		{
			return (status[1] & 0xF0) == 0x70;
		}

		inline bool isDualShock2Reply( const uint8_t *status ) const
		{
			return status[1] == 0x79;
		}

		inline bool isDigitalReply( const uint8_t *status ) const
		{
			return (status[1] & 0xF0) == 0x40;
		}

		inline bool isNegconReply( const uint8_t *status ) const
		{
			return status[1] == 0x23;
		}

		inline bool isJogconReply( const uint8_t *status ) const
		{
			return (status[1] & 0xF0) == 0xE0;
		}

		inline bool isGunconReply( const uint8_t *status ) const
		{
			return status[1] == 0x63;
		}

		bool read( uint8_t *inputBuffer )
		{
//			analogSticksValid = false;
//			analogButtonDataValid = false;

			uint8_t out[ 5 ] = { 0x01, 0x42, 0x00 };
			out[3] = T_0_IMPLEMENTATION::GetSmallMotor() ? 0xFF : 0x00;
			out[4] = T_0_IMPLEMENTATION::GetLargeMotor();

			bool ARet = autoShift( false, out, 5, inputBuffer );

//			Serial.println( ARet );
			if( ARet )
			{
				if( isConfigReply( inputBuffer ))
				{
//					Serial.println( "isConfigReply" );
					// We're stuck in config mode, try to get out
					exitConfigMode();
				}

				else
				{
//					Serial.println( "TEST1" );
					// We surely have buttons
//					previousButtonWord = buttonWord;
/*
					buttonWord = ( uint16_t( inputBuffer[4] ) << 8) | inputBuffer[3];

					// See if we have anything more to read
					if( isDualShock2Reply (inputBuffer))
						protocol = PSPROTO_DUALSHOCK2;

					else if( isDualShockReply (inputBuffer))
						protocol = PSPROTO_DUALSHOCK;

					else if( isFlightstickReply (inputBuffer))
						protocol = PSPROTO_FLIGHTSTICK;

					else if( isNegconReply (inputBuffer))
						protocol = PSPROTO_NEGCON;

					else if( isJogconReply (inputBuffer))
						protocol = PSPROTO_JOGCON;

					else if( isGunconReply (inputBuffer))
						protocol = PSPROTO_GUNCON;

					else
						protocol = PSPROTO_DIGITAL;

					switch (protocol) {
						case PSPROTO_DUALSHOCK2:
							// We also have analog button data
							analogButtonDataValid = true;
							for( int i = 0; i < PS2ControllerConst::PSX_ANALOG_BTN_DATA_SIZE; ++ i )
								analogButtonData[i] = inputBuffer[i + 9];

							// Now fall through to DualShock case, the next line
							// avoids GCC warning
							// FALLTHRU

						case PSPROTO_GUNCON:
							// The Guncon uses the same reply format as DualShocks,
							// by just falling through we'll end up with:
							// - A (Left side) -> Start
							// - B (Right side) -> Cross
							// - Trigger -> Circle
							// - Low uint8_t of HSYNC -> RX
							// - High uint8_t of HSYNC -> RY
							// - Low uint8_t of VSYNC -> LX
							// - High uint8_t of VSYNC -> LY

						case PSPROTO_DUALSHOCK:
						case PSPROTO_FLIGHTSTICK:
							// We have analog stick data
							analogSticksValid = true;
							rx = inputBuffer[5];
							ry = inputBuffer[6];
							lx = inputBuffer[7];
							ly = inputBuffer[8];
							break;
						case PSPROTO_NEGCON:
							// Map the twist axis to X axis of left analog
							analogSticksValid = true;
							lx = inputBuffer[5];

							// Map analog button data to their reasonable counterparts
							analogButtonDataValid = true;
							analogButtonData[PSAB_CROSS] = inputBuffer[6];
							analogButtonData[PSAB_SQUARE] = inputBuffer[7];
							analogButtonData[PSAB_L1] = inputBuffer[8];

							// Make up "missing" digital data
							if( analogButtonData[PSAB_SQUARE] >= PS2ControllerConst::NEGCON_I_II_BUTTON_THRESHOLD )
								buttonWord &= ~PSB_SQUARE;

							if( analogButtonData[PSAB_CROSS] >= PS2ControllerConst::NEGCON_I_II_BUTTON_THRESHOLD )
								buttonWord &= ~PSB_CROSS;

							if( analogButtonData[PSAB_L1] >= PS2ControllerConst::NEGCON_L_BUTTON_THRESHOLD )
								buttonWord &= ~PSB_L1;

							break;
						case PSPROTO_JOGCON:
							// Map the wheel X axis of left analog, half a rotation
							// per direction: uint8_t 5 has the wheel position, it is
							// 0 at startup, then we have 0xFF down to 0x80 for
							// left/CCW, and 0x01 up to 0x80 for right/CW
							//
							// uint8_t 6 is the number of full CW rotations
							// uint8_t 7 is 0 if wheel is still, 1 if it is rotating CW
							//        and 2 if rotation CCW
							// uint8_t 8 seems to stay at 0
							//
							// We'll want to cap the movement halfway in each
							// direction, for ease of use/implementation.

							analogSticksValid = true;
							if( inputBuffer[6] < 0x80 ) {
								// CW up to half
								lx = inputBuffer[5] < 0x80 ? inputBuffer[5] : (0x80 - 1);
							} else {
								// CCW down to half
								lx = inputBuffer[5] > 0x80 ? inputBuffer[5] : (0x80 + 1);
							}

							// Bring to the usual 0-255 range
							lx += 0x80;
							break;
						default:
							// We are already done
							break;
					}
*/

					return true;
				}
			}

			return false;
		}

		bool enterConfigMode()
		{
//			Serial.println( "enterConfigMode" );
			bool ARes = false;
            bool AResult = false;

			unsigned long start = millis();
			uint8_t cnt = 0;
			do
			{
				uint8_t inputBuffer[ PS2ControllerConst::BUFFER_SIZE ];
				AResult = autoShift( true, PS2ControllerConst::enter_config, sizeof( PS2ControllerConst::enter_config ), inputBuffer );
  /*
				Serial.print( "AResult: " );
				Serial.println( AResult );

				for( int i = 0; i < PS2ControllerConst::BUFFER_SIZE; ++ i )
				{
					Serial.print( inputBuffer[ i ], HEX );
					Serial.print( ", " );
				}

				Serial.println();
*/
				AResult = AResult && isConfigReply( inputBuffer );

				if ( AResult )
					break;

				++ cnt;

				ARes = ( cnt >= 3 );

//				Serial.println( cnt );
//                delay( 1000 );

				if( ! ARes )
					delay( PS2ControllerConst::COMMAND_RETRY_INTERVAL );

			}
			while( !ARes && millis() - start <= PS2ControllerConst::COMMAND_TIMEOUT );

//			delay( PS2ControllerConst::MODE_SWITCH_DELAY );

//				Serial.println( ARes );
			return AResult;
		}

/*
		PsxControllerType getControllerType()
		{
			PsxControllerType ret = PSCTRL_UNKNOWN;

			uint8_t inputBuffer[ PS2ControllerConst::BUFFER_SIZE ];
			if( autoShift( true, PS2ControllerConst::type_read, sizeof( PS2ControllerConst::type_read ), inputBuffer ) )
			{
				const uint8_t& controllerType = inputBuffer[3];
				if( controllerType == 0x03 )
					ret = PSCTRL_DUALSHOCK;
				//~ } else if( controllerType == 0x01 && inputBuffer[1] == 0x42) {
					//~ return 4;		// ???

				else if( controllerType == 0x01 && inputBuffer[1] != 0x42 )
					ret = PSCTRL_GUITHERO;

				else if( controllerType == 0x0C )
					ret = PSCTRL_DSWIRELESS;

			}

			return ret;
		}
*/
		bool enableRumble()
		{
//			Serial.println( "enableRumble" );
//            delay( 2000 );
			bool AResult = false;
			bool ret = true;
//			uint8_t out[ sizeof(PS2ControllerConst::enable_rumble)];
//			uint8_t out[ 5 ] = {0x01, 0x4D, 0x00};

//			memcpy_P( out, PS2ControllerConst::enable_rumble, sizeof( PS2ControllerConst::enable_rumble ) );
//			out[3] = enabled ? 0x00 : 0xff;
//			out[4] = enabled ? 0x01 : 0xff;

			unsigned long start = millis();
			uint8_t cnt = 0;
			do
			{
				uint8_t inputBuffer[ PS2ControllerConst::BUFFER_SIZE ];
				AResult = autoShift( true, PS2ControllerConst::enable_rumble, sizeof( PS2ControllerConst::enable_rumble ), inputBuffer );
				if( AResult )
					break;

				++ cnt;

				ret = ( cnt >= 3 );

				if( !ret )
					delay( PS2ControllerConst::COMMAND_RETRY_INTERVAL );

			}
			while(!ret && millis () - start <= PS2ControllerConst::COMMAND_TIMEOUT );
//			delay( PS2ControllerConst::MODE_SWITCH_DELAY );

			return AResult;
		}

		bool enableAnalogSticks()
		{
			bool AResult = false;
			uint8_t out[ sizeof( PS2ControllerConst::set_mode ) ];

			memcpy_P(out, PS2ControllerConst::set_mode, sizeof( PS2ControllerConst::set_mode));
			out[3] = T_0_IMPLEMENTATION::GetUsesJoystick() ? 0x01 : 0x00;
			out[4] = T_0_IMPLEMENTATION::GetLockAnalogMode() ? 0x03 : 0x00;

			bool ret = false;
			unsigned long start = millis();
			uint8_t cnt = 0;
			do
			{
				uint8_t inputBuffer[ PS2ControllerConst::BUFFER_SIZE ];

				/* We can't know if we have successfully enabled analog mode until
				 * we get out of config mode, so let's just be happy if we get a few
				 * consecutive valid replies
				 */

				AResult = autoShift( false, out, sizeof( PS2ControllerConst::set_mode), inputBuffer );
				if( AResult )
					break;

				++ cnt;

				ret = ( cnt >= 3 );

				if( !ret)
					delay( PS2ControllerConst::COMMAND_RETRY_INTERVAL );

			}
			while( !ret && millis() - start <= PS2ControllerConst::COMMAND_TIMEOUT );
//			delay( PS2ControllerConst::MODE_SWITCH_DELAY );

			return AResult;
		}

		bool enableAnalogButtons()
		{
			bool AResult = false;

			bool ret = false;
			unsigned long start = millis();
			uint8_t cnt = 0;
			do
			{
				uint8_t inputBuffer[ PS2ControllerConst::BUFFER_SIZE ];

				/* We can't know if we have successfully enabled analog mode until
				 * we get out of config mode, so let's just be happy if we get a few
				 * consecutive valid replies
				 */
				AResult = autoShift( true, PS2ControllerConst::set_pressures, sizeof( PS2ControllerConst::set_pressures ), inputBuffer );
				if( AResult )
					break;

				++ cnt;

				ret = ( cnt >= 3 );

				if( !ret )
					delay( PS2ControllerConst::COMMAND_RETRY_INTERVAL );

			}
			while( !ret && millis() - start <= PS2ControllerConst::COMMAND_TIMEOUT );

//			delay( PS2ControllerConst::MODE_SWITCH_DELAY );

			return AResult;
		}

	public:
		inline void SystemInit()
		{
			T_ChipSelectOutputPin::SetPinValueHigh();
			T_ErrorOutputPin::SetPinValueLow();
		}

		inline void SystemStart()
		{
			ReadData( false );
		}

		inline void SystemLoopBegin()
		{
			ReadData( true );
		}

	public:
		inline PS2Controller()
		{
			FState() = stNotConnected;
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_Enabled,
		typename T_Locked
	> class TArduinoPS2ControllerAnalogMode :
		public T_Enabled,
		public T_Locked
	{
	public:
		_V_PROP_( Enabled )
		_V_PROP_( Locked )

	};
//---------------------------------------------------------------------------
	template <
		typename T_AnalogMode,
		typename T_DirectionalButtonsOutputPins_Up_Analog,
		typename T_DirectionalButtonsOutputPins_Up_Digital,
		typename T_DirectionalButtonsOutputPins_Down_Analog,
		typename T_DirectionalButtonsOutputPins_Down_Digital,
		typename T_DirectionalButtonsOutputPins_Left_Analog,
		typename T_DirectionalButtonsOutputPins_Left_Digital,
		typename T_DirectionalButtonsOutputPins_Right_Analog,
		typename T_DirectionalButtonsOutputPins_Right_Digital,
		typename T_FLargeMotor,
		typename T_FSmallMotor,
		typename T_LargeVibrateMotorInputPin_o_IsConnected,
		typename T_LeftFrontButtonsOutputPins_L1_Analog,
		typename T_LeftFrontButtonsOutputPins_L1_Digital,
		typename T_LeftFrontButtonsOutputPins_L2_Analog,
		typename T_LeftFrontButtonsOutputPins_L2_Digital,
		typename T_LeftStickOutputPins_X,
		typename T_LeftStickOutputPins_Y,
		typename T_LeftStickOutputPins_Button,
		typename T_RightFrontButtonsOutputPins_R1_Analog,
		typename T_RightFrontButtonsOutputPins_R1_Digital,
		typename T_RightFrontButtonsOutputPins_R2_Analog,
		typename T_RightFrontButtonsOutputPins_R2_Digital,
		typename T_RightStickOutputPins_X,
		typename T_RightStickOutputPins_Y,
		typename T_RightStickOutputPins_Button,
		typename T_SelectButtonOutputPin,
		typename T_ShapeColorButtonsOutputPins_Green_Triangle_Analog,
		typename T_ShapeColorButtonsOutputPins_Green_Triangle_Digital,
		typename T_ShapeColorButtonsOutputPins_Blue_Cross_Analog,
		typename T_ShapeColorButtonsOutputPins_Blue_Cross_Digital,
		typename T_ShapeColorButtonsOutputPins_Pink_Square_Analog,
		typename T_ShapeColorButtonsOutputPins_Pink_Square_Digital,
		typename T_ShapeColorButtonsOutputPins_Red_Circle_Analog,
		typename T_ShapeColorButtonsOutputPins_Red_Circle_Digital,
		typename T_SmallVibrateMotorInputPin_o_IsConnected,
		typename T_StartButtonOutputPin
	> class PS2_DualShockController :
		public T_AnalogMode,
		public T_DirectionalButtonsOutputPins_Up_Analog,
		public T_DirectionalButtonsOutputPins_Up_Digital,
		public T_DirectionalButtonsOutputPins_Down_Analog,
		public T_DirectionalButtonsOutputPins_Down_Digital,
		public T_DirectionalButtonsOutputPins_Left_Analog,
		public T_DirectionalButtonsOutputPins_Left_Digital,
		public T_DirectionalButtonsOutputPins_Right_Analog,
		public T_DirectionalButtonsOutputPins_Right_Digital,
		public T_FLargeMotor,
		public T_FSmallMotor,
		public T_LargeVibrateMotorInputPin_o_IsConnected,
		public T_LeftFrontButtonsOutputPins_L1_Analog,
		public T_LeftFrontButtonsOutputPins_L1_Digital,
		public T_LeftFrontButtonsOutputPins_L2_Analog,
		public T_LeftFrontButtonsOutputPins_L2_Digital,
		public T_LeftStickOutputPins_X,
		public T_LeftStickOutputPins_Y,
		public T_LeftStickOutputPins_Button,
		public T_RightFrontButtonsOutputPins_R1_Analog,
		public T_RightFrontButtonsOutputPins_R1_Digital,
		public T_RightFrontButtonsOutputPins_R2_Analog,
		public T_RightFrontButtonsOutputPins_R2_Digital,
		public T_RightStickOutputPins_X,
		public T_RightStickOutputPins_Y,
		public T_RightStickOutputPins_Button,
		public T_SelectButtonOutputPin,
		public T_ShapeColorButtonsOutputPins_Green_Triangle_Analog,
		public T_ShapeColorButtonsOutputPins_Green_Triangle_Digital,
		public T_ShapeColorButtonsOutputPins_Blue_Cross_Analog,
		public T_ShapeColorButtonsOutputPins_Blue_Cross_Digital,
		public T_ShapeColorButtonsOutputPins_Pink_Square_Analog,
		public T_ShapeColorButtonsOutputPins_Pink_Square_Digital,
		public T_ShapeColorButtonsOutputPins_Red_Circle_Analog,
		public T_ShapeColorButtonsOutputPins_Red_Circle_Digital,
		public T_SmallVibrateMotorInputPin_o_IsConnected,
		public T_StartButtonOutputPin
	{
	public:
		_V_PIN_( DirectionalButtonsOutputPins_Up_Analog )
		_V_PIN_( DirectionalButtonsOutputPins_Up_Digital )
		_V_PIN_( DirectionalButtonsOutputPins_Down_Analog )
		_V_PIN_( DirectionalButtonsOutputPins_Down_Digital )
		_V_PIN_( DirectionalButtonsOutputPins_Left_Analog )
		_V_PIN_( DirectionalButtonsOutputPins_Left_Digital )
		_V_PIN_( DirectionalButtonsOutputPins_Right_Analog )
		_V_PIN_( DirectionalButtonsOutputPins_Right_Digital )
		_V_PIN_( LeftFrontButtonsOutputPins_L1_Analog )
		_V_PIN_( LeftFrontButtonsOutputPins_L1_Digital )
		_V_PIN_( LeftFrontButtonsOutputPins_L2_Analog )
		_V_PIN_( LeftFrontButtonsOutputPins_L2_Digital )
		_V_PIN_( LeftStickOutputPins_X )
		_V_PIN_( LeftStickOutputPins_Y )
		_V_PIN_( LeftStickOutputPins_Button )
		_V_PIN_( RightFrontButtonsOutputPins_R1_Analog )
		_V_PIN_( RightFrontButtonsOutputPins_R1_Digital )
		_V_PIN_( RightFrontButtonsOutputPins_R2_Analog )
		_V_PIN_( RightFrontButtonsOutputPins_R2_Digital )
		_V_PIN_( RightStickOutputPins_X )
		_V_PIN_( RightStickOutputPins_Y )
		_V_PIN_( RightStickOutputPins_Button )
		_V_PIN_( SelectButtonOutputPin )
		_V_PIN_( ShapeColorButtonsOutputPins_Green_Triangle_Analog )
		_V_PIN_( ShapeColorButtonsOutputPins_Green_Triangle_Digital )
		_V_PIN_( ShapeColorButtonsOutputPins_Blue_Cross_Analog )
		_V_PIN_( ShapeColorButtonsOutputPins_Blue_Cross_Digital )
		_V_PIN_( ShapeColorButtonsOutputPins_Pink_Square_Analog )
		_V_PIN_( ShapeColorButtonsOutputPins_Pink_Square_Digital )
		_V_PIN_( ShapeColorButtonsOutputPins_Red_Circle_Analog )
		_V_PIN_( ShapeColorButtonsOutputPins_Red_Circle_Digital )
		_V_PIN_( StartButtonOutputPin )

	public:
		_V_PROP_( AnalogMode )

	protected:
		_V_PROP_( LargeVibrateMotorInputPin_o_IsConnected )
		_V_PROP_( SmallVibrateMotorInputPin_o_IsConnected )

	protected:
		_V_PROP_( FSmallMotor )
		_V_PROP_( FLargeMotor );

	protected:
		static const uint16_t PSB_SELECT      = 0x0001;
		static const uint16_t PSB_L3          = 0x0002;
		static const uint16_t PSB_R3          = 0x0004;
		static const uint16_t PSB_START       = 0x0008;
		static const uint16_t PSB_PAD_UP      = 0x0010;
		static const uint16_t PSB_PAD_RIGHT   = 0x0020;
		static const uint16_t PSB_PAD_DOWN    = 0x0040;
		static const uint16_t PSB_PAD_LEFT    = 0x0080;
		static const uint16_t PSB_L2          = 0x0100;
		static const uint16_t PSB_R2          = 0x0200;
		static const uint16_t PSB_L1          = 0x0400;
		static const uint16_t PSB_R1          = 0x0800;
		static const uint16_t PSB_GREEN       = 0x1000;
		static const uint16_t PSB_RED         = 0x2000;
		static const uint16_t PSB_BLUE        = 0x4000;
		static const uint16_t PSB_PINK        = 0x8000;
		static const uint16_t PSB_TRIANGLE    = 0x1000;
		static const uint16_t PSB_CIRCLE      = 0x2000;
		static const uint16_t PSB_CROSS       = 0x4000;
		static const uint16_t PSB_SQUARE      = 0x8000;

//These are stick values
		static const uint8_t PSS_RX = 5;
		static const uint8_t PSS_RY = 6;
		static const uint8_t PSS_LX = 7;
		static const uint8_t PSS_LY = 8;

//These are analog buttons
		static const uint8_t PSAB_PAD_RIGHT   = 9;
		static const uint8_t PSAB_PAD_UP      = 11;
		static const uint8_t PSAB_PAD_DOWN    = 12;
		static const uint8_t PSAB_PAD_LEFT    = 10;
		static const uint8_t PSAB_L2          = 19;
		static const uint8_t PSAB_R2          = 20;
		static const uint8_t PSAB_L1          = 17;
		static const uint8_t PSAB_R1          = 18;
		static const uint8_t PSAB_GREEN       = 13;
		static const uint8_t PSAB_RED         = 14;
		static const uint8_t PSAB_BLUE        = 15;
		static const uint8_t PSAB_PINK        = 16;
		static const uint8_t PSAB_TRIANGLE    = 13;
		static const uint8_t PSAB_CIRCLE      = 14;
		static const uint8_t PSAB_CROSS       = 15;
		static const uint8_t PSAB_SQUARE      = 16;

	protected:
		inline bool GetLockAnalogMode()
		{
			return AnalogMode().Locked().GetValue();
		}

		constexpr inline bool GetUsesJoystick()
		{
			return AnalogMode().Enabled().GetValue();
		}

		inline bool GetSmallMotor()
		{
			return FSmallMotor().GetValue();
		}

		inline int8_t GetLargeMotor()
		{
			return FLargeMotor().GetValue();
		}

		inline bool GetUsesMotors()
		{
			return LargeVibrateMotorInputPin_o_IsConnected().GetValue() || SmallVibrateMotorInputPin_o_IsConnected().GetValue();
		}

		constexpr inline bool GetUsesAnalogButtons()
		{
			return true;
/*
			return 	T_DirectionalButtonsOutputPins_Up_Analog::GetPinIsConnected() ||
					T_DirectionalButtonsOutputPins_Down_Analog::GetPinIsConnected() ||
					T_DirectionalButtonsOutputPins_Left_Analog::GetPinIsConnected() ||
					T_DirectionalButtonsOutputPins_Right_Analog::GetPinIsConnected() ||

					T_ShapeColorButtonsOutputPins_Green_Triangle_Analog::GetPinIsConnected() ||
					T_ShapeColorButtonsOutputPins_Blue_Cross_Analog::GetPinIsConnected() ||
					T_ShapeColorButtonsOutputPins_Pink_Square_Analog::GetPinIsConnected() ||
					T_ShapeColorButtonsOutputPins_Red_Circle_Analog::GetPinIsConnected();
*/
		}

		inline void Process( const uint8_t *PS2data, bool AError )
		{
			if( AError )
			{
				T_SelectButtonOutputPin::SetPinValue( false );
				T_StartButtonOutputPin::SetPinValue( false );

				T_DirectionalButtonsOutputPins_Up_Digital::SetPinValue( false );
				T_DirectionalButtonsOutputPins_Down_Digital::SetPinValue( false );
				T_DirectionalButtonsOutputPins_Left_Digital::SetPinValue( false );
				T_DirectionalButtonsOutputPins_Right_Digital::SetPinValue( false );

				T_LeftFrontButtonsOutputPins_L1_Digital::SetPinValue( false );
				T_LeftFrontButtonsOutputPins_L2_Digital::SetPinValue( false );

				T_LeftStickOutputPins_Button::SetPinValue( false );

				T_RightFrontButtonsOutputPins_R1_Digital::SetPinValue( false );
				T_RightFrontButtonsOutputPins_R2_Digital::SetPinValue( false );

				T_RightStickOutputPins_Button::SetPinValue( false );

				T_ShapeColorButtonsOutputPins_Green_Triangle_Digital::SetPinValue( false );
				T_ShapeColorButtonsOutputPins_Blue_Cross_Digital::SetPinValue( false );
				T_ShapeColorButtonsOutputPins_Pink_Square_Digital::SetPinValue( false );
				T_ShapeColorButtonsOutputPins_Red_Circle_Digital::SetPinValue( false );

				T_DirectionalButtonsOutputPins_Up_Analog::SetPinValue( 0.0f );
				T_DirectionalButtonsOutputPins_Down_Analog::SetPinValue( 0.0f );
				T_DirectionalButtonsOutputPins_Left_Analog::SetPinValue( 0.0f );
				T_DirectionalButtonsOutputPins_Right_Analog::SetPinValue( 0.0f );

				T_LeftFrontButtonsOutputPins_L1_Analog::SetPinValue( 0.0f );
				T_LeftFrontButtonsOutputPins_L2_Analog::SetPinValue( 0.0f );

				T_LeftStickOutputPins_X::SetPinValue( 0.5f );
				T_LeftStickOutputPins_Y::SetPinValue( 0.5f );

				T_RightFrontButtonsOutputPins_R1_Analog::SetPinValue( 0.0f );
				T_RightFrontButtonsOutputPins_R2_Analog::SetPinValue( 0.0f );

				T_RightStickOutputPins_X::SetPinValue( 0.5f );
				T_RightStickOutputPins_Y::SetPinValue( 0.5f );

				T_ShapeColorButtonsOutputPins_Green_Triangle_Analog::SetPinValue( 0.0f );
				T_ShapeColorButtonsOutputPins_Blue_Cross_Analog::SetPinValue( 0.0f );
				T_ShapeColorButtonsOutputPins_Pink_Square_Analog::SetPinValue( 0.0f );
				T_ShapeColorButtonsOutputPins_Red_Circle_Analog::SetPinValue( 0.0f );
			}

			else
			{
				uint16_t buttons = ( uint16_t(PS2data[4] ) << 8) + PS2data[3];   //store as one value for multiple functions

				T_SelectButtonOutputPin::SetPinValue( (( ~buttons & PSB_SELECT ) != 0 ) );
				T_StartButtonOutputPin::SetPinValue( (( ~buttons & PSB_START ) != 0 ) );

				T_DirectionalButtonsOutputPins_Up_Digital::SetPinValue( (( ~buttons & PSB_PAD_UP ) != 0 ) );
				T_DirectionalButtonsOutputPins_Down_Digital::SetPinValue( (( ~buttons & PSB_PAD_DOWN ) != 0 ) );
				T_DirectionalButtonsOutputPins_Left_Digital::SetPinValue( (( ~buttons & PSB_PAD_LEFT ) != 0 ) );
				T_DirectionalButtonsOutputPins_Right_Digital::SetPinValue( (( ~buttons & PSB_PAD_RIGHT ) != 0 ) );

				T_LeftFrontButtonsOutputPins_L1_Digital::SetPinValue( (( ~buttons & PSB_L1 ) != 0 ) );
				T_LeftFrontButtonsOutputPins_L2_Digital::SetPinValue( (( ~buttons & PSB_L2 ) != 0 ) );

				T_LeftStickOutputPins_Button::SetPinValue( (( ~buttons & PSB_L3 ) != 0 ) );

				T_RightFrontButtonsOutputPins_R1_Digital::SetPinValue( (( ~buttons & PSB_R1 ) != 0 ) );
				T_RightFrontButtonsOutputPins_R2_Digital::SetPinValue( (( ~buttons & PSB_R2 ) != 0 ) );

				T_RightStickOutputPins_Button::SetPinValue( (( ~buttons & PSB_R3 ) != 0 ) );

				T_ShapeColorButtonsOutputPins_Green_Triangle_Digital::SetPinValue( (( ~buttons & PSB_TRIANGLE ) != 0 ) );
				T_ShapeColorButtonsOutputPins_Blue_Cross_Digital::SetPinValue( (( ~buttons & PSB_CROSS ) != 0 ) );
				T_ShapeColorButtonsOutputPins_Pink_Square_Digital::SetPinValue( (( ~buttons & PSB_SQUARE ) != 0 ) );
				T_ShapeColorButtonsOutputPins_Red_Circle_Digital::SetPinValue( (( ~buttons & PSB_CIRCLE ) != 0 ) );

				T_DirectionalButtonsOutputPins_Up_Analog::SetPinValue( PS2data[ PSAB_PAD_UP ] / 255.f );
				T_DirectionalButtonsOutputPins_Down_Analog::SetPinValue( PS2data[ PSAB_PAD_DOWN ] / 255.f );
				T_DirectionalButtonsOutputPins_Left_Analog::SetPinValue( PS2data[ PSAB_PAD_LEFT ] / 255.f );
				T_DirectionalButtonsOutputPins_Right_Analog::SetPinValue( PS2data[ PSAB_PAD_RIGHT ] / 255.f );

				T_LeftFrontButtonsOutputPins_L1_Analog::SetPinValue( PS2data[ PSAB_L1 ] / 255.f );
				T_LeftFrontButtonsOutputPins_L2_Analog::SetPinValue( PS2data[ PSAB_L2 ] / 255.f );

				T_LeftStickOutputPins_X::SetPinValue( PS2data[ PSS_LX ] / 255.f );
				T_LeftStickOutputPins_Y::SetPinValue( 1.0f - ( PS2data[ PSS_LY ] / 255.f ));

				T_RightFrontButtonsOutputPins_R1_Analog::SetPinValue( PS2data[ PSAB_R1 ] / 255.f );
				T_RightFrontButtonsOutputPins_R2_Analog::SetPinValue( PS2data[ PSAB_R2 ] / 255.f );

				T_RightStickOutputPins_X::SetPinValue( ( PS2data[ PSS_RX ] ) / 255.f );
				T_RightStickOutputPins_Y::SetPinValue( 1.0f - ( PS2data[ PSS_RY ] / 255.f ));

				T_ShapeColorButtonsOutputPins_Green_Triangle_Analog::SetPinValue( PS2data[ PSAB_TRIANGLE ] / 255.f );
				T_ShapeColorButtonsOutputPins_Blue_Cross_Analog::SetPinValue( PS2data[ PSAB_CROSS ] / 255.f );
				T_ShapeColorButtonsOutputPins_Pink_Square_Analog::SetPinValue( PS2data[ PSAB_SQUARE ] / 255.f );
				T_ShapeColorButtonsOutputPins_Red_Circle_Analog::SetPinValue( PS2data[ PSAB_CIRCLE ] / 255.f );
			}
		}

	public:
		inline void SmallVibrateMotorInputPin_o_Receive( void *_Data )
		{
			FSmallMotor() = *(bool *)_Data;
		}

		inline void LargeVibrateMotorInputPin_o_Receive( void *_Data )
		{
			FLargeMotor() = (int8_t)( MitovConstrain( *(float *)_Data, 0.0f, 1.0f ) * 255 + 0.5 );
		}

	public:
		inline PS2_DualShockController()
		{
			FSmallMotor() = false;
			FLargeMotor() = 0;
		}

	};
//---------------------------------------------------------------------------
	template <
		typename T_FretButtonsOutputPins_Green,
		typename T_FretButtonsOutputPins_Red,
		typename T_FretButtonsOutputPins_Yellow,
		typename T_FretButtonsOutputPins_Blue,
		typename T_FretButtonsOutputPins_Orange,
		typename T_StarPowerButtonOutputPin,
		typename T_StrumBarOutputPins_Up,
		typename T_StrumBarOutputPins_Down,
		typename T_StrumBarOutputPins_Left,
		typename T_StrumBarOutputPins_Right,
		typename T_WhammyBarOutputPin
	> class PS2Guitar :
		public T_FretButtonsOutputPins_Green,
		public T_FretButtonsOutputPins_Red,
		public T_FretButtonsOutputPins_Yellow,
		public T_FretButtonsOutputPins_Blue,
		public T_FretButtonsOutputPins_Orange,
		public T_StarPowerButtonOutputPin,
		public T_StrumBarOutputPins_Up,
		public T_StrumBarOutputPins_Down,
		public T_StrumBarOutputPins_Left,
		public T_StrumBarOutputPins_Right,
		public T_WhammyBarOutputPin
	{
	public:
		_V_PIN_( FretButtonsOutputPins_Green )
		_V_PIN_( FretButtonsOutputPins_Red )
		_V_PIN_( FretButtonsOutputPins_Yellow )
		_V_PIN_( FretButtonsOutputPins_Blue )
		_V_PIN_( FretButtonsOutputPins_Orange )
		_V_PIN_( StarPowerButtonOutputPin )
		_V_PIN_( StrumBarOutputPins_Up )
		_V_PIN_( StrumBarOutputPins_Down )
		_V_PIN_( StrumBarOutputPins_Left )
		_V_PIN_( StrumBarOutputPins_Right )
		_V_PIN_( WhammyBarOutputPin )

	protected:
		//Guitar  button constants
		static const uint16_t WHAMMY_BAR	= 0x0008;
		static const uint16_t UP_STRUM		= 0x0010;
		static const uint16_t DOWN_STRUM		= 0x0040;
		static const uint16_t LEFT_STRUM		= 0x0080;
		static const uint16_t RIGHT_STRUM		= 0x0020;
		static const uint16_t STAR_POWER		= 0x0100;
		static const uint16_t GREEN_FRET		= 0x0200;
		static const uint16_t YELLOW_FRET		= 0x1000;
		static const uint16_t RED_FRET		= 0x2000;
		static const uint16_t BLUE_FRET		= 0x4000;
		static const uint16_t ORANGE_FRET		= 0x8000;

	protected:
		inline constexpr bool GetLockAnalogMode()
		{
			return false;
		}

		constexpr inline bool GetSmallMotor()
		{
			return false;
		}

		constexpr inline int8_t GetLargeMotor()
		{
			return 0x00;
		}

		constexpr inline bool GetUsesMotors()
		{
            return false;
		}

		constexpr inline bool GetUsesJoystick()
		{
			return true;
		}

		constexpr inline bool GetUsesAnalogButtons()
		{
			return true;
		}

		inline void Process( const uint8_t *PS2data, bool AError )
		{
			if( AError )
			{
				T_FretButtonsOutputPins_Green::SetPinValue( false );
				T_FretButtonsOutputPins_Red::SetPinValue( false );
				T_FretButtonsOutputPins_Yellow::SetPinValue( false );
				T_FretButtonsOutputPins_Blue::SetPinValue( false );
				T_FretButtonsOutputPins_Orange::SetPinValue( false );

				T_StarPowerButtonOutputPin::SetPinValue( false );

				T_StrumBarOutputPins_Up::SetPinValue( false );
				T_StrumBarOutputPins_Down::SetPinValue( false );
				T_StrumBarOutputPins_Left::SetPinValue( false );
				T_StrumBarOutputPins_Right::SetPinValue( false );

				T_WhammyBarOutputPin::SetPinValue( false );
			}

			else
			{
				uint16_t buttons = ( uint16_t( PS2data[4] ) << 8) + PS2data[3];   //store as one value for multiple functions

				T_FretButtonsOutputPins_Green::SetPinValue( (( ~buttons & GREEN_FRET ) != 0 ) );
				T_FretButtonsOutputPins_Red::SetPinValue( (( ~buttons & RED_FRET ) != 0 ) );
				T_FretButtonsOutputPins_Yellow::SetPinValue( (( ~buttons & YELLOW_FRET ) != 0 ) );
				T_FretButtonsOutputPins_Blue::SetPinValue( (( ~buttons & BLUE_FRET ) != 0 ) );
				T_FretButtonsOutputPins_Orange::SetPinValue( (( ~buttons & ORANGE_FRET ) != 0 ) );

				T_StarPowerButtonOutputPin::SetPinValue( (( ~buttons & STAR_POWER ) != 0 ) );

				T_StrumBarOutputPins_Up::SetPinValue( (( ~buttons & UP_STRUM ) != 0 ) );
				T_StrumBarOutputPins_Down::SetPinValue( (( ~buttons & DOWN_STRUM ) != 0 ) );
				T_StrumBarOutputPins_Left::SetPinValue( (( ~buttons & LEFT_STRUM ) != 0 ) );
				T_StrumBarOutputPins_Right::SetPinValue( (( ~buttons & RIGHT_STRUM ) != 0 ) );

				T_WhammyBarOutputPin::SetPinValue( (( ~buttons & WHAMMY_BAR ) != 0 ) );
			}
		}

	};
//---------------------------------------------------------------------------
}

#include "Mitov_BuildChecks_End.h"