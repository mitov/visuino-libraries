Mitov : Namespace
//---------------------------------------------------------------------------
  [Name( 'GetIndex' )]
  [ArduinoPopulatedCallChainParameter( 'uint8_t &', 'AInIndex' )]
  [ArduinoPopulatedCallChainParameter( 'uint8_t &', 'AOutIndex' )]
  TArduinoTexasInstrumentsADS1X15ChannelGetIndexCallChainAttribute : ArduinoCallChainCompleteAttribute;
//---------------------------------------------------------------------------
  [Name( 'SetValue' )]
  [ArduinoPopulatedCallChainParameter( 'float', 'AValue' )]
  [ArduinoCallChainIndex]
  TArduinoTexasInstrumentsADS1X15ChannelSetValueCallChainAttribute : ArduinoCallChainCompleteAttribute;
//---------------------------------------------------------------------------
  [Name( 'GetCountActiveChannels' )]
  [ArduinoPopulatedCallChainParameter( 'uint8_t &', 'AInIndex' )]
  TArduinoTexasInstrumentsADS1X15ChannelGetCountActiveChannelsCallChainAttribute : ArduinoCallChainCompleteAttribute;
//---------------------------------------------------------------------------
  [Name( 'GetConfig' )]
  [ArduinoPopulatedCallChainParameter( 'uint16_t &', 'AValue' )]
  [ArduinoCallChainIndex]
  TArduinoTexasInstrumentsADS1X15ChannelGetConfigCallChainAttribute : ArduinoCallChainCompleteAttribute;
//---------------------------------------------------------------------------
  [ArduinoClass( 'Mitov::TArduinoTexasInstrumentsADS1X1XComparator' )]
  TArduinoTexasInstrumentsADS1X1XComparator : TArduinoCodePersistent

    [ArduinoUseBindingCheckSetter_Parent( 'UpdateConfig' )]
    Enabled : Boolean = False

    [ArduinoUseBindingCheckSetter_Parent( 'UpdateConfig' )]
    WindowMode : Boolean = False

    [ArduinoUseBindingCheckSetter_Parent( 'UpdateConfig' )]
    Latching : Boolean = False

    [ArduinoUseBindingCheckSetter_Parent( 'UpdateConfig' )]
    ActiveHigh : Boolean = False

    [PropertyFixedListDesign]
    [PropertyListDesignValue( '1' )]
    [PropertyListDesignValue( '2' )]
    [PropertyListDesignValue( '4' )]
    [ArduinoUseBindingSetter( 'SetComparatorQueueSize' )]
    QueueSize : Unsigned = 1

    [ValueRange( -1.0, 1.0 )]
    [ArduinoUseBindingCheckSetter_Parent( 'UpdateConfig' )]
    ThresholdHigh : Single = 0.9

    [ValueRange( -1.0, 1.0 )]
    [ArduinoUseBindingCheckSetter_Parent( 'UpdateConfig' )]
    ThresholdLow : Single = 0.1

    [ArduinoUseBindingCheckSetter_Parent( 'UpdateConfig' )]
    UseAsReady : Boolean = True

  ;
//---------------------------------------------------------------------------
  [ArduinoInclude( 'Mitov_TexasInstruments_ADS1X1X.h' )]
  [CreateName( 'ADC' )]
  [Category( TArduinoAnalogMeasurementToolbarCategory )]
  TArduinoTexasInstrumentsADS1X1X_BasicExtender : Extender
  
    [DesignRange( $48, $4B )]
    Address = $48

  ;
//---------------------------------------------------------------------------
  TArduinoTexasInstrumentsADS111X_SampleRateExtender : Extender

    [PropertyFixedListDesign]
    [PropertyListDesignValue( '8' )]
    [PropertyListDesignValue( '16' )]
    [PropertyListDesignValue( '32' )]
    [PropertyListDesignValue( '64' )]
    [PropertyListDesignValue( '128' )]
    [PropertyListDesignValue( '250' )]
    [PropertyListDesignValue( '475' )]
    [PropertyListDesignValue( '860' )]
    [ArduinoUseBindingCheckSetter( 'UpdateConfig' )]
    SampleRate : Unsigned = 128

  ;
//---------------------------------------------------------------------------
  TArduinoTexasInstrumentsADS101X_SampleRateExtender : Extender

    [PropertyFixedListDesign]
    [PropertyListDesignValue( '128' )]
    [PropertyListDesignValue( '250' )]
    [PropertyListDesignValue( '490' )]
    [PropertyListDesignValue( '920' )]
    [PropertyListDesignValue( '1600' )]
    [PropertyListDesignValue( '2400' )]
    [PropertyListDesignValue( '3300' )]
    [ArduinoUseBindingCheckSetter( 'UpdateConfig' )]
    SampleRate : Unsigned = 1600

  ;
//---------------------------------------------------------------------------
  TArduinoTexasInstrumentsADS1X1X_Comparator_Extender : TArduinoTexasInstrumentsADS1X1X_BasicExtender

    Comparator : TArduinoTexasInstrumentsADS1X1XComparator

  ;
//---------------------------------------------------------------------------
  [ArduinoWebKeywords( 'ADS1115' )]
  TArduinoTexasInstrumentsADS1X15_Extender : TArduinoTexasInstrumentsADS1X1X_Comparator_Extender;
//---------------------------------------------------------------------------
  TArduinoTexasInstrumentsADS1X1X_RangeExtender : Extender

    [ArduinoUseBindingCheckSetter( 'UpdateConfig' )]
    [PropertyFixedListDesign]
    [PropertyListDesignValue( '0.256' )]
    [PropertyListDesignValue( '0.512' )]
    [PropertyListDesignValue( '1.024' )]
    [PropertyListDesignValue( '2.048' )]
    [PropertyListDesignValue( '4.096' )]
    [PropertyListDesignValue( '6.144' )]
    [ValueRange( 0.256, 6.144 )]
    [NamePostfix( '(V)' )]
    Range : Single = 2.048
    
  ;
//---------------------------------------------------------------------------
  [ArduinoClass( 'Mitov::TArduinoTexasInstrumentsMultiChannelADS1X15_Channel' )]
  [ArduinoDeclaredClass]
  [ArduinoExcludeNotConnectedAndDefault]
  [ArduinoOwnerTemplateTypeAndReference]
  [ArduinoTemplateParameterCollectionItemIndex]
  [TArduinoTexasInstrumentsADS1X15ChannelGetIndexCallChain]
  [TArduinoTexasInstrumentsADS1X15ChannelSetValueCallChain]
  [TArduinoTexasInstrumentsADS1X15ChannelGetCountActiveChannelsCallChain]
  [TArduinoTexasInstrumentsADS1X15ChannelGetConfigCallChain]
  [Image( TArduinoMakeJSONAnalogElement )]
  [ArduinoOptionalVariable( Boolean, 'FClocked', True )]
  *TArduinoTexasInstrumentsMultiChannelADS1X15_Channel : TArduinoCodeFixedNamePersistent, TArduinoTexasInstrumentsADS1X1X_RangeExtender

    [ArduinoVariableIfPinConnected( 'FClocked' )]
//    [ArduinoPinNotConnectedAddCodeEntry( ceLoopBegin )]
//    [ArduinoPinIsConnectedFlag]
    ClockInputPin : TOWArduinoClockSinkPin

    [OWPrimaryPin]
    OutputPin : TOWArduinoAnalogSourcePin
    
    [ArduinoUseBindingCheckSetter( 'UpdateConfig' )]
    Differential : Boolean = False
    
    Enabled : Boolean = True

  ;
//---------------------------------------------------------------------------
  [ArduinoTemplateParameter( 'Is12bit', 'false' )]
  +TArduinoTexasInstrumentsMultiChannelADS1115_Channel : TArduinoTexasInstrumentsMultiChannelADS1X15_Channel, TArduinoTexasInstrumentsADS111X_SampleRateExtender;
//---------------------------------------------------------------------------
  [ArduinoTemplateParameter( 'Is12bit', 'true' )]
  +TArduinoTexasInstrumentsMultiChannelADS1015_Channel : TArduinoTexasInstrumentsMultiChannelADS1X15_Channel, TArduinoTexasInstrumentsADS101X_SampleRateExtender;
//---------------------------------------------------------------------------
  [ArduinoClass( 'Mitov::TArduinoTexasInstrumentsMultiChannelADS1X15_Channel_0' )]
  *TArduinoTexasInstrumentsMultiChannelADS1X15_Channel_0 : TArduinoTexasInstrumentsMultiChannelADS1X15_Channel

    [ArduinoUseBindingCheckSetter( 'UpdateConfig' )]
    [Name( 'Pair With Channel[3]' )]
    PairWithChannel3 : Boolean = False

  ;
//---------------------------------------------------------------------------
  [ArduinoTemplateParameter( 'Is12bit', 'false' )]
  +TArduinoTexasInstrumentsMultiChannelADS1115_Channel_0 : TArduinoTexasInstrumentsMultiChannelADS1X15_Channel_0, TArduinoTexasInstrumentsADS111X_SampleRateExtender;
//---------------------------------------------------------------------------
  [ArduinoTemplateParameter( 'Is12bit', 'true' )]
  +TArduinoTexasInstrumentsMultiChannelADS1015_Channel_0 : TArduinoTexasInstrumentsMultiChannelADS1X15_Channel_0, TArduinoTexasInstrumentsADS101X_SampleRateExtender;
//---------------------------------------------------------------------------
  [ArduinoClass( 'Mitov::TArduinoTexasInstrumentsMultiChannelADS1X15_Channel_3' )]
  *TArduinoTexasInstrumentsMultiChannelADS1X15_Channel_3 : TArduinoTexasInstrumentsMultiChannelADS1X15_Channel

    [ArduinoUseBindingCheckSetter( 'UpdateConfig' )]
    [MaxValue( 2 )]
    PairWithChannel : Unsigned = 0

  ;
//---------------------------------------------------------------------------
  [ArduinoTemplateParameter( 'Is12bit', 'false' )]
  +TArduinoTexasInstrumentsMultiChannelADS1115_Channel_3 : TArduinoTexasInstrumentsMultiChannelADS1X15_Channel_3, TArduinoTexasInstrumentsADS111X_SampleRateExtender;
//---------------------------------------------------------------------------
  [ArduinoTemplateParameter( 'Is12bit', 'true' )]
  +TArduinoTexasInstrumentsMultiChannelADS1015_Channel_3 : TArduinoTexasInstrumentsMultiChannelADS1X15_Channel_3, TArduinoTexasInstrumentsADS101X_SampleRateExtender;
//---------------------------------------------------------------------------
  [ArduinoClass( 'Mitov::TArduinoTexasInstrumentsADS1X15ChannelIndex' )]
  TArduinoTexasInstrumentsADS1X15ChannelIndex : TArduinoCodeDisabledPersistent

    [MaxValue( 3 )]
    [ArduinoUseBindingCheckSetter( 'UpdateConfig' )]
    Index : Unsigned = 0
    
  ;
//---------------------------------------------------------------------------
  [ArduinoClass( 'Mitov::TexasInstrumentsMultiChannelADS1X15' )]
  [ArduinoVariable( Unsigned, 2, 'FIndex' )]
  [ArduinoStart]
  [ArduinoLoopBegin]
  [ArduinoVariable( Boolean, 'FClocked' )]
  TArduinoTexasInstrumentsMultiChannelADS1X15 : TArduinoI2CAddressControlPinEnabledComponent, TArduinoTexasInstrumentsADS1X1X_Comparator_Extender
  
    ChannelIndex : TArduinoTexasInstrumentsADS1X15ChannelIndex

//    [ArduinoUseBindingCheckSetter( 'UpdateConfig' )]
    SingleShot : Boolean = True
  
    [TArduinoTexasInstrumentsADS1X15ChannelGetIndexCallChain]
    [TArduinoTexasInstrumentsADS1X15ChannelSetValueCallChain]
    [TArduinoTexasInstrumentsADS1X15ChannelGetCountActiveChannelsCallChain]
    [TArduinoTexasInstrumentsADS1X15ChannelGetConfigCallChain]
//    [ArduinoTemplateParameterCollectionCountIncluded]
    Channels : TArduinoFixedPersistentCollection    
    
  ;
//---------------------------------------------------------------------------
  [Name( 'Texas Instruments ADC ADS1115 Auto Scan (I2C)' )]
  [ArduinoWebKeywords( 'ADS1115' )]
  +TArduinoTexasInstrumentsMultiChannelADS1115 : TArduinoTexasInstrumentsMultiChannelADS1X15
  
    [AddNamedItem( TArduinoTexasInstrumentsMultiChannelADS1115_Channel_0 )]
    [AddNamedItem( TArduinoTexasInstrumentsMultiChannelADS1115_Channel )]
    [AddNamedItem( TArduinoTexasInstrumentsMultiChannelADS1115_Channel )]
    [AddNamedItem( TArduinoTexasInstrumentsMultiChannelADS1115_Channel_3 )]
    Channels
    
  ;
//---------------------------------------------------------------------------
  [ArduinoStart]
  [ArduinoLoopBegin]
  [ArduinoOptionalVariable( Boolean, 'FClocked', True )]
  [ArduinoVariable( Boolean, 'FReadClocked' )]
  TArduinoTexasInstrumentsADS1X1X : TArduinoClockedEnableI2CComponent, TArduinoTexasInstrumentsADS1X1X_BasicExtender, TArduinoTexasInstrumentsADS1X1X_RangeExtender

    [ArduinoVariableIfPinConnected( 'FClocked' )]
    ClockInputPin

    [OWPrimaryPin]
    OutputPin : TOWArduinoAnalogSourcePin
    
//    [ArduinoUseBindingCheckSetter( 'UpdateConfig' )]
    SingleShot : Boolean = False

  ;
//---------------------------------------------------------------------------
  TArduinoTexasInstrumentsADS111X : TArduinoTexasInstrumentsADS1X1X, TArduinoTexasInstrumentsADS111X_SampleRateExtender;
//---------------------------------------------------------------------------
  [Name( 'Texas Instruments ADC ADS1113 (I2C)' )]
  [ArduinoWebKeywords( 'ADS1113' )]
  [ArduinoClass( 'Mitov::TexasInstrumentsADS1X13' )]
  [ArduinoTemplateParameter( 'Is12bit', 'false' )]
  +TArduinoTexasInstrumentsADS1113 : TArduinoTexasInstrumentsADS111X;
//---------------------------------------------------------------------------
  [Name( 'Texas Instruments ADC ADS1114 (I2C)' )]
  [ArduinoWebKeywords( 'ADS1114' )]
  [ArduinoClass( 'Mitov::TexasInstrumentsADS1X14' )]
  [ArduinoTemplateParameter( 'Is12bit', 'false' )]
  +TArduinoTexasInstrumentsADS1114 : TArduinoTexasInstrumentsADS111X, TArduinoTexasInstrumentsADS1X1X_Comparator_Extender;
//---------------------------------------------------------------------------
  [Name( 'Texas Instruments ADC ADS1015 Auto Scan (I2C)' )]
  [ArduinoWebKeywords( 'ADS1015' )]
  +TArduinoTexasInstrumentsMultiChannelADS1015 : TArduinoTexasInstrumentsMultiChannelADS1X15
  
    [AddNamedItem( TArduinoTexasInstrumentsMultiChannelADS1015_Channel_0 )]
    [AddNamedItem( TArduinoTexasInstrumentsMultiChannelADS1015_Channel )]
    [AddNamedItem( TArduinoTexasInstrumentsMultiChannelADS1015_Channel )]
    [AddNamedItem( TArduinoTexasInstrumentsMultiChannelADS1015_Channel_3 )]
    Channels
    
  ;
//---------------------------------------------------------------------------
  TArduinoTexasInstrumentsADS101X : TArduinoTexasInstrumentsADS1X1X,TArduinoTexasInstrumentsADS101X_SampleRateExtender;
//---------------------------------------------------------------------------
  [Name( 'Texas Instruments ADC ADS1013 (I2C)' )]
  [ArduinoWebKeywords( 'ADS1013' )]
  [ArduinoClass( 'Mitov::TexasInstrumentsADS1X13' )]
  [ArduinoTemplateParameter( 'Is12bit', 'true' )]
  +TArduinoTexasInstrumentsADS1013 : TArduinoTexasInstrumentsADS101X;
//---------------------------------------------------------------------------
  [Name( 'Texas Instruments ADC ADS1014 (I2C)' )]
  [ArduinoWebKeywords( 'ADS1014' )]
  [ArduinoClass( 'Mitov::TexasInstrumentsADS1X14' )]
  [ArduinoTemplateParameter( 'Is12bit', 'true' )]
  +TArduinoTexasInstrumentsADS1014 : TArduinoTexasInstrumentsADS101X, TArduinoTexasInstrumentsADS1X1X_Comparator_Extender;
//---------------------------------------------------------------------------
; // Mitov