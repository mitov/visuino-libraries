Mitov : Namespace
//---------------------------------------------------------------------------
  [Name( 'Texas Instruments' )]
  TTexasInstrumentsCategory : TClassCategory;
//---------------------------------------------------------------------------
  [ArduinoBoardIndex( 'https://raw.githubusercontent.com/Andy4495/TI_Platform_Cores_For_Arduino/main/json/package_energia_optimized_index.json' )]
  ArduinoTexasInstrumentsBoardIndexAttribute : TAttributeCollectionAttribute;
//---------------------------------------------------------------------------
  ['{2C659DD4-508B-4156-B050-61EB348131C7}']
  IAdafruitLaunchPadShield : Interface;
//---------------------------------------------------------------------------
  [ArduinoShieldsType( IAdafruitLaunchPadShield )]
  TAdafruitLaunchPadShields : TArduinoShields;
//---------------------------------------------------------------------------
  TAdafruitLaunchPadShieldsExtender : Extender

    [ComponentEditorEntryDesign( 'Add S&hields ...' )]
    Shields : TAdafruitLaunchPadShields

  ;
//---------------------------------------------------------------------------
  [Category( TTexasInstrumentsCategory )]
  [ArduinoTexasInstrumentsBoardIndex]
  [ArduinoBoardArchitecture( 'msp432r' )]
  [ArduinoVoltageReference( 5.0 )]
  [ArduinoDefine( 'VISUINO_TEXAS_INSTRUMENTS' )]
  TArduinoBasicTexasInstruments_Board : TArduinoBoardExtendableImplementation;
//---------------------------------------------------------------------------
  [ArduinoTemplateParameter( 'TXPin', 'Mitov::ConstantProperty<87, uint32_t, 4 >' )]
  +TexasInstruments_MSP432_P401R_Serial0 : TArduinoBasicBreakHardwareSerial;
//---------------------------------------------------------------------------
  [ArduinoI2C( 1 )]
  TArduinoAdditionalDigitalPullUpDownInterruptI2C1Channel : TArduinoAdditionalDigitalPullUpDownInterruptChannel;
//---------------------------------------------------------------------------
  [ArduinoSPI( 0 )]
  +TArduinoAdditionalDigitalPullUpDownInterruptSPI0Channel : TArduinoAdditionalDigitalPullUpDownInterruptChannel;
//---------------------------------------------------------------------------
  [ArduinoI2C( 0 )]
  +TArduinoAdditionalDigitalPullUpDownInterruptI2C0SPI0Channel : TArduinoAdditionalDigitalPullUpDownInterruptSPI0Channel;
//---------------------------------------------------------------------------
  [ArduinoClass( 'Mitov::InvertedDigitalPullUpInput' )]
  [ArduinoTemplateParameter( 'C_PIN', '73' )]
  [CreateName( 'Button 1' )]
  +TArduinoTexasInstruments_MSP432_P401R_Button1Module : TArduinoBasicInterruptButtonModule;
//---------------------------------------------------------------------------
  [ArduinoClass( 'Mitov::InvertedDigitalPullUpInput' )]
  [ArduinoTemplateParameter( 'C_PIN', '74' )]
  [CreateName( 'Button 2' )]
  +TArduinoTexasInstruments_MSP432_P401R_Button2Module : TArduinoBasicInterruptButtonModule;
//---------------------------------------------------------------------------
  [ArduinoTemplateParameter( 'C_Red', '75' )] // Red
  [ArduinoTemplateParameter( 'C_Green', '76' )] // Green
  [ArduinoTemplateParameter( 'C_Blue', '77' )] // Blue
  [ArduinoShieldPin( '75' )]
  [ArduinoShieldPin( '76' )]
  [ArduinoShieldPin( '77' )]
  +TArduinoTexasInstruments_MSP432_P401R_LEDModule : TArduinoBasicRGBLEDModule;
//---------------------------------------------------------------------------
  [Name( 'Texas Instruments MSP432 P401R' )]
  [Image( TTexasInstrumentsCategory )]
  [ArduinoBoardCompileParams( 'energia:msp432r:MSP-EXP432P401R')]
  +TArduinoTexasInstruments_MSP432_P401R_Board : TArduinoBasicTexasInstruments_Board, TAdafruitLaunchPadShieldsExtender
  
//    [AddItem( TArduinoEEPROMModule )]
    [AddItem( TArduinoTexasInstruments_MSP432_P401R_Button1Module )]
    [AddItem( TArduinoTexasInstruments_MSP432_P401R_Button2Module )]
    [AddItem( TArduinoTexasInstruments_MSP432_P401R_LEDModule )]
    Modules : TArduinoModules
  
    [AddItem( TexasInstruments_MSP432_P401R_Serial0 )]
    Serial : TArduinoSerials
    
    [ArduinoBoard_Add_DigitalNamedAnalogChannels(  2, 15, 1, TArduinoCombinedAnalogDigitalPullUpDownInterruptChannel, 'P6.0 Digital', 'AnalogIn', '', 'A' )]
    [ArduinoBoard_Add_DigitalAnalogChannels(  3, 1, TArduinoAdditionalDigitalPullUpDownPWMSerial0InterruptChannel, 'P3.2 Digital(RX)' )]
    [ArduinoBoard_Add_DigitalAnalogChannels(  4, 1, TArduinoAdditionalDigitalPullUpDownPWMSerial0InterruptChannel, 'P3.3 Digital(TX)' )]
    [ArduinoBoard_Add_DigitalNamedAnalogChannels(  5, 12, 1, TArduinoCombinedAnalogDigitalPullUpDownInterruptChannel, 'P4.1 Digital', 'AnalogIn', '', 'A' )]
    [ArduinoBoard_Add_DigitalNamedAnalogChannels(  6, 10, 1, TArduinoCombinedAnalogDigitalPullUpDownInterruptChannel, 'P4.3 Digital', 'AnalogIn', '', 'A' )]
    [ArduinoBoard_Add_DigitalAnalogChannels(  7, 1, TArduinoAdditionalDigitalPullUpDownInterruptSPI0Channel, 'P1.5 Digital(SPI-SCK)' )]
    [ArduinoBoard_Add_DigitalNamedAnalogChannels(  8,  7, 1, TArduinoCombinedAnalogDigitalPullUpDownInterruptChannel, 'P4.6 Digital', 'AnalogIn', '', 'A' )]
    [ArduinoBoard_Add_DigitalAnalogChannels(  9, 1, TArduinoAdditionalDigitalPullUpDownInterruptI2C1Channel, 'P6.5 Digital(I2C[1]-SCL)' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 10, 1, TArduinoAdditionalDigitalPullUpDownInterruptI2C1Channel, 'P6.4 Digital(I2C[1]-SDA)' )]

    [ArduinoBoard_Add_DigitalAnalogChannels( 11, 1, TArduinoAdditionalDigitalPullUpDownPWMInterruptChannel, 'P6.4 Digital' )]
    [ArduinoBoard_Add_DigitalNamedAnalogChannels( 12,  3, 1, TArduinoCombinedAnalogDigitalPullUpDownInterruptChannel, 'P5.2 Digital', 'AnalogIn', '', 'A' )]
    [ArduinoBoard_Add_DigitalNamedAnalogChannels( 13,  5, 1, TArduinoCombinedAnalogDigitalPullUpDownInterruptChannel, 'P5.0 Digital', 'AnalogIn', '', 'A' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 14, 1, TArduinoAdditionalDigitalPullUpDownInterruptI2C0SPI0Channel, 'P1.7 Digital(I2C[0]-SCL)(SPI-MISO)' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 15, 1, TArduinoAdditionalDigitalPullUpDownInterruptI2C0SPI0Channel, 'P1.6 Digital(I2C[0]-SDA)(SPI-MOSI)' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 17, 1, TArduinoAdditionalDigitalPullUpDownPWMInterruptChannel, 'P5.7 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 18, 1, TArduinoAdditionalDigitalPullUpDownPWMInterruptChannel, 'P3.0 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 19, 1, TArduinoAdditionalDigitalPullUpDownPWMInterruptChannel, 'P2.5 Digital' )]

    [ArduinoBoard_Add_DigitalNamedAnalogChannels( 23, 14, 1, TArduinoCombinedAnalogDigitalPullUpDownInterruptChannel, 'P6.1 Digital', 'AnalogIn', '', 'A' )]
    [ArduinoBoard_Add_DigitalNamedAnalogChannels( 24, 13, 1, TArduinoCombinedAnalogDigitalPullUpDownInterruptChannel, 'P4.0 Digital', 'AnalogIn', '', 'A' )]
    [ArduinoBoard_Add_DigitalNamedAnalogChannels( 25, 11, 1, TArduinoCombinedAnalogDigitalPullUpDownInterruptChannel, 'P4.2 Digital', 'AnalogIn', '', 'A' )]
    [ArduinoBoard_Add_DigitalNamedAnalogChannels( 26,  9, 1, TArduinoCombinedAnalogDigitalPullUpDownInterruptChannel, 'P4.4 Digital', 'AnalogIn', '', 'A' )]
    [ArduinoBoard_Add_DigitalNamedAnalogChannels( 27,  8, 1, TArduinoCombinedAnalogDigitalPullUpDownInterruptChannel, 'P4.5 Digital', 'AnalogIn', '', 'A' )]
    [ArduinoBoard_Add_DigitalNamedAnalogChannels( 28,  6, 1, TArduinoCombinedAnalogDigitalPullUpDownInterruptChannel, 'P4.7 Digital', 'AnalogIn', '', 'A' )]
    [ArduinoBoard_Add_DigitalNamedAnalogChannels( 29,  1, 1, TArduinoCombinedAnalogDigitalPullUpDownInterruptChannel, 'P5.4 Digital', 'AnalogIn', '', 'A' )]
    [ArduinoBoard_Add_DigitalNamedAnalogChannels( 30,  0, 1, TArduinoCombinedAnalogDigitalPullUpDownInterruptChannel, 'P5.5 Digital', 'AnalogIn', '', 'A' )]

    [ArduinoBoard_Add_DigitalAnalogChannels( 31, 1, TArduinoAdditionalDigitalPullUpDownPWMInterruptChannel, 'P3.7 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 32, 1, TArduinoAdditionalDigitalPullUpDownPWMInterruptChannel, 'P3.5 Digital' )]
    [ArduinoBoard_Add_DigitalNamedAnalogChannels( 33,  4, 1, TArduinoCombinedAnalogDigitalPullUpDownInterruptChannel, 'P5.1 Digital', 'AnalogIn', '', 'A' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 34, 1, TArduinoAdditionalDigitalPullUpDownPWMInterruptChannel, 'P2.3 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 35, 1, TArduinoAdditionalDigitalPullUpDownPWMInterruptChannel, 'P6.7 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 36, 1, TArduinoAdditionalDigitalPullUpDownPWMInterruptChannel, 'P6.6 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 37, 1, TArduinoAdditionalDigitalPullUpDownPWMInterruptChannel, 'P5.6 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 38, 1, TArduinoAdditionalDigitalPullUpDownPWMInterruptChannel, 'P2.4 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 39, 1, TArduinoAdditionalDigitalPullUpDownPWMInterruptChannel, 'P2.6 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 40, 1, TArduinoAdditionalDigitalPullUpDownPWMInterruptChannel, 'P2.7 Digital' )]

    [ArduinoBoard_Add_DigitalNamedAnalogChannels( 41, 20, 1, TArduinoCombinedAnalogDigitalPullUpDownInterruptChannel, 'P8.5 Digital', 'AnalogIn', '', 'A' )]
    [ArduinoBoard_Add_DigitalNamedAnalogChannels( 42, 17, 1, TArduinoCombinedAnalogDigitalPullUpDownInterruptChannel, 'P9.0 Digital', 'AnalogIn', '', 'A' )]
    [ArduinoBoard_Add_DigitalNamedAnalogChannels( 43, 21, 1, TArduinoCombinedAnalogDigitalPullUpDownInterruptChannel, 'P8.4 Digital', 'AnalogIn', '', 'A' )]
    [ArduinoBoard_Add_DigitalNamedAnalogChannels( 44, 23, 1, TArduinoCombinedAnalogDigitalPullUpDownInterruptChannel, 'P8.2 Digital', 'AnalogIn', '', 'A' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 45, 1, TArduinoAdditionalDigitalPullUpDownInterruptChannel, 'P9.2 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 46, 1, TArduinoAdditionalDigitalPullUpDownInterruptChannel, 'P6.2 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 47, 1, TArduinoAdditionalDigitalPullUpDownPWMInterruptChannel, 'P7.3 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 48, 1, TArduinoAdditionalDigitalPullUpDownPWMInterruptChannel, 'P7.1 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 49, 1, TArduinoAdditionalDigitalPullUpDownInterruptChannel, 'P9.4 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 50, 1, TArduinoAdditionalDigitalPullUpDownInterruptChannel, 'P9.6 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 51, 1, TArduinoAdditionalDigitalPullUpDownInterruptChannel, 'P8.0 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 52, 1, TArduinoAdditionalDigitalPullUpDownPWMInterruptChannel, 'P7.4 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 53, 1, TArduinoAdditionalDigitalPullUpDownPWMInterruptChannel, 'P7.6 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 54, 1, TArduinoAdditionalDigitalPullUpDownInterruptChannel, 'P10.0 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 55, 1, TArduinoAdditionalDigitalPullUpDownInterruptChannel, 'P10.2 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 56, 1, TArduinoAdditionalDigitalPullUpDownInterruptChannel, 'P10.4 Digital' )]
    
    [ArduinoBoard_Add_DigitalNamedAnalogChannels( 57, 19, 1, TArduinoCombinedAnalogDigitalPullUpDownInterruptChannel, 'P8.6 Digital', 'AnalogIn', '', 'A' )]
    [ArduinoBoard_Add_DigitalNamedAnalogChannels( 58, 18, 1, TArduinoCombinedAnalogDigitalPullUpDownInterruptChannel, 'P8.7 Digital', 'AnalogIn', '', 'A' )]
    [ArduinoBoard_Add_DigitalNamedAnalogChannels( 59, 16, 1, TArduinoCombinedAnalogDigitalPullUpDownInterruptChannel, 'P9.1 Digital', 'AnalogIn', '', 'A' )]
    [ArduinoBoard_Add_DigitalNamedAnalogChannels( 60, 22, 1, TArduinoCombinedAnalogDigitalPullUpDownInterruptChannel, 'P8.3 Digital', 'AnalogIn', '', 'A' )]
    [ArduinoBoard_Add_DigitalNamedAnalogChannels( 61,  2, 1, TArduinoCombinedAnalogDigitalPullUpDownInterruptChannel, 'P5.3 Digital', 'AnalogIn', '', 'A' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 62, 1, TArduinoAdditionalDigitalPullUpDownInterruptChannel, 'P9.3 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 63, 1, TArduinoAdditionalDigitalPullUpDownInterruptChannel, 'P6.3 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 64, 1, TArduinoAdditionalDigitalPullUpDownPWMInterruptChannel, 'P7.2 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 65, 1, TArduinoAdditionalDigitalPullUpDownPWMInterruptChannel, 'P7.0 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 66, 1, TArduinoAdditionalDigitalPullUpDownInterruptChannel, 'P9.5 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 67, 1, TArduinoAdditionalDigitalPullUpDownInterruptChannel, 'P9.7 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 68, 1, TArduinoAdditionalDigitalPullUpDownPWMInterruptChannel, 'P7.5 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 69, 1, TArduinoAdditionalDigitalPullUpDownPWMInterruptChannel, 'P7.5 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 70, 1, TArduinoAdditionalDigitalPullUpDownInterruptChannel, 'P10.1 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 71, 1, TArduinoAdditionalDigitalPullUpDownInterruptChannel, 'P10.3 Digital' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 72, 1, TArduinoAdditionalDigitalPullUpDownInterruptChannel, 'P10.5 Digital' )]
    
    [ArduinoBoard_Add_DigitalAnalogChannels( 75, 1, TArduinoAdditionalDigitalPullUpDownPWMInterruptChannel, 'P2.0 Digital(LED2 Red)' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 76, 1, TArduinoAdditionalDigitalPullUpDownPWMInterruptChannel, 'P2.1 Digital(LED2 Green)' )]
    [ArduinoBoard_Add_DigitalAnalogChannels( 77, 1, TArduinoAdditionalDigitalPullUpDownPWMInterruptChannel, 'P2.2 Digital(LED2 Blue)' )]

    [ArduinoBoard_Add_DigitalAnalogChannels( 78, 1, TArduinoAdditionalDigitalPullUpDownInterruptChannel, 'P1.0 Digital(LED1)' )]
    [Name( 'Channels' )]
    Digital : TArduinoDigitalChannels
    
  ;
//---------------------------------------------------------------------------
; // Mitov