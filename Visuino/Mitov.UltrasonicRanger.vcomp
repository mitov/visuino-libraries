Mitov : Namespace
//---------------------------------------------------------------------------
  TArduinoRangerBasicUnits : Enum

    rdCm
    rdInch

  ;
//---------------------------------------------------------------------------
  TArduinoRangerUnits : TArduinoRangerBasicUnits

    rdTime

  ;
//---------------------------------------------------------------------------
  TArduinoBasicUltrasonicRanger_Basic_Extender : Extender

    TimeoutOutputPin : TOWArduinoDigitalSourcePin

    Enabled : Boolean = True
    Units : TArduinoRangerUnits = rdCm

    [DesignRange( 0, 10000 )]
    [MaxValue( $FFFF )]
    [MinValue( 0 )]
    [ NamePostfix( '(mS)' ) ]
    Timeout : Unsigned = 1000

    [DesignRange( -10000.0, 10000.0 )]
    TimeoutValue : Single = 10000
    
  ;
//---------------------------------------------------------------------------
  [ArduinoInclude( 'Mitov_UltrasonicRanger.h' )]
  [ArduinoLoopBegin]
  [ArduinoStart]
  [ArduinoVariable( Boolean, 'FClocked' )]
  [ArduinoVariable( Unsigned, 3, 'FState' )]
  TArduinoBasicUltrasonicRanger_Extender : TArduinoBasicUltrasonicRanger_Basic_Extender

    [DesignRange( 1, 100 )]
    [MaxValue( $FFFF )]
    [MinValue( 1 )]
    [ NamePostfix( '(uS)' ) ]
    PingTime : Unsigned = 2

    [DesignRange( 0, 10000 )]
    [MaxValue( $FFFF )]
    [MinValue( 0 )]
    [ NamePostfix( '(mS)' ) ]
    PauseTime : Unsigned = 100

  ;
//---------------------------------------------------------------------------
  TArduinoBasicUltrasonicRanger_Outputs_Extender : Extender

    [Name( 'Ping(Trigger)' )]
    PingOutputPin : TOWArduinoDigitalSourcePin

    [ArduinoInterruptMode( '_Interrupt', True, 'InterruptHandler', Change )]
    EchoInputPin : TOWArduinoDigitalOptionalInterruptSinkPin

  ;
//---------------------------------------------------------------------------
  [ArduinoClass( 'Mitov::UltrasonicRanger' )]
  [Category( TArduinoAnalogMeasurementToolbarCategory )]
  [CreateName( 'UltrasonicRanger' )]
  [ArduinoWebKeywords( 'ultrasonic module' )]
  TArduinoBasicUltrasonicRanger : TArduinoCommonClockedAnalogSource, TArduinoBasicUltrasonicRanger_Extender

    [ArduinoPinIsConnectedFlag]
    ClockInputPin

  ;
//---------------------------------------------------------------------------
  TArduinoUltrasonicRanger_Basic : TArduinoBasicUltrasonicRanger, TArduinoBasicUltrasonicRanger_Outputs_Extender;
//---------------------------------------------------------------------------
  [Name( 'Ultrasonic Ranger(Ping)' )]
  +TArduinoUltrasonicRanger : TArduinoUltrasonicRanger_Basic;
//---------------------------------------------------------------------------
  [Image( TArduinoUltrasonicRanger )]
  [ArduinoInclude( 'Mitov_UltrasonicRanger.h' )]
  [ArduinoClass( 'Mitov::UltrasonicRangerElement' )]
  [Name( 'Ultrasonic Ranger(Ping)' )]
  [ArduinoWebKeywords( 'ultrasonic module' )]
  [ArduinoLoopBegin]
  [ArduinoStart]
//  [ArduinoVariable( Boolean, 'FClocked' )]
  [ArduinoVariable( Unsigned, 3, 'FState' )]
  +TArduinoMultiSensorStartUltrasonicRangerElement : TArduinoMultiSensorStartBasicElement, TArduinoBasicUltrasonicRanger_Basic_Extender

    [OWPrimaryPin]
    OutputPin : TOWArduinoAnalogSourcePin

    [ArduinoInterruptMode( '_Interrupt', True, 'InterruptHandler', Change )]
    EchoInputPin : TOWArduinoDigitalOptionalInterruptSinkPin

  ;
//---------------------------------------------------------------------------
  [ArduinoClass( 'Mitov::UltrasonicRangerShieldElement' )]
  [ArduinoDeclaredClass]
  [ArduinoExcludeNotConnected]
  [Image( TArduinoUltrasonicRanger )]
  TArduinoUltrasonicRangerShieldElement : TArduinoCodePersistent, TArduinoBasicUltrasonicRanger_Extender

    [OWPrimaryPin]
    OutputPin : TOWArduinoAnalogSourcePin

    [ArduinoPinIsConnectedFlag]
    ClockInputPin : TOWArduinoClockSinkPin

  ;
//---------------------------------------------------------------------------
  [ArduinoClass( 'Mitov::UltrasonicRangerShieldElement' )]
  [ArduinoDeclaredClass]
  [ArduinoExcludeNotConnected]
  [Image( TArduinoUltrasonicRanger )]
  [CreateName( 'Ultrasonic Ranger' )]
  TArduinoUltrasonicRangerModule : TArduinoShield,  TArduinoBasicUltrasonicRanger_Extender

    [OWPrimaryPin]
    OutputPin : TOWArduinoAnalogSourcePin

    [ArduinoPinIsConnectedFlag]
    ClockInputPin : TOWArduinoClockSinkPin

  ;
//---------------------------------------------------------------------------
{
  [Name( 'Left RGB LED' )]
  [ArduinoTemplateParameter( 'C_PIN', '0' )]
  TArduinoUltrasonicRanger_RGB_LeftLed : TArduinoNeoPixelRGBLedBasicRGBLed;
//---------------------------------------------------------------------------
  [Name( 'Right RGB LED' )]
  [ArduinoTemplateParameter( 'C_PIN', '1' )]
  TArduinoUltrasonicRanger_RGB_RightLed : TArduinoNeoPixelRGBLedBasicRGBLed;
}
//---------------------------------------------------------------------------
//  [ArduinoDeclaredClass]
//  [ArduinoExcludeNotConnectedAndDefault]
//  [ArduinoStart]
  [ArduinoInclude( 'Mitov_UltrasonicRanger_RGB.h' )]
  [ArduinoClass( 'Mitov::UltrasonicRanger_RGB_Led' )]
  [ArduinoOwnerTemplateTypeAndReference]
  [Image( TArduinoBasicRGBLEDModule )]
  *TArduinoUltrasonicRanger_RGB_Led : TArduinoBasicColorInitialValueInputPinChannel
  
    [ArduinoUseBindingCheckSetter_Parent( 'UpdateOutputs' )]
    Enabled
    
    [OWExcludeBindings]
    [ArduinoFlexibleVariableIfPinConnected( InputPin )]
    [ExcludeSystemColors]
    [ArduinoFullColor]
    InitialValue
    
  ;
//---------------------------------------------------------------------------
  *TArduinoUltrasonicRanger_RGB_Led_Left : TArduinoUltrasonicRanger_RGB_Led
  
    [ArduinoPinCall( 'Left_InputPin' )]
    InputPin
    
  ;
//---------------------------------------------------------------------------
  *TArduinoUltrasonicRanger_RGB_Led_Right : TArduinoUltrasonicRanger_RGB_Led
  
    [ArduinoPinCall( 'Right_InputPin' )]
    InputPin
    
  ;
//---------------------------------------------------------------------------
  [ArduinoDeclaredClass]
  [ArduinoClass( 'Mitov::UltrasonicRanger_RGB_Leds' )]
  [ArduinoExcludeNotConnectedAndDefault]
  [Image( TArduinoBasicRGBLEDModule )]
  [ArduinoLoopEnd]
  [ArduinoInit]
  [ArduinoVariable( Boolean, 'FModified' )]
  *TArduinoUltrasonicRanger_RGB_Leds : TArduinoCodePersistent
  
    [OWRequiredPin]
    [ArduinoExclude]
    [ArduinoDirectPinTemplate]
    OutputPin : TOWArduinoDigitalOutputSourcePin
    
    Left : TArduinoUltrasonicRanger_RGB_Led_Left
    Right : TArduinoUltrasonicRanger_RGB_Led_Right
    
    [ArduinoUseBindingCheckSetter( 'UpdateBrightness' )]
    [ValueRange( 0.0, 1.0 )]
    Brightness : Single = 0.1
    
  ;
//---------------------------------------------------------------------------
  [Name( 'RGB Ultrasonic Ranger(Ping)' )]
  +TArduinoUltrasonicRanger_RGB : TArduinoUltrasonicRanger_Basic

{
    [Name( 'LEDs' )]
    [AddItem( TArduinoUltrasonicRanger_RGB_LeftLed )]
    [AddItem( TArduinoUltrasonicRanger_RGB_RightLed )]
    Leds : TArduinoNeoPixelRGBLeds
}    
    Leds : TArduinoUltrasonicRanger_RGB_Leds
    
  ;
//---------------------------------------------------------------------------
; // Mitov