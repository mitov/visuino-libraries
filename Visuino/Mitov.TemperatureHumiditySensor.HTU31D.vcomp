// Developed by Swellington Santos
Mitov : Namespace
//---------------------------------------------------------------------------
  TArduinoTemperatureHumidityHTU31DBasic : TArduinoCodeEnabledPersistent

    [OWPrimaryPin]
    OutputPin : TOWArduinoAnalogSourcePin

    [PropertyFixedListDesign]
    [PropertyListDesignValue( '1' )]
    [PropertyListDesignValue( '2' )]
    [PropertyListDesignValue( '4' )]
    [PropertyListDesignValue( '8' )]
    Averaging : Unsigned = 4

  ;
//---------------------------------------------------------------------------
  [Image( TArduinoMPU9250Thermometer )]
  [ArduinoClass('Mitov::TArduinoTemperatureHumidityHTU31DTemperature')]
  *TArduinoTemperatureHumidityHTU31DTemperature : TArduinoTemperatureHumidityHTU31DBasic

    InFahrenheit : Boolean = False

  ;
//---------------------------------------------------------------------------
  [Image( TArduinoMeasurementTemperatureHumidityToolbarCategory )]
  [ArduinoClass('Mitov::TArduinoTemperatureHumidityHTU31DHumidity')]
  *TArduinoTemperatureHumidityHTU31DHumidity : TArduinoTemperatureHumidityHTU31DBasic

    [NamePostfix( ' (%)' )]
    OutputPin

  ;
//---------------------------------------------------------------------------
  [ArduinoInclude('Mitov_TemperatureHumidityHTU31D.h')]
  [ArduinoClass('Mitov::TArduinoTemperatureHumidityHTU31D')]
  [Name('Temperature and Humidity Sensor HTU31D (I2C)')]
  [Category( TArduinoAnalogMeasurementToolbarCategory )]
  [CreateName( 'TemperatureHumiditySensor' )]
  [ArduinoVariable( Boolean, 'FInitialized' )]
  [ArduinoVariable( Boolean, 'FRequesting' )]
  [ArduinoOptionalVariable( Boolean, 'FClocked', True )]
  [ArduinoStart]
  [ArduinoLoopBegin]
  +TArduinoTemperatureHumidityHTU31D : TArduinoClockedEnableI2CComponent

    [ArduinoVariableIfPinConnected( 'FClocked' )]
    [ArduinoPinIsConnectedFlag]
    ClockInputPin

    ResetInputPin : TOWArduinoClockSinkPin

    Temperature : TArduinoTemperatureHumidityHTU31DTemperature
    Humidity : TArduinoTemperatureHumidityHTU31DHumidity

    [DesignRange($40, $41)]
    Address = $40

    [ArduinoUseBindingCheckSetter( 'UpdateHeating' )]
    Heating : Boolean = False

  ;
//---------------------------------------------------------------------------
; // Mitov